
AutoTurnInCharacterDB = {
	["IGNORED_NPC"] = {
		["87391"] = "fate-twister-seress",
		["142063"] = "Tezran",
		["141584"] = "Zurvan",
		["127037"] = "Nabiru",
		["111243"] = "Archmage Lan'dalock",
		["119388"] = "Chieftain Hatuun",
		["88570"] = "Fate-Twister Tiklal",
		["15077"] = "Riggle Bassbait",
	},
	["togglekey"] = 4,
	["weapon"] = {
	},
	["questlevel"] = true,
	["trivial"] = false,
	["showrewardtext"] = true,
	["stat"] = {
	},
	["watchlevel"] = true,
	["completeonly"] = false,
	["enabled"] = true,
	["todarkmoon"] = true,
	["autoequip"] = false,
	["relictoggle"] = true,
	["tournament"] = 2,
	["darkmoonautostart"] = true,
	["darkmoonteleport"] = true,
	["debug"] = false,
	["reviveBattlePet"] = false,
	["armor"] = {
	},
	["version"] = "7.5.1",
	["artifactpowertoggle"] = true,
	["lootreward"] = 1,
	["all"] = 1,
	["questshare"] = false,
	["secondary"] = {
	},
}
