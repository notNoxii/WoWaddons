VER 3 00000000000000D2 ".andy" "INV_MISC_QUESTIONMARK"
/target Monstrous Soul
END
VER 3 00000000000000D3 ".lords" "INV_MISC_QUESTIONMARK"
/script WeakAuras.ScanEvents("PIECES_WA_MACRO", true)
END
VER 3 00000000000000C2 "1" "INV_MISC_QUESTIONMARK"
#showtooltip
/run UIErrorsFrame:Hide()
/use [talent:2/1] Cosmic Aspirant's Badge of Ferocity
/cast Ashen Hallow
/cast Vanquisher's Hammer
/cast Divine Toll
END
VER 3 00000000000000A3 "1" "INV_MISC_QUESTIONMARK"
#showtooltip
/use Potion of Spectral Intellect
/use 14
END
VER 3 00000000000000CF "2" "INV_MISC_QUESTIONMARK"
#showtooltip
/use Potion of Spectral Strength
/use 14
END
VER 3 00000000000000A1 "a" "INV_MISC_QUESTIONMARK"
#showtooltip
/use Cosmic Healing Potion
/use 14
END
VER 3 00000000000000B1 "AAP_MACRO" "INV_MISC_QUESTIONMARK"
#showtooltip
/click ExtraActionButton1
END
VER 3 000000000000009F "b" "INV_MISC_QUESTIONMARK"
#showtooltip
/cast Door of Shadows
/cast Fleshcraft
/use Phial of Serenity
END
VER 3 00000000000000A2 "bear" "INV_MISC_QUESTIONMARK"
#showtooltip Bear Form
/cast [noform:1] Bear Form
END
VER 3 00000000000000AC "bite" "INV_MISC_QUESTIONMARK"
#showtooltip 
/cast [talent:3/2] Ferocious Bite
/cast [talent:3/1] Sunfire
/castsequence [talent:3/3] reset=20 Bear Form, Frenzied Regeneration, Frenzied Regeneration, Frenzied Regeneration
END
VER 3 00000000000000AA "boom" "INV_MISC_QUESTIONMARK"
#showtooltip
/use 13
/cast [@player] Final Reckoning
END
VER 3 00000000000000AF "cen" "INV_MISC_QUESTIONMARK"
#showtooltip 
/cast [@mouseover,help,nodead][help,nodead][@player] Guardian of Ancient Kings
END
VER 3 00000000000000C9 "cleanse" "INV_MISC_QUESTIONMARK"
#showtooltip
/cast [@mouseover,exists][] Cleanse Toxins
END
VER 3 00000000000000BA "convoke" "INV_MISC_QUESTIONMARK"
#showtooltip 
/stopmacro [channeling] 
/castsequence Shadowed Orb of Torment, Ashen Hallow
/cqs
END
VER 3 00000000000000C0 "cs" "INV_MISC_QUESTIONMARK"
#showtooltip
/targetnearestenemy [harm,nodead]
/startattack
/cast Crusader Strike
/targetlasttarget
END
VER 3 00000000000000A4 "cyclone" "136022"
#showtooltip
/cast [@focus,nodead] Cyclone; Cyclone
END
VER 3 00000000000000B3 "dis" "INV_MISC_QUESTIONMARK"
/cast disenchant
END
VER 3 00000000000000C5 "f" "INV_MISC_QUESTIONMARK"
/target [@focustarget]
END
VER 3 00000000000000A5 "f7" "INV_MISC_QUESTIONMARK"
/tar [target=focustarget]
END
VER 3 00000000000000D1 "inv" "132092"
/invite Mildasaurus-Area52
/invite Mildfu-Area52
/invite Toiletstain-Hyjal
END
VER 3 00000000000000D4 "judge prism" "INV_MISC_QUESTIONMARK"
#showtooltip 
/cast [talent:2/3] Holy Prism
/cast [talent:2/2] Judgment
/cast [talent:2/1] Judgment
END
VER 3 00000000000000BF "opener" "INV_MISC_QUESTIONMARK"
#showtooltip 
/cast [@mouseover,help,nodead][help,nodead][@player] Beacon of Faith
END
VER 3 00000000000000B9 "pvptree" "INV_MISC_QUESTIONMARK"
#showtooltip
/cast [@focus,exists][] Turn Evil
END
VER 3 00000000000000A7 "rak" "INV_MISC_QUESTIONMARK"
#showtooltip
/cast [@target,harm] Judgment;
/stopmacro [harm]
/targetenemy
/cast Judgment
/targetlasttarget
END
VER 3 00000000000000A8 "ret sera" "INV_MISC_QUESTIONMARK"
#showtooltip 
/cast Seraphim
END
VER 3 00000000000000AB "rip" "INV_MISC_QUESTIONMARK"
#showtooltip 
/cast [talent:3/2] Rip
/cast [talent:3/1] Moonfire
/cast [talent:3/3] Incapacitating Roar
END
VER 3 00000000000000A6 "roots" "INV_MISC_QUESTIONMARK"
#showtooltip
/cast [@focus,exists,nodead] Mass Entanglement; Mass Entanglement
END
VER 3 00000000000000A0 "shift" "INV_MISC_QUESTIONMARK"
#showtooltip Cat Form
/use [nostance][stance:1][stance:3] [stance:4] !Cat Form(Shapeshift)
END
VER 3 00000000000000CA "spellwa" "INV_MISC_QUESTIONMARK"
#showtooltip 
/use 13
END
VER 3 00000000000000BD "starfire" "INV_MISC_QUESTIONMARK"
#showtooltip Starfire
/cast [noform:4] Moonkin Form
/cast Starfire
END
VER 3 00000000000000BB "starsurge" "INV_MISC_QUESTIONMARK"
#showtooltip
/cast [nochanneling: Convoke the Spirits] Starsurge
END
VER 3 00000000000000B7 "tranq" "INV_MISC_QUESTIONMARK"
#showtooltip
/cast Tranquility
END
VER 3 00000000000000A9 "tree" "INV_MISC_QUESTIONMARK"
#showtooltip
/use [talent:5/3][nostance][stance:2][stance:3] !Incarnation: Tree of Life
/cast Berserking
/use [talent:6/3][@mouseover,exists] [] Overgrowth
END
VER 3 00000000000000C3 "trees" "INV_MISC_QUESTIONMARK"
#showtooltip 
/cast [talent:6/1] Force of Nature
/cast [talent:6/3] Stellar Flare
END
VER 3 00000000000000B6 "trinket" "INV_MISC_QUESTIONMARK"
#showtooltip
/focus [@mouseover]
END
VER 3 00000000000000CE "TSMMacro" "Achievement_Faction_GoldenLotus"
/click TSMCraftingBtn
/click TSMAuctioningBtn
/click TSMDestroyBtn
END
VER 3 00000000000000D0 "wogbig" "INV_MISC_QUESTIONMARK"
#showtooltip 
/cast Rule of Law
/cast [@mouseover,help,nodead][help,nodead][@player] Word of Glory
END
VER 3 00000000000000BE "wrath" "INV_MISC_QUESTIONMARK"
#showtooltip Wrath
/cast [noform:4] Moonkin Form
/cast Wrath
END
VER 3 00000000000000AD "z" "INV_MISC_QUESTIONMARK"
#showtooltip
/cast Berserking
/cast Celestial Alignment
/cast innervate [target=mildasaurus]
/cast [noform:4] Moonkin Form
/use Potion of Spectral Intellect
END
VER 3 00000000000000AE "zz" "INV_MISC_QUESTIONMARK"
#showtooltip
/use [@mouseover,exists] [] Overgrowth
/cast Berserking
END
VER 3 00000000000000B0 "zzz" "INV_MISC_QUESTIONMARK"
#showtooltip
/cast [@focus,exists,nodead] Solar Beam; Solar Beam
END
VER 3 00000000000000B5 "zzzz" "INV_MISC_QUESTIONMARK"
/click ExtraActionButton1
/wm [@cursor] 4
END
