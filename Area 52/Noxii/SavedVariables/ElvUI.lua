
ElvCharacterDB = {
	["ChatEditHistory"] = {
	},
	["ChatHistoryLog"] = {
		{
			"%s has earned the achievement |cffffff00|Hachievement:14297:Player-113-0A72D2F2:1:6:14:21:4294967295:4294967295:4294967295:4294967295|h[Three Choose One]|h|r!", -- [1]
			"Oriye-Draka", -- [2]
			"", -- [3]
			"", -- [4]
			"Oriye-Draka", -- [5]
			"", -- [6]
			0, -- [7]
			0, -- [8]
			"", -- [9]
			0, -- [10]
			2, -- [11]
			"Player-113-0A72D2F2", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			[52] = "|cffa22fc8Oriye-Draka|r",
			[51] = 1623710534,
			[50] = "CHAT_MSG_GUILD_ACHIEVEMENT",
		}, -- [1]
		{
			"%s has earned the achievement |cffffff00|Hachievement:13878:Player-113-090F87C6:1:6:14:21:4294967295:4294967295:4294967295:4294967295|h[The Master of Revendreth]|h|r!", -- [1]
			"Kmoney-Draka", -- [2]
			"", -- [3]
			"", -- [4]
			"Kmoney-Draka", -- [5]
			"", -- [6]
			0, -- [7]
			0, -- [8]
			"", -- [9]
			0, -- [10]
			4, -- [11]
			"Player-113-090F87C6", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			[52] = "|cffa22fc8Kmoney-Draka|r",
			[51] = 1623710556,
			[50] = "CHAT_MSG_GUILD_ACHIEVEMENT",
		}, -- [2]
		{
			"%s has earned the achievement |cffffff00|Hachievement:14206:Player-113-090F87C6:1:6:14:21:4294967295:4294967295:4294967295:4294967295|h[Blade of the Primus]|h|r!", -- [1]
			"Kmoney-Draka", -- [2]
			"", -- [3]
			"", -- [4]
			"Kmoney-Draka", -- [5]
			"", -- [6]
			0, -- [7]
			0, -- [8]
			"", -- [9]
			0, -- [10]
			5, -- [11]
			"Player-113-090F87C6", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			[52] = "|cffa22fc8Kmoney-Draka|r",
			[51] = 1623710556,
			[50] = "CHAT_MSG_GUILD_ACHIEVEMENT",
		}, -- [3]
		{
			"%s has earned the achievement |cffffff00|Hachievement:14164:Player-113-090F87C6:1:6:14:21:4294967295:4294967295:4294967295:4294967295|h[Awaken, Ardenweald]|h|r!", -- [1]
			"Kmoney-Draka", -- [2]
			"", -- [3]
			"", -- [4]
			"Kmoney-Draka", -- [5]
			"", -- [6]
			0, -- [7]
			0, -- [8]
			"", -- [9]
			0, -- [10]
			6, -- [11]
			"Player-113-090F87C6", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			[52] = "|cffa22fc8Kmoney-Draka|r",
			[51] = 1623710556,
			[50] = "CHAT_MSG_GUILD_ACHIEVEMENT",
		}, -- [4]
		{
			"%s has earned the achievement |cffffff00|Hachievement:14281:Player-113-090F87C6:1:6:14:21:4294967295:4294967295:4294967295:4294967295|h[The Path to Ascension]|h|r!", -- [1]
			"Kmoney-Draka", -- [2]
			"", -- [3]
			"", -- [4]
			"Kmoney-Draka", -- [5]
			"", -- [6]
			0, -- [7]
			0, -- [8]
			"", -- [9]
			0, -- [10]
			7, -- [11]
			"Player-113-090F87C6", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			[52] = "|cffa22fc8Kmoney-Draka|r",
			[51] = 1623710556,
			[50] = "CHAT_MSG_GUILD_ACHIEVEMENT",
		}, -- [5]
		{
			"hi:)", -- [1]
			"Ezpzz-Tichondrius", -- [2]
			"", -- [3]
			"1. General - Ardenweald", -- [4]
			"Ezpzz-Tichondrius", -- [5]
			"", -- [6]
			1, -- [7]
			1, -- [8]
			"General - Ardenweald", -- [9]
			0, -- [10]
			2, -- [11]
			"Player-11-0E0B4C8F", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			true, -- [17]
			[52] = "|cfffe7b09Ezpzz-Tichondrius|r",
			[51] = 1623711514,
			[50] = "CHAT_MSG_CHANNEL",
		}, -- [6]
		{
			"Grats Keyana", -- [1]
			"Zoraduntov-Suramar", -- [2]
			"", -- [3]
			"", -- [4]
			"", -- [5]
			"", -- [6]
			0, -- [7]
			0, -- [8]
			"", -- [9]
			0, -- [10]
			3, -- [11]
			"Player-113-062C1B3E", -- [12]
			0, -- [13]
			false, -- [14]
			false, -- [15]
			false, -- [16]
			false, -- [17]
			[52] = "|cfffe7b09Zoraduntov|r",
			[51] = 1623711524,
			[50] = "CHAT_MSG_GUILD",
		}, -- [7]
	},
}
