
PremadeGroupsFilterState = {
	["t1c4f0"] = {
		["enabled"] = true,
		["tanks"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["ilvl"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["dps"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["noilvl"] = {
			["act"] = false,
		},
		["expression"] = "",
		["difficulty"] = {
			["act"] = false,
			["val"] = 3,
		},
		["defeated"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["members"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["heals"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["sorting"] = "",
	},
	["t3c3f1"] = {
		["enabled"] = true,
		["tanks"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["ilvl"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["dps"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["noilvl"] = {
			["act"] = false,
		},
		["expression"] = "",
		["difficulty"] = {
			["val"] = 3,
			["act"] = false,
		},
		["defeated"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["heals"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["members"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["sorting"] = "",
	},
	["t1c3f1"] = {
		["enabled"] = true,
		["tanks"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["ilvl"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["dps"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["noilvl"] = {
			["act"] = false,
		},
		["expression"] = "",
		["difficulty"] = {
			["act"] = true,
			["val"] = 2,
		},
		["members"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["heals"] = {
			["max"] = "3",
			["min"] = "",
			["act"] = true,
		},
		["defeated"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["sorting"] = "",
	},
	["t2c3f1"] = {
		["enabled"] = true,
		["tanks"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["ilvl"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["dps"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["noilvl"] = {
			["act"] = false,
		},
		["expression"] = "",
		["difficulty"] = {
			["val"] = 3,
			["act"] = false,
		},
		["defeated"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["heals"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["members"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["sorting"] = "",
	},
	["t3c9f0"] = {
		["enabled"] = true,
		["tanks"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["ilvl"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["dps"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["noilvl"] = {
			["act"] = false,
		},
		["expression"] = "",
		["difficulty"] = {
			["act"] = false,
			["val"] = 3,
		},
		["defeated"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["members"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["heals"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["sorting"] = "",
	},
	["t1c9f0"] = {
		["enabled"] = true,
		["tanks"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["ilvl"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["dps"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["noilvl"] = {
			["act"] = false,
		},
		["expression"] = "",
		["difficulty"] = {
			["val"] = 3,
			["act"] = false,
		},
		["defeated"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["heals"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["members"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["sorting"] = "",
	},
	["t1c2f0"] = {
		["enabled"] = true,
		["tanks"] = {
			["max"] = "0",
			["min"] = "",
			["act"] = true,
		},
		["ilvl"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["dps"] = {
			["max"] = "2",
			["min"] = "",
			["act"] = true,
		},
		["noilvl"] = {
			["act"] = false,
		},
		["expression"] = "gmbt\n",
		["difficulty"] = {
			["act"] = false,
			["val"] = 4,
		},
		["members"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["heals"] = {
			["max"] = "0",
			["min"] = "",
			["act"] = true,
		},
		["defeated"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["sorting"] = "",
	},
	["t1c7f0"] = {
		["enabled"] = true,
		["tanks"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["ilvl"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["dps"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["noilvl"] = {
			["act"] = false,
		},
		["expression"] = "",
		["difficulty"] = {
			["act"] = false,
			["val"] = 3,
		},
		["defeated"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["members"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["heals"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["sorting"] = "",
	},
	["t3c4f0"] = {
		["enabled"] = true,
		["tanks"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["ilvl"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["dps"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["noilvl"] = {
			["act"] = false,
		},
		["expression"] = "",
		["difficulty"] = {
			["act"] = false,
			["val"] = 3,
		},
		["defeated"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["members"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["heals"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["sorting"] = "",
	},
	["t2c4f0"] = {
		["enabled"] = true,
		["tanks"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["ilvl"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["dps"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["noilvl"] = {
			["act"] = false,
		},
		["expression"] = "pvprating>=2000 and heals==0\n",
		["difficulty"] = {
			["act"] = true,
			["val"] = 5,
		},
		["defeated"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["members"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["heals"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["sorting"] = "",
	},
	["t1c6f0"] = {
		["enabled"] = false,
		["tanks"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["ilvl"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["dps"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["noilvl"] = {
			["act"] = false,
		},
		["expression"] = "",
		["difficulty"] = {
			["act"] = false,
			["val"] = 3,
		},
		["defeated"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["members"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["heals"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["sorting"] = "",
	},
	["t2c2f0"] = {
		["enabled"] = true,
		["tanks"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["ilvl"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["dps"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["noilvl"] = {
			["act"] = false,
		},
		["expression"] = "",
		["difficulty"] = {
			["act"] = false,
			["val"] = 3,
		},
		["members"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["heals"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["defeated"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["sorting"] = "",
	},
	["t2c7f0"] = {
		["enabled"] = true,
		["tanks"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["ilvl"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["dps"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["noilvl"] = {
			["act"] = false,
		},
		["expression"] = "",
		["difficulty"] = {
			["act"] = false,
			["val"] = 3,
		},
		["defeated"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["members"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["heals"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["sorting"] = "",
	},
	["t2c9f0"] = {
		["enabled"] = true,
		["tanks"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["ilvl"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["dps"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["noilvl"] = {
			["act"] = false,
		},
		["expression"] = "pvprating>=1600",
		["difficulty"] = {
			["val"] = 3,
			["act"] = false,
		},
		["defeated"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["heals"] = {
			["max"] = "2",
			["min"] = "",
			["act"] = true,
		},
		["members"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["sorting"] = "",
	},
	["expert"] = false,
	["moveable"] = false,
	["t1c1f0"] = {
		["enabled"] = false,
		["tanks"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["ilvl"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["dps"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["noilvl"] = {
			["act"] = false,
		},
		["expression"] = "",
		["difficulty"] = {
			["val"] = 3,
			["act"] = false,
		},
		["defeated"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["heals"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["members"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["sorting"] = "",
	},
	["t2c4f9"] = {
		["enabled"] = false,
		["tanks"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["ilvl"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["dps"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["noilvl"] = {
			["act"] = false,
		},
		["expression"] = "pvprating>=2000 and heals==0",
		["difficulty"] = {
			["act"] = true,
			["val"] = 5,
		},
		["defeated"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["members"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["heals"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["sorting"] = "",
	},
	["t1c113f0"] = {
		["enabled"] = true,
		["tanks"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["ilvl"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["dps"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["noilvl"] = {
			["act"] = false,
		},
		["expression"] = "",
		["difficulty"] = {
			["val"] = 3,
			["act"] = false,
		},
		["defeated"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["heals"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["members"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["sorting"] = "",
	},
	["t1c3f2"] = {
		["enabled"] = true,
		["tanks"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["ilvl"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["dps"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["noilvl"] = {
			["act"] = false,
		},
		["expression"] = "",
		["difficulty"] = {
			["val"] = 3,
			["act"] = false,
		},
		["defeated"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["heals"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["members"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["sorting"] = "",
	},
	["t1c4f9"] = {
		["enabled"] = true,
		["tanks"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["ilvl"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["dps"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["noilvl"] = {
			["act"] = false,
		},
		["expression"] = "",
		["difficulty"] = {
			["act"] = true,
			["val"] = 5,
		},
		["defeated"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["members"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["heals"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["sorting"] = "",
	},
	["t3c4f9"] = {
		["enabled"] = true,
		["tanks"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["ilvl"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["dps"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["noilvl"] = {
			["act"] = false,
		},
		["expression"] = "",
		["difficulty"] = {
			["val"] = 3,
			["act"] = false,
		},
		["defeated"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["heals"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["members"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["sorting"] = "",
	},
	["t2c6f0"] = {
		["enabled"] = true,
		["tanks"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["ilvl"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["dps"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["noilvl"] = {
			["act"] = false,
		},
		["expression"] = "",
		["difficulty"] = {
			["val"] = 3,
			["act"] = false,
		},
		["defeated"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["heals"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["members"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["sorting"] = "",
	},
	["t1c3f5"] = {
		["enabled"] = true,
		["tanks"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["ilvl"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["dps"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["noilvl"] = {
			["act"] = false,
		},
		["expression"] = "",
		["difficulty"] = {
			["val"] = 3,
			["act"] = false,
		},
		["defeated"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["heals"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["members"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["sorting"] = "",
	},
	["t3c2f0"] = {
		["enabled"] = true,
		["tanks"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["ilvl"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["dps"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["noilvl"] = {
			["act"] = false,
		},
		["expression"] = "",
		["difficulty"] = {
			["act"] = false,
			["val"] = 3,
		},
		["members"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["heals"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["defeated"] = {
			["max"] = "",
			["min"] = "",
			["act"] = false,
		},
		["sorting"] = "",
	},
}
