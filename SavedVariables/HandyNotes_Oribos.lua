
HandyNotes_OribosDB = {
	["char"] = {
		["Noxiishields - Area 52"] = {
			["hidden"] = {
			},
		},
	},
	["profileKeys"] = {
		["Noxiipally - Area 52"] = "Noxiipally - Area 52",
		["Noxiishields - Area 52"] = "Noxiishields - Area 52",
	},
	["profiles"] = {
		["Noxiipally - Area 52"] = {
		},
		["Noxiishields - Area 52"] = {
			["show_portaltrainer"] = true,
			["icon_scale_innkeeper"] = 1.25,
			["icon_scale_reforge"] = 1.25,
			["show_stablemaster"] = true,
			["icon_scale_tpplatform"] = 1.5,
			["icon_scale_barber"] = 1.25,
			["fmaster_waypoint"] = true,
			["show_vendor"] = true,
			["show_mail"] = true,
			["show_guildvault"] = true,
			["icon_scale_trainer"] = 1.25,
			["show_innkeeper"] = true,
			["icon_alpha_stablemaster"] = 1,
			["icon_alpha_auctioneer"] = 1,
			["icon_scale_mail"] = 1.25,
			["icon_scale_vendor"] = 1.25,
			["show_reforge"] = true,
			["force_nodes"] = false,
			["icon_alpha_zonegateway"] = 1,
			["show_zonegateway"] = true,
			["icon_alpha_portal"] = 1,
			["icon_scale_zonegateway"] = 2,
			["icon_scale_guildvault"] = 1.3,
			["icon_alpha_transmogrifier"] = 1,
			["show_banker"] = true,
			["icon_alpha_portaltrainer"] = 1,
			["icon_alpha_banker"] = 1,
			["icon_alpha_tpplatform"] = 1,
			["icon_scale_void"] = 1.25,
			["icon_alpha_guildvault"] = 1,
			["show_tpplatform"] = true,
			["icon_alpha_void"] = 1,
			["icon_scale_portal"] = 1.5,
			["fmaster_waypoint_dropdown"] = 1,
			["icon_scale_portaltrainer"] = 1.25,
			["easy_waypoint"] = true,
			["icon_scale_banker"] = 1.25,
			["icon_alpha_vendor"] = 1,
			["icon_alpha_reforge"] = 1,
			["show_prints"] = false,
			["icon_alpha_barber"] = 1,
			["icon_alpha_trainer"] = 1,
			["show_auctioneer"] = true,
			["show_barber"] = true,
			["show_portal"] = true,
			["show_trainer"] = true,
			["icon_scale_auctioneer"] = 1.25,
			["show_onlymytrainers"] = false,
			["icon_alpha_innkeeper"] = 1,
			["icon_scale_transmogrifier"] = 1.25,
			["show_void"] = true,
			["show_transmogrifier"] = true,
			["icon_scale_stablemaster"] = 1.25,
			["icon_alpha_mail"] = 1,
		},
	},
}
