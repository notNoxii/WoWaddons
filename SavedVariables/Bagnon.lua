
Bagnon_Sets = {
	["tackleColor"] = {
	},
	["leatherColor"] = {
	},
	["engineerColor"] = {
	},
	["herbColor"] = {
	},
	["inscribeColor"] = {
	},
	["soulColor"] = {
	},
	["locked"] = true,
	["quiverColor"] = {
	},
	["reagentColor"] = {
	},
	["gemColor"] = {
	},
	["enchantColor"] = {
	},
	["keyColor"] = {
	},
	["normalColor"] = {
	},
	["profiles"] = {
	},
	["mineColor"] = {
	},
	["version"] = "9.2.0",
	["fridgeColor"] = {
	},
	["global"] = {
		["inventory"] = {
			["rules"] = {
				"all", -- [1]
				"all/normal", -- [2]
				"all/trade", -- [3]
				"all/reagent", -- [4]
				"all/keys", -- [5]
				"all/quiver", -- [6]
				"equip", -- [7]
				"equip/armor", -- [8]
				"equip/weapon", -- [9]
				"equip/trinket", -- [10]
				"use", -- [11]
				"use/consume", -- [12]
				"use/enhance", -- [13]
				"trade", -- [14]
				"trade/goods", -- [15]
				"trade/gem", -- [16]
				"trade/glyph", -- [17]
				"trade/recipe", -- [18]
				"quest", -- [19]
				"misc", -- [20]
			},
			["columns"] = 12,
			["hiddenBags"] = {
			},
			["color"] = {
				0.1568627450980392, -- [1]
				0.1647058823529412, -- [2]
				0.2117647058823529, -- [3]
				1, -- [4]
			},
			["hiddenRules"] = {
			},
			["y"] = 0,
			["x"] = 0,
			["borderColor"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
				0, -- [4]
			},
			["point"] = "BOTTOMRIGHT",
			["bagBreak"] = false,
			["strata"] = "HIGH",
		},
		["vault"] = {
			["rules"] = {
				"all", -- [1]
				"all/normal", -- [2]
				"all/trade", -- [3]
				"all/reagent", -- [4]
				"all/keys", -- [5]
				"all/quiver", -- [6]
				"equip", -- [7]
				"equip/armor", -- [8]
				"equip/weapon", -- [9]
				"equip/trinket", -- [10]
				"use", -- [11]
				"use/consume", -- [12]
				"use/enhance", -- [13]
				"trade", -- [14]
				"trade/goods", -- [15]
				"trade/gem", -- [16]
				"trade/glyph", -- [17]
				"trade/recipe", -- [18]
				"quest", -- [19]
				"misc", -- [20]
			},
			["hiddenBags"] = {
			},
			["color"] = {
			},
			["hiddenRules"] = {
			},
			["borderColor"] = {
			},
		},
		["guild"] = {
			["rules"] = {
				"all", -- [1]
				"all/normal", -- [2]
				"all/trade", -- [3]
				"all/reagent", -- [4]
				"all/keys", -- [5]
				"all/quiver", -- [6]
				"equip", -- [7]
				"equip/armor", -- [8]
				"equip/weapon", -- [9]
				"equip/trinket", -- [10]
				"use", -- [11]
				"use/consume", -- [12]
				"use/enhance", -- [13]
				"trade", -- [14]
				"trade/goods", -- [15]
				"trade/gem", -- [16]
				"trade/glyph", -- [17]
				"trade/recipe", -- [18]
				"quest", -- [19]
				"misc", -- [20]
			},
			["borderColor"] = {
			},
			["color"] = {
			},
			["hiddenRules"] = {
			},
			["hiddenBags"] = {
			},
		},
		["bank"] = {
			["rules"] = {
				"all", -- [1]
				"all/normal", -- [2]
				"all/trade", -- [3]
				"all/reagent", -- [4]
				"all/keys", -- [5]
				"all/quiver", -- [6]
				"equip", -- [7]
				"equip/armor", -- [8]
				"equip/weapon", -- [9]
				"equip/trinket", -- [10]
				"use", -- [11]
				"use/consume", -- [12]
				"use/enhance", -- [13]
				"trade", -- [14]
				"trade/goods", -- [15]
				"trade/gem", -- [16]
				"trade/glyph", -- [17]
				"trade/recipe", -- [18]
				"quest", -- [19]
				"misc", -- [20]
			},
			["hiddenBags"] = {
			},
			["color"] = {
			},
			["hiddenRules"] = {
			},
			["borderColor"] = {
			},
		},
	},
	["displayBlizzard"] = false,
}
