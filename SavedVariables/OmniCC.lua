
OmniCCDB = {
	["profileKeys"] = {
		["Noxiipally - Area 52"] = "Default",
		["Noxiishields - Area 52"] = "Default",
	},
	["global"] = {
		["dbVersion"] = 6,
		["addonVersion"] = "9.2.0",
		["disableBlizzardCooldownText"] = true,
	},
	["profiles"] = {
		["Default"] = {
			["rules"] = {
				{
					["enabled"] = false,
					["patterns"] = {
						"Aura", -- [1]
						"Buff", -- [2]
						"Debuff", -- [3]
					},
					["name"] = "Auras",
					["id"] = "auras",
					["priority"] = 0,
					["theme"] = "Default",
				}, -- [1]
				{
					["enabled"] = false,
					["patterns"] = {
						"Plate", -- [1]
					},
					["name"] = "Unit Nameplates",
					["id"] = "plates",
					["priority"] = 0,
					["theme"] = "Default",
				}, -- [2]
				{
					["enabled"] = false,
					["patterns"] = {
						"ActionButton", -- [1]
					},
					["name"] = "ActionBars",
					["id"] = "actions",
					["priority"] = 0,
					["theme"] = "Default",
				}, -- [3]
				{
					["enabled"] = true,
					["patterns"] = {
						"PlaterMainAuraIcon", -- [1]
						"PlaterSecondaryAuraIcon", -- [2]
						"ExtraIconRowIcon", -- [3]
					},
					["id"] = "Plater Nameplates Rule",
					["priority"] = 4,
					["theme"] = "Plater Nameplates Theme",
				}, -- [4]
			},
			["defaultTheme"] = "Default",
			["themes"] = {
				["Default"] = {
					["textStyles"] = {
						["soon"] = {
							["a"] = 1,
							["b"] = 0.1,
							["scale"] = 1.5,
							["g"] = 0.1,
							["r"] = 1,
						},
						["seconds"] = {
							["a"] = 1,
							["b"] = 0.1,
							["scale"] = 1,
							["g"] = 1,
							["r"] = 1,
						},
						["days"] = {
							["a"] = 1,
							["b"] = 0.7,
							["scale"] = 0.75,
							["g"] = 0.7,
							["r"] = 0.7,
						},
						["minutes"] = {
							["a"] = 1,
							["b"] = 1,
							["scale"] = 1,
							["g"] = 1,
							["r"] = 1,
						},
						["hours"] = {
							["a"] = 1,
							["b"] = 0.7,
							["scale"] = 0.75,
							["g"] = 0.7,
							["r"] = 0.7,
						},
						["charging"] = {
							["a"] = 0.8,
							["r"] = 0.8,
							["scale"] = 0.75,
							["g"] = 1,
							["b"] = 0.3,
						},
						["controlled"] = {
							["a"] = 1,
							["b"] = 0.1,
							["scale"] = 1.5,
							["g"] = 0.1,
							["r"] = 1,
						},
					},
					["fontFace"] = "Interface\\Addons\\gmFonts\\fonts\\Ubuntu-R.ttf",
					["timerOffset"] = 0,
					["cooldownOpacity"] = 1,
					["scaleText"] = true,
					["tenthsDuration"] = 0,
					["fontOutline"] = "OUTLINE",
					["minSize"] = 0.5,
					["mmSSDuration"] = 0,
					["enableText"] = true,
					["effect"] = "pulse",
					["fontSize"] = 18,
					["maxDuration"] = 0,
					["xOff"] = 0,
					["name"] = "Default",
					["fontShadow"] = {
						["y"] = 0,
						["x"] = 0,
						["r"] = 1,
						["b"] = 1,
						["g"] = 1,
						["a"] = 0,
					},
					["minDuration"] = 2,
					["minEffectDuration"] = 30,
					["anchor"] = "CENTER",
					["yOff"] = 0,
				},
				["Plater Nameplates Theme"] = {
					["textStyles"] = {
						["soon"] = {
							["a"] = 1,
							["b"] = 0.1,
							["scale"] = 1.5,
							["g"] = 0.1,
							["r"] = 1,
						},
						["seconds"] = {
							["a"] = 1,
							["b"] = 0.1,
							["scale"] = 1,
							["g"] = 1,
							["r"] = 1,
						},
						["days"] = {
							["a"] = 1,
							["b"] = 0.7,
							["scale"] = 0.75,
							["g"] = 0.7,
							["r"] = 0.7,
						},
						["minutes"] = {
							["a"] = 1,
							["b"] = 1,
							["scale"] = 1,
							["g"] = 1,
							["r"] = 1,
						},
						["hours"] = {
							["a"] = 1,
							["b"] = 0.7,
							["scale"] = 0.75,
							["g"] = 0.7,
							["r"] = 0.7,
						},
						["charging"] = {
							["a"] = 0.8,
							["r"] = 0.8,
							["scale"] = 0.75,
							["g"] = 1,
							["b"] = 0.3,
						},
						["controlled"] = {
							["a"] = 1,
							["b"] = 0.1,
							["scale"] = 1.5,
							["g"] = 0.1,
							["r"] = 1,
						},
					},
					["fontFace"] = "Interface\\Addons\\gmFonts\\fonts\\Ubuntu-R.ttf",
					["timerOffset"] = 0,
					["cooldownOpacity"] = 1,
					["scaleText"] = true,
					["tenthsDuration"] = 0,
					["fontOutline"] = "OUTLINE",
					["minSize"] = 0.5,
					["mmSSDuration"] = 0,
					["enableText"] = true,
					["effect"] = "pulse",
					["maxDuration"] = 0,
					["xOff"] = 0,
					["fontSize"] = 18,
					["fontShadow"] = {
						["y"] = 0,
						["x"] = 0,
						["r"] = 1,
						["b"] = 1,
						["g"] = 1,
						["a"] = 0,
					},
					["minDuration"] = 2,
					["minEffectDuration"] = 30,
					["anchor"] = "CENTER",
					["yOff"] = 0,
				},
			},
		},
	},
}
OmniCC4Config = nil
