
ShoesRenownTrackerDB = {
	["profileKeys"] = {
		["Noxiipally - Area 52"] = "Default",
		["Noxiishields - Area 52"] = "Default",
	},
	["global"] = {
		["chars"] = {
			["Area 52"] = {
				["Noxiishields"] = {
					["class"] = "PRIEST",
					["covenants"] = {
						{
							["name"] = "Kyrian",
							["renown"] = 80,
						}, -- [1]
						{
							["name"] = "Venthyr",
							["renown"] = "N/A",
						}, -- [2]
						{
							["name"] = "Night Fae",
							["renown"] = "N/A",
						}, -- [3]
						{
							["name"] = "Necrolord",
							["renown"] = "N/A",
						}, -- [4]
					},
					["mplus"] = {
						[380] = {
							["tyrannical"] = "N/A",
							["name"] = "SD",
							["fortified"] = "N/A",
						},
						[381] = {
							["tyrannical"] = 15,
							["name"] = "SOA",
							["fortified"] = "N/A",
						},
						[382] = {
							["tyrannical"] = "N/A",
							["name"] = "TOP",
							["fortified"] = "N/A",
						},
						[391] = {
							["tyrannical"] = "N/A",
							["name"] = "TAZ1",
							["fortified"] = "N/A",
						},
						[375] = {
							["tyrannical"] = 15,
							["name"] = "MOTS",
							["fortified"] = "N/A",
						},
						[376] = {
							["tyrannical"] = "N/A",
							["name"] = "NW",
							["fortified"] = "N/A",
						},
						[392] = {
							["tyrannical"] = "N/A",
							["name"] = "TAZ2",
							["fortified"] = "N/A",
						},
						[378] = {
							["tyrannical"] = 15,
							["name"] = "HOA",
							["fortified"] = 15,
						},
						[379] = {
							["tyrannical"] = "N/A",
							["name"] = "PF",
							["fortified"] = 15,
						},
						[377] = {
							["tyrannical"] = "N/A",
							["name"] = "DOS",
							["fortified"] = "N/A",
						},
					},
				},
				["Noxiipally"] = {
					["class"] = "PALADIN",
					["covenants"] = {
						{
							["name"] = "Kyrian",
							["renown"] = 73,
						}, -- [1]
						{
							["name"] = "Venthyr",
							["renown"] = 80,
						}, -- [2]
						{
							["name"] = "Night Fae",
							["renown"] = 42,
						}, -- [3]
						{
							["name"] = "Necrolord",
							["renown"] = 80,
						}, -- [4]
					},
					["mplus"] = {
						[380] = {
							["tyrannical"] = 15,
							["name"] = "SD",
							["fortified"] = 15,
						},
						[381] = {
							["tyrannical"] = 15,
							["name"] = "SOA",
							["fortified"] = 17,
						},
						[382] = {
							["tyrannical"] = 16,
							["name"] = "TOP",
							["fortified"] = 15,
						},
						[391] = {
							["tyrannical"] = 15,
							["name"] = "TAZ1",
							["fortified"] = 16,
						},
						[375] = {
							["tyrannical"] = 16,
							["name"] = "MOTS",
							["fortified"] = 17,
						},
						[376] = {
							["tyrannical"] = 20,
							["name"] = "NW",
							["fortified"] = 19,
						},
						[392] = {
							["tyrannical"] = 16,
							["name"] = "TAZ2",
							["fortified"] = 16,
						},
						[378] = {
							["tyrannical"] = 16,
							["name"] = "HOA",
							["fortified"] = 15,
						},
						[379] = {
							["tyrannical"] = 16,
							["name"] = "PF",
							["fortified"] = 20,
						},
						[377] = {
							["tyrannical"] = 17,
							["name"] = "DOS",
							["fortified"] = 17,
						},
					},
				},
			},
		},
	},
}
