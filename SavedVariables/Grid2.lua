
Grid2DB = {
	["namespaces"] = {
		["LibDualSpec-1.0"] = {
		},
		["Grid2Layout"] = {
			["profiles"] = {
				["Noxiipally - Area 52"] = {
					["BackgroundG"] = 0.1019607843137255,
					["BackgroundTexture"] = "None",
					["PosY"] = -531.8582752958955,
					["PosX"] = 497.2838584776946,
					["BackgroundR"] = 0.1019607843137255,
					["ScaleSize"] = 1.55,
					["FrameLock"] = true,
					["BackgroundA"] = 0,
					["displayHeaderBosses"] = true,
					["BorderTexture"] = "None",
					["displayHeaderFocus"] = true,
					["horizontal"] = false,
					["Positions"] = {
						["By Group10001"] = {
							"TOPLEFT", -- [1]
							1280.963616941568, -- [2]
							-541.7783199478772, -- [3]
						},
						["By Group10002"] = {
							"TOPLEFT", -- [1]
							362.2465973140206, -- [2]
							-438.8254962734427, -- [3]
						},
						["By Group"] = {
							"TOPLEFT", -- [1]
							497.2820420715834, -- [2]
							-532.4015320903127, -- [3]
						},
						["By Group10003"] = {
							"TOPLEFT", -- [1]
							268.0066650635163, -- [2]
							-264.2335398906889, -- [3]
						},
					},
					["BackgroundB"] = 0.1019607843137255,
				},
			},
		},
		["Grid2AoeHeals"] = {
			["global"] = {
			},
		},
		["Grid2Options"] = {
		},
		["Grid2RaidDebuffs"] = {
			["global"] = {
			},
			["profiles"] = {
				["Noxiipally - Area 52"] = {
					["lastSelectedModule"] = "[Custom Debuffs]",
					["debuffs"] = {
					},
					["enabledModules"] = {
					},
					["defaultEJ_difficulty"] = 16,
				},
			},
		},
		["Grid2Frame"] = {
			["global"] = {
			},
			["profiles"] = {
				["Noxiipally - Area 52"] = {
					["font"] = "Ubuntu Medium",
					["frameHeight"] = 44,
					["frameWidth"] = 95,
					["frameBorder"] = 1,
					["barTexture"] = "Grid2 Flat",
					["frameTexture"] = "Grid2 Flat",
					["frameContentColor"] = {
						["b"] = 0.9921568627450981,
						["g"] = 0.9137254901960784,
						["r"] = 0.5450980392156862,
					},
				},
				["Noxiishields - Area 52"] = {
					["frameColor"] = {
						["a"] = 1,
						["r"] = 0,
						["g"] = 0,
						["b"] = 0,
					},
					["fontSize"] = 11,
					["frameBorder"] = 2,
					["iconSize"] = 14,
					["mouseoverTexture"] = "Blizzard Quest Title Highlight",
					["frameBorderDistance"] = 1,
					["frameHeight"] = 48,
					["blinkType"] = "Flash",
					["frameBorderTexture"] = "Grid2 Flat",
					["barTexture"] = "Gradient",
					["mouseoverColor"] = {
						["a"] = 1,
						["r"] = 1,
						["g"] = 1,
						["b"] = 1,
					},
					["frameWidths"] = {
					},
					["mouseoverHighlight"] = false,
					["frameTexture"] = "Gradient",
					["frameContentColor"] = {
						["a"] = 1,
						["r"] = 0,
						["g"] = 0,
						["b"] = 0,
					},
					["blinkFrequency"] = 2,
					["frameHeights"] = {
					},
					["frameBorderColor"] = {
						["a"] = 0,
						["r"] = 0,
						["g"] = 0,
						["b"] = 0,
					},
					["frameWidth"] = 48,
					["orientation"] = "VERTICAL",
				},
			},
		},
	},
	["profileKeys"] = {
		["Noxiipally - Area 52"] = "Noxiipally - Area 52",
		["Noxiishields - Area 52"] = "Noxiishields - Area 52",
	},
	["global"] = {
	},
	["profiles"] = {
		["Noxiipally - Area 52"] = {
			["indicators"] = {
				["corner-top-left"] = {
					["type"] = "text",
					["fontSize"] = 8,
					["level"] = 9,
					["location"] = {
						["y"] = 0,
						["relPoint"] = "TOPLEFT",
						["point"] = "TOPLEFT",
						["x"] = 0,
					},
					["duration"] = true,
					["textlength"] = 12,
					["font"] = "Friz Quadrata TT",
				},
				["side-top"] = {
					["type"] = "text",
					["fontSize"] = 8,
					["level"] = 9,
					["location"] = {
						["y"] = 0,
						["relPoint"] = "TOP",
						["point"] = "TOP",
						["x"] = 0,
					},
					["duration"] = true,
					["textlength"] = 12,
					["font"] = "Friz Quadrata TT",
				},
				["corner-bottom-right"] = {
					["type"] = "icon",
					["location"] = {
						["y"] = 0,
						["relPoint"] = "BOTTOMRIGHT",
						["point"] = "BOTTOMRIGHT",
						["x"] = 0,
					},
					["level"] = 8,
					["fontSize"] = 8,
					["size"] = 12,
				},
				["text-down"] = {
					["type"] = "text",
					["location"] = {
						["y"] = 1,
						["relPoint"] = "BOTTOM",
						["point"] = "BOTTOM",
						["x"] = 0,
					},
					["level"] = 6,
					["textlength"] = 6,
					["fontSize"] = 10,
				},
				["Raid-Debuffs"] = {
					["location"] = {
						["y"] = 0,
						["relPoint"] = "BOTTOMLEFT",
						["point"] = "BOTTOMLEFT",
						["x"] = 0,
					},
					["type"] = "icons",
					["level"] = 8,
					["iconSpacing"] = 2,
				},
				["icon-left"] = {
					["type"] = "icon",
					["location"] = {
						["y"] = 0,
						["relPoint"] = "LEFT",
						["point"] = "LEFT",
						["x"] = 0,
					},
					["level"] = 8,
					["fontSize"] = 8,
					["size"] = 21,
				},
				["border"] = {
					["type"] = "border",
					["color1"] = {
						["a"] = 0,
						["r"] = 0,
						["g"] = 0,
						["b"] = 0,
					},
				},
				["corner-top-right-color"] = {
					["type"] = "text-color",
				},
				["background"] = {
					["type"] = "background",
				},
				["side-top-color"] = {
					["type"] = "text-color",
				},
				["icon-center"] = {
					["type"] = "icon",
					["level"] = 8,
					["location"] = {
						["y"] = 0,
						["relPoint"] = "CENTER",
						["point"] = "CENTER",
						["x"] = 0,
					},
					["reverseCooldown"] = true,
					["fontSize"] = 8,
					["size"] = 16,
				},
				["health-color"] = {
					["type"] = "bar-color",
				},
				["icon-right"] = {
					["type"] = "icon",
					["location"] = {
						["y"] = 3,
						["relPoint"] = "RIGHT",
						["point"] = "RIGHT",
						["x"] = 0,
					},
					["level"] = 8,
					["fontSize"] = 8,
					["size"] = 12,
				},
				["heals-color"] = {
					["type"] = "bar-color",
				},
				["tooltip"] = {
					["type"] = "tooltip",
					["showDefault"] = true,
					["showTooltip"] = 4,
				},
				["alpha"] = {
					["type"] = "alpha",
				},
				["heals"] = {
					["type"] = "bar",
					["texture"] = "Gradient",
					["anchorTo"] = "health",
					["location"] = {
						["y"] = 0,
						["relPoint"] = "CENTER",
						["point"] = "CENTER",
						["x"] = 0,
					},
					["level"] = 1,
					["opacity"] = 0.25,
					["color1"] = {
						["a"] = 0,
						["r"] = 0,
						["g"] = 0,
						["b"] = 0,
					},
				},
				["text-down-color"] = {
					["type"] = "text-color",
				},
				["corner-top-left-color"] = {
					["type"] = "text-color",
				},
				["text-up"] = {
					["type"] = "text",
					["location"] = {
						["y"] = -1,
						["relPoint"] = "TOP",
						["point"] = "TOP",
						["x"] = 0,
					},
					["level"] = 7,
					["textlength"] = 5,
					["fontSize"] = 9,
				},
				["corner-bottom-left"] = {
					["type"] = "square",
					["location"] = {
						["y"] = 0,
						["relPoint"] = "BOTTOMLEFT",
						["point"] = "BOTTOMLEFT",
						["x"] = 0,
					},
					["level"] = 5,
					["size"] = 5,
					["color1"] = {
						["a"] = 1,
						["r"] = 1,
						["g"] = 1,
						["b"] = 1,
					},
				},
				["text-up-color"] = {
					["type"] = "text-color",
				},
				["health"] = {
					["type"] = "bar",
					["texture"] = "Clean",
					["orientation"] = "HORIZONTAL",
					["level"] = 2,
					["location"] = {
						["y"] = 0,
						["relPoint"] = "CENTER",
						["point"] = "CENTER",
						["x"] = 0,
					},
					["color1"] = {
						["a"] = 1,
						["r"] = 0,
						["g"] = 0,
						["b"] = 0,
					},
				},
				["corner-top-right"] = {
					["type"] = "text",
					["fontSize"] = 8,
					["level"] = 9,
					["location"] = {
						["y"] = 0,
						["relPoint"] = "TOPRIGHT",
						["point"] = "TOPRIGHT",
						["x"] = 0,
					},
					["duration"] = true,
					["textlength"] = 12,
					["font"] = "Friz Quadrata TT",
				},
			},
			["statuses"] = {
				["debuff-Forbearance"] = {
					["type"] = "debuff",
					["spellName"] = 25771,
					["color1"] = {
						["a"] = 1,
						["r"] = 1,
						["g"] = 0,
						["b"] = 0,
					},
				},
				["resurrection"] = {
					["color2"] = {
						["a"] = 0.75,
						["b"] = 0,
						["g"] = 1,
						["r"] = 1,
					},
					["type"] = "resurrection",
					["color1"] = {
						["a"] = 1,
						["b"] = 0,
						["g"] = 1,
						["r"] = 0,
					},
					["colorCount"] = 2,
				},
				["health-deficit"] = {
					["threshold"] = 0.05,
					["type"] = "health-deficit",
					["color1"] = {
						["a"] = 1,
						["b"] = 1,
						["g"] = 1,
						["r"] = 1,
					},
				},
				["creaturecolor"] = {
					["type"] = "creaturecolor",
					["colors"] = {
						["UNKNOWN_UNIT"] = {
							["a"] = 1,
							["b"] = 0.5,
							["g"] = 0.5,
							["r"] = 0.5,
						},
						["Demon"] = {
							["a"] = 1,
							["b"] = 0.69,
							["g"] = 0.25,
							["r"] = 0.5,
						},
						["Elemental"] = {
							["a"] = 1,
							["b"] = 0.9,
							["g"] = 0.3,
							["r"] = 0.1,
						},
						["HOSTILE"] = {
							["a"] = 1,
							["b"] = 0.1,
							["g"] = 0.1,
							["r"] = 1,
						},
						["Beast"] = {
							["a"] = 1,
							["b"] = 0.28,
							["g"] = 0.75,
							["r"] = 0.94,
						},
						["Humanoid"] = {
							["a"] = 1,
							["b"] = 0.85,
							["g"] = 0.67,
							["r"] = 0.92,
						},
					},
					["colorHostile"] = true,
				},
				["dungeon-role"] = {
					["color2"] = {
						["b"] = 0,
						["g"] = 0.75,
						["r"] = 0,
					},
					["type"] = "dungeon-role",
					["color3"] = {
						["b"] = 0.75,
						["g"] = 0,
						["r"] = 0,
					},
					["opacity"] = 0.75,
					["color1"] = {
						["b"] = 0,
						["g"] = 0,
						["r"] = 0.75,
					},
					["colorCount"] = 3,
				},
				["threat"] = {
					["color2"] = {
						["a"] = 1,
						["b"] = 1,
						["g"] = 1,
						["r"] = 0.5,
					},
					["type"] = "threat",
					["color3"] = {
						["a"] = 1,
						["b"] = 1,
						["g"] = 1,
						["r"] = 1,
					},
					["color1"] = {
						["a"] = 1,
						["b"] = 0,
						["g"] = 0,
						["r"] = 1,
					},
					["colorCount"] = 3,
				},
				["heal-absorbs"] = {
					["thresholdMedium"] = 75000,
					["type"] = "heal-absorbs",
					["color3"] = {
						["a"] = 1,
						["b"] = 0,
						["g"] = 1,
						["r"] = 1,
					},
					["thresholdLow"] = 25000,
					["color1"] = {
						["a"] = 1,
						["b"] = 0,
						["g"] = 0,
						["r"] = 1,
					},
					["color2"] = {
						["a"] = 1,
						["b"] = 0,
						["g"] = 0.5,
						["r"] = 1,
					},
					["colorCount"] = 3,
				},
				["role"] = {
					["color2"] = {
						["a"] = 1,
						["b"] = 1,
						["g"] = 1,
						["r"] = 0.5,
					},
					["type"] = "role",
					["color1"] = {
						["a"] = 1,
						["b"] = 0.5,
						["g"] = 1,
						["r"] = 1,
					},
					["colorCount"] = 2,
				},
				["buff-DivineShield-mine"] = {
					["spellName"] = 642,
					["type"] = "buff",
					["mine"] = true,
					["color1"] = {
						["a"] = 1,
						["r"] = 1,
						["g"] = 1,
						["b"] = 1,
					},
				},
				["master-looter"] = {
					["color1"] = {
						["a"] = 1,
						["b"] = 0,
						["g"] = 0.5,
						["r"] = 1,
					},
					["type"] = "master-looter",
				},
				["target"] = {
					["color1"] = {
						["a"] = 0.75,
						["b"] = 0.8,
						["g"] = 0.8,
						["r"] = 0.8,
					},
					["type"] = "target",
				},
				["raid-debuffs"] = {
					["enableIcons"] = true,
				},
				["buff-BeaconofVirtue-mine"] = {
					["type"] = "buff",
					["spellName"] = 200025,
					["useSpellId"] = true,
					["mine"] = 1,
					["color1"] = {
						["a"] = 1,
						["r"] = 1,
						["g"] = 1,
						["b"] = 1,
					},
				},
				["debuff-Typeless"] = {
					["type"] = "debuffType",
					["subType"] = "Typeless",
					["color1"] = {
						["a"] = 1,
						["b"] = 0,
						["g"] = 0,
						["r"] = 0,
					},
				},
				["monk-stagger"] = {
					["color2"] = {
						["a"] = 1,
						["b"] = 0,
						["g"] = 1,
						["r"] = 1,
					},
					["type"] = "monk-stagger",
					["color3"] = {
						["a"] = 1,
						["b"] = 0,
						["g"] = 1,
						["r"] = 0,
					},
					["color1"] = {
						["a"] = 1,
						["b"] = 0,
						["g"] = 0,
						["r"] = 1,
					},
					["colorCount"] = 3,
				},
				["buff-BeaconOfLight-mine"] = {
					["spellName"] = 53563,
					["type"] = "buff",
					["mine"] = true,
					["color1"] = {
						["a"] = 1,
						["r"] = 1,
						["g"] = 1,
						["b"] = 1,
					},
				},
				["buff-DivineProtection-mine"] = {
					["spellName"] = 498,
					["type"] = "buff",
					["mine"] = true,
					["color1"] = {
						["a"] = 1,
						["r"] = 1,
						["g"] = 1,
						["b"] = 1,
					},
				},
				["buff-HandOfSalvation-mine"] = {
					["spellName"] = 1038,
					["type"] = "buff",
					["mine"] = true,
					["color1"] = {
						["a"] = 1,
						["r"] = 0.8,
						["g"] = 0.8,
						["b"] = 0.7,
					},
				},
				["charmed"] = {
					["color1"] = {
						["a"] = 1,
						["b"] = 0.1,
						["g"] = 0.1,
						["r"] = 1,
					},
					["type"] = "charmed",
				},
				["phased"] = {
					["color1"] = {
						["a"] = 1,
						["b"] = 0,
						["g"] = 0,
						["r"] = 1,
					},
					["type"] = "phased",
				},
				["ready-check"] = {
					["threshold"] = 10,
					["type"] = "ready-check",
					["color4"] = {
						["a"] = 1,
						["b"] = 1,
						["g"] = 0,
						["r"] = 1,
					},
					["color1"] = {
						["a"] = 1,
						["b"] = 0,
						["g"] = 1,
						["r"] = 1,
					},
					["color2"] = {
						["a"] = 1,
						["b"] = 0,
						["g"] = 1,
						["r"] = 0,
					},
					["color3"] = {
						["a"] = 1,
						["b"] = 0,
						["g"] = 0,
						["r"] = 1,
					},
					["colorCount"] = 4,
				},
				["banzai"] = {
					["color1"] = {
						["a"] = 1,
						["b"] = 1,
						["g"] = 0,
						["r"] = 1,
					},
					["type"] = "banzai",
				},
				["banzai-threat"] = {
					["color1"] = {
						["a"] = 1,
						["b"] = 0,
						["g"] = 0,
						["r"] = 1,
					},
					["type"] = "banzai-threat",
				},
				["raid-icon-target"] = {
					["type"] = "raid-icon-target",
					["color6"] = {
						["a"] = 1,
						["b"] = 1,
						["g"] = 0.71,
						["r"] = 0,
					},
					["opacity"] = 0.5,
					["color1"] = {
						["a"] = 1,
						["b"] = 0,
						["g"] = 0.92,
						["r"] = 1,
					},
					["color2"] = {
						["a"] = 1,
						["b"] = 0,
						["g"] = 0.57,
						["r"] = 0.98,
					},
					["color4"] = {
						["a"] = 1,
						["b"] = 0,
						["g"] = 0.95,
						["r"] = 0.04,
					},
					["color7"] = {
						["a"] = 1,
						["b"] = 0.168,
						["g"] = 0.24,
						["r"] = 1,
					},
					["color3"] = {
						["a"] = 1,
						["b"] = 0.9,
						["g"] = 0.22,
						["r"] = 0.83,
					},
					["color8"] = {
						["a"] = 1,
						["b"] = 0.98,
						["g"] = 0.98,
						["r"] = 0.98,
					},
					["color5"] = {
						["a"] = 1,
						["b"] = 0.875,
						["g"] = 0.82,
						["r"] = 0.7,
					},
					["colorCount"] = 8,
				},
				["name"] = {
					["defaultName"] = 1,
					["type"] = "name",
				},
				["death"] = {
					["color1"] = {
						["a"] = 1,
						["b"] = 1,
						["g"] = 1,
						["r"] = 1,
					},
					["type"] = "death",
				},
				["buff-HandOfProtection-mine"] = {
					["spellName"] = 1022,
					["type"] = "buff",
					["mine"] = true,
					["color1"] = {
						["a"] = 1,
						["r"] = 1,
						["g"] = 1,
						["b"] = 1,
					},
				},
				["debuff-Magic"] = {
					["type"] = "debuffType",
					["subType"] = "Magic",
					["color1"] = {
						["a"] = 1,
						["b"] = 1,
						["g"] = 0.6,
						["r"] = 0.2,
					},
				},
				["friendcolor"] = {
					["color2"] = {
						["a"] = 0.75,
						["b"] = 0,
						["g"] = 1,
						["r"] = 0,
					},
					["type"] = "friendcolor",
					["color3"] = {
						["a"] = 1,
						["b"] = 0,
						["g"] = 0,
						["r"] = 1,
					},
					["color1"] = {
						["a"] = 1,
						["b"] = 0.2117647058823529,
						["g"] = 0.1647058823529412,
						["r"] = 0.1568627450980392,
					},
					["colorCount"] = 3,
				},
				["pvp"] = {
					["color1"] = {
						["a"] = 0.75,
						["b"] = 1,
						["g"] = 1,
						["r"] = 0,
					},
					["type"] = "pvp",
				},
				["combat-mine"] = {
					["color1"] = {
						["a"] = 1,
						["b"] = 0,
						["g"] = 0,
						["r"] = 1,
					},
					["type"] = "combat-mine",
				},
				["buff-HandOfSalvation"] = {
					["spellName"] = 1044,
					["useSpellId"] = true,
					["type"] = "buff",
					["color1"] = {
						["a"] = 1,
						["r"] = 1,
						["g"] = 1,
						["b"] = 1,
					},
				},
				["combat"] = {
					["color1"] = {
						["a"] = 1,
						["b"] = 0,
						["g"] = 0,
						["r"] = 1,
					},
					["type"] = "combat",
				},
				["shields-overflow"] = {
					["color1"] = {
						["a"] = 1,
						["b"] = 1,
						["g"] = 1,
						["r"] = 1,
					},
					["type"] = "shields-overflow",
				},
				["range"] = {
					["type"] = "range",
					["default"] = 0.25,
					["elapsed"] = 0.5,
					["range"] = 38,
					["color1"] = {
						["a"] = 1,
						["b"] = 0,
						["g"] = 0,
						["r"] = 1,
					},
				},
				["afk"] = {
					["color1"] = {
						["a"] = 1,
						["b"] = 0,
						["g"] = 0,
						["r"] = 1,
					},
					["type"] = "afk",
				},
				["voice"] = {
					["color1"] = {
						["a"] = 1,
						["b"] = 0,
						["g"] = 1,
						["r"] = 1,
					},
					["type"] = "voice",
				},
				["mana"] = {
					["color1"] = {
						["a"] = 1,
						["b"] = 1,
						["g"] = 0,
						["r"] = 0,
					},
					["type"] = "mana",
				},
				["debuff-Disease"] = {
					["type"] = "debuffType",
					["subType"] = "Disease",
					["color1"] = {
						["a"] = 1,
						["b"] = 0,
						["g"] = 0.4,
						["r"] = 0.6,
					},
				},
				["poweralt"] = {
					["color1"] = {
						["a"] = 1,
						["b"] = 0.5,
						["g"] = 0,
						["r"] = 1,
					},
					["type"] = "poweralt",
				},
				["overhealing"] = {
					["color1"] = {
						["a"] = 1,
						["b"] = 1,
						["g"] = 0.5,
						["r"] = 0.5,
					},
					["type"] = "overhealing",
				},
				["buff-BeaconOfLight"] = {
					["type"] = "buff",
					["spellName"] = 53563,
					["useSpellId"] = true,
					["load"] = {
						["playerClass"] = {
							["PALADIN"] = true,
						},
					},
					["color1"] = {
						["a"] = 1,
						["r"] = 0.7,
						["g"] = 1,
						["b"] = 1,
					},
				},
				["classcolor"] = {
					["type"] = "classcolor",
					["colors"] = {
						["HUNTER"] = {
							["a"] = 1,
							["b"] = 0.447057843208313,
							["g"] = 0.8274491429328918,
							["r"] = 0.6666651964187622,
						},
						["Humanoid"] = {
							["a"] = 1,
							["b"] = 0.85,
							["g"] = 0.67,
							["r"] = 0.92,
						},
						["PALADIN"] = {
							["a"] = 1,
							["b"] = 0.7294101715087891,
							["g"] = 0.549018383026123,
							["r"] = 0.9568606615066528,
						},
						["MAGE"] = {
							["a"] = 1,
							["r"] = 0.2470582872629166,
							["g"] = 0.7803904414176941,
							["b"] = 0.9215666055679321,
						},
						["Demon"] = {
							["a"] = 1,
							["b"] = 0.69,
							["g"] = 0.25,
							["r"] = 0.54,
						},
						["DRUID"] = {
							["a"] = 1,
							["b"] = 0.03921560198068619,
							["g"] = 0.4862734377384186,
							["r"] = 0.9999977946281433,
						},
						["MONK"] = {
							["a"] = 1,
							["b"] = 0.5960771441459656,
							["g"] = 0.9999977946281433,
							["r"] = 0,
						},
						["DEATHKNIGHT"] = {
							["a"] = 1,
							["b"] = 0.2274504750967026,
							["g"] = 0.117646798491478,
							["r"] = 0.7686257362365723,
						},
						["Elemental"] = {
							["a"] = 1,
							["b"] = 0.9,
							["g"] = 0.3,
							["r"] = 0.1,
						},
						["ROGUE"] = {
							["a"] = 1,
							["b"] = 0.4078422486782074,
							["g"] = 0.9568606615066528,
							["r"] = 0.9999977946281433,
						},
						["PRIEST"] = {
							["a"] = 1,
							["b"] = 0.9999977946281433,
							["g"] = 0.9999977946281433,
							["r"] = 0.9999977946281433,
						},
						["SHAMAN"] = {
							["a"] = 1,
							["b"] = 0.8666647672653198,
							["g"] = 0.4392147064208984,
							["r"] = 0,
						},
						["UNKNOWN_PET"] = {
							["a"] = 1,
							["b"] = 0,
							["g"] = 1,
							["r"] = 0,
						},
						["WARLOCK"] = {
							["a"] = 1,
							["b"] = 0.933331310749054,
							["g"] = 0.5333321690559387,
							["r"] = 0.5294106006622314,
						},
						["DEMONHUNTER"] = {
							["a"] = 1,
							["b"] = 0.7882335782051086,
							["g"] = 0.188234880566597,
							["r"] = 0.639214277267456,
						},
						["UNKNOWN_UNIT"] = {
							["a"] = 1,
							["b"] = 0.5,
							["g"] = 0.5,
							["r"] = 0.5,
						},
						["HOSTILE"] = {
							["a"] = 1,
							["b"] = 0.1,
							["g"] = 0.1,
							["r"] = 1,
						},
						["Beast"] = {
							["a"] = 1,
							["b"] = 0.28,
							["g"] = 0.76,
							["r"] = 0.94,
						},
						["WARRIOR"] = {
							["a"] = 1,
							["b"] = 0.427450031042099,
							["g"] = 0.6078417897224426,
							["r"] = 0.7764688730239868,
						},
					},
					["colorHostile"] = true,
				},
				["vehicle"] = {
					["color1"] = {
						["a"] = 0.75,
						["b"] = 1,
						["g"] = 1,
						["r"] = 0,
					},
					["type"] = "vehicle",
				},
				["health-current"] = {
					["color2"] = {
						["a"] = 1,
						["b"] = 0.2117647058823529,
						["g"] = 0.1647058823529412,
						["r"] = 0.1568627450980392,
					},
					["type"] = "health-current",
					["color3"] = {
						["a"] = 1,
						["b"] = 0.2117647058823529,
						["g"] = 0.1647058823529412,
						["r"] = 0.1568627450980392,
					},
					["color1"] = {
						["a"] = 1,
						["b"] = 0.2117647058823529,
						["g"] = 0.1647058823529412,
						["r"] = 0.1568627450980392,
					},
					["colorCount"] = 3,
				},
				["feign-death"] = {
					["color1"] = {
						["a"] = 1,
						["b"] = 1,
						["g"] = 0.5,
						["r"] = 1,
					},
					["type"] = "feign-death",
				},
				["self"] = {
					["color1"] = {
						["a"] = 1,
						["b"] = 0.25,
						["g"] = 1,
						["r"] = 0.25,
					},
					["type"] = "self",
				},
				["raid-icon-player"] = {
					["type"] = "raid-icon-player",
					["color6"] = {
						["a"] = 1,
						["b"] = 1,
						["g"] = 0.71,
						["r"] = 0,
					},
					["opacity"] = 1,
					["color1"] = {
						["a"] = 1,
						["b"] = 0,
						["g"] = 0.92,
						["r"] = 1,
					},
					["color2"] = {
						["a"] = 1,
						["b"] = 0,
						["g"] = 0.57,
						["r"] = 0.98,
					},
					["color4"] = {
						["a"] = 1,
						["b"] = 0,
						["g"] = 0.95,
						["r"] = 0.04,
					},
					["color7"] = {
						["a"] = 1,
						["b"] = 0.168,
						["g"] = 0.24,
						["r"] = 1,
					},
					["color3"] = {
						["a"] = 1,
						["b"] = 0.9,
						["g"] = 0.22,
						["r"] = 0.83,
					},
					["color8"] = {
						["a"] = 1,
						["b"] = 0.98,
						["g"] = 0.98,
						["r"] = 0.98,
					},
					["color5"] = {
						["a"] = 1,
						["b"] = 0.875,
						["g"] = 0.82,
						["r"] = 0.7,
					},
					["colorCount"] = 8,
				},
				["shields"] = {
					["thresholdMedium"] = 50000,
					["type"] = "shields",
					["color3"] = {
						["a"] = 1,
						["b"] = 0,
						["g"] = 1,
						["r"] = 1,
					},
					["thresholdLow"] = 25000,
					["color1"] = {
						["a"] = 1,
						["b"] = 0.4823529411764706,
						["g"] = 0.9803921568627451,
						["r"] = 0.3137254901960784,
					},
					["color2"] = {
						["a"] = 1,
						["b"] = 0,
						["g"] = 0.5,
						["r"] = 1,
					},
					["colorCount"] = 3,
				},
				["buff-BestowFaith-mine"] = {
					["spellName"] = 223306,
					["type"] = "buff",
					["mine"] = 1,
					["color1"] = {
						["a"] = 1,
						["r"] = 1,
						["g"] = 1,
						["b"] = 1,
					},
				},
				["raid-assistant"] = {
					["color1"] = {
						["a"] = 1,
						["b"] = 0.2,
						["g"] = 0.25,
						["r"] = 1,
					},
					["type"] = "raid-assistant",
				},
				["leader"] = {
					["color1"] = {
						["a"] = 1,
						["b"] = 1,
						["g"] = 0.7,
						["r"] = 0,
					},
					["type"] = "leader",
				},
				["my-heals-incoming"] = {
					["flags"] = 0,
					["type"] = "my-heals-incoming",
					["multiplier"] = 1,
					["color1"] = {
						["a"] = 1,
						["b"] = 0,
						["g"] = 1,
						["r"] = 0,
					},
				},
				["debuff-Poison"] = {
					["type"] = "debuffType",
					["subType"] = "Poison",
					["color1"] = {
						["a"] = 1,
						["b"] = 0,
						["g"] = 0.6,
						["r"] = 0,
					},
				},
				["summon"] = {
					["color2"] = {
						["a"] = 1,
						["b"] = 0,
						["g"] = 1,
						["r"] = 0,
					},
					["type"] = "summon",
					["color3"] = {
						["a"] = 1,
						["b"] = 0,
						["g"] = 0,
						["r"] = 1,
					},
					["color1"] = {
						["a"] = 1,
						["b"] = 0,
						["g"] = 1,
						["r"] = 1,
					},
					["colorCount"] = 3,
				},
				["health-low"] = {
					["threshold"] = 0.4,
					["type"] = "health-low",
					["color1"] = {
						["a"] = 1,
						["b"] = 0.2117647058823529,
						["g"] = 0.1647058823529412,
						["r"] = 0.1568627450980392,
					},
				},
				["power"] = {
					["type"] = "power",
					["color6"] = {
						["a"] = 1,
						["b"] = 0.8,
						["g"] = 0,
						["r"] = 0.4,
					},
					["color10"] = {
						["a"] = 1,
						["b"] = 0,
						["g"] = 0.61,
						["r"] = 1,
					},
					["color1"] = {
						["a"] = 1,
						["b"] = 1,
						["g"] = 0.5,
						["r"] = 0,
					},
					["color2"] = {
						["a"] = 1,
						["b"] = 0,
						["g"] = 0,
						["r"] = 1,
					},
					["color7"] = {
						["a"] = 1,
						["b"] = 1,
						["g"] = 0.5,
						["r"] = 0,
					},
					["color3"] = {
						["a"] = 1,
						["b"] = 0,
						["g"] = 0.5,
						["r"] = 1,
					},
					["color9"] = {
						["a"] = 1,
						["b"] = 0.992,
						["g"] = 0.259,
						["r"] = 0.788,
					},
					["color4"] = {
						["a"] = 1,
						["b"] = 0,
						["g"] = 1,
						["r"] = 1,
					},
					["color8"] = {
						["a"] = 1,
						["b"] = 0.9,
						["g"] = 0.52,
						["r"] = 0.3,
					},
					["color5"] = {
						["a"] = 1,
						["b"] = 0.8,
						["g"] = 0.8,
						["r"] = 0,
					},
					["colorCount"] = 10,
				},
				["direction"] = {
					["color1"] = {
						["a"] = 1,
						["b"] = 0,
						["g"] = 1,
						["r"] = 0,
					},
					["type"] = "direction",
				},
				["debuff-Curse"] = {
					["type"] = "debuffType",
					["subType"] = "Curse",
					["color1"] = {
						["a"] = 1,
						["b"] = 1,
						["g"] = 0,
						["r"] = 0.6,
					},
				},
				["offline"] = {
					["color1"] = {
						["a"] = 1,
						["b"] = 1,
						["g"] = 1,
						["r"] = 1,
					},
					["type"] = "offline",
				},
				["heals-incoming"] = {
					["flags"] = 0,
					["type"] = "heals-incoming",
					["includePlayerHeals"] = false,
					["multiplier"] = 1,
					["color1"] = {
						["a"] = 1,
						["b"] = 0,
						["g"] = 1,
						["r"] = 0,
					},
				},
				["lowmana"] = {
					["threshold"] = 0.75,
					["type"] = "lowmana",
					["color1"] = {
						["a"] = 1,
						["b"] = 1,
						["g"] = 0,
						["r"] = 0.5,
					},
				},
			},
			["formatting"] = {
				["secondsElapsedFormat"] = "%ds",
				["longDurationStackFormat"] = "%.1f:%d",
				["longDecimalFormat"] = "%.1f",
				["shortDurationStackFormat"] = "%.0f:%d",
				["invertDurationStack"] = false,
				["minutesElapsedFormat"] = "%dm",
				["shortDecimalFormat"] = "%.0f",
			},
			["versions"] = {
				["Grid2"] = 9,
				["Grid2RaidDebuffs"] = 4,
			},
			["statusMap"] = {
				["corner-top-left"] = {
				},
				["side-top"] = {
					["buff-FlashOfLight-mine"] = 99,
				},
				["corner-bottom-right"] = {
					["debuff-Forbearance"] = 99,
				},
				["text-down"] = {
					["resurrection"] = 54,
					["feign-death"] = 52,
					["death"] = 50,
					["afk"] = 51,
					["offline"] = 53,
				},
				["Raid-Debuffs"] = {
					["debuff-Disease"] = 51,
					["debuff-Poison"] = 54,
					["debuff-Curse"] = 55,
					["debuff-Magic"] = 53,
					["debuff-Typeless"] = 56,
				},
				["icon-left"] = {
				},
				["border"] = {
					["debuff-Disease"] = 90,
					["health-low"] = 55,
					["debuff-Poison"] = 80,
					["target"] = 91,
					["debuff-Magic"] = 70,
					["debuff-Curse"] = 60,
				},
				["corner-top-right-color"] = {
					["buff-DivineShield-mine"] = 97,
					["buff-DivineProtection-mine"] = 95,
					["buff-HandOfProtection-mine"] = 93,
				},
				["text-down-color"] = {
					["classcolor"] = 99,
				},
				["side-top-color"] = {
					["buff-FlashOfLight-mine"] = 99,
				},
				["icon-center"] = {
					["resurrection"] = 157,
					["death"] = 155,
					["ready-check"] = 150,
					["offline"] = 156,
					["buff-BeaconofVirtue-mine"] = 159,
					["raid-icon-player"] = 158,
				},
				["health-color"] = {
					["health-current"] = 51,
				},
				["corner-top-right"] = {
					["buff-DivineShield-mine"] = 97,
					["buff-DivineProtection-mine"] = 95,
					["buff-HandOfProtection-mine"] = 93,
				},
				["heals-color"] = {
					["classcolor"] = 99,
				},
				["alpha"] = {
					["offline"] = 97,
					["range"] = 99,
					["death"] = 98,
				},
				["corner-top-left-color"] = {
					["buff-BeaconOfLight"] = 99,
					["buff-BeaconOfLight-mine"] = 89,
				},
				["health"] = {
					["health-current"] = 99,
				},
				["text-up"] = {
					["name"] = 50,
				},
				["corner-bottom-left"] = {
				},
				["text-up-color"] = {
					["classcolor"] = 50,
				},
				["heals"] = {
					["heals-incoming"] = 99,
				},
				["icon-right"] = {
					["buff-BestowFaith-mine"] = 51,
				},
			},
			["themes"] = {
				["enabled"] = {
				},
				["indicators"] = {
					[0] = {
					},
				},
				["names"] = {
				},
			},
		},
		["Noxiishields - Area 52"] = {
			["indicators"] = {
				["corner-top-left"] = {
					["location"] = {
						["y"] = 0,
						["relPoint"] = "TOPLEFT",
						["point"] = "TOPLEFT",
						["x"] = 0,
					},
					["type"] = "square",
					["level"] = 9,
					["size"] = 5,
				},
				["text-down"] = {
					["type"] = "text",
					["location"] = {
						["y"] = 4,
						["relPoint"] = "BOTTOM",
						["point"] = "BOTTOM",
						["x"] = 0,
					},
					["level"] = 6,
					["textlength"] = 6,
					["fontSize"] = 10,
				},
				["icon-left"] = {
					["type"] = "icon",
					["location"] = {
						["y"] = 0,
						["relPoint"] = "LEFT",
						["point"] = "LEFT",
						["x"] = -2,
					},
					["level"] = 8,
					["fontSize"] = 8,
					["size"] = 12,
				},
				["border"] = {
					["color1"] = {
						["a"] = 0,
						["b"] = 0,
						["g"] = 0,
						["r"] = 0,
					},
					["type"] = "border",
				},
				["background"] = {
					["type"] = "background",
				},
				["icon-center"] = {
					["type"] = "icon",
					["location"] = {
						["y"] = 0,
						["relPoint"] = "CENTER",
						["point"] = "CENTER",
						["x"] = 0,
					},
					["level"] = 8,
					["fontSize"] = 8,
					["size"] = 14,
				},
				["health-color"] = {
					["type"] = "bar-color",
				},
				["icon-right"] = {
					["type"] = "icon",
					["location"] = {
						["y"] = 0,
						["relPoint"] = "RIGHT",
						["point"] = "RIGHT",
						["x"] = 2,
					},
					["level"] = 8,
					["fontSize"] = 8,
					["size"] = 12,
				},
				["heals-color"] = {
					["type"] = "bar-color",
				},
				["tooltip"] = {
					["type"] = "tooltip",
					["showDefault"] = true,
					["showTooltip"] = 4,
				},
				["alpha"] = {
					["type"] = "alpha",
				},
				["text-up"] = {
					["type"] = "text",
					["location"] = {
						["y"] = -8,
						["relPoint"] = "TOP",
						["point"] = "TOP",
						["x"] = 0,
					},
					["level"] = 7,
					["textlength"] = 6,
					["fontSize"] = 8,
				},
				["corner-top-right"] = {
					["location"] = {
						["y"] = 0,
						["relPoint"] = "TOPRIGHT",
						["point"] = "TOPRIGHT",
						["x"] = 0,
					},
					["type"] = "square",
					["level"] = 9,
					["size"] = 5,
				},
				["health"] = {
					["type"] = "bar",
					["location"] = {
						["y"] = 0,
						["relPoint"] = "CENTER",
						["point"] = "CENTER",
						["x"] = 0,
					},
					["level"] = 2,
					["color1"] = {
						["a"] = 1,
						["b"] = 0,
						["g"] = 0,
						["r"] = 0,
					},
					["texture"] = "Gradient",
				},
				["text-down-color"] = {
					["type"] = "text-color",
				},
				["corner-bottom-left"] = {
					["type"] = "square",
					["location"] = {
						["y"] = 0,
						["relPoint"] = "BOTTOMLEFT",
						["point"] = "BOTTOMLEFT",
						["x"] = 0,
					},
					["level"] = 5,
					["color1"] = {
						["a"] = 1,
						["b"] = 1,
						["g"] = 1,
						["r"] = 1,
					},
					["size"] = 5,
				},
				["text-up-color"] = {
					["type"] = "text-color",
				},
				["heals"] = {
					["type"] = "bar",
					["color1"] = {
						["a"] = 0,
						["b"] = 0,
						["g"] = 0,
						["r"] = 0,
					},
					["anchorTo"] = "health",
					["location"] = {
						["y"] = 0,
						["relPoint"] = "CENTER",
						["point"] = "CENTER",
						["x"] = 0,
					},
					["level"] = 1,
					["opacity"] = 0.25,
					["texture"] = "Gradient",
				},
				["side-bottom"] = {
					["location"] = {
						["y"] = 0,
						["relPoint"] = "BOTTOM",
						["point"] = "BOTTOM",
						["x"] = 0,
					},
					["type"] = "square",
					["level"] = 9,
					["size"] = 5,
				},
			},
			["statuses"] = {
				["heal-absorbs"] = {
					["thresholdMedium"] = 75000,
					["type"] = "heal-absorbs",
					["color3"] = {
						["a"] = 1,
						["r"] = 1,
						["g"] = 1,
						["b"] = 0,
					},
					["thresholdLow"] = 25000,
					["colorCount"] = 3,
					["color2"] = {
						["a"] = 1,
						["r"] = 1,
						["g"] = 0.5,
						["b"] = 0,
					},
					["color1"] = {
						["a"] = 1,
						["r"] = 1,
						["g"] = 0,
						["b"] = 0,
					},
				},
				["health-deficit"] = {
					["threshold"] = 0.05,
					["type"] = "health-deficit",
					["color1"] = {
						["a"] = 1,
						["r"] = 1,
						["g"] = 1,
						["b"] = 1,
					},
				},
				["creaturecolor"] = {
					["type"] = "creaturecolor",
					["colors"] = {
						["UNKNOWN_UNIT"] = {
							["a"] = 1,
							["r"] = 0.5,
							["g"] = 0.5,
							["b"] = 0.5,
						},
						["Demon"] = {
							["a"] = 1,
							["r"] = 0.5,
							["g"] = 0.25,
							["b"] = 0.69,
						},
						["Elemental"] = {
							["a"] = 1,
							["r"] = 0.1,
							["g"] = 0.3,
							["b"] = 0.9,
						},
						["Humanoid"] = {
							["a"] = 1,
							["r"] = 0.92,
							["g"] = 0.67,
							["b"] = 0.85,
						},
						["Beast"] = {
							["a"] = 1,
							["r"] = 0.94,
							["g"] = 0.75,
							["b"] = 0.28,
						},
						["HOSTILE"] = {
							["a"] = 1,
							["r"] = 1,
							["g"] = 0.1,
							["b"] = 0.1,
						},
					},
					["colorHostile"] = true,
				},
				["role"] = {
					["color2"] = {
						["a"] = 1,
						["r"] = 0.5,
						["g"] = 1,
						["b"] = 1,
					},
					["type"] = "role",
					["colorCount"] = 2,
					["color1"] = {
						["a"] = 1,
						["r"] = 1,
						["g"] = 1,
						["b"] = 0.5,
					},
				},
				["buff-PowerWordShield"] = {
					["type"] = "buff",
					["color1"] = {
						["a"] = 1,
						["b"] = 1,
						["g"] = 1,
						["r"] = 0,
					},
					["spellName"] = 17,
				},
				["master-looter"] = {
					["type"] = "master-looter",
					["color1"] = {
						["a"] = 1,
						["r"] = 1,
						["g"] = 0.5,
						["b"] = 0,
					},
				},
				["dungeon-role"] = {
					["color2"] = {
						["r"] = 0,
						["g"] = 0.75,
						["b"] = 0,
					},
					["type"] = "dungeon-role",
					["color3"] = {
						["r"] = 0,
						["g"] = 0,
						["b"] = 0.75,
					},
					["opacity"] = 0.75,
					["colorCount"] = 3,
					["color1"] = {
						["r"] = 0.75,
						["g"] = 0,
						["b"] = 0,
					},
				},
				["raid-debuffs"] = {
					["type"] = "raid-debuffs",
					["debuffs"] = {
					},
					["color1"] = {
						["a"] = 1,
						["r"] = 1,
						["g"] = 0.5,
						["b"] = 1,
					},
				},
				["heals-incoming"] = {
					["flags"] = 0,
					["type"] = "heals-incoming",
					["multiplier"] = 1,
					["includePlayerHeals"] = false,
					["color1"] = {
						["a"] = 1,
						["r"] = 0,
						["g"] = 1,
						["b"] = 0,
					},
				},
				["debuff-Typeless"] = {
					["type"] = "debuffType",
					["subType"] = "Typeless",
					["color1"] = {
						["a"] = 1,
						["r"] = 0,
						["g"] = 0,
						["b"] = 0,
					},
				},
				["buff-SpiritOfRedemption"] = {
					["color1"] = {
						["a"] = 1,
						["b"] = 1,
						["g"] = 1,
						["r"] = 1,
					},
					["type"] = "buff",
					["blinkThreshold"] = 3,
					["spellName"] = 27827,
				},
				["threat"] = {
					["color2"] = {
						["a"] = 1,
						["r"] = 0.5,
						["g"] = 1,
						["b"] = 1,
					},
					["type"] = "threat",
					["color3"] = {
						["a"] = 1,
						["r"] = 1,
						["g"] = 1,
						["b"] = 1,
					},
					["colorCount"] = 3,
					["color1"] = {
						["a"] = 1,
						["r"] = 1,
						["g"] = 0,
						["b"] = 0,
					},
				},
				["debuff-WeakenedSoul"] = {
					["type"] = "debuff",
					["color1"] = {
						["a"] = 1,
						["b"] = 0.9,
						["g"] = 0.2,
						["r"] = 0,
					},
					["spellName"] = 6788,
				},
				["raid-icon-player"] = {
					["type"] = "raid-icon-player",
					["color6"] = {
						["a"] = 1,
						["r"] = 0,
						["g"] = 0.71,
						["b"] = 1,
					},
					["opacity"] = 1,
					["color1"] = {
						["a"] = 1,
						["r"] = 1,
						["g"] = 0.92,
						["b"] = 0,
					},
					["color2"] = {
						["a"] = 1,
						["r"] = 0.98,
						["g"] = 0.57,
						["b"] = 0,
					},
					["color4"] = {
						["a"] = 1,
						["r"] = 0.04,
						["g"] = 0.95,
						["b"] = 0,
					},
					["colorCount"] = 8,
					["color3"] = {
						["a"] = 1,
						["r"] = 0.83,
						["g"] = 0.22,
						["b"] = 0.9,
					},
					["color8"] = {
						["a"] = 1,
						["r"] = 0.98,
						["g"] = 0.98,
						["b"] = 0.98,
					},
					["color5"] = {
						["a"] = 1,
						["r"] = 0.7,
						["g"] = 0.82,
						["b"] = 0.875,
					},
					["color7"] = {
						["a"] = 1,
						["r"] = 1,
						["g"] = 0.24,
						["b"] = 0.168,
					},
				},
				["buff-Renew-mine"] = {
					["color1"] = {
						["a"] = 1,
						["b"] = 1,
						["g"] = 1,
						["r"] = 1,
					},
					["type"] = "buff",
					["mine"] = true,
					["spellName"] = 139,
				},
				["name"] = {
					["type"] = "name",
				},
				["friendcolor"] = {
					["color2"] = {
						["a"] = 0.75,
						["r"] = 0,
						["g"] = 1,
						["b"] = 0,
					},
					["type"] = "friendcolor",
					["color3"] = {
						["a"] = 1,
						["r"] = 1,
						["g"] = 0,
						["b"] = 0,
					},
					["colorCount"] = 3,
					["color1"] = {
						["a"] = 1,
						["r"] = 0,
						["g"] = 1,
						["b"] = 0,
					},
				},
				["debuff-Poison"] = {
					["type"] = "debuffType",
					["subType"] = "Poison",
					["color1"] = {
						["a"] = 1,
						["r"] = 0,
						["g"] = 0.6,
						["b"] = 0,
					},
				},
				["raid-icon-target"] = {
					["type"] = "raid-icon-target",
					["color6"] = {
						["a"] = 1,
						["r"] = 0,
						["g"] = 0.71,
						["b"] = 1,
					},
					["opacity"] = 0.5,
					["color1"] = {
						["a"] = 1,
						["r"] = 1,
						["g"] = 0.92,
						["b"] = 0,
					},
					["color2"] = {
						["a"] = 1,
						["r"] = 0.98,
						["g"] = 0.57,
						["b"] = 0,
					},
					["color4"] = {
						["a"] = 1,
						["r"] = 0.04,
						["g"] = 0.95,
						["b"] = 0,
					},
					["colorCount"] = 8,
					["color3"] = {
						["a"] = 1,
						["r"] = 0.83,
						["g"] = 0.22,
						["b"] = 0.9,
					},
					["color8"] = {
						["a"] = 1,
						["r"] = 0.98,
						["g"] = 0.98,
						["b"] = 0.98,
					},
					["color5"] = {
						["a"] = 1,
						["r"] = 0.7,
						["g"] = 0.82,
						["b"] = 0.875,
					},
					["color7"] = {
						["a"] = 1,
						["r"] = 1,
						["g"] = 0.24,
						["b"] = 0.168,
					},
				},
				["monk-stagger"] = {
					["color2"] = {
						["a"] = 1,
						["r"] = 1,
						["g"] = 1,
						["b"] = 0,
					},
					["type"] = "monk-stagger",
					["color3"] = {
						["a"] = 1,
						["r"] = 0,
						["g"] = 1,
						["b"] = 0,
					},
					["colorCount"] = 3,
					["color1"] = {
						["a"] = 1,
						["r"] = 1,
						["g"] = 0,
						["b"] = 0,
					},
				},
				["death"] = {
					["type"] = "death",
					["color1"] = {
						["a"] = 1,
						["r"] = 1,
						["g"] = 1,
						["b"] = 1,
					},
				},
				["ready-check"] = {
					["threshold"] = 10,
					["type"] = "ready-check",
					["color4"] = {
						["a"] = 1,
						["r"] = 1,
						["g"] = 0,
						["b"] = 1,
					},
					["colorCount"] = 4,
					["color3"] = {
						["a"] = 1,
						["r"] = 1,
						["g"] = 0,
						["b"] = 0,
					},
					["color2"] = {
						["a"] = 1,
						["r"] = 0,
						["g"] = 1,
						["b"] = 0,
					},
					["color1"] = {
						["a"] = 1,
						["r"] = 1,
						["g"] = 1,
						["b"] = 0,
					},
				},
				["overhealing"] = {
					["type"] = "overhealing",
					["color1"] = {
						["a"] = 1,
						["r"] = 0.5,
						["g"] = 0.5,
						["b"] = 1,
					},
				},
				["target"] = {
					["type"] = "target",
					["color1"] = {
						["a"] = 0.75,
						["r"] = 0.8,
						["g"] = 0.8,
						["b"] = 0.8,
					},
				},
				["pvp"] = {
					["type"] = "pvp",
					["color1"] = {
						["a"] = 0.75,
						["r"] = 0,
						["g"] = 1,
						["b"] = 1,
					},
				},
				["combat-mine"] = {
					["type"] = "combat-mine",
					["color1"] = {
						["a"] = 1,
						["r"] = 1,
						["g"] = 0,
						["b"] = 0,
					},
				},
				["voice"] = {
					["type"] = "voice",
					["color1"] = {
						["a"] = 1,
						["r"] = 1,
						["g"] = 1,
						["b"] = 0,
					},
				},
				["combat"] = {
					["type"] = "combat",
					["color1"] = {
						["a"] = 1,
						["r"] = 1,
						["g"] = 0,
						["b"] = 0,
					},
				},
				["resurrection"] = {
					["color2"] = {
						["a"] = 0.75,
						["r"] = 1,
						["g"] = 1,
						["b"] = 0,
					},
					["type"] = "resurrection",
					["colorCount"] = 2,
					["color1"] = {
						["a"] = 1,
						["r"] = 0,
						["g"] = 1,
						["b"] = 0,
					},
				},
				["summon"] = {
					["color2"] = {
						["a"] = 1,
						["r"] = 0,
						["g"] = 1,
						["b"] = 0,
					},
					["type"] = "summon",
					["color3"] = {
						["a"] = 1,
						["r"] = 1,
						["g"] = 0,
						["b"] = 0,
					},
					["colorCount"] = 3,
					["color1"] = {
						["a"] = 1,
						["r"] = 1,
						["g"] = 1,
						["b"] = 0,
					},
				},
				["mana"] = {
					["type"] = "mana",
					["color1"] = {
						["a"] = 1,
						["r"] = 0,
						["g"] = 0,
						["b"] = 1,
					},
				},
				["leader"] = {
					["type"] = "leader",
					["color1"] = {
						["a"] = 1,
						["r"] = 0,
						["g"] = 0.7,
						["b"] = 1,
					},
				},
				["buff-PrayerOfMending-mine"] = {
					["type"] = "buff",
					["mine"] = true,
					["color1"] = {
						["a"] = 1,
						["b"] = 0.2,
						["g"] = 0.2,
						["r"] = 1,
					},
					["color2"] = {
						["a"] = 0.4,
						["b"] = 0.4,
						["g"] = 1,
						["r"] = 1,
					},
					["color3"] = {
						["a"] = 1,
						["b"] = 0.6,
						["g"] = 0.6,
						["r"] = 1,
					},
					["colorCount"] = 5,
					["color4"] = {
						["a"] = 1,
						["b"] = 0.8,
						["g"] = 0.8,
						["r"] = 1,
					},
					["color5"] = {
						["a"] = 1,
						["b"] = 1,
						["g"] = 1,
						["r"] = 1,
					},
					["spellName"] = 33076,
				},
				["debuff-Disease"] = {
					["type"] = "debuffType",
					["subType"] = "Disease",
					["color1"] = {
						["a"] = 1,
						["r"] = 0.6,
						["g"] = 0.4,
						["b"] = 0,
					},
				},
				["poweralt"] = {
					["type"] = "poweralt",
					["color1"] = {
						["a"] = 1,
						["r"] = 1,
						["g"] = 0,
						["b"] = 0.5,
					},
				},
				["feign-death"] = {
					["type"] = "feign-death",
					["color1"] = {
						["a"] = 1,
						["r"] = 1,
						["g"] = 0.5,
						["b"] = 1,
					},
				},
				["charmed"] = {
					["type"] = "charmed",
					["color1"] = {
						["a"] = 1,
						["r"] = 1,
						["g"] = 0.1,
						["b"] = 0.1,
					},
				},
				["classcolor"] = {
					["type"] = "classcolor",
					["colors"] = {
						["UNKNOWN_UNIT"] = {
							["a"] = 1,
							["r"] = 0.5,
							["g"] = 0.5,
							["b"] = 0.5,
						},
						["Humanoid"] = {
							["a"] = 1,
							["r"] = 0.92,
							["g"] = 0.67,
							["b"] = 0.85,
						},
						["PALADIN"] = {
							["a"] = 1,
							["r"] = 0.9568606615066528,
							["g"] = 0.549018383026123,
							["b"] = 0.7294101715087891,
						},
						["MAGE"] = {
							["a"] = 1,
							["r"] = 0.2470582872629166,
							["g"] = 0.7803904414176941,
							["b"] = 0.9215666055679321,
						},
						["Demon"] = {
							["a"] = 1,
							["r"] = 0.54,
							["g"] = 0.25,
							["b"] = 0.69,
						},
						["DRUID"] = {
							["a"] = 1,
							["r"] = 0.9999977946281433,
							["g"] = 0.4862734377384186,
							["b"] = 0.03921560198068619,
						},
						["MONK"] = {
							["a"] = 1,
							["r"] = 0,
							["g"] = 0.9999977946281433,
							["b"] = 0.5960771441459656,
						},
						["DEATHKNIGHT"] = {
							["a"] = 1,
							["r"] = 0.7686257362365723,
							["g"] = 0.117646798491478,
							["b"] = 0.2274504750967026,
						},
						["Elemental"] = {
							["a"] = 1,
							["r"] = 0.1,
							["g"] = 0.3,
							["b"] = 0.9,
						},
						["WARRIOR"] = {
							["a"] = 1,
							["r"] = 0.7764688730239868,
							["g"] = 0.6078417897224426,
							["b"] = 0.427450031042099,
						},
						["PRIEST"] = {
							["a"] = 1,
							["r"] = 0.9999977946281433,
							["g"] = 0.9999977946281433,
							["b"] = 0.9999977946281433,
						},
						["ROGUE"] = {
							["a"] = 1,
							["r"] = 0.9999977946281433,
							["g"] = 0.9568606615066528,
							["b"] = 0.4078422486782074,
						},
						["WARLOCK"] = {
							["a"] = 1,
							["r"] = 0.5294106006622314,
							["g"] = 0.5333321690559387,
							["b"] = 0.933331310749054,
						},
						["HOSTILE"] = {
							["a"] = 1,
							["r"] = 1,
							["g"] = 0.1,
							["b"] = 0.1,
						},
						["DEMONHUNTER"] = {
							["a"] = 1,
							["r"] = 0.639214277267456,
							["g"] = 0.188234880566597,
							["b"] = 0.7882335782051086,
						},
						["UNKNOWN_PET"] = {
							["a"] = 1,
							["r"] = 0,
							["g"] = 1,
							["b"] = 0,
						},
						["SHAMAN"] = {
							["a"] = 1,
							["r"] = 0,
							["g"] = 0.4392147064208984,
							["b"] = 0.8666647672653198,
						},
						["Beast"] = {
							["a"] = 1,
							["r"] = 0.94,
							["g"] = 0.76,
							["b"] = 0.28,
						},
						["HUNTER"] = {
							["a"] = 1,
							["r"] = 0.6666651964187622,
							["g"] = 0.8274491429328918,
							["b"] = 0.447057843208313,
						},
					},
					["colorHostile"] = true,
				},
				["vehicle"] = {
					["type"] = "vehicle",
					["color1"] = {
						["a"] = 0.75,
						["r"] = 0,
						["g"] = 1,
						["b"] = 1,
					},
				},
				["health-current"] = {
					["color2"] = {
						["a"] = 1,
						["r"] = 1,
						["g"] = 1,
						["b"] = 0,
					},
					["type"] = "health-current",
					["color3"] = {
						["a"] = 1,
						["r"] = 1,
						["g"] = 0,
						["b"] = 0,
					},
					["colorCount"] = 3,
					["color1"] = {
						["a"] = 1,
						["r"] = 0,
						["g"] = 1,
						["b"] = 0,
					},
				},
				["afk"] = {
					["type"] = "afk",
					["color1"] = {
						["a"] = 1,
						["r"] = 1,
						["g"] = 0,
						["b"] = 0,
					},
				},
				["self"] = {
					["type"] = "self",
					["color1"] = {
						["a"] = 1,
						["r"] = 0.25,
						["g"] = 1,
						["b"] = 0.25,
					},
				},
				["shields-overflow"] = {
					["type"] = "shields-overflow",
					["color1"] = {
						["a"] = 1,
						["r"] = 1,
						["g"] = 1,
						["b"] = 1,
					},
				},
				["shields"] = {
					["thresholdMedium"] = 50000,
					["type"] = "shields",
					["color3"] = {
						["a"] = 1,
						["r"] = 1,
						["g"] = 1,
						["b"] = 0,
					},
					["thresholdLow"] = 25000,
					["colorCount"] = 3,
					["color2"] = {
						["a"] = 1,
						["r"] = 1,
						["g"] = 0.5,
						["b"] = 0,
					},
					["color1"] = {
						["a"] = 1,
						["r"] = 0,
						["g"] = 1,
						["b"] = 0,
					},
				},
				["banzai-threat"] = {
					["type"] = "banzai-threat",
					["color1"] = {
						["a"] = 1,
						["r"] = 1,
						["g"] = 0,
						["b"] = 0,
					},
				},
				["raid-assistant"] = {
					["type"] = "raid-assistant",
					["color1"] = {
						["a"] = 1,
						["r"] = 1,
						["g"] = 0.25,
						["b"] = 0.2,
					},
				},
				["power"] = {
					["type"] = "power",
					["color6"] = {
						["a"] = 1,
						["r"] = 0.4,
						["g"] = 0,
						["b"] = 0.8,
					},
					["color10"] = {
						["a"] = 1,
						["r"] = 1,
						["g"] = 0.61,
						["b"] = 0,
					},
					["color1"] = {
						["a"] = 1,
						["r"] = 0,
						["g"] = 0.5,
						["b"] = 1,
					},
					["color2"] = {
						["a"] = 1,
						["r"] = 1,
						["g"] = 0,
						["b"] = 0,
					},
					["colorCount"] = 10,
					["color3"] = {
						["a"] = 1,
						["r"] = 1,
						["g"] = 0.5,
						["b"] = 0,
					},
					["color9"] = {
						["a"] = 1,
						["r"] = 0.788,
						["g"] = 0.259,
						["b"] = 0.992,
					},
					["color4"] = {
						["a"] = 1,
						["r"] = 1,
						["g"] = 1,
						["b"] = 0,
					},
					["color8"] = {
						["a"] = 1,
						["r"] = 0.3,
						["g"] = 0.52,
						["b"] = 0.9,
					},
					["color5"] = {
						["a"] = 1,
						["r"] = 0,
						["g"] = 0.8,
						["b"] = 0.8,
					},
					["color7"] = {
						["a"] = 1,
						["r"] = 0,
						["g"] = 0.5,
						["b"] = 1,
					},
				},
				["my-heals-incoming"] = {
					["flags"] = 0,
					["type"] = "my-heals-incoming",
					["multiplier"] = 1,
					["color1"] = {
						["a"] = 1,
						["r"] = 0,
						["g"] = 1,
						["b"] = 0,
					},
				},
				["banzai"] = {
					["type"] = "banzai",
					["color1"] = {
						["a"] = 1,
						["r"] = 1,
						["g"] = 0,
						["b"] = 1,
					},
				},
				["phased"] = {
					["type"] = "phased",
					["color1"] = {
						["a"] = 1,
						["r"] = 1,
						["g"] = 0,
						["b"] = 0,
					},
				},
				["health-low"] = {
					["threshold"] = 0.4,
					["type"] = "health-low",
					["color1"] = {
						["a"] = 1,
						["r"] = 1,
						["g"] = 0,
						["b"] = 0,
					},
				},
				["debuff-Magic"] = {
					["type"] = "debuffType",
					["subType"] = "Magic",
					["color1"] = {
						["a"] = 1,
						["r"] = 0.2,
						["g"] = 0.6,
						["b"] = 1,
					},
				},
				["range"] = {
					["type"] = "range",
					["default"] = 0.25,
					["elapsed"] = 0.5,
					["range"] = 38,
					["color1"] = {
						["a"] = 1,
						["r"] = 1,
						["g"] = 0,
						["b"] = 0,
					},
				},
				["debuff-Curse"] = {
					["type"] = "debuffType",
					["subType"] = "Curse",
					["color1"] = {
						["a"] = 1,
						["r"] = 0.6,
						["g"] = 0,
						["b"] = 1,
					},
				},
				["offline"] = {
					["type"] = "offline",
					["color1"] = {
						["a"] = 1,
						["r"] = 1,
						["g"] = 1,
						["b"] = 1,
					},
				},
				["direction"] = {
					["type"] = "direction",
					["color1"] = {
						["a"] = 1,
						["r"] = 0,
						["g"] = 1,
						["b"] = 0,
					},
				},
				["lowmana"] = {
					["threshold"] = 0.75,
					["type"] = "lowmana",
					["color1"] = {
						["a"] = 1,
						["r"] = 0.5,
						["g"] = 0,
						["b"] = 1,
					},
				},
			},
			["formatting"] = {
				["secondsElapsedFormat"] = "%ds",
				["longDurationStackFormat"] = "%.1f:%d",
				["longDecimalFormat"] = "%.1f",
				["shortDurationStackFormat"] = "%.0f:%d",
				["invertDurationStack"] = false,
				["minutesElapsedFormat"] = "%dm",
				["shortDecimalFormat"] = "%.0f",
			},
			["versions"] = {
				["Grid2"] = 9,
				["Grid2RaidDebuffs"] = 4,
			},
			["statusMap"] = {
				["corner-top-left"] = {
					["buff-Renew-mine"] = 99,
				},
				["heals"] = {
					["heals-incoming"] = 99,
				},
				["icon-right"] = {
					["buff-PrayerOfMending-mine"] = 99,
				},
				["text-down"] = {
					["name"] = 99,
				},
				["heals-color"] = {
					["classcolor"] = 99,
				},
				["icon-left"] = {
					["raid-icon-player"] = 155,
				},
				["alpha"] = {
					["offline"] = 97,
					["range"] = 99,
					["death"] = 98,
				},
				["corner-top-right"] = {
					["buff-PowerWordShield"] = 99,
					["debuff-WeakenedSoul"] = 89,
				},
				["text-down-color"] = {
					["classcolor"] = 99,
				},
				["border"] = {
					["debuff-Disease"] = 90,
					["health-low"] = 55,
					["debuff-Poison"] = 70,
					["target"] = 50,
					["debuff-Magic"] = 80,
					["debuff-Curse"] = 60,
				},
				["health"] = {
					["health-current"] = 99,
				},
				["text-up"] = {
					["charmed"] = 65,
					["feign-death"] = 96,
					["health-deficit"] = 50,
					["offline"] = 93,
					["vehicle"] = 70,
					["death"] = 95,
				},
				["corner-bottom-left"] = {
					["threat"] = 99,
				},
				["text-up-color"] = {
					["charmed"] = 65,
					["feign-death"] = 96,
					["health-deficit"] = 50,
					["offline"] = 93,
					["vehicle"] = 70,
					["death"] = 95,
				},
				["health-color"] = {
					["classcolor"] = 99,
				},
				["icon-center"] = {
					["ready-check"] = 150,
					["raid-debuffs"] = 155,
					["death"] = 155,
				},
			},
			["themes"] = {
				["enabled"] = {
				},
				["indicators"] = {
				},
				["names"] = {
				},
			},
		},
	},
}
