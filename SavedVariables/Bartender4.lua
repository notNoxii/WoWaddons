
Bartender4DB = {
	["namespaces"] = {
		["StatusTrackingBar"] = {
			["profiles"] = {
				["Noxiipally - Area 52"] = {
					["enabled"] = false,
					["alpha"] = 1,
					["fadeoutdelay"] = 0.2,
					["fadeout"] = false,
					["position"] = {
						["y"] = 62,
						["x"] = -515,
						["point"] = "BOTTOM",
						["scale"] = 1.264999985694885,
						["growVertical"] = "DOWN",
						["growHorizontal"] = "RIGHT",
					},
					["version"] = 3,
					["clickthrough"] = false,
					["visibility"] = {
						["overridebar"] = true,
						["vehicleui"] = true,
						["stance"] = {
						},
					},
					["fadeoutalpha"] = 0.1,
				},
				["Noxiishields - Area 52"] = {
					["enabled"] = true,
					["alpha"] = 1,
					["version"] = 3,
					["fadeoutdelay"] = 0.2,
					["position"] = {
						["growHorizontal"] = "RIGHT",
						["x"] = -515,
						["point"] = "BOTTOM",
						["scale"] = 1.264999985694885,
						["y"] = 62,
						["growVertical"] = "DOWN",
					},
					["fadeout"] = false,
					["clickthrough"] = false,
					["visibility"] = {
						["overridebar"] = true,
						["stance"] = {
						},
						["vehicleui"] = true,
					},
					["fadeoutalpha"] = 0.1,
				},
			},
		},
		["ActionBars"] = {
			["profiles"] = {
				["Noxiipally - Area 52"] = {
					["actionbars"] = {
						{
							["flyoutDirection"] = "UP",
							["showgrid"] = false,
							["rows"] = 4,
							["hideequipped"] = false,
							["fadeoutdelay"] = 0,
							["skin"] = {
								["Zoom"] = false,
							},
							["clickthrough"] = false,
							["autoassist"] = false,
							["enabled"] = true,
							["buttons"] = 12,
							["buttonOffset"] = 0,
							["alpha"] = 1,
							["fadeoutalpha"] = 0,
							["hidemacrotext"] = false,
							["fadeout"] = true,
							["hidehotkey"] = false,
							["position"] = {
								["y"] = 123,
								["x"] = -489,
								["point"] = "BOTTOMRIGHT",
								["scale"] = 0.800000011920929,
								["growVertical"] = "UP",
								["growHorizontal"] = "RIGHT",
							},
							["version"] = 3,
							["padding"] = 2,
							["visibility"] = {
								["overridebar"] = false,
								["vehicleui"] = false,
								["stance"] = {
								},
							},
							["states"] = {
								["enabled"] = true,
								["ctrl"] = 0,
								["default"] = 0,
								["actionbar"] = false,
								["possess"] = true,
								["shift"] = 0,
								["alt"] = 0,
								["stance"] = {
									["DRUID"] = {
										["prowl"] = 8,
										["cat"] = 7,
										["bear"] = 9,
									},
									["ROGUE"] = {
										["stealth"] = 7,
									},
								},
							},
						}, -- [1]
						{
							["flyoutDirection"] = "UP",
							["showgrid"] = false,
							["rows"] = 4,
							["hideequipped"] = false,
							["fadeoutdelay"] = 0,
							["skin"] = {
								["Zoom"] = false,
							},
							["clickthrough"] = false,
							["autoassist"] = false,
							["enabled"] = true,
							["buttons"] = 12,
							["fadeout"] = true,
							["alpha"] = 1,
							["buttonOffset"] = 0,
							["hidemacrotext"] = false,
							["version"] = 3,
							["hidehotkey"] = false,
							["position"] = {
								["y"] = 123,
								["x"] = -397,
								["point"] = "BOTTOMRIGHT",
								["scale"] = 0.800000011920929,
								["growVertical"] = "UP",
								["growHorizontal"] = "RIGHT",
							},
							["fadeoutalpha"] = 0,
							["padding"] = 2,
							["visibility"] = {
								["overridebar"] = false,
								["vehicleui"] = false,
								["stance"] = {
								},
							},
							["states"] = {
								["enabled"] = false,
								["ctrl"] = 0,
								["default"] = 0,
								["actionbar"] = false,
								["possess"] = false,
								["alt"] = 0,
								["shift"] = 0,
								["stance"] = {
								},
							},
						}, -- [2]
						{
							["flyoutDirection"] = "UP",
							["showgrid"] = false,
							["rows"] = 12,
							["hideequipped"] = false,
							["fadeout"] = false,
							["skin"] = {
								["Zoom"] = false,
							},
							["clickthrough"] = false,
							["autoassist"] = false,
							["enabled"] = false,
							["buttons"] = 12,
							["fadeoutdelay"] = 0.2,
							["alpha"] = 1,
							["buttonOffset"] = 0,
							["padding"] = 5,
							["fadeoutalpha"] = 0.1,
							["hidehotkey"] = false,
							["position"] = {
								["y"] = 3.903717041015625,
								["x"] = -361.8973388671875,
								["point"] = "RIGHT",
								["scale"] = 1,
								["growVertical"] = "DOWN",
								["growHorizontal"] = "RIGHT",
							},
							["version"] = 3,
							["hidemacrotext"] = false,
							["visibility"] = {
								["overridebar"] = true,
								["vehicleui"] = true,
								["stance"] = {
								},
							},
							["states"] = {
								["enabled"] = false,
								["ctrl"] = 0,
								["default"] = 0,
								["actionbar"] = false,
								["possess"] = false,
								["alt"] = 0,
								["shift"] = 0,
								["stance"] = {
								},
							},
						}, -- [3]
						{
							["flyoutDirection"] = "UP",
							["showgrid"] = false,
							["rows"] = 2,
							["hideequipped"] = false,
							["fadeoutdelay"] = 0,
							["skin"] = {
								["Zoom"] = false,
							},
							["clickthrough"] = false,
							["autoassist"] = false,
							["enabled"] = true,
							["buttons"] = 12,
							["fadeout"] = true,
							["alpha"] = 1,
							["buttonOffset"] = 0,
							["hidemacrotext"] = false,
							["version"] = 3,
							["hidehotkey"] = false,
							["position"] = {
								["y"] = 60,
								["x"] = -488.4966057648708,
								["point"] = "BOTTOMRIGHT",
								["scale"] = 0.800000011920929,
								["growVertical"] = "UP",
								["growHorizontal"] = "RIGHT",
							},
							["fadeoutalpha"] = 0,
							["padding"] = 2,
							["visibility"] = {
								["overridebar"] = false,
								["vehicleui"] = false,
								["stance"] = {
								},
							},
							["states"] = {
								["enabled"] = false,
								["ctrl"] = 0,
								["default"] = 0,
								["actionbar"] = false,
								["possess"] = false,
								["alt"] = 0,
								["shift"] = 0,
								["stance"] = {
								},
							},
						}, -- [4]
						{
							["flyoutDirection"] = "UP",
							["showgrid"] = false,
							["rows"] = 1,
							["hideequipped"] = false,
							["fadeout"] = false,
							["skin"] = {
								["Zoom"] = false,
							},
							["clickthrough"] = false,
							["autoassist"] = false,
							["enabled"] = false,
							["buttons"] = 12,
							["fadeoutdelay"] = 0.2,
							["alpha"] = 1,
							["buttonOffset"] = 0,
							["padding"] = 6,
							["fadeoutalpha"] = 0.1,
							["hidehotkey"] = false,
							["position"] = {
								["y"] = 110,
								["x"] = 3,
								["point"] = "BOTTOM",
								["scale"] = 1,
								["growVertical"] = "DOWN",
								["growHorizontal"] = "RIGHT",
							},
							["version"] = 3,
							["hidemacrotext"] = false,
							["visibility"] = {
								["overridebar"] = true,
								["vehicleui"] = true,
								["stance"] = {
								},
							},
							["states"] = {
								["enabled"] = false,
								["ctrl"] = 0,
								["default"] = 0,
								["actionbar"] = false,
								["possess"] = false,
								["alt"] = 0,
								["shift"] = 0,
								["stance"] = {
								},
							},
						}, -- [5]
						{
							["flyoutDirection"] = "UP",
							["showgrid"] = false,
							["rows"] = 2,
							["hideequipped"] = false,
							["fadeoutdelay"] = 0,
							["skin"] = {
								["Zoom"] = false,
							},
							["clickthrough"] = false,
							["autoassist"] = false,
							["enabled"] = true,
							["buttons"] = 12,
							["fadeout"] = true,
							["alpha"] = 1,
							["buttonOffset"] = 0,
							["hidemacrotext"] = false,
							["version"] = 3,
							["hidehotkey"] = false,
							["position"] = {
								["y"] = 0,
								["x"] = -488.6737542050105,
								["point"] = "BOTTOMRIGHT",
								["scale"] = 0.800000011920929,
								["growVertical"] = "UP",
								["growHorizontal"] = "RIGHT",
							},
							["fadeoutalpha"] = 0,
							["padding"] = 2,
							["visibility"] = {
								["overridebar"] = false,
								["vehicleui"] = false,
								["stance"] = {
								},
							},
							["states"] = {
								["enabled"] = false,
								["ctrl"] = 0,
								["default"] = 0,
								["actionbar"] = false,
								["possess"] = false,
								["alt"] = 0,
								["shift"] = 0,
								["stance"] = {
								},
							},
						}, -- [6]
						{
							["flyoutDirection"] = "UP",
							["showgrid"] = false,
							["rows"] = 1,
							["hideequipped"] = false,
							["fadeout"] = false,
							["skin"] = {
								["Zoom"] = false,
							},
							["clickthrough"] = false,
							["autoassist"] = false,
							["enabled"] = false,
							["buttons"] = 12,
							["fadeoutdelay"] = 0.2,
							["alpha"] = 1,
							["buttonOffset"] = 0,
							["hidemacrotext"] = false,
							["hidehotkey"] = false,
							["fadeoutalpha"] = 0.1,
							["position"] = {
								["y"] = -227.5000152587891,
								["x"] = -231.5000610351563,
								["point"] = "CENTER",
								["scale"] = 1,
								["growVertical"] = "DOWN",
								["growHorizontal"] = "RIGHT",
							},
							["version"] = 3,
							["padding"] = 2,
							["visibility"] = {
								["overridebar"] = true,
								["vehicleui"] = true,
								["stance"] = {
								},
							},
							["states"] = {
								["enabled"] = false,
								["ctrl"] = 0,
								["default"] = 0,
								["actionbar"] = false,
								["possess"] = false,
								["alt"] = 0,
								["shift"] = 0,
								["stance"] = {
								},
							},
						}, -- [7]
						{
							["flyoutDirection"] = "UP",
							["showgrid"] = false,
							["rows"] = 1,
							["hideequipped"] = false,
							["fadeout"] = false,
							["skin"] = {
								["Zoom"] = false,
							},
							["clickthrough"] = false,
							["autoassist"] = false,
							["enabled"] = false,
							["buttons"] = 12,
							["fadeoutdelay"] = 0.2,
							["alpha"] = 1,
							["buttonOffset"] = 0,
							["hidemacrotext"] = false,
							["hidehotkey"] = false,
							["fadeoutalpha"] = 0.1,
							["position"] = {
								["y"] = -227.5000152587891,
								["x"] = -231.5000610351563,
								["point"] = "CENTER",
								["scale"] = 1,
								["growVertical"] = "DOWN",
								["growHorizontal"] = "RIGHT",
							},
							["version"] = 3,
							["padding"] = 2,
							["visibility"] = {
								["overridebar"] = true,
								["vehicleui"] = true,
								["stance"] = {
								},
							},
							["states"] = {
								["enabled"] = false,
								["ctrl"] = 0,
								["default"] = 0,
								["actionbar"] = false,
								["possess"] = false,
								["alt"] = 0,
								["shift"] = 0,
								["stance"] = {
								},
							},
						}, -- [8]
						{
							["flyoutDirection"] = "UP",
							["showgrid"] = false,
							["rows"] = 1,
							["hideequipped"] = false,
							["fadeout"] = false,
							["skin"] = {
								["Zoom"] = false,
							},
							["clickthrough"] = false,
							["autoassist"] = false,
							["enabled"] = false,
							["buttons"] = 12,
							["fadeoutdelay"] = 0.2,
							["alpha"] = 1,
							["buttonOffset"] = 0,
							["hidemacrotext"] = false,
							["hidehotkey"] = false,
							["fadeoutalpha"] = 0.1,
							["position"] = {
								["y"] = -227.5000152587891,
								["x"] = -231.5000610351563,
								["point"] = "CENTER",
								["scale"] = 1,
								["growVertical"] = "DOWN",
								["growHorizontal"] = "RIGHT",
							},
							["version"] = 3,
							["padding"] = 2,
							["visibility"] = {
								["overridebar"] = true,
								["vehicleui"] = true,
								["stance"] = {
								},
							},
							["states"] = {
								["enabled"] = false,
								["ctrl"] = 0,
								["default"] = 0,
								["actionbar"] = false,
								["possess"] = false,
								["alt"] = 0,
								["shift"] = 0,
								["stance"] = {
								},
							},
						}, -- [9]
						{
							["flyoutDirection"] = "UP",
							["showgrid"] = false,
							["rows"] = 1,
							["hideequipped"] = false,
							["fadeoutdelay"] = 0.2,
							["skin"] = {
								["Zoom"] = false,
							},
							["clickthrough"] = false,
							["autoassist"] = false,
							["enabled"] = false,
							["buttons"] = 12,
							["alpha"] = 1,
							["fadeout"] = false,
							["fadeoutalpha"] = 0.1,
							["hidemacrotext"] = false,
							["hidehotkey"] = false,
							["position"] = {
								["scale"] = 1,
								["growVertical"] = "DOWN",
								["growHorizontal"] = "RIGHT",
							},
							["buttonOffset"] = 0,
							["padding"] = 2,
							["visibility"] = {
								["overridebar"] = true,
								["vehicleui"] = true,
								["stance"] = {
								},
							},
							["states"] = {
								["enabled"] = false,
								["ctrl"] = 0,
								["default"] = 0,
								["actionbar"] = false,
								["possess"] = false,
								["alt"] = 0,
								["shift"] = 0,
								["stance"] = {
								},
							},
						}, -- [10]
					},
				},
				["Noxiishields - Area 52"] = {
					["actionbars"] = {
						{
							["version"] = 3,
							["padding"] = 6,
							["position"] = {
								["x"] = -510,
								["point"] = "BOTTOM",
								["y"] = 41.75,
							},
						}, -- [1]
						{
							["enabled"] = false,
							["version"] = 3,
							["position"] = {
								["x"] = -231.5000305175781,
								["point"] = "CENTER",
								["y"] = -227.4999542236328,
							},
						}, -- [2]
						{
							["rows"] = 12,
							["version"] = 3,
							["padding"] = 5,
							["position"] = {
								["x"] = -82,
								["point"] = "BOTTOMRIGHT",
								["y"] = 610,
							},
						}, -- [3]
						{
							["rows"] = 12,
							["version"] = 3,
							["padding"] = 5,
							["position"] = {
								["x"] = -42,
								["point"] = "BOTTOMRIGHT",
								["y"] = 610,
							},
						}, -- [4]
						{
							["version"] = 3,
							["padding"] = 6,
							["position"] = {
								["x"] = 3,
								["point"] = "BOTTOM",
								["y"] = 110,
							},
						}, -- [5]
						{
							["version"] = 3,
							["padding"] = 6,
							["position"] = {
								["x"] = -510,
								["point"] = "BOTTOM",
								["y"] = 110,
							},
						}, -- [6]
						{
						}, -- [7]
						{
						}, -- [8]
						[10] = {
						},
					},
				},
			},
		},
		["LibDualSpec-1.0"] = {
			["char"] = {
				["Noxiipally - Area 52"] = {
				},
				["Noxiishields - Area 52"] = {
				},
			},
			["profiles"] = {
				["Noxiishields - Area 52"] = {
				},
			},
		},
		["ExtraActionBar"] = {
			["profiles"] = {
				["Noxiipally - Area 52"] = {
					["enabled"] = true,
					["alpha"] = 1,
					["hideArtwork"] = false,
					["version"] = 3,
					["fadeoutdelay"] = 0.2,
					["position"] = {
						["y"] = -73.31621529633048,
						["x"] = -519.1317624713702,
						["point"] = "RIGHT",
						["scale"] = 0.4000000059604645,
						["growVertical"] = "DOWN",
						["growHorizontal"] = "RIGHT",
					},
					["fadeout"] = false,
					["clickthrough"] = false,
					["visibility"] = {
						["overridebar"] = false,
						["stance"] = {
						},
						["vehicleui"] = false,
					},
					["fadeoutalpha"] = 0.1,
				},
				["Noxiishields - Area 52"] = {
					["version"] = 3,
					["position"] = {
						["x"] = -63.5,
						["point"] = "CENTER",
						["y"] = -261.0714416503906,
					},
				},
			},
		},
		["MicroMenu"] = {
			["profiles"] = {
				["Noxiipally - Area 52"] = {
					["enabled"] = true,
					["fadeoutdelay"] = 0.1,
					["hidemacrotext"] = false,
					["padding"] = -2,
					["fadeout"] = true,
					["rows"] = 1,
					["alpha"] = 1,
					["position"] = {
						["y"] = -179.4856211749902,
						["x"] = -204.400277658191,
						["point"] = "RIGHT",
						["scale"] = 0.699999988079071,
						["growVertical"] = "DOWN",
						["growHorizontal"] = "RIGHT",
					},
					["fadeoutalpha"] = 0,
					["vertical"] = false,
					["hidehotkey"] = false,
					["skin"] = {
						["Zoom"] = false,
					},
					["hideequipped"] = false,
					["clickthrough"] = false,
					["visibility"] = {
						["stance"] = {
						},
						["possess"] = false,
						["overridebar"] = false,
						["always"] = false,
						["vehicleui"] = true,
					},
					["version"] = 3,
				},
				["Noxiishields - Area 52"] = {
					["enabled"] = true,
					["version"] = 3,
					["padding"] = -2,
					["vertical"] = false,
					["clickthrough"] = false,
					["rows"] = 1,
					["alpha"] = 1,
					["hidehotkey"] = false,
					["position"] = {
						["growHorizontal"] = "RIGHT",
						["x"] = 37.5,
						["point"] = "BOTTOM",
						["scale"] = 1,
						["y"] = 41.75,
						["growVertical"] = "DOWN",
					},
					["hideequipped"] = false,
					["fadeoutdelay"] = 0.2,
					["skin"] = {
						["Zoom"] = false,
					},
					["fadeoutalpha"] = 0.1,
					["hidemacrotext"] = false,
					["visibility"] = {
						["possess"] = false,
						["overridebar"] = true,
						["vehicleui"] = true,
						["stance"] = {
						},
					},
					["fadeout"] = false,
				},
			},
		},
		["BagBar"] = {
			["profiles"] = {
				["Noxiipally - Area 52"] = {
					["rows"] = 1,
					["hideequipped"] = false,
					["fadeout"] = false,
					["skin"] = {
						["Zoom"] = false,
					},
					["clickthrough"] = false,
					["keyring"] = true,
					["enabled"] = false,
					["onebag"] = false,
					["alpha"] = 1,
					["hidemacrotext"] = false,
					["version"] = 3,
					["fadeoutalpha"] = 0.1,
					["position"] = {
						["y"] = 38.5,
						["x"] = 345,
						["point"] = "BOTTOM",
						["scale"] = 1,
						["growVertical"] = "DOWN",
						["growHorizontal"] = "RIGHT",
					},
					["hidehotkey"] = false,
					["padding"] = 2,
					["visibility"] = {
						["possess"] = false,
						["overridebar"] = true,
						["stance"] = {
						},
						["vehicleui"] = true,
					},
					["fadeoutdelay"] = 0.2,
				},
				["Noxiishields - Area 52"] = {
					["rows"] = 1,
					["hideequipped"] = false,
					["fadeout"] = false,
					["skin"] = {
						["Zoom"] = false,
					},
					["clickthrough"] = false,
					["keyring"] = true,
					["enabled"] = true,
					["onebag"] = false,
					["alpha"] = 1,
					["version"] = 3,
					["hidemacrotext"] = false,
					["hidehotkey"] = false,
					["position"] = {
						["growHorizontal"] = "RIGHT",
						["x"] = 345,
						["point"] = "BOTTOM",
						["scale"] = 1,
						["y"] = 38.5,
						["growVertical"] = "DOWN",
					},
					["fadeoutdelay"] = 0.2,
					["padding"] = 2,
					["visibility"] = {
						["possess"] = false,
						["overridebar"] = true,
						["vehicleui"] = true,
						["stance"] = {
						},
					},
					["fadeoutalpha"] = 0.1,
				},
			},
		},
		["BlizzardArt"] = {
			["profiles"] = {
				["Noxiipally - Area 52"] = {
					["enabled"] = false,
					["rightCap"] = "DWARF",
					["visibility"] = {
						["possess"] = false,
						["overridebar"] = true,
						["vehicleui"] = true,
						["stance"] = {
						},
					},
					["alpha"] = 1,
					["artLayout"] = "CLASSIC",
					["fadeoutalpha"] = 0.1,
					["leftCap"] = "DWARF",
					["fadeout"] = false,
					["position"] = {
						["y"] = 47,
						["x"] = -512,
						["point"] = "BOTTOM",
						["scale"] = 1,
						["growVertical"] = "DOWN",
						["growHorizontal"] = "RIGHT",
					},
					["version"] = 3,
					["clickthrough"] = false,
					["artSkin"] = "DWARF",
					["fadeoutdelay"] = 0.2,
				},
				["Noxiishields - Area 52"] = {
					["enabled"] = true,
					["version"] = 3,
					["position"] = {
						["x"] = -512,
						["point"] = "BOTTOM",
						["y"] = 47,
					},
				},
			},
		},
		["StanceBar"] = {
			["profiles"] = {
				["Noxiipally - Area 52"] = {
					["enabled"] = false,
					["version"] = 3,
					["fadeoutalpha"] = 0.1,
					["padding"] = 2,
					["rows"] = 1,
					["alpha"] = 1,
					["fadeout"] = false,
					["position"] = {
						["y"] = 143,
						["x"] = -460,
						["point"] = "BOTTOM",
						["scale"] = 1,
						["growVertical"] = "DOWN",
						["growHorizontal"] = "RIGHT",
					},
					["hideequipped"] = false,
					["hidehotkey"] = true,
					["skin"] = {
						["Zoom"] = false,
					},
					["fadeoutdelay"] = 0.2,
					["hidemacrotext"] = false,
					["visibility"] = {
						["overridebar"] = true,
						["stance"] = {
						},
						["vehicleui"] = true,
					},
					["clickthrough"] = false,
				},
				["Noxiishields - Area 52"] = {
					["version"] = 3,
					["position"] = {
						["x"] = -82.50001525878906,
						["point"] = "CENTER",
						["y"] = -14.99996948242188,
					},
				},
			},
		},
		["PetBar"] = {
			["profiles"] = {
				["Noxiipally - Area 52"] = {
					["enabled"] = true,
					["hidemacrotext"] = false,
					["fadeoutdelay"] = 0.2,
					["showgrid"] = false,
					["padding"] = 2,
					["rows"] = 1,
					["alpha"] = 1,
					["skin"] = {
						["Zoom"] = false,
					},
					["version"] = 3,
					["hideequipped"] = false,
					["hidehotkey"] = true,
					["position"] = {
						["y"] = -43.47674560546875,
						["x"] = 390.540771484375,
						["point"] = "CENTER",
						["scale"] = 1,
						["growVertical"] = "DOWN",
						["growHorizontal"] = "RIGHT",
					},
					["fadeoutalpha"] = 0.1,
					["clickthrough"] = false,
					["visibility"] = {
						["nopet"] = true,
						["stance"] = {
						},
						["vehicle"] = true,
						["overridebar"] = true,
						["vehicleui"] = true,
					},
					["fadeout"] = false,
				},
				["Noxiishields - Area 52"] = {
					["version"] = 3,
					["position"] = {
						["x"] = -460,
						["point"] = "BOTTOM",
						["y"] = 143,
					},
				},
			},
		},
		["Vehicle"] = {
			["profiles"] = {
				["Noxiipally - Area 52"] = {
					["enabled"] = true,
					["alpha"] = 1,
					["fadeoutdelay"] = 0.2,
					["fadeout"] = false,
					["position"] = {
						["y"] = -92.81631469726562,
						["x"] = -475.331787109375,
						["point"] = "RIGHT",
						["scale"] = 1,
						["growVertical"] = "DOWN",
						["growHorizontal"] = "RIGHT",
					},
					["version"] = 3,
					["clickthrough"] = false,
					["visibility"] = {
						["overridebar"] = false,
						["stance"] = {
						},
						["vehicleui"] = false,
					},
					["fadeoutalpha"] = 0.1,
				},
				["Noxiishields - Area 52"] = {
					["enabled"] = true,
					["alpha"] = 1,
					["version"] = 3,
					["fadeoutdelay"] = 0.2,
					["position"] = {
						["growHorizontal"] = "RIGHT",
						["x"] = 104.4999389648438,
						["point"] = "CENTER",
						["scale"] = 1,
						["y"] = 42.50003051757813,
						["growVertical"] = "DOWN",
					},
					["fadeout"] = false,
					["clickthrough"] = false,
					["visibility"] = {
						["overridebar"] = false,
						["vehicleui"] = false,
						["stance"] = {
						},
					},
					["fadeoutalpha"] = 0.1,
				},
			},
		},
	},
	["profileKeys"] = {
		["Noxiipally - Area 52"] = "Noxiipally - Area 52",
		["Noxiishields - Area 52"] = "Noxiishields - Area 52",
	},
	["profiles"] = {
		["Noxiipally - Area 52"] = {
			["selfcastmodifier"] = true,
			["selfcastrightclick"] = false,
			["blizzardVehicle"] = true,
			["focuscastmodifier"] = false,
			["snapping"] = true,
			["colors"] = {
				["range"] = {
					["b"] = 0.1,
					["g"] = 0.1,
					["r"] = 0.8,
				},
				["mana"] = {
					["b"] = 1,
					["g"] = 0.5,
					["r"] = 0.5,
				},
			},
			["buttonlock"] = false,
			["tooltip"] = "enabled",
			["mouseovermod"] = "NONE",
			["minimapIcon"] = {
				["minimapPos"] = 301.7675781856182,
				["hide"] = true,
			},
			["outofrange"] = "hotkey",
		},
		["Noxiishields - Area 52"] = {
			["mouseovermod"] = "NONE",
			["selfcastrightclick"] = false,
			["tooltip"] = "enabled",
			["focuscastmodifier"] = false,
			["minimapIcon"] = {
			},
			["colors"] = {
				["range"] = {
					["r"] = 0.8,
					["g"] = 0.1,
					["b"] = 0.1,
				},
				["mana"] = {
					["r"] = 0.5,
					["g"] = 0.5,
					["b"] = 1,
				},
			},
			["buttonlock"] = false,
			["snapping"] = true,
			["blizzardVehicle"] = true,
			["selfcastmodifier"] = true,
			["outofrange"] = "hotkey",
		},
	},
}
