
Omen3DB = {
	["profileKeys"] = {
		["Noxiishields - Area 52"] = "Noxiishields - Area 52",
	},
	["profiles"] = {
		["Noxiishields - Area 52"] = {
			["PositionY"] = 623.5714111328125,
			["Background"] = {
				["EdgeSize"] = 1,
				["BarInset"] = 2,
			},
			["PositionX"] = 880.67041015625,
			["Shown"] = false,
			["Bar"] = {
				["Spacing"] = 1,
			},
		},
	},
}
