
RareShareDB = {
	["Modules"] = {
	},
	["Config"] = {
		["MapPin"] = true,
		["CChannel"] = {
			["CName"] = "General - Zereth Mortis",
			["CID"] = 1,
		},
		["TomTom"] = {
			["Rares"] = true,
			["Master"] = true,
			["Duplicates"] = true,
		},
		["Sound"] = {
			["Rares"] = true,
			["Master"] = true,
			["Duplicates"] = true,
		},
		["OnDeath"] = false,
		["ChatAnnounce"] = true,
		["Duplicates"] = true,
	},
	["LastAnnounce"] = {
		["Time"] = 1648667761,
		["ID"] = 182318,
	},
}
