
EnhancedRaidFramesDB = {
	["namespaces"] = {
		["LibDualSpec-1.0"] = {
			["char"] = {
				["Noxiishields - Area 52"] = {
				},
			},
		},
	},
	["global"] = {
		["DB_VERSION"] = 2,
	},
	["profileKeys"] = {
		["Noxiipally - Area 52"] = "Noxiipally - Area 52",
		["Noxiishields - Area 52"] = "Noxiishields - Area 52",
	},
	["profiles"] = {
		["Noxiipally - Area 52"] = {
			nil, -- [1]
			nil, -- [2]
			nil, -- [3]
			nil, -- [4]
			{
				["indicatorSize"] = 30,
				["auras"] = "Beacon of Virtue",
				["showTooltip"] = false,
				["mineOnly"] = true,
			}, -- [5]
			nil, -- [6]
			nil, -- [7]
			nil, -- [8]
			{
				["indicatorSize"] = 22,
				["showTooltip"] = false,
				["mineOnly"] = true,
				["tooltipLocation"] = "ANCHOR_PRESERVE",
				["auras"] = "Bestow Faith\nBeacon of Light\nBeacon of Faith",
			}, -- [9]
			["showBuffs"] = false,
			["rangeAlpha"] = 0.35,
			["frameScale"] = 1.3,
			["indicatorFont"] = "Ubuntu Medium",
		},
		["Noxiishields - Area 52"] = {
		},
	},
}
