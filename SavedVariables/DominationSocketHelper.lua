
DSHDB = {
	["char"] = {
		["Noxiipally - Area 52"] = {
			["sets"] = {
				["heal"] = {
					["key"] = 87,
					["icon"] = 3,
				},
				["dps"] = {
					["key"] = 309,
					["icon"] = 0,
				},
			},
			["quickslots"] = {
				["extended"] = false,
			},
		},
		["Noxiishields - Area 52"] = {
			["sets"] = {
			},
			["quickslots"] = {
				["extended"] = true,
			},
		},
	},
	["profileKeys"] = {
		["Noxiipally - Area 52"] = "Noxiipally - Area 52",
		["Noxiishields - Area 52"] = "Noxiishields - Area 52",
		["Koragoraia - Illidan"] = "Koragoraia - Illidan",
		["Welorialin - Area 52"] = "Welorialin - Area 52",
	},
	["profiles"] = {
		["Noxiipally - Area 52"] = {
		},
		["Noxiishields - Area 52"] = {
		},
	},
}
