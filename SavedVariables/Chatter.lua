
ChatterDB = {
	["namespaces"] = {
		["PlayerNames"] = {
			["realm"] = {
				["Area 52"] = {
					["levels"] = {
					},
					["names"] = {
					},
				},
			},
			["global"] = {
			},
			["profiles"] = {
				["Default"] = {
					["blizzardNameColoring"] = true,
					["useTabComplete"] = true,
					["emphasizeSelfInText"] = true,
					["noRealNames"] = false,
					["leftBracket"] = "[",
					["separator"] = ":",
					["bnetBrackets"] = true,
					["saveData"] = false,
					["rightBracket"] = "]",
					["colorSelfInText"] = true,
				},
			},
		},
		["AltLinks"] = {
			["profiles"] = {
				["Noxiipally - Area 52"] = {
					["guildranks"] = {
						false, -- [1]
						false, -- [2]
						false, -- [3]
						false, -- [4]
						false, -- [5]
						false, -- [6]
						false, -- [7]
						false, -- [8]
						false, -- [9]
						[0] = false,
					},
				},
				["Area 52"] = {
					["guildranks"] = {
						false, -- [1]
						false, -- [2]
						false, -- [3]
						false, -- [4]
						false, -- [5]
						false, -- [6]
						false, -- [7]
						false, -- [8]
						false, -- [9]
						[0] = false,
					},
				},
				["Default"] = {
					["guildNotes"] = true,
					["guildsuffix"] = "",
					["guildranks"] = {
						false, -- [1]
						false, -- [2]
						false, -- [3]
						false, -- [4]
						false, -- [5]
						false, -- [6]
						false, -- [7]
						false, -- [8]
						false, -- [9]
						[0] = false,
					},
					["guildprefix"] = "",
					["color"] = {
						0.6, -- [1]
						0.6, -- [2]
						0.6, -- [3]
					},
					["colorMode"] = "COLOR_MOD",
					["rightBracket"] = "]",
					["leftBracket"] = "[",
				},
				["PALADIN"] = {
					["guildranks"] = {
						false, -- [1]
						false, -- [2]
						false, -- [3]
						false, -- [4]
						false, -- [5]
						false, -- [6]
						false, -- [7]
						false, -- [8]
						false, -- [9]
						[0] = false,
					},
				},
			},
			["realm"] = {
				["Area 52"] = {
				},
			},
		},
		["Scrollback"] = {
			["profiles"] = {
				["Default"] = {
				},
			},
		},
		["Invite Links"] = {
			["profiles"] = {
				["Noxiipally - Area 52"] = {
					["words"] = {
						["inv"] = "inv",
						["invite"] = "invite",
					},
				},
				["Area 52"] = {
					["words"] = {
						["inv"] = "inv",
						["invite"] = "invite",
					},
				},
				["Default"] = {
					["words"] = {
						["inv"] = "inv",
						["invite"] = "invite",
					},
					["altClickToInvite"] = true,
				},
				["PALADIN"] = {
					["words"] = {
						["inv"] = "inv",
						["invite"] = "invite",
					},
				},
			},
		},
		["ChannelColors"] = {
			["profiles"] = {
				["Noxiipally - Area 52"] = {
					["colors"] = {
						["Raid Warning"] = {
							["b"] = 0.3333333432674408,
							["g"] = 0.3333333432674408,
							["r"] = 1,
						},
						["Instance Leader"] = {
							["b"] = 0.2039215862751007,
							["g"] = 0.1333333402872086,
							["r"] = 1,
						},
						["General"] = {
							["b"] = 0.7529412508010864,
							["g"] = 0.7529412508010864,
							["r"] = 1,
						},
						["Real ID Whisper"] = {
							["b"] = 0.9921569228172302,
							["g"] = 0.9137255549430847,
							["r"] = 0.545098066329956,
						},
						["Instance"] = {
							["b"] = 0.3333333432674408,
							["g"] = 0.3333333432674408,
							["r"] = 1,
						},
						["Raid Leader"] = {
							["b"] = 0.2039215862751007,
							["g"] = 0.1333333402872086,
							["r"] = 1,
						},
						["Say"] = {
							["b"] = 0.9490196704864502,
							["g"] = 0.9725490808486938,
							["r"] = 0.9725490808486938,
						},
						["Yell"] = {
							["b"] = 0.3333333432674408,
							["g"] = 0.3333333432674408,
							["r"] = 1,
						},
						["LocalDefense"] = {
							["b"] = 0.7529412508010864,
							["g"] = 0.7529412508010864,
							["r"] = 1,
						},
						["Party"] = {
							["b"] = 1,
							["g"] = 0.6666666865348816,
							["r"] = 0.6666666865348816,
						},
						["Whisper"] = {
							["b"] = 0.7764706611633301,
							["g"] = 0.4745098352432251,
							["r"] = 1,
						},
						["Trade"] = {
							["b"] = 0.7529412508010864,
							["g"] = 0.7529412508010864,
							["r"] = 1,
						},
						["Party Leader"] = {
							["b"] = 1,
							["g"] = 0.6666666865348816,
							["r"] = 0.6666666865348816,
						},
						["Raid"] = {
							["b"] = 0.3333333432674408,
							["g"] = 0.3333333432674408,
							["r"] = 1,
						},
						["Guild"] = {
							["b"] = 0.4823529720306397,
							["g"] = 0.9803922176361084,
							["r"] = 0.3137255012989044,
						},
						["Officer"] = {
							["b"] = 0.250980406999588,
							["g"] = 0.7529412508010864,
							["r"] = 0.250980406999588,
						},
					},
				},
				["Area 52"] = {
					["colors"] = {
						["Raid Warning"] = {
							["b"] = 0.3333333432674408,
							["g"] = 0.3333333432674408,
							["r"] = 1,
						},
						["Instance Leader"] = {
							["b"] = 0.2039215862751007,
							["g"] = 0.1333333402872086,
							["r"] = 1,
						},
						["General"] = {
							["b"] = 0.7529412508010864,
							["g"] = 0.7529412508010864,
							["r"] = 1,
						},
						["Real ID Whisper"] = {
							["b"] = 0.9921569228172302,
							["g"] = 0.9137255549430847,
							["r"] = 0.545098066329956,
						},
						["Instance"] = {
							["b"] = 0.3333333432674408,
							["g"] = 0.3333333432674408,
							["r"] = 1,
						},
						["Raid Leader"] = {
							["b"] = 0.2039215862751007,
							["g"] = 0.1333333402872086,
							["r"] = 1,
						},
						["Say"] = {
							["b"] = 0.9490196704864502,
							["g"] = 0.9725490808486938,
							["r"] = 0.9725490808486938,
						},
						["Yell"] = {
							["b"] = 0.3333333432674408,
							["g"] = 0.3333333432674408,
							["r"] = 1,
						},
						["LocalDefense"] = {
							["b"] = 0.7529412508010864,
							["g"] = 0.7529412508010864,
							["r"] = 1,
						},
						["Party"] = {
							["b"] = 1,
							["g"] = 0.6666666865348816,
							["r"] = 0.6666666865348816,
						},
						["Whisper"] = {
							["b"] = 0.7764706611633301,
							["g"] = 0.4745098352432251,
							["r"] = 1,
						},
						["Trade"] = {
							["b"] = 0.7529412508010864,
							["g"] = 0.7529412508010864,
							["r"] = 1,
						},
						["Party Leader"] = {
							["b"] = 1,
							["g"] = 0.6666666865348816,
							["r"] = 0.6666666865348816,
						},
						["Raid"] = {
							["b"] = 0.3333333432674408,
							["g"] = 0.3333333432674408,
							["r"] = 1,
						},
						["Guild"] = {
							["b"] = 0.4823529720306397,
							["g"] = 0.9803922176361084,
							["r"] = 0.3137255012989044,
						},
						["Officer"] = {
							["b"] = 0.250980406999588,
							["g"] = 0.7529412508010864,
							["r"] = 0.250980406999588,
						},
					},
				},
				["Default"] = {
					["colors"] = {
						["Raid Warning"] = {
							["b"] = 0.3333333432674408,
							["g"] = 0.3333333432674408,
							["r"] = 1,
						},
						["Instance Leader"] = {
							["b"] = 0.2039215862751007,
							["g"] = 0.1333333402872086,
							["r"] = 1,
						},
						["General"] = {
							["b"] = 0.7529412508010864,
							["g"] = 0.7529412508010864,
							["r"] = 1,
						},
						["Real ID Whisper"] = {
							["b"] = 0.9921569228172302,
							["g"] = 0.9137255549430847,
							["r"] = 0.545098066329956,
						},
						["Instance"] = {
							["b"] = 0.3333333432674408,
							["g"] = 0.3333333432674408,
							["r"] = 1,
						},
						["Raid Leader"] = {
							["b"] = 0.2039215862751007,
							["g"] = 0.1333333402872086,
							["r"] = 1,
						},
						["Say"] = {
							["b"] = 0.9490196704864502,
							["g"] = 0.9725490808486938,
							["r"] = 0.9725490808486938,
						},
						["Yell"] = {
							["b"] = 0.3333333432674408,
							["g"] = 0.3333333432674408,
							["r"] = 1,
						},
						["LocalDefense"] = {
							["b"] = 0.7529412508010864,
							["g"] = 0.7529412508010864,
							["r"] = 1,
						},
						["Party"] = {
							["b"] = 1,
							["g"] = 0.6666666865348816,
							["r"] = 0.6666666865348816,
						},
						["Whisper"] = {
							["b"] = 0.7764706611633301,
							["g"] = 0.4745098352432251,
							["r"] = 1,
						},
						["Trade"] = {
							["b"] = 0.7529412508010864,
							["g"] = 0.7529412508010864,
							["r"] = 1,
						},
						["Party Leader"] = {
							["b"] = 1,
							["g"] = 0.6666666865348816,
							["r"] = 0.6666666865348816,
						},
						["Raid"] = {
							["b"] = 0.3333333432674408,
							["g"] = 0.3333333432674408,
							["r"] = 1,
						},
						["Guild"] = {
							["b"] = 0.4823529720306397,
							["g"] = 0.9803922176361084,
							["r"] = 0.3137255012989044,
						},
						["Officer"] = {
							["b"] = 0.250980406999588,
							["g"] = 0.7529412508010864,
							["r"] = 0.250980406999588,
						},
					},
				},
				["PALADIN"] = {
					["colors"] = {
						["Raid Warning"] = {
							["b"] = 0.3333333432674408,
							["g"] = 0.3333333432674408,
							["r"] = 1,
						},
						["Instance Leader"] = {
							["b"] = 0.2039215862751007,
							["g"] = 0.1333333402872086,
							["r"] = 1,
						},
						["General"] = {
							["b"] = 0.7529412508010864,
							["g"] = 0.7529412508010864,
							["r"] = 1,
						},
						["Real ID Whisper"] = {
							["b"] = 0.9921569228172302,
							["g"] = 0.9137255549430847,
							["r"] = 0.545098066329956,
						},
						["Instance"] = {
							["b"] = 0.3333333432674408,
							["g"] = 0.3333333432674408,
							["r"] = 1,
						},
						["Raid Leader"] = {
							["b"] = 0.2039215862751007,
							["g"] = 0.1333333402872086,
							["r"] = 1,
						},
						["Say"] = {
							["b"] = 0.9490196704864502,
							["g"] = 0.9725490808486938,
							["r"] = 0.9725490808486938,
						},
						["Yell"] = {
							["b"] = 0.3333333432674408,
							["g"] = 0.3333333432674408,
							["r"] = 1,
						},
						["LocalDefense"] = {
							["b"] = 0.7529412508010864,
							["g"] = 0.7529412508010864,
							["r"] = 1,
						},
						["Party"] = {
							["b"] = 1,
							["g"] = 0.6666666865348816,
							["r"] = 0.6666666865348816,
						},
						["Whisper"] = {
							["b"] = 0.7764706611633301,
							["g"] = 0.4745098352432251,
							["r"] = 1,
						},
						["Trade"] = {
							["b"] = 0.7529412508010864,
							["g"] = 0.7529412508010864,
							["r"] = 1,
						},
						["Party Leader"] = {
							["b"] = 1,
							["g"] = 0.6666666865348816,
							["r"] = 0.6666666865348816,
						},
						["Raid"] = {
							["b"] = 0.3333333432674408,
							["g"] = 0.3333333432674408,
							["r"] = 1,
						},
						["Guild"] = {
							["b"] = 0.4823529720306397,
							["g"] = 0.9803922176361084,
							["r"] = 0.3137255012989044,
						},
						["Officer"] = {
							["b"] = 0.250980406999588,
							["g"] = 0.7529412508010864,
							["r"] = 0.250980406999588,
						},
					},
				},
			},
		},
		["StickyChannels"] = {
			["profiles"] = {
				["Default"] = {
					["BN_WHISPER"] = true,
					["YELL"] = true,
					["RAID_WARNING"] = true,
					["CHANNEL"] = true,
					["EMOTE"] = true,
					["SAY"] = true,
					["OFFICER"] = true,
					["WHISPER"] = true,
				},
			},
		},
		["ChatFrameBorders"] = {
			["profiles"] = {
				["Default"] = {
					["frames"] = {
						["FRAME_1"] = {
							["backgroundColor"] = {
								["a"] = 1,
								["r"] = 0,
								["g"] = 0,
								["b"] = 0,
							},
							["edgeSize"] = 12,
							["combatLogFix"] = false,
							["borderColor"] = {
								["a"] = 1,
								["r"] = 1,
								["g"] = 1,
								["b"] = 1,
							},
							["enable"] = false,
							["background"] = "Solid",
							["border"] = "Blizzard Tooltip",
							["inset"] = 3,
						},
						["FRAME_9"] = {
							["backgroundColor"] = {
								["a"] = 1,
								["r"] = 0,
								["g"] = 0,
								["b"] = 0,
							},
							["edgeSize"] = 12,
							["combatLogFix"] = false,
							["borderColor"] = {
								["a"] = 1,
								["r"] = 1,
								["g"] = 1,
								["b"] = 1,
							},
							["enable"] = false,
							["background"] = "Blizzard Tooltip",
							["border"] = "Blizzard Tooltip",
							["inset"] = 3,
						},
						["FRAME_6"] = {
							["backgroundColor"] = {
								["a"] = 1,
								["r"] = 0,
								["g"] = 0,
								["b"] = 0,
							},
							["edgeSize"] = 12,
							["combatLogFix"] = false,
							["borderColor"] = {
								["a"] = 1,
								["r"] = 1,
								["g"] = 1,
								["b"] = 1,
							},
							["enable"] = true,
							["background"] = "Blizzard Tooltip",
							["border"] = "Blizzard Tooltip",
							["inset"] = 3,
						},
						["FRAME_5"] = {
							["backgroundColor"] = {
								["a"] = 1,
								["r"] = 0,
								["g"] = 0,
								["b"] = 0,
							},
							["edgeSize"] = 12,
							["combatLogFix"] = false,
							["borderColor"] = {
								["a"] = 1,
								["r"] = 1,
								["g"] = 1,
								["b"] = 1,
							},
							["enable"] = true,
							["background"] = "Blizzard Tooltip",
							["border"] = "Blizzard Tooltip",
							["inset"] = 3,
						},
						["FRAME_8"] = {
							["backgroundColor"] = {
								["a"] = 1,
								["r"] = 0,
								["g"] = 0,
								["b"] = 0,
							},
							["edgeSize"] = 12,
							["combatLogFix"] = false,
							["borderColor"] = {
								["a"] = 1,
								["r"] = 1,
								["g"] = 1,
								["b"] = 1,
							},
							["enable"] = true,
							["background"] = "Blizzard Tooltip",
							["border"] = "Blizzard Tooltip",
							["inset"] = 3,
						},
						["FRAME_10"] = {
							["backgroundColor"] = {
								["a"] = 1,
								["r"] = 0,
								["g"] = 0,
								["b"] = 0,
							},
							["edgeSize"] = 12,
							["combatLogFix"] = false,
							["borderColor"] = {
								["a"] = 1,
								["r"] = 1,
								["g"] = 1,
								["b"] = 1,
							},
							["enable"] = false,
							["background"] = "Blizzard Tooltip",
							["border"] = "Blizzard Tooltip",
							["inset"] = 3,
						},
						["FRAME_3"] = {
							["backgroundColor"] = {
								["a"] = 1,
								["r"] = 0,
								["g"] = 0,
								["b"] = 0,
							},
							["edgeSize"] = 12,
							["combatLogFix"] = false,
							["borderColor"] = {
								["a"] = 1,
								["r"] = 1,
								["g"] = 1,
								["b"] = 1,
							},
							["enable"] = false,
							["background"] = "Blizzard Tooltip",
							["border"] = "Blizzard Tooltip",
							["inset"] = 3,
						},
						["FRAME_4"] = {
							["backgroundColor"] = {
								["a"] = 1,
								["r"] = 0,
								["g"] = 0,
								["b"] = 0,
							},
							["edgeSize"] = 12,
							["combatLogFix"] = false,
							["borderColor"] = {
								["a"] = 1,
								["r"] = 1,
								["g"] = 1,
								["b"] = 1,
							},
							["enable"] = true,
							["background"] = "Blizzard Tooltip",
							["border"] = "Blizzard Tooltip",
							["inset"] = 3,
						},
						["FRAME_2"] = {
							["backgroundColor"] = {
								["a"] = 1,
								["r"] = 0,
								["g"] = 0,
								["b"] = 0,
							},
							["edgeSize"] = 12,
							["combatLogFix"] = true,
							["borderColor"] = {
								["a"] = 1,
								["r"] = 1,
								["g"] = 1,
								["b"] = 1,
							},
							["enable"] = false,
							["background"] = "Blizzard Tooltip",
							["border"] = "Blizzard Tooltip",
							["inset"] = 3,
						},
						["FRAME_7"] = {
							["backgroundColor"] = {
								["a"] = 1,
								["r"] = 0,
								["g"] = 0,
								["b"] = 0,
							},
							["edgeSize"] = 12,
							["combatLogFix"] = false,
							["borderColor"] = {
								["a"] = 1,
								["r"] = 1,
								["g"] = 1,
								["b"] = 1,
							},
							["enable"] = true,
							["background"] = "Blizzard Tooltip",
							["border"] = "Blizzard Tooltip",
							["inset"] = 3,
						},
					},
				},
			},
		},
		["Buttons"] = {
			["profiles"] = {
				["Default"] = {
				},
			},
		},
		["Server Positioning"] = {
		},
		["JustifyText"] = {
		},
		["Timestamps"] = {
			["profiles"] = {
				["Default"] = {
					["color"] = {
						["b"] = 0.45,
						["g"] = 0.45,
						["r"] = 0.45,
					},
					["format"] = "%X",
					["frames"] = {
						["Frame3"] = true,
						["Frame4"] = true,
						["Frame18"] = true,
						["Frame12"] = true,
						["Frame5"] = true,
						["Frame19"] = true,
						["Frame6"] = true,
						["Frame17"] = true,
						["Frame11"] = true,
						["Frame13"] = true,
						["Frame14"] = true,
						["Frame20"] = true,
						["Frame7"] = true,
						["Frame15"] = true,
						["Frame16"] = true,
						["Frame1"] = true,
					},
				},
			},
		},
		["EditBox"] = {
			["profiles"] = {
				["Default"] = {
					["hideDialog"] = true,
					["edgeSize"] = 12,
					["useAlt"] = false,
					["attach"] = "BOTTOM",
					["border"] = "Blizzard Tooltip",
					["background"] = "Blizzard Tooltip",
					["font"] = "Arial Narrow",
					["tileSize"] = 16,
					["borderColor"] = {
						["a"] = 1,
						["b"] = 1,
						["g"] = 1,
						["r"] = 1,
					},
					["height"] = 22,
					["colorByChannel"] = true,
					["inset"] = 3,
					["backgroundColor"] = {
						["a"] = 1,
						["b"] = 0,
						["g"] = 0,
						["r"] = 0,
					},
				},
			},
		},
		["Highlight"] = {
			["profiles"] = {
				["Default"] = {
					["words"] = {
						["noxiipally"] = "Noxiipally",
					},
				},
			},
		},
		["ChatTabs"] = {
			["profiles"] = {
				["Default"] = {
					["height"] = 29,
					["tabFlash"] = true,
					["alpha"] = 0,
				},
			},
		},
		["ChannelNames"] = {
			["profiles"] = {
				["Default"] = {
					["addSpace"] = true,
					["channels"] = {
						["busy BN Whisper To"] = "<Busy>[BN:To]",
						["Raid Warning"] = "[RW]",
						["Guild"] = "[G]",
						["Instance Leader"] = "[IL]",
						["BN Whisper To"] = "[BN:To]",
						["Raid Leader"] = "[RL]",
						["BN Whisper From"] = "[BN:From]",
						["away BN Whisper To"] = "<Away>[BN:To]",
						["Whisper To"] = "[W:To]",
						["Party"] = "[P]",
						["Whisper From"] = "[W:From]",
						["Raid"] = "[R]",
						["Instance"] = "[I]",
						["Dungeon Guide"] = "[DG]",
						["Party Leader"] = "[PL]",
						["Officer"] = "[O]",
					},
				},
			},
		},
		["CopyChat"] = {
			["profiles"] = {
				["Default"] = {
					["copyIcon"] = false,
				},
			},
		},
		["UrlCopy"] = {
		},
		["Editbox History"] = {
			["realm"] = {
				["Area 52"] = {
					["history"] = {
						"/cw Ouru i tried swapping off elvui, and got spammed with the same errors and game freezing when entering boss combat", -- [1]
						"/cw Ouru yeah. i fixed it by turning on and off plater. but i know bigdebuffs normally wants to attach to nameplates", -- [2]
						"/plater", -- [3]
						"/wa", -- [4]
						"/wa", -- [5]
						"/wa", -- [6]
						"/wa", -- [7]
						" yeah very minimal. still 40ish in combat", -- [8]
						" 0 chance", -- [9]
						"/ert", -- [10]
						"/ert", -- [11]
						"/wa", -- [12]
						"/raid can, just rather save usually", -- [13]
						"/raid swapped to faith to try that", -- [14]
						"/p faith can do a lot more seed healing, just loses a lot of burst raid healing", -- [15]
						"/p idc which i use", -- [16]
						"/wa", -- [17]
						"/wa", -- [18]
						"/p i had beacon on circle there, beaconing far two and martyr'ing the close", -- [19]
						"/wa", -- [20]
						"/wa", -- [21]
						"/cw Krazyspriest-Area52 ok", -- [22]
						"/wa", -- [23]
						"/bw", -- [24]
						"/raid healthstone pls", -- [25]
						"/raid repair pls", -- [26]
						" zz", -- [27]
						" naa", -- [28]
						" tomorrow", -- [29]
						" yup", -- [30]
						" i need 2", -- [31]
						" lol", -- [32]
					},
				},
			},
		},
		["ChatFont"] = {
			["profiles"] = {
				["Default"] = {
					["outline"] = "",
					["font"] = "Ubuntu Bold",
					["fontsize"] = 13,
					["frames"] = {
						["FRAME_1"] = {
						},
						["FRAME_9"] = {
						},
						["FRAME_6"] = {
						},
						["FRAME_5"] = {
						},
						["FRAME_8"] = {
						},
						["FRAME_7"] = {
						},
						["FRAME_3"] = {
						},
						["FRAME_4"] = {
						},
						["FRAME_10"] = {
						},
						["FRAME_2"] = {
						},
					},
				},
			},
		},
		["Mousewheel Scroll"] = {
		},
	},
	["profileKeys"] = {
		["Noxiipally - Area 52"] = "Default",
		["Noxiishields - Area 52"] = "Default",
	},
	["profiles"] = {
		["Noxiipally - Area 52"] = {
		},
		["Area 52"] = {
		},
		["Default"] = {
			["modules"] = {
				["All Edge resizing"] = false,
				["Borders/Background"] = true,
			},
			["welcomeMessaged"] = true,
		},
		["PALADIN"] = {
		},
	},
}
