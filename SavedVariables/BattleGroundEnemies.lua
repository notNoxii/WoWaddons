
BattleGroundEnemiesDB = {
	["profileKeys"] = {
		["Noxiipally - Area 52"] = "Default",
		["Noxiishields - Area 52"] = "Default",
	},
	["profiles"] = {
		["Default"] = {
			["Debug"] = false,
			["onlyShowWhenNewVersion"] = true,
			["DisableArenaFrames"] = true,
			["MyFocus_Color"] = {
				0, -- [1]
				0.988235294117647, -- [2]
				0.729411764705882, -- [3]
				1, -- [4]
			},
			["Enemies"] = {
				["ConvertCyrillic"] = true,
				["40"] = {
					["DrTracking_Cooldown_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["Auras_Debuffs_Container_RelativeTo"] = "BuffContainer",
					["NumericTargetindicator_Enabled"] = true,
					["Auras_Buffs_ShowStealOrPurgeable"] = true,
					["NumericTargetindicator_Fontsize"] = 18,
					["DrTracking_DisplayType"] = "Countdowntext",
					["PlayerCount_EnableTextshadow"] = false,
					["Auras_Buffs_Container_OffsetY"] = 0,
					["Auras_Debuffs_DebuffTypeFiltering_Enabled"] = true,
					["Auras_Buffs_HorizontalGrowDirection"] = "leftwards",
					["Auras_Debuffs_Container_OffsetX"] = 2,
					["Auras_Debuffs_DisplayType"] = "Frame",
					["NumericTargetindicator_EnableTextshadow"] = false,
					["CovenantIcon_Enabled"] = true,
					["Position_Y"] = 616.2874471624018,
					["Trinket_Width"] = 28,
					["Spec_AuraDisplay_Cooldown_Fontsize"] = 12,
					["CovenantIcon_VerticalPosition"] = 3,
					["BarVerticalSpacing"] = 1,
					["Auras_Buffs_Outline"] = "OUTLINE",
					["Auras_Debuffs_EnableTextshadow"] = true,
					["Auras_Debuffs_Size"] = 15,
					["Racial_RelativePoint"] = "RIGHT",
					["DrTracking_HorizontalSpacing"] = 1,
					["PlayerCount_Enabled"] = true,
					["Auras_Debuffs_HorizontalSpacing"] = 1,
					["RacialFiltering_Filterlist"] = {
					},
					["Trinket_RelativePoint"] = "RIGHT",
					["Racial_Cooldown_Fontsize"] = 12,
					["RoleIcon_VerticalPosition"] = 2,
					["Racial_OffsetX"] = 1,
					["Auras_Debuffs_HorizontalGrowDirection"] = "leftwards",
					["Auras_Buffs_IconsPerRow"] = 8,
					["Trinket_Enabled"] = true,
					["Auras_Debuffs_SpellIDFiltering_Enabled"] = false,
					["NumericTargetindicator_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["Trinket_ShowNumbers"] = true,
					["DrTracking_Cooldown_Outline"] = "OUTLINE",
					["Auras_Debuffs_Fontsize"] = 12,
					["DrTracking_ShowNumbers"] = true,
					["NumericTargetindicator_Textcolor"] = {
						1, -- [1]
						1, -- [2]
						1, -- [3]
						1, -- [4]
					},
					["DrTracking_Container_OffsetX"] = 1,
					["SymbolicTargetindicator_Enabled"] = true,
					["Auras_Debuffs_Container_RelativePoint"] = "BOTTOMLEFT",
					["DrTrackingFiltering_Filterlist"] = {
					},
					["Racial_Cooldown_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["Trinket_BasicPoint"] = "LEFT",
					["Trinket_RelativeTo"] = "Button",
					["Auras_Debuffs_Enabled"] = true,
					["Auras_Buffs_Filtering_Enabled"] = false,
					["Name_Outline"] = "",
					["Name_Textcolor"] = {
						1, -- [1]
						1, -- [2]
						1, -- [3]
						1, -- [4]
					},
					["Racial_RelativeTo"] = "Trinket",
					["Spec_AuraDisplay_Cooldown_Outline"] = "OUTLINE",
					["PowerBar_Texture"] = "UI-StatusBar",
					["Auras_Buffs_Cooldown_Outline"] = "OUTLINE",
					["DrTracking_Container_BasicPoint"] = "RIGHT",
					["Auras_Buffs_ShowNumbers"] = true,
					["Auras_Buffs_Enabled"] = true,
					["Trinket_Cooldown_Fontsize"] = 12,
					["Auras_Buffs_VerticalSpacing"] = 1,
					["Trinket_Cooldown_EnableTextshadow"] = false,
					["BarHorizontalSpacing"] = 100,
					["Framescale"] = 1,
					["Racial_Cooldown_EnableTextshadow"] = false,
					["BarVerticalGrowdirection"] = "downwards",
					["BarWidth"] = 220,
					["Auras_Buffs_Container_RelativeTo"] = "Button",
					["DrTracking_Container_BorderThickness"] = 1,
					["Auras_Buffs_Cooldown_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["Auras_Debuffs_VerticalGrowdirection"] = "upwards",
					["PlayerCount_Textcolor"] = {
						1, -- [1]
						1, -- [2]
						1, -- [3]
						1, -- [4]
					},
					["PlayerCount_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["HealthBar_Texture"] = "UI-StatusBar",
					["HealthBar_Background"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						0.66, -- [4]
					},
					["Name_Fontsize"] = 13,
					["Auras_Buffs_Container_RelativePoint"] = "BOTTOMLEFT",
					["Spec_Enabled"] = true,
					["Racial_Width"] = 28,
					["Racial_BasicPoint"] = "LEFT",
					["NumericTargetindicator_Outline"] = "",
					["Auras_Enabled"] = true,
					["Auras_Buffs_Size"] = 15,
					["Racial_ShowNumbers"] = true,
					["RacialFiltering_Enabled"] = false,
					["Auras_Buffs_Fontsize"] = 12,
					["Auras_Debuffs_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["Position_X"] = 237.0173775147487,
					["BarColumns"] = 1,
					["Trinket_OffsetX"] = 1,
					["CovenantIcon_Size"] = 20,
					["Trinket_Cooldown_Outline"] = "OUTLINE",
					["Auras_Buffs_HorizontalSpacing"] = 1,
					["Auras_Debuffs_Filtering_Enabled"] = false,
					["Auras_Debuffs_Cooldown_Outline"] = "OUTLINE",
					["Racial_Cooldown_Outline"] = "OUTLINE",
					["Racial_Enabled"] = true,
					["PowerBar_Background"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						0.66, -- [4]
					},
					["Auras_Debuffs_VerticalSpacing"] = 1,
					["BarHorizontalGrowdirection"] = "rightwards",
					["Trinket_Cooldown_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["Auras_Debuffs_SpellIDFiltering_Filterlist"] = {
					},
					["HealthBar_HealthPrediction_Enabled"] = true,
					["PowerBar_Enabled"] = false,
					["Auras_Buffs_EnableTextshadow"] = true,
					["DrTracking_Cooldown_EnableTextshadow"] = false,
					["Auras_Buffs_SpellIDFiltering_Enabled"] = false,
					["DrTracking_Container_Color"] = {
						0, -- [1]
						0, -- [2]
						1, -- [3]
						1, -- [4]
					},
					["DrTracking_Cooldown_Fontsize"] = 12,
					["PowerBar_Height"] = 4,
					["Auras_Debuffs_Filtering_Blizzlike"] = false,
					["PlayerCount_Fontsize"] = 14,
					["Auras_Buffs_Filtering_Blizzlike"] = false,
					["Auras_Debuffs_Cooldown_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["Auras_Buffs_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["Spec_AuraDisplay_Cooldown_EnableTextshadow"] = false,
					["DrTracking_Container_RelativePoint"] = "Left",
					["Auras_Debuffs_Cooldown_Fontsize"] = 12,
					["DrTracking_Container_RelativeTo"] = "Button",
					["RoleIcon_Enabled"] = true,
					["Spec_AuraDisplay_Enabled"] = true,
					["Auras_Debuffs_Outline"] = "OUTLINE",
					["Auras_Buffs_SpellIDFiltering_Filterlist"] = {
					},
					["Auras_Debuffs_Container_OffsetY"] = 0,
					["Spec_AuraDisplay_ShowNumbers"] = true,
					["BarHeight"] = 22,
					["Auras_Buffs_Container_Point"] = "BOTTOMRIGHT",
					["Auras_Debuffs_IconsPerRow"] = 8,
					["Auras_Buffs_VerticalGrowdirection"] = "upwards",
					["Auras_Debuffs_Coloring_Enabled"] = true,
					["Auras_Buffs_ShowMine"] = true,
					["DrTracking_GrowDirection"] = "leftwards",
					["RoleIcon_Size"] = 13,
					["Auras_Buffs_Cooldown_EnableTextshadow"] = false,
					["Auras_Buffs_Cooldown_Fontsize"] = 12,
					["Auras_Buffs_Textcolor"] = {
						1, -- [1]
						1, -- [2]
						1, -- [3]
						1, -- [4]
					},
					["Enabled"] = false,
					["Spec_AuraDisplay_Cooldown_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["Auras_Buffs_Container_OffsetX"] = 2,
					["Auras_Debuffs_Container_Point"] = "BOTTOMRIGHT",
					["DrTrackingFiltering_Enabled"] = false,
					["Auras_Debuffs_ShowMine"] = true,
					["Auras_Debuffs_ShowStealOrPurgeable"] = true,
					["Name_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["Name_EnableTextshadow"] = true,
					["Auras_Debuffs_DebuffTypeFiltering_Filterlist"] = {
					},
					["Auras_Debuffs_Cooldown_EnableTextshadow"] = false,
					["Auras_Debuffs_ShowNumbers"] = true,
					["DrTracking_Enabled"] = true,
					["Auras_Debuffs_Textcolor"] = {
						1, -- [1]
						1, -- [2]
						1, -- [3]
						1, -- [4]
					},
					["Spec_Width"] = 36,
					["PlayerCount_Outline"] = "OUTLINE",
				},
				["LeftButtonValue"] = "",
				["LevelText_EnableTextshadow"] = false,
				["LeftButtonType"] = "Target",
				["MiddleButtonType"] = "Custom",
				["RangeIndicator_Everything"] = false,
				["RightButtonValue"] = "",
				["Enabled"] = true,
				["RangeIndicator_Range"] = 28767,
				["LevelText_TextShadowcolor"] = {
					0, -- [1]
					0, -- [2]
					0, -- [3]
					1, -- [4]
				},
				["LevelText_Fontsize"] = 18,
				["LevelText_Textcolor"] = {
					1, -- [1]
					1, -- [2]
					1, -- [3]
					1, -- [4]
				},
				["ShowRealmnames"] = true,
				["5"] = {
					["DrTracking_Cooldown_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["Auras_Debuffs_IconsPerRow"] = 8,
					["NumericTargetindicator_Enabled"] = true,
					["Auras_Buffs_ShowStealOrPurgeable"] = true,
					["NumericTargetindicator_Fontsize"] = 18,
					["DrTracking_DisplayType"] = "Countdowntext",
					["ObjectiveAndRespawn_ObjectiveEnabled"] = false,
					["PlayerCount_EnableTextshadow"] = false,
					["Auras_Buffs_Container_OffsetY"] = 0,
					["Auras_Debuffs_DebuffTypeFiltering_Enabled"] = false,
					["Auras_Buffs_HorizontalGrowDirection"] = "leftwards",
					["Auras_Debuffs_Container_OffsetX"] = 2,
					["Auras_Debuffs_DisplayType"] = "Frame",
					["NumericTargetindicator_EnableTextshadow"] = false,
					["CovenantIcon_Enabled"] = true,
					["Position_Y"] = 491.9087780674818,
					["Trinket_Width"] = 28,
					["Spec_AuraDisplay_Cooldown_Fontsize"] = 12,
					["CovenantIcon_VerticalPosition"] = 3,
					["BarVerticalSpacing"] = 1,
					["Auras_Buffs_Outline"] = "OUTLINE",
					["Auras_Debuffs_EnableTextshadow"] = true,
					["Auras_Debuffs_Size"] = 27,
					["Racial_RelativePoint"] = "RIGHT",
					["DrTracking_HorizontalSpacing"] = 1,
					["PlayerCount_Enabled"] = true,
					["Auras_Debuffs_HorizontalSpacing"] = 1,
					["BarColumns"] = 1,
					["Trinket_OffsetX"] = 1,
					["Racial_Cooldown_Fontsize"] = 12,
					["RoleIcon_VerticalPosition"] = 2,
					["Auras_Enabled"] = false,
					["Auras_Debuffs_HorizontalGrowDirection"] = "leftwards",
					["Auras_Buffs_IconsPerRow"] = 8,
					["Trinket_Enabled"] = true,
					["ObjectiveAndRespawn_Outline"] = "THICKOUTLINE",
					["NumericTargetindicator_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["Trinket_ShowNumbers"] = true,
					["DrTracking_Cooldown_Outline"] = "OUTLINE",
					["Auras_Debuffs_Fontsize"] = 12,
					["DrTracking_ShowNumbers"] = true,
					["NumericTargetindicator_Textcolor"] = {
						1, -- [1]
						1, -- [2]
						1, -- [3]
						1, -- [4]
					},
					["DrTracking_Container_OffsetX"] = 1,
					["SymbolicTargetindicator_Enabled"] = true,
					["Auras_Debuffs_Container_RelativePoint"] = "LEFT",
					["DrTrackingFiltering_Filterlist"] = {
					},
					["Racial_Cooldown_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["Trinket_BasicPoint"] = "LEFT",
					["Trinket_RelativeTo"] = "Button",
					["Auras_Debuffs_Enabled"] = true,
					["Auras_Buffs_Filtering_Enabled"] = false,
					["Name_Outline"] = "",
					["ObjectiveAndRespawn_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["Name_Textcolor"] = {
						1, -- [1]
						1, -- [2]
						1, -- [3]
						1, -- [4]
					},
					["Racial_RelativeTo"] = "Trinket",
					["Spec_AuraDisplay_Cooldown_Outline"] = "OUTLINE",
					["PowerBar_Texture"] = "UI-StatusBar",
					["Auras_Buffs_Cooldown_Outline"] = "OUTLINE",
					["DrTracking_Container_BasicPoint"] = "RIGHT",
					["Auras_Buffs_ShowNumbers"] = true,
					["Auras_Buffs_Enabled"] = false,
					["Trinket_Cooldown_Fontsize"] = 12,
					["Auras_Buffs_VerticalSpacing"] = 1,
					["Trinket_Cooldown_EnableTextshadow"] = false,
					["BarHorizontalSpacing"] = 100,
					["Framescale"] = 1,
					["Racial_Cooldown_EnableTextshadow"] = false,
					["BarVerticalGrowdirection"] = "downwards",
					["BarWidth"] = 220,
					["Auras_Buffs_Container_RelativeTo"] = "Button",
					["DrTracking_Container_BorderThickness"] = 1,
					["Auras_Buffs_Cooldown_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["ObjectiveAndRespawn_EnableTextshadow"] = false,
					["ObjectiveAndRespawn_Textcolor"] = {
						1, -- [1]
						1, -- [2]
						1, -- [3]
						1, -- [4]
					},
					["Auras_Debuffs_VerticalGrowdirection"] = "upwards",
					["DrTracking_Enabled"] = true,
					["Auras_Debuffs_SpellIDFiltering_Enabled"] = false,
					["PlayerCount_Textcolor"] = {
						1, -- [1]
						1, -- [2]
						1, -- [3]
						1, -- [4]
					},
					["Spec_Width"] = 36,
					["ObjectiveAndRespawn_Width"] = 36,
					["Racial_BasicPoint"] = "LEFT",
					["HealthBar_Texture"] = "UI-StatusBar",
					["Trinket_RelativePoint"] = "RIGHT",
					["DrTracking_Container_RelativePoint"] = "LEFT",
					["HealthBar_Background"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						0.66, -- [4]
					},
					["Name_Fontsize"] = 13,
					["Auras_Buffs_Container_RelativePoint"] = "BOTTOMLEFT",
					["Spec_Enabled"] = true,
					["Auras_Buffs_HorizontalSpacing"] = 1,
					["Trinket_Cooldown_Outline"] = "OUTLINE",
					["PowerBar_Background"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						0.66, -- [4]
					},
					["NumericTargetindicator_Outline"] = "",
					["Auras_Debuffs_SpellIDFiltering_Filterlist"] = {
					},
					["Auras_Buffs_Size"] = 15,
					["HealthBar_HealthPrediction_Enabled"] = true,
					["RacialFiltering_Enabled"] = false,
					["Auras_Buffs_Fontsize"] = 12,
					["Auras_Debuffs_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["Position_X"] = 174.0129648605071,
					["Racial_Cooldown_Outline"] = "OUTLINE",
					["Racial_OffsetX"] = 2,
					["CovenantIcon_Size"] = 20,
					["ObjectiveAndRespawn_RelativePoint"] = "LEFT",
					["Auras_Buffs_Filtering_Blizzlike"] = false,
					["Auras_Debuffs_Filtering_Enabled"] = false,
					["Auras_Debuffs_Cooldown_Outline"] = "OUTLINE",
					["RoleIcon_Enabled"] = true,
					["Racial_Width"] = 28,
					["PowerBar_Enabled"] = false,
					["Auras_Debuffs_VerticalSpacing"] = 1,
					["BarHorizontalGrowdirection"] = "rightwards",
					["Auras_Debuffs_ShowStealOrPurgeable"] = true,
					["Racial_Enabled"] = true,
					["Auras_Debuffs_ShowMine"] = true,
					["ObjectiveAndRespawn_RelativeTo"] = "NumericTargetindicator",
					["Auras_Buffs_EnableTextshadow"] = true,
					["DrTracking_Cooldown_EnableTextshadow"] = false,
					["Auras_Buffs_SpellIDFiltering_Enabled"] = false,
					["DrTracking_Container_Color"] = {
						0, -- [1]
						0, -- [2]
						1, -- [3]
						1, -- [4]
					},
					["DrTracking_Cooldown_Fontsize"] = 12,
					["PowerBar_Height"] = 4,
					["Auras_Debuffs_Filtering_Blizzlike"] = false,
					["Auras_Buffs_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["ObjectiveAndRespawn_Fontsize"] = 17,
					["RacialFiltering_Filterlist"] = {
					},
					["Spec_AuraDisplay_ShowNumbers"] = true,
					["Auras_Buffs_Cooldown_Fontsize"] = 12,
					["Auras_Debuffs_Cooldown_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["ObjectiveAndRespawn_OffsetX"] = -2,
					["Auras_Debuffs_Cooldown_Fontsize"] = 12,
					["DrTracking_Container_RelativeTo"] = "Button",
					["Spec_AuraDisplay_Enabled"] = true,
					["Auras_Debuffs_Outline"] = "OUTLINE",
					["Auras_Debuffs_Coloring_Enabled"] = true,
					["Auras_Debuffs_Container_OffsetY"] = 0,
					["Auras_Debuffs_Container_RelativeTo"] = "DRContainer",
					["BarHeight"] = 28,
					["Auras_Buffs_Container_Point"] = "BOTTOMRIGHT",
					["Auras_Buffs_SpellIDFiltering_Filterlist"] = {
					},
					["Auras_Buffs_VerticalGrowdirection"] = "upwards",
					["Spec_AuraDisplay_Cooldown_EnableTextshadow"] = false,
					["Auras_Buffs_ShowMine"] = true,
					["DrTracking_GrowDirection"] = "leftwards",
					["RoleIcon_Size"] = 13,
					["PlayerCount_Fontsize"] = 14,
					["Auras_Buffs_Cooldown_EnableTextshadow"] = false,
					["Auras_Buffs_Textcolor"] = {
						1, -- [1]
						1, -- [2]
						1, -- [3]
						1, -- [4]
					},
					["Enabled"] = true,
					["Spec_AuraDisplay_Cooldown_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["Auras_Debuffs_DebuffTypeFiltering_Filterlist"] = {
					},
					["Auras_Debuffs_Container_Point"] = "RIGHT",
					["DrTrackingFiltering_Enabled"] = false,
					["Racial_ShowNumbers"] = true,
					["Auras_Buffs_Container_OffsetX"] = 2,
					["Name_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["Name_EnableTextshadow"] = true,
					["Trinket_Cooldown_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["Auras_Debuffs_Cooldown_EnableTextshadow"] = false,
					["Auras_Debuffs_ShowNumbers"] = true,
					["PlayerCount_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["Auras_Debuffs_Textcolor"] = {
						1, -- [1]
						1, -- [2]
						1, -- [3]
						1, -- [4]
					},
					["ObjectiveAndRespawn_BasicPoint"] = "RIGHT",
					["PlayerCount_Outline"] = "OUTLINE",
				},
				["MiddleButtonValue"] = "",
				["RangeIndicator_Frames"] = {
				},
				["15"] = {
					["DrTracking_Cooldown_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["Auras_Debuffs_IconsPerRow"] = 8,
					["NumericTargetindicator_Enabled"] = true,
					["Auras_Buffs_ShowStealOrPurgeable"] = true,
					["NumericTargetindicator_Fontsize"] = 18,
					["DrTracking_DisplayType"] = "Countdowntext",
					["ObjectiveAndRespawn_ObjectiveEnabled"] = true,
					["PlayerCount_EnableTextshadow"] = false,
					["Auras_Buffs_Container_OffsetY"] = 0,
					["Auras_Debuffs_DebuffTypeFiltering_Enabled"] = false,
					["Auras_Buffs_HorizontalGrowDirection"] = "leftwards",
					["Auras_Debuffs_Container_OffsetX"] = 2,
					["Auras_Debuffs_DisplayType"] = "Frame",
					["NumericTargetindicator_EnableTextshadow"] = false,
					["CovenantIcon_Enabled"] = true,
					["Position_Y"] = 485.9336219510369,
					["Trinket_Width"] = 28,
					["Spec_AuraDisplay_Cooldown_Fontsize"] = 12,
					["CovenantIcon_VerticalPosition"] = 3,
					["BarVerticalSpacing"] = 1,
					["Auras_Buffs_Outline"] = "OUTLINE",
					["Auras_Debuffs_EnableTextshadow"] = true,
					["Auras_Debuffs_Size"] = 27,
					["Racial_RelativePoint"] = "RIGHT",
					["DrTracking_HorizontalSpacing"] = 1,
					["PlayerCount_Enabled"] = true,
					["Auras_Debuffs_HorizontalSpacing"] = 1,
					["BarColumns"] = 1,
					["Trinket_OffsetX"] = 1,
					["Racial_Cooldown_Fontsize"] = 12,
					["RoleIcon_VerticalPosition"] = 2,
					["Auras_Enabled"] = true,
					["Auras_Debuffs_HorizontalGrowDirection"] = "leftwards",
					["Auras_Buffs_IconsPerRow"] = 8,
					["Trinket_Enabled"] = true,
					["ObjectiveAndRespawn_Outline"] = "THICKOUTLINE",
					["NumericTargetindicator_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["Trinket_ShowNumbers"] = true,
					["DrTracking_Cooldown_Outline"] = "OUTLINE",
					["Auras_Debuffs_Fontsize"] = 12,
					["DrTracking_ShowNumbers"] = true,
					["NumericTargetindicator_Textcolor"] = {
						1, -- [1]
						1, -- [2]
						1, -- [3]
						1, -- [4]
					},
					["DrTracking_Container_OffsetX"] = 1,
					["SymbolicTargetindicator_Enabled"] = true,
					["Auras_Debuffs_Container_RelativePoint"] = "LEFT",
					["DrTrackingFiltering_Filterlist"] = {
					},
					["Racial_Cooldown_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["Trinket_BasicPoint"] = "LEFT",
					["Trinket_RelativeTo"] = "Button",
					["Auras_Debuffs_Enabled"] = true,
					["Auras_Buffs_Filtering_Enabled"] = false,
					["Name_Outline"] = "",
					["ObjectiveAndRespawn_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["Name_Textcolor"] = {
						1, -- [1]
						1, -- [2]
						1, -- [3]
						1, -- [4]
					},
					["Racial_RelativeTo"] = "Trinket",
					["Spec_AuraDisplay_Cooldown_Outline"] = "OUTLINE",
					["PowerBar_Texture"] = "UI-StatusBar",
					["Auras_Buffs_Cooldown_Outline"] = "OUTLINE",
					["DrTracking_Container_BasicPoint"] = "RIGHT",
					["Auras_Buffs_ShowNumbers"] = true,
					["Auras_Buffs_Enabled"] = false,
					["Trinket_Cooldown_Fontsize"] = 12,
					["Auras_Buffs_VerticalSpacing"] = 1,
					["Trinket_Cooldown_EnableTextshadow"] = false,
					["BarHorizontalSpacing"] = 100,
					["Framescale"] = 1,
					["Racial_Cooldown_EnableTextshadow"] = false,
					["BarVerticalGrowdirection"] = "downwards",
					["BarWidth"] = 220,
					["Auras_Buffs_Container_RelativeTo"] = "Button",
					["DrTracking_Container_BorderThickness"] = 1,
					["Auras_Buffs_Cooldown_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["ObjectiveAndRespawn_EnableTextshadow"] = false,
					["ObjectiveAndRespawn_Textcolor"] = {
						1, -- [1]
						1, -- [2]
						1, -- [3]
						1, -- [4]
					},
					["Auras_Debuffs_VerticalGrowdirection"] = "upwards",
					["DrTracking_Enabled"] = true,
					["Auras_Debuffs_SpellIDFiltering_Enabled"] = false,
					["PlayerCount_Textcolor"] = {
						1, -- [1]
						1, -- [2]
						1, -- [3]
						1, -- [4]
					},
					["Spec_Width"] = 36,
					["ObjectiveAndRespawn_Width"] = 36,
					["Racial_BasicPoint"] = "LEFT",
					["HealthBar_Texture"] = "UI-StatusBar",
					["Trinket_RelativePoint"] = "RIGHT",
					["DrTracking_Container_RelativePoint"] = "LEFT",
					["HealthBar_Background"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						0.66, -- [4]
					},
					["Name_Fontsize"] = 13,
					["Auras_Buffs_Container_RelativePoint"] = "BOTTOMLEFT",
					["Spec_Enabled"] = true,
					["Auras_Buffs_HorizontalSpacing"] = 1,
					["Trinket_Cooldown_Outline"] = "OUTLINE",
					["PowerBar_Background"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						0.66, -- [4]
					},
					["NumericTargetindicator_Outline"] = "",
					["Auras_Debuffs_SpellIDFiltering_Filterlist"] = {
					},
					["Auras_Buffs_Size"] = 15,
					["HealthBar_HealthPrediction_Enabled"] = true,
					["RacialFiltering_Enabled"] = false,
					["Auras_Buffs_Fontsize"] = 12,
					["Auras_Debuffs_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["Position_X"] = 1076.711850933614,
					["Racial_Cooldown_Outline"] = "OUTLINE",
					["Racial_OffsetX"] = 2,
					["CovenantIcon_Size"] = 20,
					["ObjectiveAndRespawn_RelativePoint"] = "LEFT",
					["Auras_Buffs_Filtering_Blizzlike"] = false,
					["Auras_Debuffs_Filtering_Enabled"] = false,
					["Auras_Debuffs_Cooldown_Outline"] = "OUTLINE",
					["RoleIcon_Enabled"] = true,
					["Racial_Width"] = 28,
					["PowerBar_Enabled"] = false,
					["Auras_Debuffs_VerticalSpacing"] = 1,
					["BarHorizontalGrowdirection"] = "rightwards",
					["Auras_Debuffs_ShowStealOrPurgeable"] = true,
					["Racial_Enabled"] = true,
					["Auras_Debuffs_ShowMine"] = true,
					["ObjectiveAndRespawn_RelativeTo"] = "NumericTargetindicator",
					["Auras_Buffs_EnableTextshadow"] = true,
					["DrTracking_Cooldown_EnableTextshadow"] = false,
					["Auras_Buffs_SpellIDFiltering_Enabled"] = false,
					["DrTracking_Container_Color"] = {
						0, -- [1]
						0, -- [2]
						1, -- [3]
						1, -- [4]
					},
					["DrTracking_Cooldown_Fontsize"] = 12,
					["PowerBar_Height"] = 4,
					["Auras_Debuffs_Filtering_Blizzlike"] = false,
					["Auras_Buffs_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["ObjectiveAndRespawn_Fontsize"] = 17,
					["RacialFiltering_Filterlist"] = {
					},
					["Spec_AuraDisplay_ShowNumbers"] = true,
					["Auras_Buffs_Cooldown_Fontsize"] = 12,
					["Auras_Debuffs_Cooldown_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["ObjectiveAndRespawn_OffsetX"] = -2,
					["Auras_Debuffs_Cooldown_Fontsize"] = 12,
					["DrTracking_Container_RelativeTo"] = "Button",
					["Spec_AuraDisplay_Enabled"] = true,
					["Auras_Debuffs_Outline"] = "OUTLINE",
					["Auras_Debuffs_Coloring_Enabled"] = true,
					["Auras_Debuffs_Container_OffsetY"] = 0,
					["Auras_Debuffs_Container_RelativeTo"] = "DRContainer",
					["BarHeight"] = 28,
					["Auras_Buffs_Container_Point"] = "BOTTOMRIGHT",
					["Auras_Buffs_SpellIDFiltering_Filterlist"] = {
					},
					["Auras_Buffs_VerticalGrowdirection"] = "upwards",
					["Spec_AuraDisplay_Cooldown_EnableTextshadow"] = false,
					["Auras_Buffs_ShowMine"] = true,
					["DrTracking_GrowDirection"] = "leftwards",
					["RoleIcon_Size"] = 13,
					["PlayerCount_Fontsize"] = 14,
					["Auras_Buffs_Cooldown_EnableTextshadow"] = false,
					["Auras_Buffs_Textcolor"] = {
						1, -- [1]
						1, -- [2]
						1, -- [3]
						1, -- [4]
					},
					["Enabled"] = true,
					["Spec_AuraDisplay_Cooldown_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["Auras_Debuffs_DebuffTypeFiltering_Filterlist"] = {
					},
					["Auras_Debuffs_Container_Point"] = "RIGHT",
					["DrTrackingFiltering_Enabled"] = false,
					["Racial_ShowNumbers"] = true,
					["Auras_Buffs_Container_OffsetX"] = 2,
					["Name_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["Name_EnableTextshadow"] = true,
					["Trinket_Cooldown_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["Auras_Debuffs_Cooldown_EnableTextshadow"] = false,
					["Auras_Debuffs_ShowNumbers"] = true,
					["PlayerCount_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["Auras_Debuffs_Textcolor"] = {
						1, -- [1]
						1, -- [2]
						1, -- [3]
						1, -- [4]
					},
					["ObjectiveAndRespawn_BasicPoint"] = "RIGHT",
					["PlayerCount_Outline"] = "OUTLINE",
				},
				["RightButtonType"] = "Focus",
				["LevelText_Enabled"] = false,
				["RangeIndicator_Alpha"] = 0.55,
				["LevelText_OnlyShowIfNotMaxLevel"] = true,
				["RangeIndicator_Enabled"] = true,
				["LevelText_Outline"] = "",
			},
			["ShowTooltips"] = true,
			["Font"] = "PT Sans Narrow Bold",
			["RBG"] = {
				["TargetCalling_SetMark"] = false,
				["ObjectiveAndRespawn_RespawnEnabled"] = true,
				["TargetCalling_NotificationEnable"] = false,
				["ObjectiveAndRespawn_Cooldown_Outline"] = "OUTLINE",
				["ObjectiveAndRespawn_Cooldown_TextShadowcolor"] = {
					0, -- [1]
					0, -- [2]
					0, -- [3]
					1, -- [4]
				},
				["ObjectiveAndRespawn_Cooldown_EnableTextshadow"] = false,
				["ObjectiveAndRespawn_ShowNumbers"] = true,
				["ObjectiveAndRespawn_Cooldown_Fontsize"] = 12,
			},
			["Highlight_Color"] = {
				1, -- [1]
				1, -- [2]
				0.5, -- [3]
				1, -- [4]
			},
			["Locked"] = false,
			["lastReadVersion"] = "9.2.0.9",
			["Allies"] = {
				["ConvertCyrillic"] = true,
				["40"] = {
					["DrTracking_Cooldown_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["Auras_Debuffs_Container_RelativeTo"] = "BuffContainer",
					["NumericTargetindicator_Enabled"] = true,
					["Auras_Buffs_ShowStealOrPurgeable"] = true,
					["NumericTargetindicator_Fontsize"] = 18,
					["DrTracking_DisplayType"] = "Countdowntext",
					["PlayerCount_EnableTextshadow"] = false,
					["Auras_Buffs_Container_OffsetY"] = 0,
					["Auras_Debuffs_DebuffTypeFiltering_Enabled"] = true,
					["Auras_Buffs_HorizontalGrowDirection"] = "leftwards",
					["Auras_Debuffs_Container_OffsetX"] = 2,
					["Auras_Debuffs_DisplayType"] = "Frame",
					["NumericTargetindicator_EnableTextshadow"] = false,
					["CovenantIcon_Enabled"] = true,
					["Position_Y"] = 618.4599080513435,
					["Trinket_Width"] = 28,
					["Spec_AuraDisplay_Cooldown_Fontsize"] = 12,
					["CovenantIcon_VerticalPosition"] = 3,
					["BarVerticalSpacing"] = 1,
					["Auras_Buffs_Outline"] = "OUTLINE",
					["Auras_Debuffs_EnableTextshadow"] = true,
					["Auras_Debuffs_Size"] = 15,
					["Racial_RelativePoint"] = "LEFT",
					["DrTracking_HorizontalSpacing"] = 1,
					["PlayerCount_Enabled"] = true,
					["Auras_Debuffs_HorizontalSpacing"] = 1,
					["RacialFiltering_Filterlist"] = {
					},
					["Trinket_RelativePoint"] = "LEFT",
					["Racial_Cooldown_Fontsize"] = 12,
					["RoleIcon_VerticalPosition"] = 2,
					["Racial_OffsetX"] = 1,
					["Auras_Debuffs_HorizontalGrowDirection"] = "leftwards",
					["Auras_Buffs_IconsPerRow"] = 8,
					["Trinket_Enabled"] = true,
					["Auras_Debuffs_SpellIDFiltering_Enabled"] = false,
					["NumericTargetindicator_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["Trinket_ShowNumbers"] = true,
					["DrTracking_Cooldown_Outline"] = "OUTLINE",
					["Auras_Debuffs_Fontsize"] = 12,
					["DrTracking_ShowNumbers"] = true,
					["NumericTargetindicator_Textcolor"] = {
						1, -- [1]
						1, -- [2]
						1, -- [3]
						1, -- [4]
					},
					["DrTracking_Container_OffsetX"] = 1,
					["SymbolicTargetindicator_Enabled"] = true,
					["Auras_Debuffs_Container_RelativePoint"] = "BOTTOMLEFT",
					["DrTrackingFiltering_Filterlist"] = {
					},
					["Racial_Cooldown_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["Trinket_BasicPoint"] = "RIGHT",
					["Trinket_RelativeTo"] = "Button",
					["Auras_Debuffs_Enabled"] = true,
					["Auras_Buffs_Filtering_Enabled"] = false,
					["Name_Outline"] = "",
					["Name_Textcolor"] = {
						1, -- [1]
						1, -- [2]
						1, -- [3]
						1, -- [4]
					},
					["Racial_RelativeTo"] = "Trinket",
					["Spec_AuraDisplay_Cooldown_Outline"] = "OUTLINE",
					["PowerBar_Texture"] = "UI-StatusBar",
					["Auras_Buffs_Cooldown_Outline"] = "OUTLINE",
					["DrTracking_Container_BasicPoint"] = "LEFT",
					["Auras_Buffs_ShowNumbers"] = true,
					["Auras_Buffs_Enabled"] = true,
					["Trinket_Cooldown_Fontsize"] = 12,
					["Auras_Buffs_VerticalSpacing"] = 1,
					["Trinket_Cooldown_EnableTextshadow"] = false,
					["BarHorizontalSpacing"] = 100,
					["Framescale"] = 1,
					["Racial_Cooldown_EnableTextshadow"] = false,
					["BarVerticalGrowdirection"] = "downwards",
					["BarWidth"] = 220,
					["Auras_Buffs_Container_RelativeTo"] = "Button",
					["DrTracking_Container_BorderThickness"] = 1,
					["Auras_Buffs_Cooldown_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["Auras_Debuffs_VerticalGrowdirection"] = "upwards",
					["PlayerCount_Textcolor"] = {
						1, -- [1]
						1, -- [2]
						1, -- [3]
						1, -- [4]
					},
					["PlayerCount_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["HealthBar_Texture"] = "UI-StatusBar",
					["HealthBar_Background"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						0.66, -- [4]
					},
					["Name_Fontsize"] = 13,
					["Auras_Buffs_Container_RelativePoint"] = "BOTTOMLEFT",
					["Spec_Enabled"] = true,
					["Racial_Width"] = 28,
					["Racial_BasicPoint"] = "RIGHT",
					["NumericTargetindicator_Outline"] = "",
					["Auras_Enabled"] = false,
					["Auras_Buffs_Size"] = 15,
					["Racial_ShowNumbers"] = true,
					["RacialFiltering_Enabled"] = false,
					["Auras_Buffs_Fontsize"] = 12,
					["Auras_Debuffs_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["Position_X"] = 39.85734041771275,
					["BarColumns"] = 1,
					["Trinket_OffsetX"] = 1,
					["CovenantIcon_Size"] = 20,
					["Trinket_Cooldown_Outline"] = "OUTLINE",
					["Auras_Buffs_HorizontalSpacing"] = 1,
					["Auras_Debuffs_Filtering_Enabled"] = false,
					["Auras_Debuffs_Cooldown_Outline"] = "OUTLINE",
					["Racial_Cooldown_Outline"] = "OUTLINE",
					["Racial_Enabled"] = true,
					["PowerBar_Background"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						0.66, -- [4]
					},
					["Auras_Debuffs_VerticalSpacing"] = 1,
					["BarHorizontalGrowdirection"] = "rightwards",
					["Trinket_Cooldown_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["Auras_Debuffs_SpellIDFiltering_Filterlist"] = {
					},
					["HealthBar_HealthPrediction_Enabled"] = true,
					["PowerBar_Enabled"] = false,
					["Auras_Buffs_EnableTextshadow"] = true,
					["DrTracking_Cooldown_EnableTextshadow"] = false,
					["Auras_Buffs_SpellIDFiltering_Enabled"] = false,
					["DrTracking_Container_Color"] = {
						0, -- [1]
						0, -- [2]
						1, -- [3]
						1, -- [4]
					},
					["DrTracking_Cooldown_Fontsize"] = 12,
					["PowerBar_Height"] = 4,
					["Auras_Debuffs_Filtering_Blizzlike"] = false,
					["PlayerCount_Fontsize"] = 14,
					["Auras_Buffs_Filtering_Blizzlike"] = false,
					["Auras_Debuffs_Cooldown_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["Auras_Buffs_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["Spec_AuraDisplay_Cooldown_EnableTextshadow"] = false,
					["DrTracking_Container_RelativePoint"] = "RIGHT",
					["Auras_Debuffs_Cooldown_Fontsize"] = 12,
					["DrTracking_Container_RelativeTo"] = "Button",
					["RoleIcon_Enabled"] = true,
					["Spec_AuraDisplay_Enabled"] = true,
					["Auras_Debuffs_Outline"] = "OUTLINE",
					["Auras_Buffs_SpellIDFiltering_Filterlist"] = {
					},
					["Auras_Debuffs_Container_OffsetY"] = 0,
					["Spec_AuraDisplay_ShowNumbers"] = true,
					["BarHeight"] = 22,
					["Auras_Buffs_Container_Point"] = "BOTTOMRIGHT",
					["Auras_Debuffs_IconsPerRow"] = 8,
					["Auras_Buffs_VerticalGrowdirection"] = "upwards",
					["Auras_Debuffs_Coloring_Enabled"] = true,
					["Auras_Buffs_ShowMine"] = true,
					["DrTracking_GrowDirection"] = "rightwards",
					["RoleIcon_Size"] = 13,
					["Auras_Buffs_Cooldown_EnableTextshadow"] = false,
					["Auras_Buffs_Cooldown_Fontsize"] = 12,
					["Auras_Buffs_Textcolor"] = {
						1, -- [1]
						1, -- [2]
						1, -- [3]
						1, -- [4]
					},
					["Enabled"] = false,
					["Spec_AuraDisplay_Cooldown_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["Auras_Buffs_Container_OffsetX"] = 2,
					["Auras_Debuffs_Container_Point"] = "BOTTOMRIGHT",
					["DrTrackingFiltering_Enabled"] = false,
					["Auras_Debuffs_ShowMine"] = true,
					["Auras_Debuffs_ShowStealOrPurgeable"] = true,
					["Name_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["Name_EnableTextshadow"] = true,
					["Auras_Debuffs_DebuffTypeFiltering_Filterlist"] = {
					},
					["Auras_Debuffs_Cooldown_EnableTextshadow"] = false,
					["Auras_Debuffs_ShowNumbers"] = true,
					["DrTracking_Enabled"] = true,
					["Auras_Debuffs_Textcolor"] = {
						1, -- [1]
						1, -- [2]
						1, -- [3]
						1, -- [4]
					},
					["Spec_Width"] = 36,
					["PlayerCount_Outline"] = "OUTLINE",
				},
				["LeftButtonValue"] = "",
				["LevelText_EnableTextshadow"] = false,
				["LeftButtonType"] = "Target",
				["MiddleButtonType"] = "Custom",
				["RangeIndicator_Everything"] = false,
				["RightButtonValue"] = "",
				["Enabled"] = true,
				["RangeIndicator_Range"] = 34471,
				["LevelText_TextShadowcolor"] = {
					0, -- [1]
					0, -- [2]
					0, -- [3]
					1, -- [4]
				},
				["LevelText_Fontsize"] = 18,
				["LevelText_Textcolor"] = {
					1, -- [1]
					1, -- [2]
					1, -- [3]
					1, -- [4]
				},
				["ShowRealmnames"] = true,
				["5"] = {
					["DrTracking_Cooldown_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["Auras_Debuffs_IconsPerRow"] = 8,
					["NumericTargetindicator_Enabled"] = true,
					["Auras_Buffs_ShowStealOrPurgeable"] = true,
					["NumericTargetindicator_Fontsize"] = 18,
					["DrTracking_DisplayType"] = "Countdowntext",
					["ObjectiveAndRespawn_ObjectiveEnabled"] = false,
					["PlayerCount_EnableTextshadow"] = false,
					["Auras_Buffs_Container_OffsetY"] = 0,
					["Auras_Debuffs_DebuffTypeFiltering_Enabled"] = true,
					["Auras_Buffs_HorizontalGrowDirection"] = "leftwards",
					["Auras_Debuffs_Container_OffsetX"] = 2,
					["Auras_Debuffs_DisplayType"] = "Frame",
					["NumericTargetindicator_EnableTextshadow"] = false,
					["CovenantIcon_Enabled"] = true,
					["Position_Y"] = 586.4146353301112,
					["Trinket_Width"] = 28,
					["Spec_AuraDisplay_Cooldown_Fontsize"] = 12,
					["CovenantIcon_VerticalPosition"] = 3,
					["BarVerticalSpacing"] = 1,
					["Auras_Buffs_Outline"] = "OUTLINE",
					["Auras_Debuffs_EnableTextshadow"] = true,
					["Auras_Debuffs_Size"] = 15,
					["Racial_RelativePoint"] = "LEFT",
					["DrTracking_HorizontalSpacing"] = 1,
					["PlayerCount_Enabled"] = true,
					["Auras_Debuffs_HorizontalSpacing"] = 1,
					["BarColumns"] = 1,
					["Trinket_OffsetX"] = -1,
					["Racial_Cooldown_Fontsize"] = 12,
					["RoleIcon_VerticalPosition"] = 2,
					["Auras_Enabled"] = false,
					["Auras_Debuffs_HorizontalGrowDirection"] = "leftwards",
					["Auras_Buffs_IconsPerRow"] = 8,
					["Trinket_Enabled"] = true,
					["ObjectiveAndRespawn_Outline"] = "THICKOUTLINE",
					["NumericTargetindicator_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["Trinket_ShowNumbers"] = true,
					["DrTracking_Cooldown_Outline"] = "OUTLINE",
					["Auras_Debuffs_Fontsize"] = 12,
					["DrTracking_ShowNumbers"] = true,
					["NumericTargetindicator_Textcolor"] = {
						1, -- [1]
						1, -- [2]
						1, -- [3]
						1, -- [4]
					},
					["DrTracking_Container_OffsetX"] = 1,
					["SymbolicTargetindicator_Enabled"] = true,
					["Auras_Debuffs_Container_RelativePoint"] = "BOTTOMLEFT",
					["DrTrackingFiltering_Filterlist"] = {
					},
					["Racial_Cooldown_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["Trinket_BasicPoint"] = "RIGHT",
					["Trinket_RelativeTo"] = "Button",
					["Auras_Debuffs_Enabled"] = true,
					["Auras_Buffs_Filtering_Enabled"] = false,
					["Name_Outline"] = "",
					["ObjectiveAndRespawn_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["Name_Textcolor"] = {
						1, -- [1]
						1, -- [2]
						1, -- [3]
						1, -- [4]
					},
					["Racial_RelativeTo"] = "Trinket",
					["Spec_AuraDisplay_Cooldown_Outline"] = "OUTLINE",
					["PowerBar_Texture"] = "UI-StatusBar",
					["Auras_Buffs_Cooldown_Outline"] = "OUTLINE",
					["DrTracking_Container_BasicPoint"] = "LEFT",
					["Auras_Buffs_ShowNumbers"] = true,
					["Auras_Buffs_Enabled"] = false,
					["Trinket_Cooldown_Fontsize"] = 12,
					["Auras_Buffs_VerticalSpacing"] = 1,
					["Trinket_Cooldown_EnableTextshadow"] = false,
					["BarHorizontalSpacing"] = 100,
					["Framescale"] = 1,
					["Racial_Cooldown_EnableTextshadow"] = false,
					["BarVerticalGrowdirection"] = "downwards",
					["BarWidth"] = 220,
					["Auras_Buffs_Container_RelativeTo"] = "Button",
					["DrTracking_Container_BorderThickness"] = 1,
					["Auras_Buffs_Cooldown_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["ObjectiveAndRespawn_EnableTextshadow"] = false,
					["ObjectiveAndRespawn_Textcolor"] = {
						1, -- [1]
						1, -- [2]
						1, -- [3]
						1, -- [4]
					},
					["Auras_Debuffs_VerticalGrowdirection"] = "upwards",
					["DrTracking_Enabled"] = true,
					["Auras_Debuffs_SpellIDFiltering_Enabled"] = false,
					["PlayerCount_Textcolor"] = {
						1, -- [1]
						1, -- [2]
						1, -- [3]
						1, -- [4]
					},
					["Spec_Width"] = 36,
					["ObjectiveAndRespawn_Width"] = 36,
					["Racial_BasicPoint"] = "RIGHT",
					["HealthBar_Texture"] = "UI-StatusBar",
					["Trinket_RelativePoint"] = "LEFT",
					["DrTracking_Container_RelativePoint"] = "RIGHT",
					["HealthBar_Background"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						0.66, -- [4]
					},
					["Name_Fontsize"] = 13,
					["Auras_Buffs_Container_RelativePoint"] = "BOTTOMLEFT",
					["Spec_Enabled"] = true,
					["Auras_Buffs_HorizontalSpacing"] = 1,
					["Trinket_Cooldown_Outline"] = "OUTLINE",
					["PowerBar_Background"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						0.66, -- [4]
					},
					["NumericTargetindicator_Outline"] = "",
					["Auras_Debuffs_SpellIDFiltering_Filterlist"] = {
					},
					["Auras_Buffs_Size"] = 15,
					["HealthBar_HealthPrediction_Enabled"] = true,
					["RacialFiltering_Enabled"] = false,
					["Auras_Buffs_Fontsize"] = 12,
					["Auras_Debuffs_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["Position_X"] = 174.0129257980079,
					["Racial_Cooldown_Outline"] = "OUTLINE",
					["Racial_OffsetX"] = 1,
					["CovenantIcon_Size"] = 20,
					["ObjectiveAndRespawn_RelativePoint"] = "LEFT",
					["Auras_Buffs_Filtering_Blizzlike"] = false,
					["Auras_Debuffs_Filtering_Enabled"] = false,
					["Auras_Debuffs_Cooldown_Outline"] = "OUTLINE",
					["RoleIcon_Enabled"] = true,
					["Racial_Width"] = 28,
					["PowerBar_Enabled"] = false,
					["Auras_Debuffs_VerticalSpacing"] = 1,
					["BarHorizontalGrowdirection"] = "rightwards",
					["Auras_Debuffs_ShowStealOrPurgeable"] = true,
					["Racial_Enabled"] = true,
					["Auras_Debuffs_ShowMine"] = true,
					["ObjectiveAndRespawn_RelativeTo"] = "NumericTargetindicator",
					["Auras_Buffs_EnableTextshadow"] = true,
					["DrTracking_Cooldown_EnableTextshadow"] = false,
					["Auras_Buffs_SpellIDFiltering_Enabled"] = false,
					["DrTracking_Container_Color"] = {
						0, -- [1]
						0, -- [2]
						1, -- [3]
						1, -- [4]
					},
					["DrTracking_Cooldown_Fontsize"] = 12,
					["PowerBar_Height"] = 4,
					["Auras_Debuffs_Filtering_Blizzlike"] = false,
					["Auras_Buffs_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["ObjectiveAndRespawn_Fontsize"] = 17,
					["RacialFiltering_Filterlist"] = {
					},
					["Spec_AuraDisplay_ShowNumbers"] = true,
					["Auras_Buffs_Cooldown_Fontsize"] = 12,
					["Auras_Debuffs_Cooldown_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["ObjectiveAndRespawn_OffsetX"] = -2,
					["Auras_Debuffs_Cooldown_Fontsize"] = 12,
					["DrTracking_Container_RelativeTo"] = "Button",
					["Spec_AuraDisplay_Enabled"] = true,
					["Auras_Debuffs_Outline"] = "OUTLINE",
					["Auras_Debuffs_Coloring_Enabled"] = true,
					["Auras_Debuffs_Container_OffsetY"] = 0,
					["Auras_Debuffs_Container_RelativeTo"] = "BuffContainer",
					["BarHeight"] = 28,
					["Auras_Buffs_Container_Point"] = "BOTTOMRIGHT",
					["Auras_Buffs_SpellIDFiltering_Filterlist"] = {
					},
					["Auras_Buffs_VerticalGrowdirection"] = "upwards",
					["Spec_AuraDisplay_Cooldown_EnableTextshadow"] = false,
					["Auras_Buffs_ShowMine"] = true,
					["DrTracking_GrowDirection"] = "rightwards",
					["RoleIcon_Size"] = 13,
					["PlayerCount_Fontsize"] = 14,
					["Auras_Buffs_Cooldown_EnableTextshadow"] = false,
					["Auras_Buffs_Textcolor"] = {
						1, -- [1]
						1, -- [2]
						1, -- [3]
						1, -- [4]
					},
					["Enabled"] = true,
					["Spec_AuraDisplay_Cooldown_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["Auras_Debuffs_DebuffTypeFiltering_Filterlist"] = {
					},
					["Auras_Debuffs_Container_Point"] = "BOTTOMRIGHT",
					["DrTrackingFiltering_Enabled"] = false,
					["Racial_ShowNumbers"] = true,
					["Auras_Buffs_Container_OffsetX"] = 2,
					["Name_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["Name_EnableTextshadow"] = true,
					["Trinket_Cooldown_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["Auras_Debuffs_Cooldown_EnableTextshadow"] = false,
					["Auras_Debuffs_ShowNumbers"] = true,
					["PlayerCount_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["Auras_Debuffs_Textcolor"] = {
						1, -- [1]
						1, -- [2]
						1, -- [3]
						1, -- [4]
					},
					["ObjectiveAndRespawn_BasicPoint"] = "RIGHT",
					["PlayerCount_Outline"] = "OUTLINE",
				},
				["MiddleButtonValue"] = "",
				["RangeIndicator_Frames"] = {
				},
				["15"] = {
					["DrTracking_Cooldown_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["Auras_Debuffs_IconsPerRow"] = 8,
					["NumericTargetindicator_Enabled"] = true,
					["Auras_Buffs_ShowStealOrPurgeable"] = true,
					["NumericTargetindicator_Fontsize"] = 18,
					["DrTracking_DisplayType"] = "Countdowntext",
					["ObjectiveAndRespawn_ObjectiveEnabled"] = true,
					["PlayerCount_EnableTextshadow"] = false,
					["Auras_Buffs_Container_OffsetY"] = 0,
					["Auras_Debuffs_DebuffTypeFiltering_Enabled"] = true,
					["Auras_Buffs_HorizontalGrowDirection"] = "leftwards",
					["Auras_Debuffs_Container_OffsetX"] = 2,
					["Auras_Debuffs_DisplayType"] = "Frame",
					["NumericTargetindicator_EnableTextshadow"] = false,
					["CovenantIcon_Enabled"] = true,
					["Position_Y"] = 579.3529948629512,
					["Trinket_Width"] = 28,
					["Spec_AuraDisplay_Cooldown_Fontsize"] = 12,
					["CovenantIcon_VerticalPosition"] = 3,
					["BarVerticalSpacing"] = 1,
					["Auras_Buffs_Outline"] = "OUTLINE",
					["Auras_Debuffs_EnableTextshadow"] = true,
					["Auras_Debuffs_Size"] = 15,
					["Racial_RelativePoint"] = "LEFT",
					["DrTracking_HorizontalSpacing"] = 1,
					["PlayerCount_Enabled"] = true,
					["Auras_Debuffs_HorizontalSpacing"] = 1,
					["BarColumns"] = 1,
					["Trinket_OffsetX"] = -1,
					["Racial_Cooldown_Fontsize"] = 12,
					["RoleIcon_VerticalPosition"] = 2,
					["Auras_Enabled"] = false,
					["Auras_Debuffs_HorizontalGrowDirection"] = "leftwards",
					["Auras_Buffs_IconsPerRow"] = 8,
					["Trinket_Enabled"] = true,
					["ObjectiveAndRespawn_Outline"] = "THICKOUTLINE",
					["NumericTargetindicator_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["Trinket_ShowNumbers"] = true,
					["DrTracking_Cooldown_Outline"] = "OUTLINE",
					["Auras_Debuffs_Fontsize"] = 12,
					["DrTracking_ShowNumbers"] = true,
					["NumericTargetindicator_Textcolor"] = {
						1, -- [1]
						1, -- [2]
						1, -- [3]
						1, -- [4]
					},
					["DrTracking_Container_OffsetX"] = 1,
					["SymbolicTargetindicator_Enabled"] = true,
					["Auras_Debuffs_Container_RelativePoint"] = "BOTTOMLEFT",
					["DrTrackingFiltering_Filterlist"] = {
					},
					["Racial_Cooldown_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["Trinket_BasicPoint"] = "RIGHT",
					["Trinket_RelativeTo"] = "Button",
					["Auras_Debuffs_Enabled"] = true,
					["Auras_Buffs_Filtering_Enabled"] = false,
					["Name_Outline"] = "",
					["ObjectiveAndRespawn_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["Name_Textcolor"] = {
						1, -- [1]
						1, -- [2]
						1, -- [3]
						1, -- [4]
					},
					["Racial_RelativeTo"] = "Trinket",
					["Spec_AuraDisplay_Cooldown_Outline"] = "OUTLINE",
					["PowerBar_Texture"] = "UI-StatusBar",
					["Auras_Buffs_Cooldown_Outline"] = "OUTLINE",
					["DrTracking_Container_BasicPoint"] = "LEFT",
					["Auras_Buffs_ShowNumbers"] = true,
					["Auras_Buffs_Enabled"] = false,
					["Trinket_Cooldown_Fontsize"] = 12,
					["Auras_Buffs_VerticalSpacing"] = 1,
					["Trinket_Cooldown_EnableTextshadow"] = false,
					["BarHorizontalSpacing"] = 100,
					["Framescale"] = 1,
					["Racial_Cooldown_EnableTextshadow"] = false,
					["BarVerticalGrowdirection"] = "downwards",
					["BarWidth"] = 220,
					["Auras_Buffs_Container_RelativeTo"] = "Button",
					["DrTracking_Container_BorderThickness"] = 1,
					["Auras_Buffs_Cooldown_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["ObjectiveAndRespawn_EnableTextshadow"] = false,
					["ObjectiveAndRespawn_Textcolor"] = {
						1, -- [1]
						1, -- [2]
						1, -- [3]
						1, -- [4]
					},
					["Auras_Debuffs_VerticalGrowdirection"] = "upwards",
					["DrTracking_Enabled"] = true,
					["Auras_Debuffs_SpellIDFiltering_Enabled"] = false,
					["PlayerCount_Textcolor"] = {
						1, -- [1]
						1, -- [2]
						1, -- [3]
						1, -- [4]
					},
					["Spec_Width"] = 36,
					["ObjectiveAndRespawn_Width"] = 36,
					["Racial_BasicPoint"] = "RIGHT",
					["HealthBar_Texture"] = "UI-StatusBar",
					["Trinket_RelativePoint"] = "LEFT",
					["DrTracking_Container_RelativePoint"] = "RIGHT",
					["HealthBar_Background"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						0.66, -- [4]
					},
					["Name_Fontsize"] = 13,
					["Auras_Buffs_Container_RelativePoint"] = "BOTTOMLEFT",
					["Spec_Enabled"] = true,
					["Auras_Buffs_HorizontalSpacing"] = 1,
					["Trinket_Cooldown_Outline"] = "OUTLINE",
					["PowerBar_Background"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						0.66, -- [4]
					},
					["NumericTargetindicator_Outline"] = "",
					["Auras_Debuffs_SpellIDFiltering_Filterlist"] = {
					},
					["Auras_Buffs_Size"] = 15,
					["HealthBar_HealthPrediction_Enabled"] = true,
					["RacialFiltering_Enabled"] = false,
					["Auras_Buffs_Fontsize"] = 12,
					["Auras_Debuffs_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["Position_X"] = 105.9122046639277,
					["Racial_Cooldown_Outline"] = "OUTLINE",
					["Racial_OffsetX"] = 1,
					["CovenantIcon_Size"] = 20,
					["ObjectiveAndRespawn_RelativePoint"] = "LEFT",
					["Auras_Buffs_Filtering_Blizzlike"] = false,
					["Auras_Debuffs_Filtering_Enabled"] = false,
					["Auras_Debuffs_Cooldown_Outline"] = "OUTLINE",
					["RoleIcon_Enabled"] = true,
					["Racial_Width"] = 28,
					["PowerBar_Enabled"] = false,
					["Auras_Debuffs_VerticalSpacing"] = 1,
					["BarHorizontalGrowdirection"] = "rightwards",
					["Auras_Debuffs_ShowStealOrPurgeable"] = true,
					["Racial_Enabled"] = true,
					["Auras_Debuffs_ShowMine"] = true,
					["ObjectiveAndRespawn_RelativeTo"] = "NumericTargetindicator",
					["Auras_Buffs_EnableTextshadow"] = true,
					["DrTracking_Cooldown_EnableTextshadow"] = false,
					["Auras_Buffs_SpellIDFiltering_Enabled"] = false,
					["DrTracking_Container_Color"] = {
						0, -- [1]
						0, -- [2]
						1, -- [3]
						1, -- [4]
					},
					["DrTracking_Cooldown_Fontsize"] = 12,
					["PowerBar_Height"] = 4,
					["Auras_Debuffs_Filtering_Blizzlike"] = false,
					["Auras_Buffs_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["ObjectiveAndRespawn_Fontsize"] = 17,
					["RacialFiltering_Filterlist"] = {
					},
					["Spec_AuraDisplay_ShowNumbers"] = true,
					["Auras_Buffs_Cooldown_Fontsize"] = 12,
					["Auras_Debuffs_Cooldown_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["ObjectiveAndRespawn_OffsetX"] = -2,
					["Auras_Debuffs_Cooldown_Fontsize"] = 12,
					["DrTracking_Container_RelativeTo"] = "Button",
					["Spec_AuraDisplay_Enabled"] = true,
					["Auras_Debuffs_Outline"] = "OUTLINE",
					["Auras_Debuffs_Coloring_Enabled"] = true,
					["Auras_Debuffs_Container_OffsetY"] = 0,
					["Auras_Debuffs_Container_RelativeTo"] = "BuffContainer",
					["BarHeight"] = 28,
					["Auras_Buffs_Container_Point"] = "BOTTOMRIGHT",
					["Auras_Buffs_SpellIDFiltering_Filterlist"] = {
					},
					["Auras_Buffs_VerticalGrowdirection"] = "upwards",
					["Spec_AuraDisplay_Cooldown_EnableTextshadow"] = false,
					["Auras_Buffs_ShowMine"] = true,
					["DrTracking_GrowDirection"] = "rightwards",
					["RoleIcon_Size"] = 13,
					["PlayerCount_Fontsize"] = 14,
					["Auras_Buffs_Cooldown_EnableTextshadow"] = false,
					["Auras_Buffs_Textcolor"] = {
						1, -- [1]
						1, -- [2]
						1, -- [3]
						1, -- [4]
					},
					["Enabled"] = true,
					["Spec_AuraDisplay_Cooldown_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["Auras_Debuffs_DebuffTypeFiltering_Filterlist"] = {
					},
					["Auras_Debuffs_Container_Point"] = "BOTTOMRIGHT",
					["DrTrackingFiltering_Enabled"] = false,
					["Racial_ShowNumbers"] = true,
					["Auras_Buffs_Container_OffsetX"] = 2,
					["Name_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["Name_EnableTextshadow"] = true,
					["Trinket_Cooldown_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["Auras_Debuffs_Cooldown_EnableTextshadow"] = false,
					["Auras_Debuffs_ShowNumbers"] = true,
					["PlayerCount_TextShadowcolor"] = {
						0, -- [1]
						0, -- [2]
						0, -- [3]
						1, -- [4]
					},
					["Auras_Debuffs_Textcolor"] = {
						1, -- [1]
						1, -- [2]
						1, -- [3]
						1, -- [4]
					},
					["ObjectiveAndRespawn_BasicPoint"] = "RIGHT",
					["PlayerCount_Outline"] = "OUTLINE",
				},
				["RightButtonType"] = "Focus",
				["LevelText_Enabled"] = false,
				["RangeIndicator_Alpha"] = 0.55,
				["LevelText_OnlyShowIfNotMaxLevel"] = true,
				["RangeIndicator_Enabled"] = true,
				["LevelText_Outline"] = "",
			},
			["MyTarget_Color"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
				1, -- [4]
			},
		},
	},
}
