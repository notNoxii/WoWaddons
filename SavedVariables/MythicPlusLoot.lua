
MythicPlusLootDB = {
	["global"] = {
		["minimap"] = {
			["minimapPos"] = 198.7257664946987,
			["hide"] = false,
		},
	},
	["profileKeys"] = {
		["Noxiipally - Area 52"] = "Noxiipally - Area 52",
		["Noxiishields - Area 52"] = "Default",
	},
	["profiles"] = {
		["Noxiipally - Area 52"] = {
			["armorType"] = 4,
			["source"] = 0,
			["slot"] = 11,
			["favoriteItems"] = {
			},
			["mythicLevel"] = 18,
			["v"] = 1,
		},
	},
}
