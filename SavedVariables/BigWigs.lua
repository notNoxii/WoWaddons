
BigWigs3DB = {
	["global"] = {
		["watchedMovies"] = {
			[-2002] = true,
			[-437] = true,
			[952] = true,
			[-593] = {
				true, -- [1]
				true, -- [2]
			},
			[956] = true,
			[-2000] = true,
			[-2004] = true,
			[-573] = true,
			[-575] = true,
			[958] = true,
		},
	},
	["namespaces"] = {
		["BigWigs_Bosses_Margrave Stradama"] = {
			["profiles"] = {
				["Default"] = {
					[322304] = 3064055,
					[322232] = 3064055,
					["stages"] = 3064055,
					[322475] = 3064055,
				},
			},
		},
		["BigWigs_Plugins_Alt Power"] = {
		},
		["BigWigs_Plugins_BossBlock"] = {
			["profiles"] = {
				["Default"] = {
					["disableAmbience"] = true,
				},
			},
		},
		["BigWigs_Bosses_Lords of Dread"] = {
			["profiles"] = {
				["Default"] = {
					[360428] = 3064055,
					[360717] = 3129591,
					["custom_off_361745"] = false,
					[362020] = 3064055,
					[360417] = 3064055,
					[360319] = 3129591,
					[366574] = 3064055,
				},
			},
		},
		["BigWigs_Bosses_So'azmi"] = {
			["profiles"] = {
				["Default"] = {
					[347623] = 3064055,
					["warmup"] = 3064055,
					[347610] = 3064055,
					[357188] = 3064055,
					[347249] = 3064055,
				},
			},
		},
		["BigWigs_Bosses_Hungering Destroyer"] = {
			["profiles"] = {
				["Default"] = {
					[329725] = 3064055,
					[329774] = 3065079,
					[329298] = 3064055,
					[334522] = 3064567,
					["custom_on_repeating_say_laser"] = true,
					["berserk"] = 19,
					["custom_off_329298"] = false,
					[332295] = 3065079,
					["custom_off_334266"] = false,
					[329455] = 3064055,
					[334266] = 4112631,
					["custom_on_repeating_yell_miasma"] = true,
				},
			},
		},
		["BigWigs_Bosses_Guardian of the First Ones"] = {
			["profiles"] = {
				["Default"] = {
					[350455] = 3064055,
					[352394] = 3064055,
					[352833] = 3064055,
					[352660] = 3064055,
					[358301] = 3064055,
					[350732] = 3064055,
					[347359] = 3064055,
					[352538] = 3064055,
					["custom_on_stop_timers"] = true,
					[350735] = 3064055,
					[352385] = 3064055,
					[355352] = 3064055,
					[356093] = 3064055,
					["custom_off_350496"] = false,
					[350496] = 4112631,
					[352589] = 3064567,
				},
			},
		},
		["BigWigs_Bosses_Dealer Xy'exa"] = {
			["profiles"] = {
				["Default"] = {
					[320230] = 3064055,
					[323687] = 3064055,
					[321948] = 3064055,
				},
			},
		},
		["BigWigs_Plugins_AltPower"] = {
		},
		["BigWigs_Plugins_Raid Icons"] = {
			["profiles"] = {
				["Default"] = {
					["disabled"] = true,
				},
			},
		},
		["BigWigs_Bosses_Lord Chamberlain"] = {
			["profiles"] = {
				["Default"] = {
					[323150] = 3064055,
					[323236] = 3064055,
					[329104] = 3064055,
					[328791] = 3064055,
					[323437] = 3064055,
				},
			},
		},
		["BigWigs_Bosses_High Sage Viryx"] = {
			["profiles"] = {
				["Default"] = {
					["custom_on_markadd"] = true,
					[153954] = 3064055,
					[154055] = 3064055,
					["add"] = 3064055,
				},
			},
		},
		["BigWigs_Bosses_Sylvanas Windrunner"] = {
			["profiles"] = {
				["Default"] = {
					[351869] = 3064055,
				},
			},
		},
		["BigWigs_Bosses_Hylbrande"] = {
			["profiles"] = {
				["Default"] = {
					[346766] = 3064055,
					[346971] = 3064055,
					[346957] = 3064055,
					[347094] = 3064055,
					[346116] = 3065079,
					[353312] = 3064055,
				},
			},
		},
		["BigWigs_Bosses_Soulbinder Nyami"] = {
			["profiles"] = {
				["Default"] = {
					[153994] = 3064055,
					[155327] = 3064055,
					[154477] = 3072247,
				},
			},
		},
		["BigWigs_Bosses_Hoptallus"] = {
			["profiles"] = {
				["Default"] = {
					[114291] = 3064055,
					[112992] = 3064055,
					[112944] = 3064055,
				},
			},
		},
		["BigWigs_Bosses_General Kaal"] = {
			["profiles"] = {
				["Default"] = {
					[323821] = 3064055,
					[324086] = 3064055,
					[323845] = 3064055,
					[322903] = 3064055,
				},
			},
		},
		["BigWigs_Bosses_High Adjudicator Aleez"] = {
			["profiles"] = {
				["Default"] = {
					[323650] = 3064055,
					[323552] = 3064055,
					[323538] = 3064055,
					[329340] = 3064055,
				},
			},
		},
		["BigWigs_Bosses_Araknath"] = {
			["profiles"] = {
				["Default"] = {
					[154159] = 3064055,
					[154110] = 3065079,
					[154135] = 3064055,
				},
			},
		},
		["BigWigs_Bosses_Theater Of Pain Trash"] = {
			["profiles"] = {
				["Default"] = {
					[333294] = 3064055,
					[341969] = 3064055,
					[330614] = 3064055,
					[330868] = 3064055,
					[330562] = 3064055,
					[342675] = 3064055,
				},
			},
		},
		["BigWigs_Bosses_Hakkar the Soulflayer"] = {
			["profiles"] = {
				["Default"] = {
					[322759] = 3064055,
					[322736] = 3065079,
					[322746] = 3064055,
					[323569] = 3064055,
				},
			},
		},
		["BigWigs_Bosses_Mortanis"] = {
			["profiles"] = {
				["Default"] = {
					[338847] = 3064055,
					[338849] = 3064055,
					[338851] = 3064055,
					[338846] = 3064055,
					[338848] = 3064055,
					[338850] = 3064055,
				},
			},
		},
		["BigWigs_Bosses_Xeri'tac"] = {
			["profiles"] = {
				["Default"] = {
					[169248] = 3064055,
					["stages"] = 3064055,
					[-10492] = 3064055,
					[-10502] = 3064055,
					[169233] = 3064055,
					[173080] = 3064055,
				},
			},
		},
		["BigWigs_Plugins_Super Emphasize"] = {
		},
		["BigWigs_Bosses_Castle Nathria Trash"] = {
			["profiles"] = {
				["Default"] = {
					[343322] = 3072247,
					[339525] = 3064055,
					[339975] = 3068151,
					[339557] = 3064055,
					[341735] = 3064055,
					[341441] = 3064055,
					[343325] = 3072247,
					[343302] = 3064055,
					[343155] = 3064055,
					[341146] = 3064055,
					[343271] = 3064055,
					[342752] = 3066103,
					[339528] = 3065079,
					[329989] = 3072247,
					[340630] = 3072247,
					[339553] = 3064055,
					[329298] = 3064055,
					[343320] = 3072247,
					[342770] = 3064567,
					[341352] = 3064055,
					[343316] = 3072247,
				},
			},
		},
		["BigWigs_Bosses_The Manastorms"] = {
			["profiles"] = {
				["Default"] = {
					[321061] = 3064055,
					[320823] = 3064055,
					[323877] = 3064055,
					[320787] = 3064055,
				},
			},
		},
		["BigWigs_Bosses_Painsmith Raznal"] = {
			["profiles"] = {
				["Default"] = {
					[355568] = 3129591,
				},
			},
		},
		["BigWigs_Bosses_The Nine"] = {
			["profiles"] = {
				["Default"] = {
					[351399] = 3064055,
				},
			},
		},
		["BigWigs_Bosses_Magmolatus"] = {
			["profiles"] = {
				["Default"] = {
					[150076] = 3064055,
					[150078] = 3064055,
					["stages"] = 3064055,
					[150038] = 3064055,
				},
			},
		},
		["BigWigs_Bosses_Yan-Zhu the Uncasked"] = {
			["profiles"] = {
				["Default"] = {
					[106851] = 3064055,
					[114548] = 3065079,
					[-5658] = 3064055,
					[114451] = 3064055,
					["summon"] = 3064055,
					[106563] = 3064055,
					[106546] = 3064055,
					[115003] = 3064055,
				},
			},
		},
		["BigWigs_Bosses_Xav the Unfallen"] = {
			["profiles"] = {
				["Default"] = {
					[320050] = 3064055,
					[317231] = 3064055,
					[331618] = 3064055,
					[320644] = 3068151,
					[320102] = 3064055,
					[339415] = 3064055,
					[320729] = 3064055,
				},
			},
		},
		["BigWigs_Bosses_Echelon"] = {
			["profiles"] = {
				["Default"] = {
					[326389] = 3064055,
					[319733] = 3064055,
					[328206] = 3064055,
					[319941] = 3064055,
				},
			},
		},
		["BigWigs_Bosses_Sire Denathrius"] = {
			["profiles"] = {
				["Default"] = {
					[333932] = 3130103,
					[326707] = 3130103,
				},
			},
		},
		["BigWigs_Bosses_The Necrotic Wake Trash"] = {
			["profiles"] = {
				["Default"] = {
					[324372] = 3064055,
					[333479] = 3064055,
					[327396] = 3064055,
					[338456] = 3068151,
					[322756] = 3064055,
					[338357] = 3064055,
					[335141] = 3064055,
					[323190] = 3064055,
					[338606] = 3064055,
					[338353] = 3064055,
					[324293] = 3064055,
					[323347] = 3064055,
					[333477] = 3064055,
					[335143] = 3064055,
					[334748] = 3064055,
					[327130] = 3064055,
					[324394] = 3064055,
					[324387] = 3064055,
					[323496] = 3064055,
					[328667] = 3064055,
				},
			},
		},
		["BigWigs_Plugins_Proximity"] = {
			["profiles"] = {
				["Default"] = {
					["height"] = 120.0002593994141,
					["posy"] = 631.243446281238,
					["posx"] = 484.9863015551855,
					["width"] = 140.0003356933594,
				},
			},
		},
		["BigWigs_Bosses_Raigonn"] = {
			["profiles"] = {
				["Default"] = {
					[111723] = 3064055,
					["stages"] = 3064055,
					[111728] = 3064055,
					[111668] = 3064055,
				},
			},
		},
		["BigWigs_Plugins_Pull"] = {
		},
		["BigWigs_Bosses_Azzakel"] = {
			["profiles"] = {
				["Default"] = {
					[153764] = 3064055,
					[153392] = 3064055,
				},
			},
		},
		["BigWigs_Bosses_Timecap'n Hooktail"] = {
			["profiles"] = {
				["Default"] = {
					[347371] = 3064055,
					[347149] = 3064055,
					[347151] = 3064055,
					[350517] = 3064055,
					[354334] = 3064055,
					[352345] = 3064311,
				},
			},
		},
		["BigWigs_Bosses_Xin the Weaponmaster"] = {
			["profiles"] = {
				["Default"] = {
					["blades"] = 3064055,
					["crossbows"] = 3064055,
					[119684] = 3064055,
				},
			},
		},
		["BigWigs_Bosses_Anduin Wrynn"] = {
			["profiles"] = {
				["Default"] = {
					[365021] = 3130103,
					[365295] = 3129591,
				},
			},
		},
		["BigWigs_Plugins_Wipe"] = {
		},
		["LibDualSpec-1.0"] = {
			["char"] = {
				["Noxiipally - Area 52"] = {
				},
			},
		},
		["BigWigs_Bosses_Remnant of Ner'zhul"] = {
			["profiles"] = {
				["Default"] = {
					[350489] = 3064055,
					[350073] = 3064055,
					[349890] = 3064055,
					["custom_off_350469"] = false,
					[351066] = 3064055,
					[355123] = 3064055,
					["custom_off_350676"] = false,
					[350469] = 4112631,
					["custom_off_-23767"] = false,
					["custom_on_stop_timers"] = true,
					[350676] = 3064055,
					[350388] = 3064055,
				},
			},
		},
		["BigWigs_Bosses_Bonemaw"] = {
			["profiles"] = {
				["Default"] = {
					[153804] = 3064055,
					[154175] = 3064055,
				},
			},
		},
		["BigWigs_Bosses_Master Snowdrift"] = {
			["profiles"] = {
				["Default"] = {
					[106434] = 3064055,
					[106747] = 3064055,
					[118961] = 3064055,
					["stages"] = 3064055,
				},
			},
		},
		["BigWigs_Plugins_InfoBox"] = {
			["profiles"] = {
				["Default"] = {
					["posx"] = 483.1254970138034,
					["posy"] = 550.7280345652689,
				},
			},
		},
		["BigWigs_Plugins_AutoReply"] = {
			["profiles"] = {
				["Default"] = {
					["exitCombat"] = 4,
					["mode"] = 4,
					["disabled"] = false,
				},
			},
		},
		["BigWigs_Bosses_Vigilant Guardian"] = {
			["profiles"] = {
				["Default"] = {
					[360355] = 3064055,
				},
			},
		},
		["BigWigs_Bosses_Plaguefall Trash"] = {
			["profiles"] = {
				["Default"] = {
					[329239] = 3064055,
					["summon_stealthlings"] = 3064055,
					[327233] = 3064055,
					[330403] = 3064055,
					[318949] = 3064055,
					[328180] = 3072247,
					[328475] = 3064055,
					[327584] = 3064055,
					[330816] = 3064055,
					[321935] = 3064055,
					[336451] = 3064055,
					[330786] = 3064055,
					[323572] = 3064055,
					[320517] = 3064055,
					[327995] = 3064055,
					[328016] = 3064055,
					[327882] = 3072247,
					[320512] = 3072247,
					[328177] = 3064055,
					[327515] = 3072247,
				},
			},
		},
		["BigWigs_Plugins_Statistics"] = {
			["profiles"] = {
				["Default"] = {
					["showBar"] = true,
				},
			},
		},
		["BigWigs_Bosses_Lihuvim, Principal Architect"] = {
			["profiles"] = {
				["Default"] = {
					[368027] = 0,
				},
			},
		},
		["BigWigs_Bosses_Mailroom Mayhem"] = {
			["profiles"] = {
				["Default"] = {
					[346962] = 3064055,
					[346286] = 3064055,
					[346947] = 3064055,
					[346742] = 3064055,
				},
			},
		},
		["BigWigs_Plugins_Victory"] = {
			["profiles"] = {
				["Default"] = {
					["bigwigsMsg"] = true,
					["blizzMsg"] = false,
				},
			},
		},
		["BigWigs_Bosses_Kul'tharok"] = {
			["profiles"] = {
				["Default"] = {
					[319669] = 3065079,
					[319637] = 3064311,
					[319521] = 3064311,
					[319626] = 3064055,
				},
			},
		},
		["BigWigs_Bosses_Soulrender Dormazain"] = {
			["profiles"] = {
				["Default"] = {
					[350650] = 3064055,
					["custom_off_nameplate_tormented"] = false,
					["custom_off_nameplate_defiance"] = false,
					[350422] = 3065079,
					[350411] = 3064055,
					["berserk"] = 19,
					[351229] = 3064055,
					[351779] = 3064055,
					[350217] = 3064055,
					[350615] = 3064055,
					[349985] = 3064055,
					["custom_off_350647"] = false,
					[-23517] = 3064055,
					[350647] = 4112631,
					[354231] = 3064055,
					["custom_off_-23289"] = false,
				},
			},
		},
		["BigWigs_Bosses_Auchindoun Trash"] = {
			["profiles"] = {
				["Default"] = {
					[157168] = 3064055,
				},
			},
		},
		["BigWigs_Bosses_Gorechop"] = {
			["profiles"] = {
				["Default"] = {
					[318406] = 3064055,
					[322795] = 3064055,
					[323515] = 3068151,
				},
			},
		},
		["BigWigs_Bosses_Mistcaller"] = {
			["profiles"] = {
				["Default"] = {
					[321828] = 4112631,
					[321834] = 3064055,
					[341709] = 3064055,
					[321891] = 4112631,
					[336499] = 3064055,
				},
			},
		},
		["BigWigs_Bosses_Gekkan"] = {
			["profiles"] = {
				["Default"] = {
					["heal"] = 3064055,
					["stages"] = 3064055,
					[118903] = 3072247,
					[118963] = 3064055,
					[118988] = 3064055,
				},
			},
		},
		["BigWigs_Bosses_Ancient Protectors"] = {
			["profiles"] = {
				["Default"] = {
					[168520] = 3064055,
					["custom_on_automark"] = true,
					[168082] = 3064055,
					[168041] = 3064055,
					[168105] = 3064055,
					[168383] = 3064055,
					[167977] = 3064055,
				},
			},
		},
		["BigWigs_Bosses_Mueh'zala"] = {
			["profiles"] = {
				["Default"] = {
					[325258] = 3064055,
					[327646] = 3068151,
					[326171] = 3064567,
					[325725] = 3064055,
					[334970] = 3064055,
				},
			},
		},
		["BigWigs_Bosses_Striker Ga'dok"] = {
			["profiles"] = {
				["Default"] = {
					[-5660] = 3064055,
					[107047] = 3068151,
					[106933] = 3064055,
					[115458] = 3064055,
				},
			},
		},
		["BigWigs_Bosses_The Jailer"] = {
			["profiles"] = {
				["Default"] = {
					[365150] = 2539735,
				},
			},
		},
		["BigWigs_Bosses_So'leah"] = {
			["profiles"] = {
				["Default"] = {
					[351119] = 3064055,
					[350796] = 3064055,
					["stages"] = 3064055,
					[353635] = 3064055,
					[351124] = 3064055,
					[350804] = 3064055,
					[351086] = 3064055,
					[351096] = 3064055,
				},
			},
		},
		["BigWigs_Bosses_Globgrog"] = {
			["profiles"] = {
				["Default"] = {
					[324490] = 3064055,
					[324527] = 3064055,
					[324667] = 3064055,
				},
			},
		},
		["BigWigs_Plugins_Messages"] = {
			["profiles"] = {
				["Default"] = {
					["emphPosition"] = {
						"TOP", -- [1]
						"TOP", -- [2]
						nil, -- [3]
						-225.9236450195313, -- [4]
					},
					["disabled"] = true,
					["emphDisabled"] = true,
					["normalPosition"] = {
						nil, -- [1]
						nil, -- [2]
						nil, -- [3]
						-159.9999694824219, -- [4]
					},
					["fontName"] = "Ubuntu Medium",
					["chat"] = true,
				},
			},
		},
		["BigWigs_Bosses_Ranjit"] = {
			["profiles"] = {
				["Default"] = {
					[156793] = 3064055,
					[153315] = 3064055,
					[165731] = 3064055,
				},
			},
		},
		["BigWigs_Bosses_Archmage Sol"] = {
			["profiles"] = {
				["Default"] = {
					[166492] = 3064055,
					[168885] = 3064055,
					["stages"] = 3064055,
					[166726] = 3064055,
				},
			},
		},
		["BigWigs_Bosses_De Other Side Trash"] = {
			["profiles"] = {
				["Default"] = {
					[332084] = 3064055,
					[332612] = 3064055,
					[332605] = 3064055,
					[332706] = 3064055,
					[331846] = 3064055,
					[332157] = 3064055,
					[333227] = 3064055,
					[328740] = 3064055,
					[332608] = 3064055,
					[332678] = 3068151,
					[333787] = 3064055,
					[331927] = 3064055,
					[340026] = 3064055,
					[334051] = 3064055,
					["soporific_shimmerdust"] = 3064055,
					[332672] = 3064055,
					[331548] = 3068151,
				},
			},
		},
		["BigWigs_Bosses_Domina Venomblade"] = {
			["profiles"] = {
				["Default"] = {
					[325552] = 3064055,
					[336258] = 3064055,
					[332313] = 3064055,
					[325245] = 3064055,
				},
			},
		},
		["BigWigs_Bosses_Teron'gor"] = {
			["profiles"] = {
				["Default"] = {
					[156921] = 3064055,
					[157001] = 3064055,
					[157168] = 3064055,
					[156854] = 3064055,
					[156975] = 3064055,
					[156856] = 3064055,
					[157039] = 3064055,
					["stages"] = 3064055,
				},
			},
		},
		["BigWigs_Bosses_Sun King's Salvation"] = {
			["profiles"] = {
				["Default"] = {
					[343026] = 3130103,
				},
			},
		},
		["BigWigs_Plugins_Sounds"] = {
			["profiles"] = {
				["Default"] = {
					["Long"] = {
						["BigWigs_Bosses_Anduin Wrynn"] = {
							[365021] = "Bike Horn",
						},
						["BigWigs_Bosses_Dausegne, the Fallen Oracle"] = {
							[359483] = "|cFFFF0000Add|r",
						},
					},
					["Warning"] = {
						["BigWigs_Bosses_Artificer Xy'mox v2"] = {
							[362803] = "Bam",
							[364040] = "|cFFFF0000Healcd|r",
						},
					},
					["Info"] = {
						["BigWigs_Bosses_The Tarragrue"] = {
							[347679] = "BigWigs: Long",
						},
						["BigWigs_Bosses_Dausegne, the Fallen Oracle"] = {
							[361630] = "BigWigs: Info",
							[363200] = "|cFFFF0000Spread|r",
							[361651] = "|cFFFF0000Behind|r",
						},
					},
					["Alarm"] = {
						["BigWigs_Bosses_Dausegne, the Fallen Oracle"] = {
							[361966] = "BigWigs: Alert",
						},
					},
					["Alert"] = {
						["BigWigs_Bosses_Rygelon"] = {
							[363533] = "|cFFFF0000Spread|r",
						},
						["BigWigs_Bosses_Painsmith Raznal"] = {
							[355568] = "Bam",
						},
						["BigWigs_Bosses_Dausegne, the Fallen Oracle"] = {
							[361018] = "BigWigs: Info",
							[361513] = "|cFFFF0000Dodge|r",
						},
					},
				},
			},
		},
		["BigWigs_Bosses_Mordretha, the Endless Empress"] = {
			["profiles"] = {
				["Default"] = {
					[323683] = 3064055,
					[323608] = 3064055,
					[324079] = 3068151,
					[339573] = 3064055,
					[324449] = 3064055,
					[339550] = 3064055,
					[339706] = 3064055,
				},
			},
		},
		["BigWigs_Bosses_Sha of Violence"] = {
			["profiles"] = {
				["Default"] = {
					[-5813] = 3064055,
					[106872] = 3064055,
				},
			},
		},
		["BigWigs_Bosses_Artificer Xy'mox"] = {
			["profiles"] = {
				["Default"] = {
					[335013] = 3064055,
					[329107] = 3064055,
					["custom_off_328437"] = false,
					[340860] = 3064055,
					[328789] = 3064055,
					[326271] = 3064055,
					[328437] = 4112631,
					[340788] = 3064055,
					["stages"] = 3064055,
					[327414] = 3064055,
					[325399] = 3064055,
					[327902] = 4112631,
					[325236] = 3068151,
					[340758] = 3064055,
				},
			},
		},
		["BigWigs_Bosses_Taran Zhu"] = {
			["profiles"] = {
				["Default"] = {
					[115002] = 3064055,
					[107356] = 3064055,
					[107087] = 3064055,
				},
			},
		},
		["BigWigs_Bosses_Trial of the King"] = {
			["profiles"] = {
				["Default"] = {
					[123655] = 3066103,
					[-6017] = 3064055,
					[119922] = 3064055,
					[-6024] = 3064055,
					[120195] = 3064055,
				},
			},
		},
		["BigWigs_Bosses_Prototype Pantheon"] = {
			["profiles"] = {
				["Default"] = {
					[361745] = 3064023,
				},
			},
		},
		["BigWigs_Bosses_The Tarragrue"] = {
			["profiles"] = {
				["Default"] = {
					[347679] = 3129591,
				},
			},
		},
		["BigWigs_Bosses_Ner'zhul"] = {
			["profiles"] = {
				["Default"] = {
					[-9680] = 3064055,
					[154350] = 3064055,
					[154442] = 3064055,
				},
			},
		},
		["BigWigs_Bosses_Artificer Xy'mox v2"] = {
			["profiles"] = {
				["Default"] = {
					[362803] = 3129591,
					[364040] = 3129591,
				},
			},
		},
		["BigWigs_Bosses_Myza's Oasis"] = {
			["profiles"] = {
				["Default"] = {
					[350922] = 3064055,
					["stages"] = 3064055,
					[350919] = 3064055,
					[355438] = 3064055,
					[356482] = 3064055,
					[350916] = 3065079,
				},
			},
		},
		["BigWigs_Bosses_Sadana Bloodfury"] = {
			["profiles"] = {
				["Default"] = {
					[164974] = 3064055,
					[153240] = 3064055,
					["custom_on_markadd"] = true,
					[153153] = 3064055,
				},
			},
		},
		["BigWigs_Plugins_Countdown"] = {
			["profiles"] = {
				["Default"] = {
					["fontName"] = "Ubuntu Bold",
					["fontColor"] = {
						["g"] = 0.3333333333333333,
						["b"] = 0.3333333333333333,
					},
					["position"] = {
						"CENTER", -- [1]
						"CENTER", -- [2]
						nil, -- [3]
						-56.31629180908203, -- [4]
					},
				},
			},
		},
		["BigWigs_Bosses_Vigilant Kaathar"] = {
			["profiles"] = {
				["Default"] = {
					[153430] = 3064055,
					[153006] = 3064055,
					[153002] = 3064055,
					[152954] = 3064055,
				},
			},
		},
		["BigWigs_Bosses_Doctor Ickus"] = {
			["profiles"] = {
				["Default"] = {
					[67382] = 3064055,
					[329217] = 3064055,
					[329110] = 3064055,
					[322358] = 3064055,
					[321406] = 3064055,
					[332617] = 3064055,
				},
			},
		},
		["BigWigs_Plugins_Colors"] = {
			["profiles"] = {
				["Default"] = {
					["blue"] = {
						["BigWigs_Plugins_Colors"] = {
							["default"] = {
								0.3803921568627451, -- [1]
								0.8392156862745098, -- [2]
								0.9098039215686274, -- [3]
								1, -- [4]
							},
						},
					},
					["cyan"] = {
						["BigWigs_Plugins_Colors"] = {
							["default"] = {
								0.9137254901960784, -- [1]
								0.9137254901960784, -- [2]
								0.9568627450980391, -- [3]
								1, -- [4]
							},
						},
					},
					["barColor"] = {
						["BigWigs_Bosses_Rygelon"] = {
							[363533] = {
								0.8588235294117647, -- [1]
								0, -- [2]
								0.9882352941176471, -- [3]
							},
						},
						["BigWigs_Plugins_Colors"] = {
							["default"] = {
								0, -- [1]
								0.9882352941176471, -- [2]
								0.4627450980392157, -- [3]
							},
						},
						["BigWigs_Bosses_Anduin Wrynn"] = {
							[365295] = {
								0, -- [1]
								0.4117647058823529, -- [2]
								0.9882352941176471, -- [3]
							},
						},
					},
					["purple"] = {
						["BigWigs_Plugins_Colors"] = {
							["default"] = {
								0.7411764705882353, -- [1]
								0.5764705882352941, -- [2]
								0.9764705882352941, -- [3]
								1, -- [4]
							},
						},
					},
					["barEmphasized"] = {
						["BigWigs_Bosses_Rygelon"] = {
							[363533] = {
								0.8588235294117647, -- [1]
								[3] = 0.9882352941176471,
							},
						},
						["BigWigs_Plugins_Colors"] = {
							["default"] = {
								nil, -- [1]
								0.3333333333333333, -- [2]
								0.3333333333333333, -- [3]
							},
						},
						["BigWigs_Bosses_Anduin Wrynn"] = {
							[365295] = {
								0, -- [1]
								0.4117647058823529, -- [2]
								0.9882352941176471, -- [3]
							},
						},
					},
					["green"] = {
						["BigWigs_Plugins_Colors"] = {
							["default"] = {
								0, -- [1]
								0.9882352941176471, -- [2]
								0.4627450980392157, -- [3]
								1, -- [4]
							},
						},
					},
					["yellow"] = {
						["BigWigs_Plugins_Colors"] = {
							["default"] = {
								0.9215686274509803, -- [1]
								nil, -- [2]
								0.5294117647058824, -- [3]
								1, -- [4]
							},
						},
					},
					["barText"] = {
						["BigWigs_Plugins_Colors"] = {
							["default"] = {
								0.9450980392156862, -- [1]
								0.9490196078431372, -- [2]
								0.9725490196078431, -- [3]
							},
						},
					},
					["barBackground"] = {
						["BigWigs_Plugins_Colors"] = {
							["default"] = {
								0.1568627450980392, -- [1]
								0.1647058823529412, -- [2]
								0.2117647058823529, -- [3]
								0.300000011920929, -- [4]
							},
						},
					},
					["flash"] = {
						["BigWigs_Plugins_Colors"] = {
							["default"] = {
								1, -- [1]
								0.7215686274509804, -- [2]
								0.4235294117647059, -- [3]
								0.2000000476837158, -- [4]
							},
						},
					},
				},
			},
		},
		["BigWigs_Bosses_Nhallish"] = {
			["profiles"] = {
				["Default"] = {
					[153067] = 3064055,
					[152979] = 3064055,
					[152801] = 3064055,
					[153623] = 3064055,
				},
			},
		},
		["BigWigs_Bosses_Blightbone"] = {
			["profiles"] = {
				["Default"] = {
					[320596] = 3064055,
					[320630] = 3064055,
					[320637] = 3064055,
					[320655] = 3065079,
				},
			},
		},
		["BigWigs_Plugins_Bars"] = {
			["profiles"] = {
				["Default"] = {
					["BigWigsEmphasizeAnchor_y"] = 239.1371968552521,
					["emphasize"] = false,
					["visibleBarLimit"] = 10,
					["BigWigsAnchor_width"] = 193.9998931884766,
					["BigWigsAnchor_y"] = 482.7638955871735,
					["emphasizeGrowup"] = true,
					["BigWigsAnchor_x"] = 1417,
					["emphasizeTime"] = 15,
					["barStyle"] = "MonoUI",
					["BigWigsEmphasizeAnchor_height"] = 32.99993133544922,
					["fontSizeEmph"] = 16,
					["fontName"] = "Ubuntu Medium",
					["BigWigsAnchor_height"] = 22.00002288818359,
					["visibleBarLimitEmph"] = 5,
					["BigWigsEmphasizeAnchor_width"] = 268.0001831054688,
					["BigWigsEmphasizeAnchor_x"] = 932.0962121538396,
					["texture"] = "Details Flat",
					["fontSize"] = 12,
				},
			},
		},
		["BigWigs_Bosses_Stone Legion Generals"] = {
			["profiles"] = {
				["Default"] = {
					[334765] = 3064055,
					["custom_off_334765"] = false,
					[333913] = 3064055,
					[342655] = 3064055,
					[342733] = 3064055,
					[334929] = 3065079,
					[333387] = 3064055,
					["custom_off_-22772"] = false,
					[340037] = 3064055,
					["custom_off_333387"] = false,
					["custom_off_-22761"] = false,
					[332683] = 3064055,
					[329808] = 3064055,
					[332406] = 3064055,
					[343898] = 3064055,
					[339690] = 4112631,
					[342698] = 3064055,
					[334498] = 3064055,
					[342256] = 3064055,
					[344496] = 4112631,
					[339885] = 3064055,
					["berserk"] = 19,
					[343063] = 3064055,
					[342544] = 3064055,
					[342722] = 3064055,
					["custom_off_339690"] = false,
					["commando"] = 3064055,
					[329636] = 3064055,
					["custom_on_stop_timers"] = true,
					[342425] = 3065079,
					["goliath"] = 3064055,
				},
			},
		},
		["BigWigs_Bosses_Rygelon"] = {
			["profiles"] = {
				["Default"] = {
					[368080] = 3064055,
					[362798] = 3064055,
					[362088] = 3064055,
					[364386] = 3064055,
					[362806] = 3064055,
					["custom_off_361548"] = false,
					[366379] = 3064055,
					[363533] = 3064567,
					[362172] = 3065079,
					[362206] = 3064055,
					[362390] = 3064055,
					[362184] = 3065079,
					[364114] = 3064055,
					[362207] = 0,
					[366606] = 3064055,
					[362275] = 3064055,
				},
			},
		},
		["BigWigs_Bosses_Witherbark"] = {
			["profiles"] = {
				["Default"] = {
					[164275] = 3064055,
					[164294] = 3064055,
					[164357] = 3065079,
				},
			},
		},
		["BigWigs_Bosses_An Affront of Challengers"] = {
			["profiles"] = {
				["Default"] = {
					[324085] = 3064055,
					[320069] = 3064055,
					[320272] = 3064055,
					[333540] = 3064055,
					[320063] = 3064055,
					[326892] = 3064055,
					[320293] = 3064055,
					[333231] = 3064055,
					[320248] = 3064055,
				},
			},
		},
		["BigWigs_Bosses_Dausegne, the Fallen Oracle"] = {
			["profiles"] = {
				["Default"] = {
					[361630] = 3129575,
					[359483] = 3064039,
					[361018] = 3064039,
					[361966] = 3129591,
					[361513] = 3064039,
					[361651] = 2932983,
				},
			},
		},
		["BigWigs_Bosses_Zo'phex the Sentinel"] = {
			["profiles"] = {
				["Default"] = {
					[345770] = 3064055,
					["warmup"] = 3064055,
					[346204] = 3064055,
					[345990] = 3064055,
					[347949] = 3064055,
				},
			},
		},
		["BigWigs_Bosses_The Grand Menagerie"] = {
			["profiles"] = {
				["Default"] = {
					[349934] = 3064055,
					["warmup"] = 3064055,
					[349987] = 3064055,
					[350010] = 3064823,
					[349954] = 3064055,
					[349797] = 3064055,
					[350101] = 3064055,
					[349663] = 3064055,
					[350086] = 3064055,
					[349627] = 3064055,
				},
			},
		},
		["BigWigs_Bosses_Tazavesh Trash"] = {
			["profiles"] = {
				["Default"] = {
					[356031] = 3064055,
					["trading_game"] = 3064055,
					[357512] = 3064055,
					[356001] = 3064055,
					[355637] = 3064055,
					[357226] = 3064055,
					[355057] = 3064055,
					[355429] = 3064055,
					[355782] = 3064055,
					[355584] = 3064055,
					[355980] = 3064055,
					[355642] = 3064055,
					[355577] = 3064055,
					[357029] = 3064055,
					[355934] = 3072247,
					[356407] = 3064055,
					[356942] = 3065079,
					[357260] = 3064055,
					[357238] = 3064055,
					[355477] = 3068151,
					[357284] = 3064055,
					[356133] = 3064055,
					[347721] = 3064055,
					[356967] = 3068151,
					[356324] = 3064055,
					["custom_on_autotalk"] = true,
					[356929] = 3064055,
					[355480] = 3064055,
					[355132] = 3064055,
					[356404] = 3064055,
				},
			},
		},
		["BigWigs_Bosses_Amarth, The Reanimator"] = {
			["profiles"] = {
				["Default"] = {
					[321226] = 3064055,
					[320171] = 3064055,
					[320012] = 3068151,
					[333488] = 3064055,
					[321247] = 3064055,
				},
			},
		},
		["BigWigs_Bosses_Nalthor the Rimebinder"] = {
			["profiles"] = {
				["Default"] = {
					[320788] = 3064055,
					[321368] = 3064055,
					[320772] = 3064055,
					[321894] = 3064055,
				},
			},
		},
	},
	["profileKeys"] = {
		["Mildew - Hyjal"] = "Default",
		["Noxiishields - Area 52"] = "Default",
		["Noxheals - Area 52"] = "Default",
		["Noxiipally - Area 52"] = "Default",
		["Noxii - Hyjal"] = "Default",
	},
	["profiles"] = {
		["Default"] = {
		},
	},
}
BigWigsIconDB = {
	["hide"] = true,
}
BigWigsStatsDB = {
	[2296] = {
		[2394] = {
			["normal"] = {
				["kills"] = 1,
				["wipes"] = 2,
				["best"] = 282.8249999999971,
			},
			["heroic"] = {
				["kills"] = 6,
				["wipes"] = 5,
				["best"] = 199.5120000000006,
			},
			["LFR"] = {
				["kills"] = 1,
				["wipes"] = 1,
				["best"] = 221.1949999999997,
			},
		},
		[2425] = {
			["heroic"] = {
				["kills"] = 7,
				["wipes"] = 8,
				["best"] = 292.6990000000005,
			},
			["LFR"] = {
				["best"] = 309.3509999999988,
				["kills"] = 1,
			},
		},
		[2418] = {
			["heroic"] = {
				["kills"] = 6,
				["wipes"] = 9,
				["best"] = 179.8700000000008,
			},
			["mythic"] = {
				["kills"] = 2,
				["wipes"] = 3,
				["best"] = 357.4439999999995,
			},
			["normal"] = {
				["best"] = 311.1859999999997,
				["kills"] = 1,
			},
		},
		[2426] = {
			["heroic"] = {
				["kills"] = 5,
				["best"] = 315.241,
				["wipes"] = 3,
			},
			["normal"] = {
				["best"] = 579.4150000000009,
				["kills"] = 2,
			},
		},
		[2420] = {
			["heroic"] = {
				["best"] = 164.369999999999,
				["kills"] = 6,
			},
			["mythic"] = {
				["kills"] = 3,
				["wipes"] = 11,
				["best"] = 315.0290000000005,
			},
			["normal"] = {
				["best"] = 328.3829999999998,
				["kills"] = 1,
			},
		},
		[2428] = {
			["heroic"] = {
				["best"] = 152.2300000000014,
				["kills"] = 5,
			},
			["mythic"] = {
				["kills"] = 4,
				["wipes"] = 4,
				["best"] = 330.953,
			},
			["normal"] = {
				["best"] = 311.7260000000006,
				["kills"] = 1,
			},
		},
		[2429] = {
			["heroic"] = {
				["kills"] = 5,
				["wipes"] = 2,
				["best"] = 154.375,
			},
			["mythic"] = {
				["kills"] = 4,
				["wipes"] = 4,
				["best"] = 248.0640000000003,
			},
			["normal"] = {
				["best"] = 321.1800000000003,
				["kills"] = 1,
			},
		},
		[2422] = {
			["heroic"] = {
				["best"] = 184.6059999999998,
				["kills"] = 6,
			},
			["mythic"] = {
				["kills"] = 2,
				["wipes"] = 15,
				["best"] = 301.8869999999988,
			},
			["normal"] = {
				["best"] = 357.3370000000014,
				["kills"] = 1,
			},
		},
		[2393] = {
			["heroic"] = {
				["kills"] = 4,
				["best"] = 112.2080000000005,
				["wipes"] = 1,
			},
			["mythic"] = {
				["best"] = 246.0819999999999,
				["kills"] = 4,
			},
			["normal"] = {
				["best"] = 296.3919999999998,
				["kills"] = 1,
			},
			["LFR"] = {
				["best"] = 231.0319999999992,
				["kills"] = 1,
			},
		},
		[2424] = {
			["heroic"] = {
				["kills"] = 5,
				["wipes"] = 19,
				["best"] = 398.8119999999999,
			},
			["normal"] = {
				["wipes"] = 3,
			},
		},
	},
	[2481] = {
		[2461] = {
			["normal"] = {
				["best"] = 136.1960000000036,
				["kills"] = 5,
			},
			["heroic"] = {
				["kills"] = 6,
				["wipes"] = 6,
				["best"] = 226.2229999999981,
			},
			["mythic"] = {
				["kills"] = 3,
				["wipes"] = 64,
				["best"] = 574.4880000000048,
			},
		},
		[2469] = {
			["heroic"] = {
				["kills"] = 5,
				["wipes"] = 76,
				["best"] = 433.7330000000002,
			},
			["normal"] = {
				["kills"] = 5,
				["wipes"] = 21,
				["best"] = 272.5699999999997,
			},
			["mythic"] = {
				["wipes"] = 261,
			},
		},
		[2470] = {
			["normal"] = {
				["best"] = 210.8600000000006,
				["kills"] = 6,
			},
			["mythic"] = {
				["kills"] = 5,
				["wipes"] = 32,
				["best"] = 418.2900000000009,
			},
			["heroic"] = {
				["kills"] = 4,
				["wipes"] = 5,
				["best"] = 307.8740000000034,
			},
			["LFR"] = {
				["best"] = 189.5750000000044,
				["kills"] = 1,
			},
		},
		[2463] = {
			["heroic"] = {
				["kills"] = 7,
				["wipes"] = 24,
				["best"] = 248.1710000000021,
			},
			["mythic"] = {
				["kills"] = 1,
				["wipes"] = 243,
				["best"] = 348.4959999999992,
			},
			["normal"] = {
				["best"] = 186.9629999999961,
				["kills"] = 5,
			},
			["LFR"] = {
				["best"] = 192.3280000000013,
				["kills"] = 1,
			},
		},
		[2464] = {
			["mythic"] = {
				["kills"] = 1,
				["wipes"] = 357,
				["best"] = 584.4569999999949,
			},
		},
		[2457] = {
			["heroic"] = {
				["kills"] = 6,
				["wipes"] = 35,
				["best"] = 355.1970000000001,
			},
			["mythic"] = {
				["kills"] = 1,
				["wipes"] = 53,
				["best"] = 537.2249999999985,
			},
			["normal"] = {
				["kills"] = 3,
				["wipes"] = 1,
				["best"] = 306.1390000000029,
			},
		},
		[2465] = {
			["normal"] = {
				["best"] = 150.323000000004,
				["kills"] = 5,
			},
			["mythic"] = {
				["kills"] = 6,
				["wipes"] = 19,
				["best"] = 273.9029999999984,
			},
			["heroic"] = {
				["kills"] = 4,
				["wipes"] = 6,
				["best"] = 221.4150000000009,
			},
			["LFR"] = {
				["best"] = 154.2940000000017,
				["kills"] = 1,
			},
		},
		[2458] = {
			["normal"] = {
				["best"] = 344.4720000000016,
				["kills"] = 4,
			},
			["mythic"] = {
				["kills"] = 6,
				["wipes"] = 5,
				["best"] = 317.5440000000017,
			},
			["heroic"] = {
				["best"] = 411.8329999999987,
				["kills"] = 4,
			},
			["LFR"] = {
				["best"] = 343.5429999999979,
				["kills"] = 1,
			},
		},
		[2459] = {
			["normal"] = {
				["best"] = 141.7920000000013,
				["kills"] = 4,
			},
			["mythic"] = {
				["kills"] = 5,
				["wipes"] = 48,
				["best"] = 311.3930000000037,
			},
			["heroic"] = {
				["best"] = 212.9459999999963,
				["kills"] = 4,
			},
		},
		[2467] = {
			["heroic"] = {
				["kills"] = 4,
				["wipes"] = 69,
				["best"] = 263.0190000000002,
			},
			["mythic"] = {
				["wipes"] = 69,
			},
			["normal"] = {
				["kills"] = 4,
				["wipes"] = 1,
				["best"] = 162.7610000000059,
			},
		},
		[2460] = {
			["normal"] = {
				["kills"] = 5,
				["best"] = 192.9919999999984,
				["wipes"] = 1,
			},
			["heroic"] = {
				["kills"] = 4,
				["wipes"] = 19,
				["best"] = 326.5049999999974,
			},
			["mythic"] = {
				["kills"] = 4,
				["wipes"] = 63,
				["best"] = 417.8489999999947,
			},
		},
	},
	[2450] = {
		[2446] = {
			["heroic"] = {
				["kills"] = 11,
				["wipes"] = 69,
				["best"] = 195.7609999999986,
			},
			["mythic"] = {
				["kills"] = 8,
				["wipes"] = 32,
				["best"] = 266.2770000000019,
			},
			["normal"] = {
				["best"] = 196.973,
				["kills"] = 5,
			},
		},
		[2439] = {
			["heroic"] = {
				["kills"] = 12,
				["wipes"] = 4,
				["best"] = 206.3890000000029,
			},
			["mythic"] = {
				["kills"] = 12,
				["wipes"] = 11,
				["best"] = 271.9779999999955,
			},
			["normal"] = {
				["best"] = 232.9429999999993,
				["kills"] = 5,
			},
		},
		[2447] = {
			["heroic"] = {
				["kills"] = 11,
				["wipes"] = 49,
				["best"] = 295.8309999999983,
			},
			["normal"] = {
				["kills"] = 4,
				["wipes"] = 3,
				["best"] = 350.7739999999994,
			},
			["mythic"] = {
				["kills"] = 1,
				["wipes"] = 133,
				["best"] = 410.7969999999987,
			},
		},
		[2440] = {
			["heroic"] = {
				["kills"] = 14,
				["wipes"] = 24,
				["best"] = 337.3169999999955,
			},
			["normal"] = {
				["kills"] = 5,
				["wipes"] = 2,
				["best"] = 362.0799999999999,
			},
			["mythic"] = {
				["kills"] = 3,
				["wipes"] = 61,
				["best"] = 365.6260000000002,
			},
		},
		[2441] = {
			["heroic"] = {
				["kills"] = 13,
				["wipes"] = 62,
				["best"] = 679.3280000000013,
			},
			["normal"] = {
				["kills"] = 8,
				["wipes"] = 9,
				["best"] = 684.487000000001,
			},
			["mythic"] = {
				["kills"] = 1,
				["wipes"] = 92,
				["best"] = 825.8959999999988,
			},
		},
		[2442] = {
			["heroic"] = {
				["kills"] = 13,
				["best"] = 197.099000000002,
				["wipes"] = 2,
			},
			["mythic"] = {
				["kills"] = 12,
				["wipes"] = 55,
				["best"] = 306.5639999999985,
			},
			["normal"] = {
				["best"] = 214.6029999999992,
				["kills"] = 5,
			},
		},
		[2435] = {
			["heroic"] = {
				["kills"] = 15,
				["wipes"] = 4,
				["best"] = 164.0930000000008,
			},
			["mythic"] = {
				["kills"] = 13,
				["wipes"] = 11,
				["best"] = 207.8589999999967,
			},
			["normal"] = {
				["kills"] = 5,
				["best"] = 184.127,
				["wipes"] = 1,
			},
		},
		[2443] = {
			["heroic"] = {
				["kills"] = 12,
				["wipes"] = 50,
				["best"] = 242.0299999999988,
			},
			["normal"] = {
				["kills"] = 6,
				["wipes"] = 7,
				["best"] = 277.6999999999998,
			},
			["mythic"] = {
				["kills"] = 7,
				["wipes"] = 95,
				["best"] = 361.8489999999983,
			},
		},
		[2444] = {
			["normal"] = {
				["best"] = 170.0029999999997,
				["kills"] = 4,
			},
			["mythic"] = {
				["kills"] = 10,
				["wipes"] = 119,
				["best"] = 226.8290000000052,
			},
			["heroic"] = {
				["kills"] = 15,
				["wipes"] = 49,
				["best"] = 158.6229999999996,
			},
		},
		[2445] = {
			["normal"] = {
				["best"] = 196.4480000000003,
				["kills"] = 4,
			},
			["mythic"] = {
				["kills"] = 9,
				["wipes"] = 84,
				["best"] = 336.6390000000029,
			},
			["heroic"] = {
				["kills"] = 15,
				["wipes"] = 18,
				["best"] = 193.0679999999993,
			},
		},
	},
}
