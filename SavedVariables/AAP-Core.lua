
AAP1 = {
	["Area52"] = {
		["Noxiishields"] = {
			["FirstLoadz"] = 1,
			["QuestCounter2"] = 99,
			[194] = 1,
			["QuestCounter"] = 99,
			["BonusSkips"] = {
			},
			["Settings"] = {
				["LockArrow"] = 0,
				["ShowQuestListOrder"] = 0,
				["AutoRepair"] = 0,
				["Greetings3"] = 0,
				["AutoFlight"] = 1,
				["AutoGossip"] = 1,
				["Hide"] = 0,
				["ShowMap10s"] = 0,
				["ShowGroup"] = 1,
				["OrderListScale"] = 1,
				["leftLiz"] = 150,
				["AutoVendor"] = 0,
				["topLiz"] = -150,
				["arrowtop"] = -800.0000813802084,
				["AutoAccept"] = 1,
				["CutScene"] = 1,
				["left"] = 1357.850189208984,
				["ShowBlobs"] = 1,
				["AutoHandIn"] = 1,
				["ArrowFPS"] = 2,
				["Lock"] = 0,
				["Hcampleft"] = 1357.850189208984,
				["Partytop"] = -300.0000305175781,
				["Greetings"] = 0,
				["arrowleft"] = 1059.785513528964,
				["ArrowScale"] = 0.64,
				["ShowQList"] = 1,
				["ShowArrow"] = 1,
				["Hcamptop"] = -240.0000244140625,
				["alpha"] = 1,
				["MiniMapBlobAlpha"] = 1,
				["Partyleft"] = 869.02412109375,
				["AutoHandInChoice"] = 0,
				["top"] = -240.0000244140625,
				["ChooseQuests"] = 0,
				["QuestButtonDetatch"] = 0,
				["Scale"] = 0.64,
				["QuestButtons"] = 1,
				["ShowMapBlobs"] = 1,
				["AutoShareQ"] = 0,
				["DisableHeirloomWarning"] = 0,
			},
			["WantedQuestList"] = {
			},
			["LoaPick"] = 0,
			["AAP_DoWarCampaign"] = 0,
			["SkippedBonusObj"] = {
			},
			["QlineSkip"] = {
			},
		},
	},
}
AAP2 = nil
AAP3 = nil
AAPQuestNames = {
	[32869] = "Beasts of Fable Book III",
}
AAP_HoldZoneVar = nil
AAP_Transport = {
	["FPs"] = {
		["Horde"] = {
			[948] = {
				["Noxiishields-Area52"] = {
					["Conts"] = {
					},
				},
			},
		},
	},
}
AAP_Custom = {
	["Noxiishields-Area52"] = {
	},
}
AAP_ZoneComplete = {
	["Noxiishields-Area52"] = {
	},
}
AAP_TaxiTimers = {
}
