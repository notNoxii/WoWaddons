
MythicDungeonToolsDB = {
	["profileKeys"] = {
		["Mildew - Hyjal"] = "Mildew - Hyjal",
		["Noxiishields - Area 52"] = "Noxiishields - Area 52",
		["Koragoraia - Illidan"] = "Koragoraia - Illidan",
		["Welorialin - Area 52"] = "Welorialin - Area 52",
		["Noxtanks - Area 52"] = "Noxtanks - Area 52",
		["Noxii - Suramar"] = "Noxii - Suramar",
		["Noxiipally - Area 52"] = "Noxiipally - Area 52",
		["Noxxii - Suramar"] = "Noxxii - Suramar",
		["Noxheals - Area 52"] = "Noxheals - Area 52",
		["Noxii - Hyjal"] = "Noxii - Hyjal",
	},
	["global"] = {
		["tooltipInCorner"] = false,
		["anchorTo"] = "CENTER",
		["colorPaletteInfo"] = {
			["forceColorBlindMode"] = false,
			["autoColoring"] = true,
			["numberCustomColors"] = 12,
			["colorPaletteIdx"] = 4,
			["customPaletteValues"] = {
				{
					1, -- [1]
					1, -- [2]
					1, -- [3]
				}, -- [1]
				{
					1, -- [1]
					1, -- [2]
					1, -- [3]
				}, -- [2]
				{
					1, -- [1]
					1, -- [2]
					1, -- [3]
				}, -- [3]
				{
					1, -- [1]
					1, -- [2]
					1, -- [3]
				}, -- [4]
				{
					1, -- [1]
					1, -- [2]
					1, -- [3]
				}, -- [5]
				{
					1, -- [1]
					1, -- [2]
					1, -- [3]
				}, -- [6]
				{
					1, -- [1]
					1, -- [2]
					1, -- [3]
				}, -- [7]
				{
					1, -- [1]
					1, -- [2]
					1, -- [3]
				}, -- [8]
				{
					1, -- [1]
					1, -- [2]
					1, -- [3]
				}, -- [9]
				{
					1, -- [1]
					1, -- [2]
					1, -- [3]
				}, -- [10]
				{
					1, -- [1]
					1, -- [2]
					1, -- [3]
				}, -- [11]
				{
					1, -- [1]
					1, -- [2]
					1, -- [3]
				}, -- [12]
			},
		},
		["anchorFrom"] = "CENTER",
		["minimap"] = {
			["minimapPos"] = 174.5607447815945,
			["hide"] = false,
		},
		["currentPreset"] = {
			1, -- [1]
			1, -- [2]
			1, -- [3]
			1, -- [4]
			1, -- [5]
			1, -- [6]
			1, -- [7]
			1, -- [8]
			1, -- [9]
			1, -- [10]
			1, -- [11]
			1, -- [12]
			1, -- [13]
			1, -- [14]
			1, -- [15]
			1, -- [16]
			1, -- [17]
			1, -- [18]
			1, -- [19]
			1, -- [20]
			1, -- [21]
			1, -- [22]
			1, -- [23]
			1, -- [24]
			1, -- [25]
			1, -- [26]
			1, -- [27]
			1, -- [28]
			2, -- [29]
			1, -- [30]
			2, -- [31]
			2, -- [32]
			3, -- [33]
			2, -- [34]
			2, -- [35]
			2, -- [36]
			2, -- [37]
			2, -- [38]
			1, -- [39]
		},
		["MDI"] = {
		},
		["currentDungeonIdx"] = 29,
		["currentDifficulty"] = 20,
		["dataCollectionActive"] = false,
		["scale"] = 0.7500001649598818,
		["maximized"] = false,
		["xoffset"] = 317.5628967285156,
		["presets"] = {
			{
				{
					["value"] = {
					},
					["text"] = "Default",
					["colorPaletteInfo"] = {
						["autoColoring"] = true,
						["colorPaletteIdx"] = 4,
					},
				}, -- [1]
				{
					["value"] = 0,
					["text"] = "<New Preset>",
				}, -- [2]
			}, -- [1]
			{
				{
					["value"] = {
					},
					["text"] = "Default",
					["colorPaletteInfo"] = {
						["autoColoring"] = true,
						["colorPaletteIdx"] = 4,
					},
				}, -- [1]
				{
					["value"] = 0,
					["text"] = "<New Preset>",
				}, -- [2]
			}, -- [2]
			{
				{
					["value"] = {
					},
					["text"] = "Default",
					["colorPaletteInfo"] = {
						["autoColoring"] = true,
						["colorPaletteIdx"] = 4,
					},
				}, -- [1]
				{
					["value"] = 0,
					["text"] = "<New Preset>",
				}, -- [2]
			}, -- [3]
			{
				{
					["value"] = {
					},
					["text"] = "Default",
					["colorPaletteInfo"] = {
						["autoColoring"] = true,
						["colorPaletteIdx"] = 4,
					},
				}, -- [1]
				{
					["value"] = 0,
					["text"] = "<New Preset>",
				}, -- [2]
			}, -- [4]
			{
				{
					["value"] = {
					},
					["text"] = "Default",
					["colorPaletteInfo"] = {
						["autoColoring"] = true,
						["colorPaletteIdx"] = 4,
					},
				}, -- [1]
				{
					["value"] = 0,
					["text"] = "<New Preset>",
				}, -- [2]
			}, -- [5]
			{
				{
					["value"] = {
					},
					["text"] = "Default",
					["colorPaletteInfo"] = {
						["autoColoring"] = true,
						["colorPaletteIdx"] = 4,
					},
				}, -- [1]
				{
					["value"] = 0,
					["text"] = "<New Preset>",
				}, -- [2]
			}, -- [6]
			{
				{
					["value"] = {
					},
					["text"] = "Default",
					["colorPaletteInfo"] = {
						["autoColoring"] = true,
						["colorPaletteIdx"] = 4,
					},
				}, -- [1]
				{
					["value"] = 0,
					["text"] = "<New Preset>",
				}, -- [2]
			}, -- [7]
			{
				{
					["value"] = {
					},
					["text"] = "Default",
					["colorPaletteInfo"] = {
						["autoColoring"] = true,
						["colorPaletteIdx"] = 4,
					},
				}, -- [1]
				{
					["value"] = 0,
					["text"] = "<New Preset>",
				}, -- [2]
			}, -- [8]
			{
				{
					["value"] = {
					},
					["text"] = "Default",
					["colorPaletteInfo"] = {
						["autoColoring"] = true,
						["colorPaletteIdx"] = 4,
					},
				}, -- [1]
				{
					["value"] = 0,
					["text"] = "<New Preset>",
				}, -- [2]
			}, -- [9]
			{
				{
					["value"] = {
					},
					["text"] = "Default",
					["colorPaletteInfo"] = {
						["autoColoring"] = true,
						["colorPaletteIdx"] = 4,
					},
				}, -- [1]
				{
					["value"] = 0,
					["text"] = "<New Preset>",
				}, -- [2]
			}, -- [10]
			{
				{
					["value"] = {
					},
					["text"] = "Default",
					["colorPaletteInfo"] = {
						["autoColoring"] = true,
						["colorPaletteIdx"] = 4,
					},
				}, -- [1]
				{
					["value"] = 0,
					["text"] = "<New Preset>",
				}, -- [2]
			}, -- [11]
			{
				{
					["value"] = {
					},
					["text"] = "Default",
					["colorPaletteInfo"] = {
						["autoColoring"] = true,
						["colorPaletteIdx"] = 4,
					},
				}, -- [1]
				{
					["value"] = 0,
					["text"] = "<New Preset>",
				}, -- [2]
			}, -- [12]
			{
				{
					["value"] = {
					},
					["text"] = "Default",
					["colorPaletteInfo"] = {
						["autoColoring"] = true,
						["colorPaletteIdx"] = 4,
					},
				}, -- [1]
				{
					["value"] = 0,
					["text"] = "<New Preset>",
				}, -- [2]
			}, -- [13]
			{
				{
					["value"] = {
					},
					["text"] = "Default",
					["colorPaletteInfo"] = {
						["autoColoring"] = true,
						["colorPaletteIdx"] = 4,
					},
				}, -- [1]
				{
					["value"] = 0,
					["text"] = "<New Preset>",
				}, -- [2]
			}, -- [14]
			{
				{
					["value"] = {
					},
					["text"] = "Default",
					["colorPaletteInfo"] = {
						["autoColoring"] = true,
						["colorPaletteIdx"] = 4,
					},
				}, -- [1]
				{
					["value"] = 0,
					["text"] = "<New Preset>",
				}, -- [2]
			}, -- [15]
			{
				{
					["value"] = {
					},
					["text"] = "Default",
					["colorPaletteInfo"] = {
						["autoColoring"] = true,
						["colorPaletteIdx"] = 4,
					},
				}, -- [1]
				{
					["value"] = 0,
					["text"] = "<New Preset>",
				}, -- [2]
			}, -- [16]
			{
				{
					["value"] = {
					},
					["text"] = "Default",
					["colorPaletteInfo"] = {
						["autoColoring"] = true,
						["colorPaletteIdx"] = 4,
					},
				}, -- [1]
				{
					["value"] = 0,
					["text"] = "<New Preset>",
				}, -- [2]
			}, -- [17]
			{
				{
					["value"] = {
					},
					["text"] = "Default",
					["colorPaletteInfo"] = {
						["autoColoring"] = true,
						["colorPaletteIdx"] = 4,
					},
				}, -- [1]
				{
					["value"] = 0,
					["text"] = "<New Preset>",
				}, -- [2]
			}, -- [18]
			{
				{
					["value"] = {
					},
					["text"] = "Default",
					["colorPaletteInfo"] = {
						["autoColoring"] = true,
						["colorPaletteIdx"] = 4,
					},
				}, -- [1]
				{
					["value"] = 0,
					["text"] = "<New Preset>",
				}, -- [2]
			}, -- [19]
			{
				{
					["value"] = {
					},
					["text"] = "Default",
					["colorPaletteInfo"] = {
						["autoColoring"] = true,
						["colorPaletteIdx"] = 4,
					},
				}, -- [1]
				{
					["value"] = 0,
					["text"] = "<New Preset>",
				}, -- [2]
			}, -- [20]
			{
				{
					["value"] = {
					},
					["text"] = "Default",
					["colorPaletteInfo"] = {
						["autoColoring"] = true,
						["colorPaletteIdx"] = 4,
					},
				}, -- [1]
				{
					["value"] = 0,
					["text"] = "<New Preset>",
				}, -- [2]
			}, -- [21]
			{
				{
					["value"] = {
					},
					["text"] = "Default",
					["colorPaletteInfo"] = {
						["autoColoring"] = true,
						["colorPaletteIdx"] = 4,
					},
				}, -- [1]
				{
					["value"] = 0,
					["text"] = "<New Preset>",
				}, -- [2]
			}, -- [22]
			{
				{
					["value"] = {
					},
					["text"] = "Default",
					["colorPaletteInfo"] = {
						["autoColoring"] = true,
						["colorPaletteIdx"] = 4,
					},
				}, -- [1]
				{
					["value"] = 0,
					["text"] = "<New Preset>",
				}, -- [2]
			}, -- [23]
			{
				{
					["value"] = {
					},
					["text"] = "Default",
					["colorPaletteInfo"] = {
						["autoColoring"] = true,
						["colorPaletteIdx"] = 4,
					},
				}, -- [1]
				{
					["value"] = 0,
					["text"] = "<New Preset>",
				}, -- [2]
			}, -- [24]
			{
				{
					["value"] = {
					},
					["text"] = "Default",
					["colorPaletteInfo"] = {
						["autoColoring"] = true,
						["colorPaletteIdx"] = 4,
					},
				}, -- [1]
				{
					["value"] = 0,
					["text"] = "<New Preset>",
				}, -- [2]
			}, -- [25]
			{
				{
					["value"] = {
					},
					["text"] = "Default",
					["colorPaletteInfo"] = {
						["autoColoring"] = true,
						["colorPaletteIdx"] = 4,
					},
				}, -- [1]
				{
					["value"] = 0,
					["text"] = "<New Preset>",
				}, -- [2]
			}, -- [26]
			{
				{
					["value"] = {
					},
					["text"] = "Default",
					["colorPaletteInfo"] = {
						["autoColoring"] = true,
						["colorPaletteIdx"] = 4,
					},
				}, -- [1]
				{
					["value"] = 0,
					["text"] = "<New Preset>",
				}, -- [2]
			}, -- [27]
			{
				{
					["value"] = {
					},
					["text"] = "Default",
					["colorPaletteInfo"] = {
						["autoColoring"] = true,
						["colorPaletteIdx"] = 4,
					},
				}, -- [1]
				{
					["value"] = 0,
					["text"] = "<New Preset>",
				}, -- [2]
			}, -- [28]
			{
				{
					["difficulty"] = 10,
					["week"] = 6,
					["mdi"] = {
						["freeholdJoined"] = false,
						["freehold"] = 1,
						["beguiling"] = 1,
					},
					["colorPaletteInfo"] = {
						["autoColoring"] = true,
						["colorPaletteIdx"] = 4,
					},
					["text"] = "Default",
					["objects"] = {
					},
					["value"] = {
						["currentPull"] = 1,
						["currentSublevel"] = 1,
						["pulls"] = {
							{
								["color"] = "ff3eff",
							}, -- [1]
						},
						["currentDungeonIdx"] = 29,
						["teeming"] = false,
						["selection"] = {
							1, -- [1]
						},
						["riftOffsets"] = {
							[6] = {
							},
						},
					},
				}, -- [1]
				{
					["difficulty"] = 20,
					["week"] = 10,
					["mdi"] = {
						["freeholdJoined"] = false,
						["beguiling"] = 1,
						["freehold"] = 1,
					},
					["value"] = {
						["currentPull"] = 33,
						["currentSublevel"] = 2,
						["riftOffsets"] = {
							[12] = {
							},
							[10] = {
							},
						},
						["currentDungeonIdx"] = 29,
						["teeming"] = false,
						["selection"] = {
							33, -- [1]
						},
						["pulls"] = {
							{
								{
									nil, -- [1]
									1, -- [2]
									3, -- [3]
									2, -- [4]
								}, -- [1]
								{
									1, -- [1]
									[5] = 2,
									[6] = 3,
								}, -- [2]
								{
									[7] = 1,
								}, -- [3]
								["color"] = "ff3eff",
								[29] = {
									1, -- [1]
								},
							}, -- [1]
							{
								{
									6, -- [1]
									5, -- [2]
									[6] = 4,
								}, -- [1]
								{
									[3] = 4,
								}, -- [2]
								nil, -- [3]
								{
									[4] = 1,
									[5] = 2,
								}, -- [4]
								{
									[7] = 1,
								}, -- [5]
								["color"] = "3eff9e",
							}, -- [2]
							{
								[31] = {
									1, -- [1]
								},
								["color"] = "ff3e3e",
							}, -- [3]
							{
								[6] = {
									1, -- [1]
								},
								["color"] = "3e9eff",
							}, -- [4]
							{
								{
									7, -- [1]
								}, -- [1]
								[4] = {
									nil, -- [1]
									5, -- [2]
									6, -- [3]
									3, -- [4]
									4, -- [5]
								},
								["color"] = "fffb3e",
							}, -- [5]
							{
								[3] = {
									3, -- [1]
									2, -- [2]
								},
								["color"] = "3eff3e",
							}, -- [6]
							{
								[33] = {
									1, -- [1]
								},
								["color"] = "ff3e9e",
							}, -- [7]
							{
								[6] = {
									2, -- [1]
								},
								["color"] = "3effff",
							}, -- [8]
							{
								[32] = {
									1, -- [1]
								},
								["color"] = "ff9b3e",
							}, -- [9]
							{
								{
									8, -- [1]
								}, -- [1]
								[4] = {
									[2] = 7,
								},
								["color"] = "3e3eff",
							}, -- [10]
							{
								[5] = {
									2, -- [1]
								},
								[2] = {
									nil, -- [1]
									7, -- [2]
									5, -- [3]
									6, -- [4]
								},
								["color"] = "a1ff3e",
							}, -- [11]
							{
								[20] = {
									4, -- [1]
									3, -- [2]
									1, -- [3]
									2, -- [4]
								},
								[24] = {
									[5] = 1,
									[6] = 2,
								},
								[21] = {
									[7] = 1,
								},
								[22] = {
									[9] = 2,
									[10] = 1,
								},
								[23] = {
									[8] = 1,
								},
								["color"] = "ff3eff",
							}, -- [12]
							{
								[34] = {
									1, -- [1]
								},
								["color"] = "3eff9e",
							}, -- [13]
							{
								[26] = {
									2, -- [1]
									7, -- [2]
									13, -- [3]
								},
								["color"] = "ff3e3e",
							}, -- [14]
							{
								[24] = {
									[2] = 4,
									[4] = 3,
								},
								[21] = {
									3, -- [1]
									[3] = 2,
								},
								[22] = {
									[6] = 4,
									[7] = 3,
								},
								[23] = {
									[5] = 2,
								},
								["color"] = "3e9eff",
							}, -- [15]
							{
								[27] = {
									[8] = 1,
								},
								[25] = {
									2, -- [1]
									1, -- [2]
									3, -- [3]
								},
								[26] = {
									[6] = 8,
									[7] = 1,
									[4] = 3,
									[5] = 6,
									[9] = 5,
								},
								["color"] = "fffb3e",
							}, -- [16]
							{
								[24] = {
									9, -- [1]
									6, -- [2]
								},
								["color"] = "3eff3e",
								[21] = {
									[3] = 6,
								},
							}, -- [17]
							{
								[20] = {
									[5] = 6,
								},
								[25] = {
									[4] = 7,
								},
								["color"] = "ff3e9e",
								[26] = {
									[3] = 15,
									[2] = 14,
									[6] = 16,
								},
								[27] = {
									3, -- [1]
								},
							}, -- [18]
							{
								[20] = {
									8, -- [1]
									7, -- [2]
								},
								["color"] = "3effff",
							}, -- [19]
							{
								["color"] = "ff9b3e",
								[22] = {
									8, -- [1]
									7, -- [2]
								},
								[23] = {
									[3] = 4,
								},
							}, -- [20]
							{
								[29] = {
								},
								["color"] = "3e3eff",
							}, -- [21]
							{
								[7] = {
									1, -- [1]
								},
								["color"] = "a1ff3e",
							}, -- [22]
							{
								[8] = {
									1, -- [1]
									3, -- [2]
									2, -- [3]
									4, -- [4]
								},
								[10] = {
									[6] = 1,
								},
								[9] = {
									[5] = 1,
								},
								["color"] = "ff3eff",
							}, -- [23]
							{
								[9] = {
									3, -- [1]
									2, -- [2]
									4, -- [3]
								},
								["color"] = "3eff9e",
							}, -- [24]
							{
								[7] = {
									2, -- [1]
								},
								["color"] = "ff3e3e",
							}, -- [25]
							{
								[8] = {
									7, -- [1]
									8, -- [2]
									5, -- [3]
									6, -- [4]
								},
								["color"] = "3e9eff",
								[11] = {
									[5] = 2,
								},
							}, -- [26]
							{
								[11] = {
									1, -- [1]
								},
								["color"] = "fffb3e",
							}, -- [27]
							{
								[11] = {
									3, -- [1]
								},
								["color"] = "3eff3e",
							}, -- [28]
							{
								[12] = {
									1, -- [1]
									3, -- [2]
									2, -- [3]
								},
								["color"] = "ff3e9e",
							}, -- [29]
							{
								[14] = {
									[2] = 1,
								},
								[13] = {
									1, -- [1]
								},
								["color"] = "3effff",
							}, -- [30]
							{
								["color"] = "ff9b3e",
								[16] = {
									1, -- [1]
								},
								[17] = {
									[2] = 2,
									[3] = 1,
								},
							}, -- [31]
							{
								[15] = {
									1, -- [1]
								},
								["color"] = "3e3eff",
							}, -- [32]
							{
								[18] = {
									[2] = 2,
									[3] = 1,
								},
								[16] = {
									2, -- [1]
								},
								["color"] = "a1ff3e",
							}, -- [33]
							{
								["color"] = "ff3eff",
								[17] = {
									nil, -- [1]
									3, -- [2]
									4, -- [3]
									5, -- [4]
								},
								[15] = {
									2, -- [1]
								},
							}, -- [34]
							{
								[18] = {
									[2] = 4,
									[4] = 3,
								},
								[16] = {
									[3] = 3,
								},
								[15] = {
									3, -- [1]
								},
								["color"] = "3eff9e",
							}, -- [35]
							{
								[19] = {
									1, -- [1]
								},
								["color"] = "ff3e3e",
							}, -- [36]
						},
					},
					["text"] = "9.1 DOS Good",
					["mdiEnabled"] = false,
					["objects"] = {
						{
							["d"] = {
								4, -- [1]
								1, -- [2]
								1, -- [3]
								true, -- [4]
								"ffffff", -- [5]
								-8, -- [6]
							},
							["t"] = {
								0, -- [1]
							},
							["l"] = {
								567.8815, -- [1]
								-333.6932, -- [2]
								553.9849, -- [3]
								-295.69605, -- [4]
							},
						}, -- [1]
					},
				}, -- [2]
				{
					["value"] = 0,
					["text"] = "<New Preset>",
				}, -- [3]
			}, -- [29]
			{
				{
					["mdiEnabled"] = false,
					["week"] = 3,
					["mdi"] = {
						["freeholdJoined"] = false,
						["beguiling"] = 1,
						["freehold"] = 1,
					},
					["difficulty"] = 10,
					["colorPaletteInfo"] = {
						["autoColoring"] = true,
						["colorPaletteIdx"] = 4,
					},
					["text"] = "Default",
					["value"] = {
						["currentPull"] = 7,
						["currentSublevel"] = 1,
						["riftOffsets"] = {
							[3] = {
							},
						},
						["currentDungeonIdx"] = 30,
						["teeming"] = false,
						["selection"] = {
							7, -- [1]
						},
						["pulls"] = {
							{
								{
									2, -- [1]
									1, -- [2]
									6, -- [3]
								}, -- [1]
								{
									1, -- [1]
									4, -- [2]
								}, -- [2]
								{
									1, -- [1]
									2, -- [2]
									5, -- [3]
									6, -- [4]
								}, -- [3]
								{
									2, -- [1]
									5, -- [2]
									6, -- [3]
								}, -- [4]
								nil, -- [5]
								nil, -- [6]
								{
									1, -- [1]
								}, -- [7]
								{
									1, -- [1]
								}, -- [8]
								["color"] = "ff3eff",
							}, -- [1]
							{
								[6] = {
									20, -- [1]
									17, -- [2]
									18, -- [3]
									19, -- [4]
								},
								[7] = {
									3, -- [1]
									2, -- [2]
								},
								[8] = {
									3, -- [1]
									2, -- [2]
								},
								[3] = {
									17, -- [1]
									18, -- [2]
								},
								["color"] = "3eff9e",
								[4] = {
									16, -- [1]
									15, -- [2]
									13, -- [3]
									12, -- [4]
								},
								[2] = {
									9, -- [1]
								},
							}, -- [2]
							{
								{
									10, -- [1]
									9, -- [2]
								}, -- [1]
								{
									8, -- [1]
									7, -- [2]
									6, -- [3]
								}, -- [2]
								{
									16, -- [1]
									15, -- [2]
									13, -- [3]
									14, -- [4]
									11, -- [5]
									12, -- [6]
								}, -- [3]
								{
									10, -- [1]
									11, -- [2]
								}, -- [4]
								["color"] = "ff3e3e",
								[9] = {
									1, -- [1]
								},
							}, -- [3]
							{
								{
									11, -- [1]
								}, -- [1]
								[11] = {
									15, -- [1]
									11, -- [2]
									12, -- [3]
									13, -- [4]
									14, -- [5]
								},
								[13] = {
									3, -- [1]
								},
								[10] = {
									6, -- [1]
								},
								[12] = {
									6, -- [1]
								},
								[4] = {
									9, -- [1]
								},
								[14] = {
									1, -- [1]
								},
								["color"] = "3e9eff",
							}, -- [4]
							{
								{
									18, -- [1]
									17, -- [2]
								}, -- [1]
								{
									10, -- [1]
									11, -- [2]
								}, -- [2]
								{
									20, -- [1]
									21, -- [2]
									19, -- [3]
								}, -- [3]
								{
									21, -- [1]
								}, -- [4]
								["color"] = "fffb3e",
								[7] = {
									4, -- [1]
								},
								[15] = {
									1, -- [1]
								},
							}, -- [5]
							{
								["color"] = "3eff3e",
								[16] = {
									5, -- [1]
									1, -- [2]
									2, -- [3]
									4, -- [4]
									3, -- [5]
									24, -- [6]
									18, -- [7]
									20, -- [8]
									21, -- [9]
									22, -- [10]
									23, -- [11]
									19, -- [12]
									14, -- [13]
									16, -- [14]
									17, -- [15]
									12, -- [16]
									13, -- [17]
									15, -- [18]
									10, -- [19]
									8, -- [20]
									9, -- [21]
									11, -- [22]
									6, -- [23]
									7, -- [24]
								},
								[17] = {
									1, -- [1]
								},
								[18] = {
									1, -- [1]
								},
							}, -- [6]
							{
								{
									15, -- [1]
								}, -- [1]
								["color"] = "ff3e9e",
								[13] = {
								},
								[4] = {
									19, -- [1]
								},
								[5] = {
									6, -- [1]
								},
							}, -- [7]
						},
					},
					["objects"] = {
					},
				}, -- [1]
				{
					["mdi"] = {
						["freeholdJoined"] = false,
						["beguiling"] = 1,
						["freehold"] = 1,
					},
					["week"] = 2,
					["difficulty"] = 16,
					["value"] = {
						["currentPull"] = 20,
						["currentSublevel"] = 1,
						["riftOffsets"] = {
							[12] = {
							},
							[2] = {
							},
						},
						["currentDungeonIdx"] = 30,
						["teeming"] = false,
						["selection"] = {
							20, -- [1]
						},
						["pulls"] = {
							{
								{
									1, -- [1]
									2, -- [2]
									3, -- [3]
								}, -- [1]
								{
									[5] = 1,
								}, -- [2]
								{
									[6] = 1,
									[7] = 2,
								}, -- [3]
								{
									[4] = 3,
								}, -- [4]
								["color"] = "ff3eff",
							}, -- [1]
							{
								{
									[2] = 6,
								}, -- [1]
								{
									[8] = 4,
								}, -- [2]
								{
									[9] = 5,
									[10] = 6,
								}, -- [3]
								{
									2, -- [1]
								}, -- [4]
								nil, -- [5]
								{
									nil, -- [1]
									nil, -- [2]
									4, -- [3]
									3, -- [4]
									1, -- [5]
									2, -- [6]
									5, -- [7]
								}, -- [6]
								["color"] = "3eff9e",
							}, -- [2]
							{
								["color"] = "ff3e3e",
								[4] = {
									5, -- [1]
									6, -- [2]
								},
								[7] = {
									[4] = 1,
								},
								[8] = {
									[3] = 1,
								},
							}, -- [3]
							{
								[6] = {
									9, -- [1]
									7, -- [2]
									6, -- [3]
									8, -- [4]
								},
								["color"] = "3e9eff",
							}, -- [4]
							{
								{
									4, -- [1]
								}, -- [1]
								[4] = {
									[2] = 1,
								},
								["color"] = "fffb3e",
							}, -- [5]
							{
								["color"] = "228b22",
							}, -- [6]
							{
								[6] = {
									[5] = 27,
									[6] = 24,
									[7] = 25,
									[8] = 26,
								},
								[7] = {
									[3] = 3,
								},
								[8] = {
									[4] = 3,
								},
								["color"] = "ff3e9e",
								[4] = {
									16, -- [1]
									15, -- [2]
								},
							}, -- [7]
							{
								[3] = {
									[2] = 17,
									[3] = 18,
								},
								[2] = {
									9, -- [1]
								},
								[6] = {
									nil, -- [1]
									nil, -- [2]
									nil, -- [3]
									17, -- [4]
									18, -- [5]
									19, -- [6]
									20, -- [7]
									15, -- [8]
								},
								["color"] = "3effff",
							}, -- [8]
							{
								["color"] = "ff9b3e",
								[4] = {
									12, -- [1]
									13, -- [2]
								},
								[7] = {
									[4] = 2,
								},
								[8] = {
									[3] = 2,
								},
							}, -- [9]
							{
								{
									[2] = 10,
								}, -- [1]
								{
									[3] = 7,
								}, -- [2]
								{
									[4] = 14,
									[5] = 13,
								}, -- [3]
								{
									10, -- [1]
								}, -- [4]
								["color"] = "3e3eff",
							}, -- [10]
							{
								{
									[4] = 9,
								}, -- [1]
								{
									6, -- [1]
								}, -- [2]
								{
									[2] = 12,
									[3] = 11,
								}, -- [3]
								{
									[5] = 11,
								}, -- [4]
								["color"] = "a1ff3e",
							}, -- [11]
							{
								[9] = {
									1, -- [1]
								},
								["color"] = "ff3eff",
							}, -- [12]
							{
								{
									11, -- [1]
								}, -- [1]
								[4] = {
									[2] = 9,
								},
								["color"] = "3eff9e",
							}, -- [13]
							{
								[14] = {
									1, -- [1]
								},
								["color"] = "ff3e3e",
							}, -- [14]
							{
								["color"] = "228b22",
							}, -- [15]
							{
								[3] = {
									[3] = 20,
									[4] = 21,
									[5] = 19,
								},
								[2] = {
									10, -- [1]
									11, -- [2]
								},
								["color"] = "fffb3e",
							}, -- [16]
							{
								{
									[3] = 17,
									[4] = 18,
								}, -- [1]
								["color"] = "3eff3e",
								[7] = {
									4, -- [1]
								},
								[4] = {
									[2] = 21,
								},
							}, -- [17]
							{
								["color"] = "228b22",
							}, -- [18]
							{
								[15] = {
									1, -- [1]
								},
								["color"] = "3effff",
							}, -- [19]
							{
								["color"] = "ff9b3e",
								[16] = {
									nil, -- [1]
									11, -- [2]
									4, -- [3]
									1, -- [4]
									5, -- [5]
									3, -- [6]
									2, -- [7]
									24, -- [8]
									8, -- [9]
									9, -- [10]
									10, -- [11]
									7, -- [12]
									6, -- [13]
									21, -- [14]
									20, -- [15]
									22, -- [16]
									18, -- [17]
									19, -- [18]
									23, -- [19]
									14, -- [20]
									15, -- [21]
									16, -- [22]
									13, -- [23]
									12, -- [24]
									17, -- [25]
								},
								[17] = {
									1, -- [1]
								},
							}, -- [20]
							{
								[18] = {
									1, -- [1]
								},
								["color"] = "3e3eff",
							}, -- [21]
						},
					},
					["text"] = "Tormented Route 1",
					["mdiEnabled"] = false,
					["objects"] = {
					},
				}, -- [2]
				{
					["value"] = 0,
					["text"] = "<New Preset>",
				}, -- [3]
			}, -- [30]
			{
				{
					["value"] = {
						["currentSublevel"] = 1,
						["selection"] = {
						},
					},
					["text"] = "Default",
					["colorPaletteInfo"] = {
						["autoColoring"] = true,
						["colorPaletteIdx"] = 4,
					},
				}, -- [1]
				{
					["mdiEnabled"] = false,
					["week"] = 1,
					["difficulty"] = 16,
					["value"] = {
						["currentPull"] = 19,
						["currentSublevel"] = 1,
						["riftOffsets"] = {
							{
							}, -- [1]
							[10] = {
							},
						},
						["currentDungeonIdx"] = 31,
						["teeming"] = false,
						["selection"] = {
							19, -- [1]
						},
						["pulls"] = {
							{
								{
									4, -- [1]
									5, -- [2]
									8, -- [3]
									7, -- [4]
									6, -- [5]
								}, -- [1]
								{
									3, -- [1]
								}, -- [2]
								nil, -- [3]
								{
									2, -- [1]
									3, -- [2]
								}, -- [4]
								[30] = {
									2, -- [1]
								},
								[31] = {
									2, -- [1]
								},
								[29] = {
									2, -- [1]
								},
								["color"] = "ff3eff",
							}, -- [1]
							{
								[3] = {
								},
								[2] = {
								},
								[4] = {
								},
								["color"] = "3eff9e",
							}, -- [2]
							{
								[3] = {
									10, -- [1]
									9, -- [2]
								},
								[4] = {
									[3] = 7,
									[4] = 8,
								},
								["color"] = "ff3e3e",
							}, -- [3]
							{
								[27] = {
									1, -- [1]
								},
								["color"] = "3e9eff",
							}, -- [4]
							{
								[5] = {
									4, -- [1]
									3, -- [2]
								},
								["color"] = "fffb3e",
							}, -- [5]
							{
								[28] = {
									1, -- [1]
								},
								["color"] = "3eff3e",
							}, -- [6]
							{
								[26] = {
									1, -- [1]
								},
								["color"] = "ff3e9e",
							}, -- [7]
							{
								["color"] = "3effff",
								[6] = {
									1, -- [1]
								},
								[7] = {
									[2] = 1,
								},
							}, -- [8]
							{
								[11] = {
									12, -- [1]
								},
								[13] = {
									9, -- [1]
								},
								[8] = {
									13, -- [1]
								},
								[3] = {
									8, -- [1]
									6, -- [2]
								},
								["color"] = "ff9b3e",
								[9] = {
									8, -- [1]
								},
							}, -- [9]
							{
								[8] = {
									[3] = 14,
								},
								[10] = {
									[2] = 4,
								},
								[9] = {
									[4] = 9,
								},
								["color"] = "3e3eff",
							}, -- [10]
							{
								nil, -- [1]
								{
									4, -- [1]
								}, -- [2]
								{
									4, -- [1]
									5, -- [2]
								}, -- [3]
								{
									4, -- [1]
								}, -- [4]
								[24] = {
									1, -- [1]
								},
								["color"] = "a1ff3e",
							}, -- [11]
							{
								[11] = {
									[4] = 13,
								},
								[8] = {
									[3] = 18,
								},
								["color"] = "ff3eff",
								[9] = {
									10, -- [1]
								},
								[23] = {
									3, -- [1]
								},
							}, -- [12]
							{
								[11] = {
									[4] = 10,
									[5] = 9,
								},
								[13] = {
									[2] = 7,
								},
								[12] = {
									9, -- [1]
								},
								[9] = {
									[3] = 7,
								},
								["color"] = "3eff9e",
							}, -- [13]
							{
								[11] = {
									11, -- [1]
								},
								[13] = {
									[3] = 8,
								},
								[8] = {
									[4] = 11,
									[5] = 12,
								},
								[12] = {
									[2] = 10,
								},
								["color"] = "ff3e3e",
							}, -- [14]
							{
								[11] = {
									[2] = 3,
									[4] = 4,
								},
								[13] = {
									[3] = 2,
								},
								[8] = {
								},
								[10] = {
									3, -- [1]
								},
								["color"] = "3e9eff",
								[9] = {
									[5] = 4,
								},
							}, -- [15]
							{
								[8] = {
									15, -- [1]
									14, -- [2]
								},
								[16] = {
								},
								[15] = {
								},
								[10] = {
									4, -- [1]
								},
								["color"] = "fffb3e",
								[19] = {
								},
								[9] = {
									9, -- [1]
								},
								[18] = {
								},
							}, -- [16]
							{
								[17] = {
									1, -- [1]
									[3] = 2,
								},
								[18] = {
								},
								["color"] = "3eff3e",
								[19] = {
								},
								[16] = {
									1, -- [1]
								},
							}, -- [17]
							{
								[17] = {
								},
								[18] = {
									[5] = 1,
								},
								["color"] = "ff3e9e",
								[19] = {
									5, -- [1]
									1, -- [2]
									2, -- [3]
									4, -- [4]
									3, -- [5]
									6, -- [6]
								},
								[16] = {
									[4] = 2,
								},
							}, -- [18]
							{
								[17] = {
									4, -- [1]
									3, -- [2]
								},
								[18] = {
									2, -- [1]
								},
								["color"] = "3effff",
								[19] = {
								},
								[16] = {
									[4] = 3,
								},
							}, -- [19]
							{
								[21] = {
									1, -- [1]
								},
								[29] = {
								},
								["color"] = "ff9b3e",
							}, -- [20]
							{
								[17] = {
								},
								[8] = {
								},
								[18] = {
								},
								["color"] = "3e3eff",
								[12] = {
								},
								[10] = {
								},
							}, -- [21]
						},
					},
					["colorPaletteInfo"] = {
						["autoColoring"] = true,
						["colorPaletteIdx"] = 4,
					},
					["text"] = "Mists Fortified (no inspired)",
					["objects"] = {
						{
							["d"] = {
								5, -- [1]
								1, -- [2]
								1, -- [3]
								true, -- [4]
								"4e7526", -- [5]
								-8, -- [6]
								true, -- [7]
							},
							["t"] = {
								0, -- [1]
							},
							["l"] = {
								724.12586383, -- [1]
								-168.697192305, -- [2]
								708.433099875, -- [3]
								-185.222146285, -- [4]
								691.43260905, -- [5]
								-153.1233142, -- [6]
							},
						}, -- [1]
						{
							["n"] = true,
							["d"] = {
								245.9665832236149, -- [1]
								-382.7348152138935, -- [2]
								1, -- [3]
								true, -- [4]
								"Avoid reavers.", -- [5]
							},
						}, -- [2]
						{
							["n"] = true,
							["d"] = {
								600.9513223815301, -- [1]
								-89.19667303713616, -- [2]
								1, -- [3]
								true, -- [4]
								"BL Droman 30ish%", -- [5]
							},
						}, -- [3]
						{
							["n"] = true,
							["d"] = {
								722.4029539512705, -- [1]
								-158.7520584761964, -- [2]
								1, -- [3]
								true, -- [4]
								"Pull packs together if you got a big dick + BL. remember to Belf racial (Arcane  Torret) Spiteclaws.", -- [5]
							},
						}, -- [4]
						{
							["n"] = true,
							["d"] = {
								632.6656950169526, -- [1]
								-173.1676824013915, -- [2]
								1, -- [3]
								true, -- [4]
								"Shrooms work as healthpots. open mid fight if you can.", -- [5]
							},
						}, -- [5]
						{
							["n"] = true,
							["d"] = {
								679.306383268929, -- [1]
								-153.1233149545635, -- [2]
								1, -- [3]
								true, -- [4]
								"Inspired- Sheep,trap,hex etc. Cleaver. Pullull em back here.", -- [5]
							},
						}, -- [6]
						{
							["n"] = true,
							["d"] = {
								515.1783600266126, -- [1]
								-184.7001815415475, -- [2]
								1, -- [3]
								true, -- [4]
								"Inspired- always cc on packs with Tender.", -- [5]
							},
						}, -- [7]
					},
					["mdi"] = {
						["freeholdJoined"] = false,
						["beguiling"] = 1,
						["freehold"] = 1,
					},
				}, -- [2]
				{
					["value"] = 0,
					["text"] = "<New Preset>",
				}, -- [3]
			}, -- [31]
			{
				{
					["value"] = {
						["currentSublevel"] = 1,
						["selection"] = {
						},
					},
					["text"] = "Default",
					["colorPaletteInfo"] = {
						["autoColoring"] = true,
						["colorPaletteIdx"] = 4,
					},
				}, -- [1]
				{
					["objects"] = {
					},
					["week"] = 11,
					["difficulty"] = 17,
					["value"] = {
						["currentPull"] = 15,
						["currentSublevel"] = 1,
						["riftOffsets"] = {
							[11] = {
							},
						},
						["currentDungeonIdx"] = 32,
						["teeming"] = false,
						["selection"] = {
							15, -- [1]
						},
						["pulls"] = {
							{
								["color"] = "1609ca",
							}, -- [1]
							{
								[4] = {
									16, -- [1]
									4, -- [2]
									3, -- [3]
									6, -- [4]
									2, -- [5]
									5, -- [6]
									10, -- [7]
									11, -- [8]
									21, -- [9]
									9, -- [10]
									19, -- [11]
									18, -- [12]
									14, -- [13]
									13, -- [14]
									7, -- [15]
									17, -- [16]
									20, -- [17]
									15, -- [18]
									8, -- [19]
									12, -- [20]
								},
								["color"] = "65cf4c",
							}, -- [2]
							{
								[15] = {
									1, -- [1]
								},
								["color"] = "5e3ebf",
							}, -- [3]
							{
								[15] = {
									2, -- [1]
								},
								["color"] = "a08711",
							}, -- [4]
							{
								[16] = {
									1, -- [1]
								},
								["color"] = "815275",
							}, -- [5]
							{
								["color"] = "e7bbe9",
								[17] = {
									[5] = 1,
								},
								[20] = {
									[4] = 1,
								},
								[18] = {
									3, -- [1]
									1, -- [2]
									2, -- [3]
								},
							}, -- [6]
							{
								[18] = {
									[2] = 5,
									[4] = 4,
								},
								[6] = {
									4, -- [1]
								},
								[21] = {
									[3] = 1,
								},
								["color"] = "281185",
							}, -- [7]
							{
								[21] = {
									[3] = 5,
								},
								[19] = {
									1, -- [1]
									2, -- [2]
								},
								[22] = {
									[4] = 1,
								},
								["color"] = "e4b0d5",
							}, -- [8]
							{
								[21] = {
									[3] = 2,
								},
								[19] = {
									4, -- [1]
									3, -- [2]
								},
								["color"] = "be2230",
							}, -- [9]
							{
								[23] = {
									6, -- [1]
									5, -- [2]
									4, -- [3]
								},
								["color"] = "7db147",
							}, -- [10]
							{
								["color"] = "4334fe",
								[17] = {
									[4] = 2,
								},
								[23] = {
									2, -- [1]
									3, -- [2]
									1, -- [3]
								},
							}, -- [11]
							{
								[21] = {
									3, -- [1]
								},
								["color"] = "2ce27a",
								[24] = {
									[2] = 1,
								},
							}, -- [12]
							{
								[18] = {
									[3] = 9,
									[4] = 8,
								},
								["color"] = "d326d1",
								[20] = {
									[2] = 3,
								},
								[21] = {
									4, -- [1]
								},
							}, -- [13]
							{
								[18] = {
									7, -- [1]
									6, -- [2]
								},
								["color"] = "29f25f",
							}, -- [14]
							{
								[18] = {
									11, -- [1]
									10, -- [2]
								},
								["color"] = "0323b3",
							}, -- [15]
							{
								[25] = {
									1, -- [1]
								},
								["color"] = "ababc8",
							}, -- [16]
							{
								[25] = {
									2, -- [1]
								},
								["color"] = "0414d4",
							}, -- [17]
							{
								[26] = {
									1, -- [1]
								},
								["color"] = "0ad6ac",
							}, -- [18]
							{
								[18] = {
									12, -- [1]
								},
								["color"] = "4c0bc9",
								[21] = {
									[2] = 8,
									[3] = 7,
								},
							}, -- [19]
							{
								[21] = {
									9, -- [1]
								},
								[20] = {
									[2] = 4,
								},
								["color"] = "d2eede",
							}, -- [20]
							{
								[21] = {
									6, -- [1]
								},
								[22] = {
									[2] = 2,
								},
								[24] = {
									[3] = 2,
								},
								["color"] = "2e51be",
							}, -- [21]
							{
								["color"] = "5bb8ff",
							}, -- [22]
							{
								[27] = {
									1, -- [1]
								},
								["color"] = "da3a38",
							}, -- [23]
							{
								["color"] = "53d350",
								[25] = {
									3, -- [1]
								},
								[4] = {
									[2] = 22,
								},
							}, -- [24]
							{
								[7] = {
									[3] = 2,
								},
								[28] = {
									[2] = 5,
									[6] = 6,
								},
								[29] = {
									[4] = 3,
								},
								[4] = {
									26, -- [1]
								},
								[30] = {
									[5] = 3,
								},
								["color"] = "2d7f65",
							}, -- [25]
							{
								["color"] = "30acad",
								[28] = {
									4, -- [1]
								},
								[4] = {
									[2] = 25,
								},
							}, -- [26]
							{
								[31] = {
									2, -- [1]
								},
								["color"] = "6569c9",
							}, -- [27]
							{
								[31] = {
									[4] = 3,
								},
								[28] = {
									8, -- [1]
								},
								[29] = {
									[2] = 6,
								},
								[30] = {
									[3] = 5,
								},
								["color"] = "d6b62f",
							}, -- [28]
							{
								["color"] = "3751c5",
								[28] = {
									9, -- [1]
								},
								[29] = {
									[2] = 7,
								},
								[31] = {
									[3] = 4,
								},
							}, -- [29]
							{
								[32] = {
									1, -- [1]
								},
								["color"] = "9ad308",
							}, -- [30]
							{
								[24] = {
									[3] = 3,
								},
								[22] = {
									3, -- [1]
								},
								[23] = {
									[2] = 7,
								},
								["color"] = "343023",
							}, -- [31]
							{
								[33] = {
									16, -- [1]
									1, -- [2]
								},
								["color"] = "47e4b0",
							}, -- [32]
							{
								[34] = {
									1, -- [1]
								},
								[33] = {
									nil, -- [1]
									3, -- [2]
									2, -- [3]
									5, -- [4]
									17, -- [5]
									4, -- [6]
									7, -- [7]
									10, -- [8]
									11, -- [9]
									9, -- [10]
									15, -- [11]
									12, -- [12]
									8, -- [13]
									6, -- [14]
									14, -- [15]
									13, -- [16]
								},
								["color"] = "550c31",
							}, -- [33]
							{
								["color"] = "31bebf",
							}, -- [34]
						},
					},
					["text"] = "Tyrannical Rogue Skip",
					["mdi"] = {
						["freeholdJoined"] = false,
						["beguiling"] = 1,
						["freehold"] = 1,
					},
					["mdiEnabled"] = false,
				}, -- [2]
				{
					["mdi"] = {
						["freeholdJoined"] = false,
						["beguiling"] = 1,
						["freehold"] = 1,
					},
					["week"] = 9,
					["value"] = {
						["currentPull"] = 3,
						["currentSublevel"] = 1,
						["riftOffsets"] = {
							[9] = {
							},
						},
						["currentDungeonIdx"] = 32,
						["teeming"] = false,
						["selection"] = {
							3, -- [1]
						},
						["pulls"] = {
							{
								[7] = {
									[4] = 1,
								},
								[8] = {
									[2] = 3,
								},
								[3] = {
									[3] = 5,
								},
								["color"] = "ff3eff",
								[9] = {
									1, -- [1]
								},
							}, -- [1]
							{
								[8] = {
									[2] = 2,
								},
								[13] = {
									1, -- [1]
								},
								[3] = {
									[3] = 3,
								},
								["color"] = "3eff9e",
							}, -- [2]
							{
								[8] = {
									1, -- [1]
								},
								["color"] = "ff3e3e",
								[9] = {
									[3] = 2,
								},
								[3] = {
									[2] = 4,
								},
							}, -- [3]
							{
								[13] = {
									[4] = 2,
								},
								[8] = {
									4, -- [1]
								},
								[3] = {
									[2] = 6,
								},
								["color"] = "3e9eff",
								[9] = {
									[3] = 3,
								},
							}, -- [4]
							{
								[41] = {
									1, -- [1]
								},
								["color"] = "fffb3e",
							}, -- [5]
							{
								[4] = {
									16, -- [1]
									4, -- [2]
									3, -- [3]
									6, -- [4]
									2, -- [5]
									5, -- [6]
									10, -- [7]
									11, -- [8]
									21, -- [9]
									9, -- [10]
									19, -- [11]
									18, -- [12]
									14, -- [13]
									13, -- [14]
									7, -- [15]
									17, -- [16]
									20, -- [17]
									15, -- [18]
									8, -- [19]
									12, -- [20]
								},
								["color"] = "3eff3e",
							}, -- [6]
							{
								[15] = {
									1, -- [1]
								},
								["color"] = "ff3e9e",
							}, -- [7]
							{
								[15] = {
									2, -- [1]
								},
								["color"] = "3effff",
							}, -- [8]
							{
								[16] = {
									1, -- [1]
								},
								["color"] = "ff9b3e",
							}, -- [9]
							{
								["color"] = "3e3eff",
								[17] = {
									[5] = 1,
								},
								[20] = {
									[4] = 1,
								},
								[18] = {
									3, -- [1]
									1, -- [2]
									2, -- [3]
								},
							}, -- [10]
							{
								[19] = {
									1, -- [1]
									2, -- [2]
								},
								["color"] = "a1ff3e",
							}, -- [11]
							{
								[17] = {
									[6] = 4,
								},
								[21] = {
									[3] = 2,
									[4] = 5,
								},
								[22] = {
									[5] = 1,
								},
								[19] = {
									4, -- [1]
									3, -- [2]
								},
								["color"] = "ff3eff",
							}, -- [12]
							{
								["color"] = "3eff9e",
								[17] = {
									[4] = 3,
								},
								[23] = {
									6, -- [1]
									5, -- [2]
									4, -- [3]
								},
							}, -- [13]
							{
								[21] = {
									3, -- [1]
								},
								["color"] = "ff3e3e",
								[24] = {
									[2] = 1,
								},
							}, -- [14]
							{
								[18] = {
									[3] = 9,
									[4] = 8,
								},
								["color"] = "3e9eff",
								[20] = {
									[2] = 3,
								},
								[21] = {
									4, -- [1]
								},
							}, -- [15]
							{
								[39] = {
									1, -- [1]
								},
								["color"] = "fffb3e",
							}, -- [16]
							{
								[25] = {
									1, -- [1]
								},
								["color"] = "3eff3e",
							}, -- [17]
							{
								[25] = {
									2, -- [1]
								},
								["color"] = "ff3e9e",
							}, -- [18]
							{
								[26] = {
									1, -- [1]
								},
								["color"] = "3effff",
							}, -- [19]
							{
								[18] = {
									12, -- [1]
								},
								["color"] = "ff9b3e",
								[21] = {
									[2] = 8,
									[3] = 7,
								},
							}, -- [20]
							{
								[21] = {
									6, -- [1]
								},
								[22] = {
									[2] = 2,
								},
								[24] = {
									[3] = 2,
								},
								["color"] = "3e3eff",
							}, -- [21]
							{
								[21] = {
									9, -- [1]
								},
								[20] = {
									[2] = 4,
								},
								["color"] = "a1ff3e",
							}, -- [22]
							{
								[38] = {
									1, -- [1]
								},
								["color"] = "ff3eff",
							}, -- [23]
							{
								[27] = {
									1, -- [1]
								},
								["color"] = "3eff9e",
							}, -- [24]
							{
								[25] = {
									8, -- [1]
								},
								["color"] = "ff3e3e",
							}, -- [25]
							{
								[28] = {
									4, -- [1]
								},
								[29] = {
									[4] = 1,
								},
								[4] = {
									[2] = 25,
								},
								[30] = {
									[3] = 2,
								},
								["color"] = "3e9eff",
							}, -- [26]
							{
								[40] = {
									1, -- [1]
								},
								["color"] = "fffb3e",
							}, -- [27]
							{
								[31] = {
									2, -- [1]
								},
								["color"] = "3eff3e",
							}, -- [28]
							{
								[31] = {
									[4] = 4,
								},
								[28] = {
									9, -- [1]
								},
								[29] = {
									[2] = 7,
								},
								[30] = {
									[3] = 6,
								},
								["color"] = "ff3e9e",
							}, -- [29]
							{
								[31] = {
									[4] = 3,
								},
								[28] = {
									8, -- [1]
								},
								[29] = {
									[2] = 6,
								},
								[30] = {
									[3] = 5,
								},
								["color"] = "3effff",
							}, -- [30]
							{
								[32] = {
									1, -- [1]
								},
								["color"] = "ff9b3e",
							}, -- [31]
							{
								[33] = {
									16, -- [1]
									1, -- [2]
								},
								["color"] = "3e3eff",
							}, -- [32]
							{
								[34] = {
									1, -- [1]
								},
								[33] = {
									nil, -- [1]
									3, -- [2]
									2, -- [3]
									5, -- [4]
									17, -- [5]
									4, -- [6]
								},
								["color"] = "a1ff3e",
							}, -- [33]
							{
								[33] = {
									12, -- [1]
									8, -- [2]
									6, -- [3]
									14, -- [4]
									13, -- [5]
								},
								["color"] = "ff3eff",
							}, -- [34]
							{
								[33] = {
									7, -- [1]
									10, -- [2]
									11, -- [3]
									9, -- [4]
									15, -- [5]
								},
								["color"] = "3eff9e",
							}, -- [35]
							{
								[35] = {
									1, -- [1]
								},
								["color"] = "ff3e3e",
							}, -- [36]
						},
					},
					["text"] = "Tyrannical No Skip",
					["difficulty"] = 15,
					["objects"] = {
						{
							["d"] = {
								9, -- [1]
								1, -- [2]
								1, -- [3]
								true, -- [4]
								"1a1ec9", -- [5]
								-8, -- [6]
								true, -- [7]
							},
							["t"] = {
								0, -- [1]
							},
							["l"] = {
								499.7969000000001, -- [1]
								-295.69605, -- [2]
								502.7903500000001, -- [3]
								-300.4812000000001, -- [4]
								502.7903500000001, -- [5]
								-300.4812000000001, -- [6]
								501.8945, -- [7]
								-305.09155, -- [8]
								501.8945, -- [9]
								-305.09155, -- [10]
								499.7969000000001, -- [11]
								-309.59265, -- [12]
								499.7969000000001, -- [13]
								-309.59265, -- [14]
								498.28925, -- [15]
								-314.18115, -- [16]
								498.28925, -- [17]
								-314.18115, -- [18]
								496.19165, -- [19]
								-318.68225, -- [20]
								496.19165, -- [21]
								-318.68225, -- [22]
								495.18655, -- [23]
								-323.5985, -- [24]
								495.18655, -- [25]
								-323.5985, -- [26]
								494.5966, -- [27]
								-328.0996, -- [28]
								494.5966, -- [29]
								-328.0996, -- [30]
								494.2907, -- [31]
								-332.6881, -- [32]
								494.2907, -- [33]
								-332.6881, -- [34]
								493.9848000000001, -- [35]
								-334.19575, -- [36]
							},
						}, -- [1]
						{
							["n"] = true,
							["d"] = {
								499.77505, -- [1]
								-268.99535, -- [2]
								1, -- [3]
								true, -- [4]
								"Grab lieutenant before fighting Doctor Ickus for MOAR BUFFS!", -- [5]
							},
						}, -- [2]
						{
							["n"] = true,
							["d"] = {
								485.22295, -- [1]
								-292.33115, -- [2]
								1, -- [3]
								true, -- [4]
								"Indeed we are jumping over the railing here to engage the tentacle and skip much of the upcoming trash", -- [5]
							},
						}, -- [3]
						{
							["n"] = true,
							["d"] = {
								501.8945, -- [1]
								-320.51765, -- [2]
								1, -- [3]
								true, -- [4]
								"Don't fall off the waterfall! There is no way back up. If one of your party falls then they'll have to sit in the ooze until they die.", -- [5]
							},
						}, -- [4]
						{
							["n"] = true,
							["d"] = {
								460.05175, -- [1]
								-398.8062000000001, -- [2]
								2, -- [3]
								true, -- [4]
								"If you have a warlock, you can use a gate here to skip pull 33, and do pulls 34 and 35 first, making pull 33 much easier", -- [5]
							},
						}, -- [5]
					},
				}, -- [3]
				{
					["value"] = 0,
					["text"] = "<New Preset>",
				}, -- [4]
			}, -- [32]
			{
				{
					["value"] = {
						["currentSublevel"] = 1,
						["selection"] = {
						},
					},
					["text"] = "Default",
					["colorPaletteInfo"] = {
						["autoColoring"] = true,
						["colorPaletteIdx"] = 4,
					},
				}, -- [1]
				{
					["objects"] = {
					},
					["week"] = 1,
					["value"] = {
						["currentPull"] = 1,
						["currentSublevel"] = 1,
						["pulls"] = {
							{
								[3] = {
									1, -- [1]
								},
								[2] = {
									[2] = 2,
									[3] = 1,
								},
								["color"] = "da0f26",
							}, -- [1]
							{
								[3] = {
									[3] = 3,
									[4] = 2,
								},
								[2] = {
									3, -- [1]
									4, -- [2]
								},
								["color"] = "7fff00",
								[5] = {
									[5] = 1,
								},
							}, -- [2]
							{
								[5] = {
									2, -- [1]
									[3] = 3,
								},
								[6] = {
									[2] = 1,
								},
								["color"] = "007fff",
							}, -- [3]
							{
								["color"] = "da0f26",
								[4] = {
									[2] = 2,
								},
								[7] = {
									1, -- [1]
								},
							}, -- [4]
							{
								[27] = {
									1, -- [1]
								},
								["color"] = "7fff00",
							}, -- [5]
							{
								{
									[2] = 1,
									[3] = 2,
								}, -- [1]
								["color"] = "007fff",
								[7] = {
									[5] = 2,
								},
								[4] = {
									1, -- [1]
									[4] = 3,
								},
							}, -- [6]
							{
								["color"] = "da0f26",
								[2] = {
									[3] = 6,
									[5] = 5,
								},
								[4] = {
									5, -- [1]
									4, -- [2]
									nil, -- [3]
									6, -- [4]
								},
							}, -- [7]
							{
								["color"] = "7fff00",
								[2] = {
									nil, -- [1]
									9, -- [2]
									8, -- [3]
									nil, -- [4]
									nil, -- [5]
									11, -- [6]
									7, -- [7]
									10, -- [8]
								},
								[7] = {
									3, -- [1]
								},
								[4] = {
									[4] = 8,
									[5] = 7,
								},
							}, -- [8]
							{
								[8] = {
									1, -- [1]
								},
								["color"] = "007fff",
							}, -- [9]
							{
								[9] = {
									2, -- [1]
									1, -- [2]
								},
								["color"] = "da0f26",
							}, -- [10]
							{
								[11] = {
									1, -- [1]
									2, -- [2]
									3, -- [3]
									4, -- [4]
								},
								[9] = {
									[5] = 3,
								},
								["color"] = "7fff00",
							}, -- [11]
							{
								[12] = {
									1, -- [1]
								},
								["color"] = "007fff",
							}, -- [12]
							{
								[28] = {
									1, -- [1]
								},
								["color"] = "da0f26",
							}, -- [13]
							{
								["color"] = "7fff00",
								[13] = {
									[3] = 1,
								},
								[9] = {
									4, -- [1]
									5, -- [2]
								},
							}, -- [14]
							{
								["color"] = "007fff",
								[13] = {
									[2] = 2,
								},
								[9] = {
									6, -- [1]
									[3] = 7,
								},
							}, -- [15]
							{
								["color"] = "da0f26",
								[13] = {
									[3] = 6,
								},
								[9] = {
									10, -- [1]
									11, -- [2]
								},
							}, -- [16]
							{
								{
									10, -- [1]
									8, -- [2]
									9, -- [3]
									11, -- [4]
									7, -- [5]
								}, -- [1]
								[15] = {
									[6] = 1,
								},
								["color"] = "7fff00",
							}, -- [17]
							{
								[30] = {
									1, -- [1]
								},
								["color"] = "007fff",
							}, -- [18]
							{
								["color"] = "da0f26",
								[16] = {
									[2] = 1,
									[3] = 2,
								},
								[17] = {
									1, -- [1]
								},
							}, -- [19]
							{
								[18] = {
									1, -- [1]
								},
								[16] = {
									[2] = 3,
								},
								["color"] = "7fff00",
								[19] = {
									[3] = 1,
								},
							}, -- [20]
							{
								["color"] = "007fff",
								[19] = {
									[2] = 2,
								},
								[20] = {
									[3] = 2,
								},
								[16] = {
									4, -- [1]
								},
							}, -- [21]
							{
								[18] = {
									2, -- [1]
								},
								[16] = {
									[2] = 5,
								},
								[20] = {
									[3] = 3,
								},
								["color"] = "da0f26",
							}, -- [22]
							{
								[12] = {
									3, -- [1]
								},
								["color"] = "7fff00",
							}, -- [23]
							{
								["color"] = "007fff",
								[19] = {
									[2] = 3,
								},
								[17] = {
									[3] = 2,
								},
								[16] = {
									6, -- [1]
								},
							}, -- [24]
							{
								[22] = {
									1, -- [1]
								},
								["color"] = "da0f26",
							}, -- [25]
							{
								["color"] = "7fff00",
								[9] = {
									[2] = 12,
								},
								[20] = {
									6, -- [1]
								},
							}, -- [26]
							{
								[9] = {
									15, -- [1]
									14, -- [2]
									13, -- [3]
								},
								["color"] = "007fff",
							}, -- [27]
							{
								[23] = {
									1, -- [1]
								},
								["color"] = "da0f26",
							}, -- [28]
							{
								[29] = {
									1, -- [1]
								},
								["color"] = "7fff00",
							}, -- [29]
							{
								["color"] = "007fff",
								[24] = {
									[3] = 1,
									[5] = 2,
								},
								[9] = {
									17, -- [1]
									[4] = 16,
								},
								[5] = {
									[2] = 5,
									[6] = 4,
								},
							}, -- [30]
							{
								["color"] = "da0f26",
								[6] = {
									[2] = 6,
								},
								[12] = {
									[3] = 5,
								},
								[13] = {
									7, -- [1]
								},
							}, -- [31]
							{
								[11] = {
									[3] = 16,
									[4] = 17,
									[5] = 15,
								},
								[10] = {
									10, -- [1]
									9, -- [2]
								},
								["color"] = "7fff00",
							}, -- [32]
							{
								[26] = {
									4, -- [1]
									6, -- [2]
									2, -- [3]
									1, -- [4]
									5, -- [5]
									3, -- [6]
								},
								["color"] = "007fff",
							}, -- [33]
							{
								["color"] = "da0f26",
								[10] = {
									[4] = 12,
									[5] = 11,
								},
								[26] = {
									7, -- [1]
									8, -- [2]
									9, -- [3]
								},
							}, -- [34]
							{
								[11] = {
									19, -- [1]
									21, -- [2]
									18, -- [3]
									22, -- [4]
									20, -- [5]
								},
								["color"] = "7fff00",
							}, -- [35]
							{
								[11] = {
									24, -- [1]
									23, -- [2]
								},
								[10] = {
									[3] = 13,
									[4] = 14,
								},
								["color"] = "007fff",
							}, -- [36]
							{
								[3] = {
									6, -- [1]
									[3] = 5,
								},
								[12] = {
									[2] = 6,
								},
								["color"] = "da0f26",
							}, -- [37]
							{
								["color"] = "7fff00",
								[5] = {
									7, -- [1]
									[5] = 6,
								},
								[24] = {
									nil, -- [1]
									5, -- [2]
									7, -- [3]
									3, -- [4]
									nil, -- [5]
									6, -- [6]
									8, -- [7]
									4, -- [8]
								},
							}, -- [38]
							{
								[25] = {
									1, -- [1]
								},
								["color"] = "007fff",
							}, -- [39]
						},
						["currentDungeonIdx"] = 33,
						["teeming"] = false,
						["selection"] = {
							1, -- [1]
						},
						["riftOffsets"] = {
							{
							}, -- [1]
						},
					},
					["text"] = "SD 9.1 by Wan no skips",
					["difficulty"] = 2,
					["mdi"] = {
						["beguiling"] = 1,
						["freeholdJoined"] = false,
						["freehold"] = 1,
					},
				}, -- [2]
				{
					["mdi"] = {
						["freeholdJoined"] = false,
						["freehold"] = 1,
						["beguiling"] = 1,
					},
					["week"] = 11,
					["difficulty"] = 15,
					["value"] = {
						["currentPull"] = 17,
						["currentSublevel"] = 1,
						["riftOffsets"] = {
							[11] = {
							},
						},
						["currentDungeonIdx"] = 33,
						["teeming"] = false,
						["selection"] = {
							17, -- [1]
						},
						["pulls"] = {
							{
								nil, -- [1]
								{
									nil, -- [1]
									2, -- [2]
									1, -- [3]
									3, -- [4]
									4, -- [5]
								}, -- [2]
								{
									1, -- [1]
									[6] = 3,
									[7] = 2,
								}, -- [3]
								{
								}, -- [4]
								{
									[8] = 1,
								}, -- [5]
								["color"] = "2a9b90",
							}, -- [1]
							{
								[5] = {
									2, -- [1]
									[3] = 3,
								},
								[6] = {
									[2] = 1,
								},
								["color"] = "d3e497",
							}, -- [2]
							{
								["color"] = "239792",
								[4] = {
									[2] = 2,
								},
								[7] = {
									1, -- [1]
								},
							}, -- [3]
							{
								[30] = {
									2, -- [1]
								},
								["color"] = "90c6f6",
							}, -- [4]
							{
								["color"] = "59191b",
								[2] = {
									[3] = 6,
									[5] = 5,
								},
								[4] = {
									5, -- [1]
									4, -- [2]
									6, -- [3]
								},
							}, -- [5]
							{
								["color"] = "51fd49",
								[2] = {
									nil, -- [1]
									9, -- [2]
									8, -- [3]
									nil, -- [4]
									nil, -- [5]
									11, -- [6]
									7, -- [7]
									10, -- [8]
								},
								[7] = {
									3, -- [1]
								},
								[4] = {
									[4] = 8,
									[5] = 7,
								},
							}, -- [6]
							{
								[8] = {
									1, -- [1]
								},
								["color"] = "2e26c9",
							}, -- [7]
							{
								[11] = {
									[3] = 1,
									[6] = 4,
									[4] = 2,
									[5] = 3,
								},
								[9] = {
									2, -- [1]
									1, -- [2]
									[7] = 3,
								},
								["color"] = "f25ed7",
							}, -- [8]
							{
								[12] = {
									1, -- [1]
								},
								["color"] = "55423d",
							}, -- [9]
							{
								[29] = {
									2, -- [1]
								},
								["color"] = "eac47f",
							}, -- [10]
							{
								["color"] = "b81d2f",
								[13] = {
									[3] = 1,
								},
								[9] = {
									4, -- [1]
									5, -- [2]
								},
							}, -- [11]
							{
								["color"] = "39cc2f",
								[13] = {
									[2] = 2,
								},
								[9] = {
									6, -- [1]
									[3] = 7,
								},
							}, -- [12]
							{
								["color"] = "3a923b",
								[10] = {
									3, -- [1]
									2, -- [2]
									5, -- [3]
									1, -- [4]
									8, -- [5]
									7, -- [6]
									6, -- [7]
									4, -- [8]
								},
								[9] = {
									[9] = 10,
									[10] = 11,
								},
								[13] = {
									[11] = 6,
								},
							}, -- [13]
							{
								{
									10, -- [1]
									8, -- [2]
									9, -- [3]
									11, -- [4]
									7, -- [5]
								}, -- [1]
								["color"] = "d2f53d",
								[15] = {
									[6] = 1,
								},
								[27] = {
									2, -- [1]
								},
							}, -- [14]
							{
								[20] = {
									[6] = 5,
									[9] = 4,
								},
								[17] = {
									1, -- [1]
								},
								[18] = {
									[5] = 4,
								},
								["color"] = "bd0668",
								[19] = {
									[8] = 4,
								},
								[16] = {
									nil, -- [1]
									1, -- [2]
									2, -- [3]
									12, -- [4]
									[7] = 9,
								},
							}, -- [15]
							{
								[20] = {
									[8] = 3,
									[4] = 1,
									[11] = 2,
								},
								[21] = {
								},
								[18] = {
									1, -- [1]
									[6] = 2,
								},
								[12] = {
									3, -- [1]
								},
								[19] = {
									[3] = 1,
									[10] = 2,
								},
								[16] = {
									3, -- [1]
									[7] = 5,
									[9] = 4,
								},
								["color"] = "859614",
							}, -- [16]
							{
								[17] = {
									[3] = 2,
								},
								[21] = {
									[5] = 1,
									[6] = 2,
									[4] = 3,
								},
								["color"] = "8f0b8e",
								[19] = {
									[2] = 3,
								},
								[16] = {
									6, -- [1]
								},
							}, -- [17]
							{
								["color"] = "ccac10",
								[22] = {
									1, -- [1]
								},
								[28] = {
								},
							}, -- [18]
							{
								["color"] = "1f67e7",
								[28] = {
									2, -- [1]
								},
								[9] = {
									[2] = 12,
								},
								[20] = {
									6, -- [1]
								},
							}, -- [19]
							{
								[9] = {
									15, -- [1]
									14, -- [2]
									13, -- [3]
								},
								["color"] = "9ff89d",
							}, -- [20]
							{
								[23] = {
									1, -- [1]
								},
								["color"] = "690481",
							}, -- [21]
							{
								[24] = {
									[3] = 1,
									[5] = 2,
								},
								["color"] = "dd5387",
								[9] = {
									17, -- [1]
									[4] = 16,
								},
								[5] = {
									[2] = 5,
									[6] = 4,
								},
							}, -- [22]
							{
								[11] = {
									[11] = 15,
									[10] = 17,
									[9] = 16,
								},
								[10] = {
									[7] = 10,
									[8] = 9,
								},
								[26] = {
									4, -- [1]
									6, -- [2]
									2, -- [3]
									1, -- [4]
									5, -- [5]
									3, -- [6]
								},
								["color"] = "e23183",
							}, -- [23]
							{
								[11] = {
									19, -- [1]
									21, -- [2]
									18, -- [3]
									22, -- [4]
									20, -- [5]
								},
								[10] = {
									[9] = 12,
									[10] = 11,
								},
								[26] = {
									[8] = 9,
									[6] = 7,
									[7] = 8,
								},
								["color"] = "23bd90",
							}, -- [24]
							{
								[11] = {
									[4] = 24,
									[5] = 23,
								},
								[18] = {
								},
								[21] = {
								},
								[10] = {
									[6] = 13,
									[7] = 14,
								},
								[12] = {
									[2] = 6,
								},
								["color"] = "55511d",
								[16] = {
								},
								[3] = {
									6, -- [1]
									[3] = 5,
								},
							}, -- [25]
							{
								[6] = {
								},
								[13] = {
								},
								["color"] = "ee891b",
								[24] = {
									nil, -- [1]
									5, -- [2]
									7, -- [3]
									3, -- [4]
									nil, -- [5]
									6, -- [6]
									8, -- [7]
									4, -- [8]
								},
								[5] = {
									7, -- [1]
									[5] = 6,
								},
								[12] = {
								},
							}, -- [26]
							{
								[25] = {
									1, -- [1]
								},
								["color"] = "8406e3",
							}, -- [27]
						},
					},
					["text"] = "SD (1 Skip)",
					["mdiEnabled"] = false,
					["objects"] = {
						{
							["n"] = true,
							["d"] = {
								452.2732452967649, -- [1]
								-345.4663827919022, -- [2]
								1, -- [3]
								true, -- [4]
								"If 2nd Sentinal spawns here, skip the frenzied ghoul pack before 2nd boss.", -- [5]
							},
						}, -- [1]
						{
							["n"] = true,
							["d"] = {
								581.3455047576252, -- [1]
								-262.4971113506909, -- [2]
								2, -- [3]
								true, -- [4]
								"Skip with either shroud, or personal stealth and/or stealth pots.", -- [5]
							},
						}, -- [2]
						{
							["n"] = true,
							["d"] = {
								575.6390458152157, -- [1]
								-209.7123661334246, -- [2]
								1, -- [3]
								true, -- [4]
								"When third pack is almost dead, activate vase then have someone back track and bring the anklebiter pack to vase for buff refresh.", -- [5]
							},
						}, -- [3]
					},
				}, -- [3]
				{
					["value"] = 0,
					["text"] = "<New Preset>",
				}, -- [4]
			}, -- [33]
			{
				{
					["value"] = {
						["currentSublevel"] = 1,
						["selection"] = {
						},
					},
					["text"] = "Default",
					["colorPaletteInfo"] = {
						["autoColoring"] = true,
						["colorPaletteIdx"] = 4,
					},
				}, -- [1]
				{
					["mdi"] = {
						["freeholdJoined"] = false,
						["beguiling"] = 1,
						["freehold"] = 1,
					},
					["week"] = 11,
					["uid"] = "puNdg6nlMkD",
					["value"] = {
						["currentPull"] = 22,
						["currentSublevel"] = 2,
						["riftOffsets"] = {
							[11] = {
							},
						},
						["currentDungeonIdx"] = 34,
						["teeming"] = false,
						["selection"] = {
							22, -- [1]
						},
						["pulls"] = {
							{
								{
									[3] = 1,
								}, -- [1]
								{
									1, -- [1]
									2, -- [2]
									[6] = 3,
								}, -- [2]
								{
									[4] = 1,
								}, -- [3]
								{
									[5] = 2,
									[7] = 1,
								}, -- [4]
								["color"] = "1a36cf",
							}, -- [1]
							{
								{
									[2] = 2,
								}, -- [1]
								{
									[3] = 4,
								}, -- [2]
								nil, -- [3]
								{
									3, -- [1]
								}, -- [4]
								["color"] = "bbb17d",
							}, -- [2]
							{
								{
									[2] = 3,
								}, -- [1]
								{
									[4] = 5,
									[5] = 6,
								}, -- [2]
								{
									2, -- [1]
								}, -- [3]
								{
									[3] = 4,
								}, -- [4]
								["color"] = "0e0a37",
							}, -- [3]
							{
								[24] = {
									1, -- [1]
								},
								["color"] = "7c7ad1",
							}, -- [4]
							{
								[5] = {
									[2] = 1,
								},
								[6] = {
									1, -- [1]
								},
								["color"] = "b029cb",
							}, -- [5]
							{
								[8] = {
									3, -- [1]
									2, -- [2]
									4, -- [3]
									1, -- [4]
								},
								[7] = {
									[5] = 3,
									[6] = 2,
									[7] = 1,
								},
								["color"] = "12d813",
							}, -- [6]
							{
								[11] = {
									3, -- [1]
									2, -- [2]
									1, -- [3]
									[5] = 6,
									[6] = 5,
									[7] = 4,
								},
								[12] = {
									[4] = 1,
								},
								["color"] = "0c87a2",
							}, -- [7]
							{
								[11] = {
									[3] = 7,
								},
								[7] = {
									[2] = 4,
								},
								[3] = {
									[4] = 4,
									[6] = 3,
								},
								[12] = {
									2, -- [1]
									[5] = 3,
								},
								["color"] = "31e9bc",
							}, -- [8]
							{
								["color"] = "5a1762",
								[12] = {
									[2] = 4,
								},
								[7] = {
									7, -- [1]
									nil, -- [2]
									6, -- [3]
									5, -- [4]
								},
							}, -- [9]
							{
								["color"] = "c0ab87",
								[7] = {
									[3] = 8,
									[4] = 9,
								},
								[9] = {
									2, -- [1]
								},
								[8] = {
									[2] = 6,
								},
							}, -- [10]
							{
								[7] = {
									nil, -- [1]
									15, -- [2]
									nil, -- [3]
									11, -- [4]
									13, -- [5]
									14, -- [6]
									10, -- [7]
									12, -- [8]
								},
								[3] = {
									[9] = 5,
								},
								[12] = {
									[3] = 5,
								},
								[13] = {
									1, -- [1]
								},
								["color"] = "98283a",
							}, -- [11]
							{
								["color"] = "8cb6c7",
								[10] = {
									2, -- [1]
								},
								[12] = {
									[2] = 6,
								},
							}, -- [12]
							{
								[13] = {
									[2] = 2,
								},
								[10] = {
									3, -- [1]
								},
								[12] = {
									[3] = 7,
								},
								[7] = {
									[5] = 16,
								},
								[9] = {
									[4] = 3,
								},
								["color"] = "fd1502",
							}, -- [13]
							{
								[25] = {
									1, -- [1]
								},
								["color"] = "27a2eb",
							}, -- [14]
							{
								[14] = {
									1, -- [1]
								},
								["color"] = "536668",
							}, -- [15]
							{
								[27] = {
									[2] = 1,
								},
								[15] = {
									1, -- [1]
								},
								["color"] = "5f425c",
							}, -- [16]
							{
								[26] = {
									1, -- [1]
								},
								["color"] = "b9cf1d",
							}, -- [17]
							{
								[18] = {
									9, -- [1]
									8, -- [2]
									11, -- [3]
									12, -- [4]
									7, -- [5]
									10, -- [6]
								},
								["color"] = "7992a8",
							}, -- [18]
							{
								[18] = {
									3, -- [1]
									5, -- [2]
									1, -- [3]
									6, -- [4]
									4, -- [5]
									2, -- [6]
								},
								["color"] = "531ab8",
							}, -- [19]
							{
								[19] = {
									1, -- [1]
								},
								["color"] = "b6c0dd",
							}, -- [20]
							{
								[21] = {
									[2] = 1,
								},
								[22] = {
									1, -- [1]
								},
								["color"] = "bd0ebf",
							}, -- [21]
							{
								[20] = {
									1, -- [1]
								},
								["color"] = "badbcd",
							}, -- [22]
							{
								[23] = {
									1, -- [1]
								},
								["color"] = "c02d36",
							}, -- [23]
						},
					},
					["text"] = "SoA +15-19 (R, 1 Skip, No Mage) 2",
					["difficulty"] = 15,
					["objects"] = {
						{
							["n"] = true,
							["d"] = {
								324.5622074267518, -- [1]
								-321.2679559255697, -- [2]
								2, -- [3]
								true, -- [4]
								"For advanced attempts of this dungeon:\nTyrannical: Pull with adds\nFortified: Pull with boss", -- [5]
							},
						}, -- [1]
						{
							["n"] = true,
							["d"] = {
								613.9857321741628, -- [1]
								-419.7817627230513, -- [2]
								2, -- [3]
								true, -- [4]
								"Spear and pull together if you have a Kyrian. Else, chain pull into first pull. Healer: Goliath. Tank and 1 MDPS kick inquisitor. Cap totem 3 seconds into pull, followed by spear if double pulling.", -- [5]
							},
						}, -- [2]
						{
							["n"] = true,
							["d"] = {
								504.070539912532, -- [1]
								-272.4834144998915, -- [2]
								2, -- [3]
								true, -- [4]
								"Skip if Oros Coldheart", -- [5]
							},
						}, -- [3]
						{
							["n"] = true,
							["d"] = {
								426.7624385232016, -- [1]
								-376.5643204098328, -- [2]
								2, -- [3]
								true, -- [4]
								"Stack here to gather pull 7", -- [5]
							},
						}, -- [4]
						{
							["n"] = true,
							["d"] = {
								373.8975453850477, -- [1]
								-300.2474939655962, -- [2]
								2, -- [3]
								true, -- [4]
								"Stack to gather pull 6\nSpear / grip / vortex adds after gathering to limit initial add jumping while tank positions.", -- [5]
							},
						}, -- [5]
						{
							["n"] = true,
							["d"] = {
								345.4258002676369, -- [1]
								-329.817322916756, -- [2]
								2, -- [3]
								true, -- [4]
								"Healer kicks every cast from Goliath.\nTank and 1 MDPS kicks Castigator if pulled seperate, or Incinerator if together.", -- [5]
							},
						}, -- [6]
						{
							["n"] = true,
							["d"] = {
								402.9166887091643, -- [1]
								-312.2784532795142, -- [2]
								3, -- [3]
								true, -- [4]
								"Skip if Oros Coldheart", -- [5]
							},
						}, -- [7]
						{
							["n"] = true,
							["d"] = {
								344.6463432090757, -- [1]
								-396.1233582520277, -- [2]
								3, -- [3]
								true, -- [4]
								"Tyrannical: Pull together / when miniboss is at 50%\nFortified: Pull when miniboss is at 25%", -- [5]
							},
						}, -- [8]
					},
				}, -- [2]
				{
					["value"] = 0,
					["text"] = "<New Preset>",
				}, -- [3]
			}, -- [34]
			{
				{
					["objects"] = {
					},
					["week"] = 4,
					["difficulty"] = 17,
					["colorPaletteInfo"] = {
						["autoColoring"] = true,
						["colorPaletteIdx"] = 4,
					},
					["text"] = "Default",
					["mdi"] = {
						["freeholdJoined"] = false,
						["freehold"] = 1,
						["beguiling"] = 1,
					},
					["value"] = {
						["currentPull"] = 1,
						["currentSublevel"] = 1,
						["pulls"] = {
							{
								["color"] = "ff3eff",
							}, -- [1]
						},
						["currentDungeonIdx"] = 35,
						["teeming"] = false,
						["selection"] = {
							1, -- [1]
						},
						["riftOffsets"] = {
							[4] = {
							},
						},
					},
				}, -- [1]
				{
					["mdiEnabled"] = false,
					["week"] = 1,
					["difficulty"] = 17,
					["mdi"] = {
						["freeholdJoined"] = false,
						["freehold"] = 1,
						["beguiling"] = 1,
					},
					["colorPaletteInfo"] = {
						["autoColoring"] = true,
						["colorPaletteIdx"] = 4,
					},
					["text"] = "Fortified left first 1 skip",
					["value"] = {
						["currentPull"] = 24,
						["currentSublevel"] = 1,
						["riftOffsets"] = {
							{
							}, -- [1]
							[10] = {
							},
						},
						["currentDungeonIdx"] = 35,
						["teeming"] = false,
						["selection"] = {
							24, -- [1]
						},
						["pulls"] = {
							{
								{
									nil, -- [1]
									3, -- [2]
									4, -- [3]
									2, -- [4]
									1, -- [5]
									[12] = 5,
								}, -- [1]
								{
									[6] = 3,
									[7] = 2,
									[15] = 4,
									[8] = 1,
									[14] = 5,
									[13] = 7,
									[11] = 6,
								}, -- [2]
								{
									1, -- [1]
									[9] = 2,
								}, -- [3]
								{
									[10] = 1,
								}, -- [4]
								["color"] = "ff3eff",
							}, -- [1]
							{
								{
									[2] = 8,
								}, -- [1]
								[3] = {
									[3] = 4,
								},
								[6] = {
									1, -- [1]
								},
								["color"] = "3eff9e",
							}, -- [2]
							{
								["color"] = "ff3e3e",
								[33] = {
									1, -- [1]
								},
							}, -- [3]
							{
								{
									[2] = 17,
									[4] = 18,
								}, -- [1]
								nil, -- [2]
								{
									[3] = 5,
									[5] = 6,
								}, -- [3]
								{
									4, -- [1]
								}, -- [4]
								["color"] = "3e9eff",
							}, -- [4]
							{
								{
									[5] = 14,
									[2] = 15,
									[6] = 16,
								}, -- [1]
								{
									[7] = 13,
									[8] = 14,
								}, -- [2]
								{
									[3] = 8,
									[4] = 7,
								}, -- [3]
								["color"] = "fffb3e",
								[7] = {
									1, -- [1]
								},
								[9] = {
								},
							}, -- [5]
							{
								[11] = {
									[3] = 1,
								},
								[2] = {
									[8] = 17,
									[7] = 15,
									[9] = 16,
								},
								[8] = {
									[2] = 1,
								},
								[10] = {
									[5] = 1,
								},
								[12] = {
									[4] = 2,
									[6] = 1,
								},
								[9] = {
									1, -- [1]
								},
								["color"] = "3eff3e",
							}, -- [6]
							{
								[14] = {
									[2] = 1,
								},
								[13] = {
									1, -- [1]
								},
								["color"] = "ff3e9e",
								[8] = {
									[3] = 2,
								},
							}, -- [7]
							{
								[13] = {
									2, -- [1]
									3, -- [2]
								},
								["color"] = "3effff",
							}, -- [8]
							{
								[11] = {
									[4] = 2,
								},
								[10] = {
									[3] = 2,
								},
								[12] = {
									[5] = 5,
								},
								["color"] = "ff9b3e",
							}, -- [9]
							{
								["color"] = "3e3eff",
								[34] = {
									1, -- [1]
								},
							}, -- [10]
							{
								[13] = {
									[3] = 4,
								},
								[8] = {
									[2] = 6,
									[5] = 5,
								},
								[18] = {
									[6] = 5,
									[7] = 2,
									[8] = 6,
									[10] = 1,
									[12] = 7,
									[13] = 4,
									[9] = 3,
									[11] = 8,
								},
								["color"] = "a1ff3e",
								[9] = {
									4, -- [1]
									3, -- [2]
								},
							}, -- [11]
							{
								[9] = {
									5, -- [1]
									6, -- [2]
								},
								["color"] = "ff3eff",
							}, -- [12]
							{
								[11] = {
									4, -- [1]
									[6] = 3,
								},
								[17] = {
									[4] = 2,
									[7] = 1,
								},
								[10] = {
									[5] = 3,
								},
								[12] = {
									[2] = 6,
									[3] = 7,
								},
								["color"] = "3eff9e",
							}, -- [13]
							{
								["color"] = "ff3e3e",
								[36] = {
									1, -- [1]
								},
							}, -- [14]
							{
								[19] = {
									1, -- [1]
								},
								["color"] = "3e9eff",
							}, -- [15]
							{
								{
									[2] = 20,
									[3] = 19,
								}, -- [1]
								["color"] = "fffb3e",
								[22] = {
									nil, -- [1]
									nil, -- [2]
									nil, -- [3]
									5, -- [4]
									4, -- [5]
									7, -- [6]
									9, -- [7]
									6, -- [8]
									8, -- [9]
								},
								[20] = {
									1, -- [1]
								},
							}, -- [16]
							{
								["color"] = "3eff3e",
								[6] = {
									2, -- [1]
								},
								[23] = {
									[2] = 1,
								},
							}, -- [17]
							{
								{
									[4] = 21,
									[7] = 22,
									[9] = 23,
								}, -- [1]
								[21] = {
									[5] = 1,
									[6] = 2,
								},
								[22] = {
									3, -- [1]
									[3] = 2,
									[10] = 1,
								},
								[20] = {
									[2] = 2,
									[8] = 3,
								},
								["color"] = "ff3e9e",
							}, -- [18]
							{
								{
									[4] = 25,
									[5] = 24,
								}, -- [1]
								["color"] = "3effff",
								[6] = {
									[3] = 3,
								},
								[20] = {
									4, -- [1]
								},
								[23] = {
									[2] = 2,
								},
							}, -- [19]
							{
								[24] = {
									[2] = 1,
								},
								[25] = {
									1, -- [1]
								},
								["color"] = "ff9b3e",
								[15] = {
								},
								[8] = {
								},
								[9] = {
								},
								[10] = {
								},
								[11] = {
								},
								[12] = {
								},
							}, -- [20]
							{
								[27] = {
									nil, -- [1]
									1, -- [2]
									2, -- [3]
									3, -- [4]
								},
								[26] = {
									1, -- [1]
								},
								["color"] = "3e3eff",
							}, -- [21]
							{
								["color"] = "a1ff3e",
								[28] = {
									1, -- [1]
									2, -- [2]
									nil, -- [3]
									3, -- [4]
								},
								[29] = {
									[3] = 1,
								},
							}, -- [22]
							{
								[30] = {
									[2] = 1,
								},
								[31] = {
									1, -- [1]
								},
								["color"] = "ff3eff",
							}, -- [23]
							{
								[32] = {
									1, -- [1]
								},
								["color"] = "3eff9e",
							}, -- [24]
						},
					},
					["objects"] = {
						{
							["d"] = {
								4, -- [1]
								1, -- [2]
								1, -- [3]
								true, -- [4]
								"fff22b", -- [5]
								-8, -- [6]
							},
							["t"] = {
								0, -- [1]
							},
							["l"] = {
								395.28835, -- [1]
								-424.69845, -- [2]
								293.59845, -- [3]
								-328.38365, -- [4]
							},
						}, -- [1]
						{
							["d"] = {
								4, -- [1]
								1, -- [2]
								1, -- [3]
								true, -- [4]
								"ff0e19", -- [5]
								-8, -- [6]
							},
							["t"] = {
								0, -- [1]
							},
							["l"] = {
								600.59095, -- [1]
								-306.48995, -- [2]
							},
						}, -- [2]
						{
							["n"] = true,
							["d"] = {
								466.1479, -- [1]
								-137.10875, -- [2]
								1, -- [3]
								true, -- [4]
								"Repair the Goliath for the Blightbone encounter", -- [5]
							},
						}, -- [3]
						{
							["n"] = true,
							["d"] = {
								417.5972, -- [1]
								-232.63695, -- [2]
								1, -- [3]
								true, -- [4]
								"Bring Blightbone to the mobs and then hit the boss with the Spear", -- [5]
							},
						}, -- [4]
						{
							["n"] = true,
							["d"] = {
								513.1254, -- [1]
								-386.72315, -- [2]
								1, -- [3]
								true, -- [4]
								"Repair the Goliath and use it for Pull #6", -- [5]
							},
						}, -- [5]
						{
							["n"] = true,
							["d"] = {
								508.51505, -- [1]
								-452.9942, -- [2]
								1, -- [3]
								true, -- [4]
								"Allow the Tank to grab the Vented Anima!", -- [5]
							},
						}, -- [6]
						{
							["n"] = true,
							["d"] = {
								396.796, -- [1]
								-425.48505, -- [2]
								1, -- [3]
								true, -- [4]
								"Skip using Shroud, Potion of Hidden Spirit, or Mind Soothe", -- [5]
							},
						}, -- [7]
						{
							["n"] = true,
							["d"] = {
								373.6787000000001, -- [1]
								-377.72095, -- [2]
								1, -- [3]
								true, -- [4]
								"Grab the Spear and save it for the Amarth encounter", -- [5]
							},
						}, -- [8]
						{
							["n"] = true,
							["d"] = {
								211.1147, -- [1]
								-329.95685, -- [2]
								1, -- [3]
								true, -- [4]
								"Repair the Goliath and use it for Pull #11", -- [5]
							},
						}, -- [9]
						{
							["n"] = true,
							["d"] = {
								325.14985, -- [1]
								-264.4724, -- [2]
								1, -- [3]
								true, -- [4]
								"Grab the Hammer", -- [5]
							},
						}, -- [10]
						{
							["n"] = true,
							["d"] = {
								289.70915, -- [1]
								-178.9515, -- [2]
								1, -- [3]
								true, -- [4]
								"Grab the Spear and save it for the Surgeon Stitchflesh encounter", -- [5]
							},
						}, -- [11]
						{
							["n"] = true,
							["d"] = {
								119.4321, -- [1]
								-164.6179, -- [2]
								1, -- [3]
								true, -- [4]
								"Allow the Tank to grab the Vented Anima!", -- [5]
							},
						}, -- [12]
						{
							["n"] = true,
							["d"] = {
								463.8755000000001, -- [1]
								-270.1097, -- [2]
								2, -- [3]
								true, -- [4]
								"Be sure to have cooldowns ready for the double pull! ", -- [5]
							},
						}, -- [13]
						{
							["n"] = true,
							["d"] = {
								428.8281, -- [1]
								-401.5593, -- [2]
								2, -- [3]
								true, -- [4]
								"IMPORTANT: Interrupt Goresplatter and Drain Fluids! ", -- [5]
							},
						}, -- [14]
					},
				}, -- [2]
				{
					["value"] = 0,
					["text"] = "<New Preset>",
				}, -- [3]
			}, -- [35]
			{
				{
					["difficulty"] = 10,
					["week"] = 1,
					["objects"] = {
					},
					["colorPaletteInfo"] = {
						["autoColoring"] = true,
						["colorPaletteIdx"] = 4,
					},
					["text"] = "Default",
					["mdi"] = {
						["freeholdJoined"] = false,
						["beguiling"] = 1,
						["freehold"] = 1,
					},
					["value"] = {
						["currentPull"] = 1,
						["currentSublevel"] = 1,
						["riftOffsets"] = {
							{
							}, -- [1]
						},
						["currentDungeonIdx"] = 36,
						["teeming"] = false,
						["selection"] = {
							1, -- [1]
						},
						["pulls"] = {
							{
								["color"] = "ff3eff",
							}, -- [1]
						},
					},
				}, -- [1]
				{
					["difficulty"] = 13,
					["week"] = 9,
					["objects"] = {
						{
							["d"] = {
								295.8806810646079, -- [1]
								-156.0491289902239, -- [2]
								2, -- [3]
								true, -- [4]
								"Soggodon The Breaker", -- [5]
							},
							["n"] = true,
						}, -- [1]
						{
							["d"] = {
								359.97875, -- [1]
								-370.90375, -- [2]
								2, -- [3]
								true, -- [4]
								"Executioner Varruth", -- [5]
							},
							["n"] = true,
						}, -- [2]
						{
							["d"] = {
								426.3420775876049, -- [1]
								-397.8712203353521, -- [2]
								3, -- [3]
								true, -- [4]
								"Oros Coldheart", -- [5]
							},
							["n"] = true,
						}, -- [3]
						{
							["d"] = {
								436.4330143352512, -- [1]
								-355.7055203541679, -- [2]
								3, -- [3]
								true, -- [4]
								"", -- [5]
							},
							["n"] = true,
						}, -- [4]
						{
							["d"] = {
								258.30796875, -- [1]
								-235.98, -- [2]
								3, -- [3]
								true, -- [4]
								"", -- [5]
							},
							["n"] = true,
						}, -- [5]
						{
							["d"] = {
								283.4207941394972, -- [1]
								-442.7261062816664, -- [2]
								4, -- [3]
								true, -- [4]
								"Incinerator Arkolath", -- [5]
							},
							["n"] = true,
						}, -- [6]
						{
							["d"] = {
								322.9099759243336, -- [1]
								-298.7638058496404, -- [2]
								4, -- [3]
								true, -- [4]
								"Pull Lieutenant to the T section", -- [5]
							},
							["n"] = true,
						}, -- [7]
					},
					["value"] = {
						["currentPull"] = 23,
						["currentSublevel"] = 3,
						["pulls"] = {
							{
								{
									1, -- [1]
								}, -- [1]
								{
									[2] = 2,
									[3] = 1,
								}, -- [2]
								{
									[4] = 1,
								}, -- [3]
								["color"] = "85123e",
							}, -- [1]
							{
								[5] = {
									[2] = 1,
								},
								[6] = {
									[3] = 1,
								},
								[4] = {
									1, -- [1]
								},
								["color"] = "43d414",
							}, -- [2]
							{
								[8] = {
									[3] = 1,
								},
								[9] = {
									2, -- [1]
									1, -- [2]
								},
								["color"] = "a35a96",
							}, -- [3]
							{
								[10] = {
									1, -- [1]
								},
								["color"] = "e0f928",
							}, -- [4]
							{
								[30] = {
									1, -- [1]
								},
								["color"] = "1a5c32",
							}, -- [5]
							{
								[8] = {
									[2] = 3,
									[3] = 2,
								},
								["color"] = "bedd6f",
								[9] = {
									[4] = 4,
									[5] = 3,
								},
								[12] = {
									1, -- [1]
								},
							}, -- [6]
							{
								[31] = {
									1, -- [1]
								},
								["color"] = "1795b5",
							}, -- [7]
							{
								[14] = {
									1, -- [1]
								},
								["color"] = "57da40",
							}, -- [8]
							{
								[15] = {
									3, -- [1]
									2, -- [2]
									1, -- [3]
									4, -- [4]
									5, -- [5]
									7, -- [6]
								},
								["color"] = "1a33ad",
							}, -- [9]
							{
								[15] = {
									6, -- [1]
									9, -- [2]
									10, -- [3]
									11, -- [4]
									12, -- [5]
									8, -- [6]
								},
								["color"] = "f03dc7",
							}, -- [10]
							{
								[16] = {
									1, -- [1]
								},
								["color"] = "e514e0",
							}, -- [11]
							{
								[17] = {
									2, -- [1]
									1, -- [2]
								},
								["color"] = "59e64f",
							}, -- [12]
							{
								[18] = {
									[2] = 1,
								},
								[16] = {
									2, -- [1]
								},
								[17] = {
									[3] = 3,
								},
								["color"] = "63515d",
							}, -- [13]
							{
								[20] = {
									3, -- [1]
									2, -- [2]
								},
								["color"] = "7772eb",
							}, -- [14]
							{
								[18] = {
									3, -- [1]
								},
								["color"] = "ad4335",
								[20] = {
									[3] = 1,
								},
								[17] = {
									[2] = 12,
								},
							}, -- [15]
							{
								[21] = {
									1, -- [1]
								},
								["color"] = "77b3c3",
							}, -- [16]
							{
								[24] = {
									1, -- [1]
								},
								[25] = {
									[7] = 1,
								},
								[22] = {
									[2] = 1,
									[12] = 2,
								},
								[23] = {
									nil, -- [1]
									nil, -- [2]
									2, -- [3]
									1, -- [4]
									3, -- [5]
									4, -- [6]
									nil, -- [7]
									8, -- [8]
									6, -- [9]
									5, -- [10]
									7, -- [11]
								},
								["color"] = "1daf59",
							}, -- [17]
							{
								["color"] = "99c16b",
								[25] = {
									[3] = 2,
								},
								[22] = {
									3, -- [1]
									4, -- [2]
								},
							}, -- [18]
							{
								[24] = {
									[3] = 2,
								},
								[25] = {
									3, -- [1]
								},
								[22] = {
									[5] = 5,
								},
								[23] = {
									[2] = 10,
									[4] = 9,
								},
								["color"] = "e296b2",
							}, -- [19]
							{
								["color"] = "1c8109",
								[22] = {
									[7] = 6,
								},
								[23] = {
									nil, -- [1]
									15, -- [2]
									14, -- [3]
									12, -- [4]
									13, -- [5]
									11, -- [6]
								},
								[26] = {
									3, -- [1]
								},
							}, -- [20]
							{
								[24] = {
									3, -- [1]
								},
								[25] = {
									[2] = 4,
								},
								["color"] = "9a8a51",
							}, -- [21]
							{
								[27] = {
									1, -- [1]
								},
								["color"] = "ce0c37",
							}, -- [22]
							{
								["color"] = "b0a984",
							}, -- [23]
						},
						["currentDungeonIdx"] = 36,
						["teeming"] = false,
						["selection"] = {
							23, -- [1]
						},
						["riftOffsets"] = {
							[9] = {
							},
						},
					},
					["text"] = "9.1 Tormented",
					["mdiEnabled"] = false,
					["mdi"] = {
						["freeholdJoined"] = false,
						["freehold"] = 1,
						["beguiling"] = 1,
					},
				}, -- [2]
				{
					["value"] = 0,
					["text"] = "<New Preset>",
				}, -- [3]
			}, -- [36]
			{
				{
					["difficulty"] = 13,
					["week"] = 6,
					["mdiEnabled"] = false,
					["mdi"] = {
						["freeholdJoined"] = false,
						["freehold"] = 1,
						["beguiling"] = 1,
					},
					["colorPaletteInfo"] = {
						["autoColoring"] = true,
						["colorPaletteIdx"] = 4,
					},
					["text"] = "Default",
					["value"] = {
						["currentPull"] = 1,
						["currentSublevel"] = 1,
						["pulls"] = {
							{
								["color"] = "ff3eff",
							}, -- [1]
						},
						["currentDungeonIdx"] = 37,
						["teeming"] = false,
						["selection"] = {
							1, -- [1]
						},
						["riftOffsets"] = {
							[6] = {
							},
						},
					},
					["objects"] = {
					},
				}, -- [1]
				{
					["difficulty"] = 10,
					["week"] = 12,
					["mdi"] = {
						["freeholdJoined"] = false,
						["freehold"] = 1,
						["beguiling"] = 1,
					},
					["value"] = {
						["currentPull"] = 7,
						["currentSublevel"] = 1,
						["riftOffsets"] = {
							[12] = {
							},
						},
						["currentDungeonIdx"] = 37,
						["teeming"] = false,
						["selection"] = {
							7, -- [1]
						},
						["pulls"] = {
							{
								{
									1, -- [1]
									2, -- [2]
								}, -- [1]
								{
									[3] = 1,
								}, -- [2]
								{
								}, -- [3]
								{
								}, -- [4]
								["color"] = "3e4b06",
							}, -- [1]
							{
								{
									3, -- [1]
									[3] = 4,
								}, -- [1]
								nil, -- [2]
								{
									[2] = 1,
								}, -- [3]
								{
									[4] = 1,
								}, -- [4]
								[34] = {
									[5] = 1,
								},
								["color"] = "feab9f",
							}, -- [2]
							{
								{
									[3] = 2,
								}, -- [1]
								nil, -- [2]
								{
									[2] = 2,
									[6] = 3,
								}, -- [3]
								{
									[5] = 4,
								}, -- [4]
								{
									[4] = 1,
								}, -- [5]
								["color"] = "682b95",
							}, -- [3]
							{
								{
									[3] = 5,
									[4] = 6,
								}, -- [1]
								nil, -- [2]
								{
									[2] = 4,
								}, -- [3]
								{
									5, -- [1]
									[5] = 6,
								}, -- [4]
								[34] = {
									[6] = 2,
								},
								["color"] = "f4db89",
							}, -- [4]
							{
								[6] = {
									1, -- [1]
								},
								["color"] = "0d5bf7",
							}, -- [5]
							{
								[34] = {
									[2] = 3,
								},
								[7] = {
									1, -- [1]
								},
								["color"] = "8791e7",
							}, -- [6]
							{
								{
									7, -- [1]
								}, -- [1]
								["color"] = "9e579f",
								[33] = {
									[3] = 4,
								},
								[4] = {
									[2] = 7,
								},
							}, -- [7]
							{
								[8] = {
									1, -- [1]
								},
								["color"] = "dec1e2",
							}, -- [8]
							{
								[9] = {
									1, -- [1]
									2, -- [2]
								},
								["color"] = "963616",
							}, -- [9]
							{
								[11] = {
									nil, -- [1]
									2, -- [2]
									1, -- [3]
									4, -- [4]
									3, -- [5]
								},
								[10] = {
									1, -- [1]
								},
								["color"] = "8798cb",
							}, -- [10]
							{
								["color"] = "fede94",
								[13] = {
									[2] = 6,
								},
								[12] = {
									8, -- [1]
								},
							}, -- [11]
							{
								[27] = {
									[4] = 1,
								},
								[25] = {
									1, -- [1]
								},
								["color"] = "8e6a2d",
								[26] = {
									[3] = 1,
								},
								[34] = {
									[2] = 11,
								},
							}, -- [12]
							{
								["color"] = "e4eba1",
								[33] = {
									[3] = 10,
								},
								[12] = {
									9, -- [1]
									10, -- [2]
								},
							}, -- [13]
							{
								[18] = {
									6, -- [1]
								},
								[33] = {
									[4] = 5,
								},
								[17] = {
									[2] = 7,
									[3] = 8,
								},
								["color"] = "d5223f",
							}, -- [14]
							{
								[34] = {
									[2] = 6,
								},
								[24] = {
									1, -- [1]
								},
								["color"] = "bb73b4",
							}, -- [15]
							{
								[30] = {
									[6] = 1,
								},
								[28] = {
									2, -- [1]
									1, -- [2]
									[5] = 3,
								},
								[29] = {
									[3] = 1,
									[4] = 2,
								},
								["color"] = "de2430",
							}, -- [16]
							{
								[14] = {
									[3] = 3,
								},
								[13] = {
									[2] = 4,
									[4] = 5,
								},
								[12] = {
									13, -- [1]
								},
								["color"] = "34c082",
							}, -- [17]
							{
								[28] = {
									6, -- [1]
									5, -- [2]
									4, -- [3]
									[8] = 7,
								},
								[29] = {
									[4] = 3,
									[6] = 4,
								},
								[35] = {
									[9] = 7,
								},
								[30] = {
									[5] = 2,
									[7] = 3,
								},
								["color"] = "707c17",
							}, -- [18]
							{
								[34] = {
									[2] = 8,
								},
								[31] = {
									1, -- [1]
								},
								["color"] = "4efdce",
							}, -- [19]
							{
								["color"] = "b5a4b5",
								[33] = {
									[3] = 9,
								},
								[12] = {
									7, -- [1]
									6, -- [2]
								},
							}, -- [20]
							{
								[15] = {
									1, -- [1]
								},
								["color"] = "2f30c8",
							}, -- [21]
							{
								[34] = {
									[2] = 12,
								},
								[32] = {
									1, -- [1]
								},
								["color"] = "1d421f",
							}, -- [22]
						},
					},
					["text"] = "S3? Tazavesh: Streets of Wonder Temporary Route BY 丷筱乄魂 Fortified",
					["objects"] = {
						{
							["n"] = true,
							["d"] = {
								"601.6", -- [1]
								"-302.3", -- [2]
								1, -- [3]
								true, -- [4]
								"Wo to skip Armored Overseer", -- [5]
							},
						}, -- [1]
						{
							["n"] = true,
							["d"] = {
								"478.2", -- [1]
								"-309.5", -- [2]
								1, -- [3]
								true, -- [4]
								"kill and die at the same time", -- [5]
							},
						}, -- [2]
						{
							["n"] = true,
							["d"] = {
								"409.7", -- [1]
								"-334.8", -- [2]
								1, -- [3]
								true, -- [4]
								"OPEN", -- [5]
							},
						}, -- [3]
						{
							["n"] = true,
							["d"] = {
								"317.3", -- [1]
								"-335.2", -- [2]
								1, -- [3]
								true, -- [4]
								"Wo Buff go to MYZA'S OASIS \n\nUse No.13 Wo Buff to Trigger the plot\n\nUse No.14 Wo Buff to exchange", -- [5]
							},
						}, -- [4]
						{
							["n"] = true,
							["d"] = {
								"454.5", -- [1]
								"-181.5", -- [2]
								1, -- [3]
								true, -- [4]
								"Wo Buff go to MYZA'S OASIS \n\nUse No.13 Wo Buff to Trigger the plot\n\nUse No.14 Wo Buff to exchange", -- [5]
							},
						}, -- [5]
						{
							["n"] = true,
							["d"] = {
								"361.1", -- [1]
								"-249.6", -- [2]
								1, -- [3]
								true, -- [4]
								"OPEN", -- [5]
							},
						}, -- [6]
						{
							["n"] = true,
							["d"] = {
								"625.4", -- [1]
								"-240.1", -- [2]
								1, -- [3]
								true, -- [4]
								"No.6", -- [5]
							},
						}, -- [7]
						{
							["n"] = true,
							["d"] = {
								"457.8", -- [1]
								"-255.6", -- [2]
								2, -- [3]
								true, -- [4]
								"If you have.", -- [5]
							},
						}, -- [8]
						{
							["n"] = true,
							["d"] = {
								"377.6", -- [1]
								"-193.4", -- [2]
								1, -- [3]
								true, -- [4]
								"USE No.13-14  Wo BUFF", -- [5]
							},
						}, -- [9]
						{
							["n"] = true,
							["d"] = {
								"344.2", -- [1]
								"-240.4", -- [2]
								1, -- [3]
								true, -- [4]
								"Kill No.16 outside\n\nOverloaded Mailemental's Expedited Zone persists for 15 seconds \n\nUse Buff to kill No.17", -- [5]
							},
						}, -- [10]
						{
							["n"] = true,
							["d"] = {
								"651", -- [1]
								"-266.1", -- [2]
								3, -- [3]
								true, -- [4]
								"Kill No.16 outside\n\nOverloaded Mailemental's Expedited Zone persists for 15 seconds \n\nUse Buff to kill No.17", -- [5]
							},
						}, -- [11]
						{
							["n"] = true,
							["d"] = {
								"441.1", -- [1]
								"-281.3", -- [2]
								3, -- [3]
								true, -- [4]
								"WO +\n\nOverloaded Mailemental's Expedited Zone  +\n\nBOSS's Urh", -- [5]
							},
						}, -- [12]
						{
							["n"] = true,
							["d"] = {
								"392.1", -- [1]
								"-285.8", -- [2]
								1, -- [3]
								true, -- [4]
								"If Wo BUFF can kill BOSS first, then go downstairs to kill No.21", -- [5]
							},
						}, -- [13]
						{
							["n"] = true,
							["d"] = {
								"438.8", -- [1]
								"-330.9", -- [2]
								1, -- [3]
								true, -- [4]
								"Hide behind the stairs to deal with Frenzied Charge", -- [5]
							},
						}, -- [14]
						{
							["n"] = true,
							["d"] = {
								"379.5", -- [1]
								"-173.7", -- [2]
								4, -- [3]
								true, -- [4]
								"If you have.", -- [5]
							},
						}, -- [15]
						{
							["n"] = true,
							["d"] = {
								"281.8", -- [1]
								"-291.4", -- [2]
								1, -- [3]
								true, -- [4]
								"P1 or P3", -- [5]
							},
						}, -- [16]
						{
							["d"] = {
								3, -- [1]
								1, -- [2]
								3, -- [3]
								true, -- [4]
								"42d51f", -- [5]
								-8, -- [6]
								true, -- [7]
							},
							["l"] = {
								"628.5", -- [1]
								"-253.2", -- [2]
								"670.2", -- [3]
								"-366", -- [4]
							},
						}, -- [17]
					},
					["mdiEnabled"] = false,
				}, -- [2]
				{
					["value"] = 0,
					["text"] = "<New Preset>",
				}, -- [3]
			}, -- [37]
			{
				{
					["difficulty"] = 17,
					["week"] = 1,
					["mdiEnabled"] = false,
					["mdi"] = {
						["freeholdJoined"] = false,
						["freehold"] = 1,
						["beguiling"] = 1,
					},
					["colorPaletteInfo"] = {
						["autoColoring"] = true,
						["colorPaletteIdx"] = 4,
					},
					["text"] = "Default",
					["value"] = {
						["currentPull"] = 1,
						["currentSublevel"] = 1,
						["pulls"] = {
							{
								["color"] = "ff3eff",
							}, -- [1]
						},
						["currentDungeonIdx"] = 38,
						["teeming"] = false,
						["selection"] = {
							1, -- [1]
						},
						["riftOffsets"] = {
							{
							}, -- [1]
						},
					},
					["objects"] = {
					},
				}, -- [1]
				{
					["mdiEnabled"] = false,
					["week"] = 1,
					["objects"] = {
						{
							["n"] = true,
							["d"] = {
								"194.5", -- [1]
								"-137.7", -- [2]
								1, -- [3]
								true, -- [4]
								"Relics in this route are personal preference!", -- [5]
							},
						}, -- [1]
						{
							["n"] = true,
							["d"] = {
								"266.6", -- [1]
								"-72.1", -- [2]
								1, -- [3]
								true, -- [4]
								"Murlocs always flee on low health - save CC for this. STUN THE FISH STICK CAST FROM THE SCALEBINDERS!!", -- [5]
							},
						}, -- [2]
						{
							["n"] = true,
							["d"] = {
								"203.2", -- [1]
								"-227.2", -- [2]
								2, -- [3]
								true, -- [4]
								"You want to synchronize these casts if possible and you have melee in your group, to do this you want to get in combat with both at the same time - stealth into the middle and then tag both packs at the same time to accomplish this.", -- [5]
							},
						}, -- [3]
						{
							["n"] = true,
							["d"] = {
								"266.6", -- [1]
								"-146.4", -- [2]
								1, -- [3]
								true, -- [4]
								"If MDT's current required count number (332) as of the time of making this route is correct, then you do not need this big guy. If more recent PTR reports of 346 then you do need it (or any other extra pack somewhere in this area)", -- [5]
							},
						}, -- [4]
					},
					["colorPaletteInfo"] = {
						["autoColoring"] = true,
						["colorPaletteIdx"] = 4,
					},
					["value"] = {
						["currentPull"] = 13,
						["currentSublevel"] = 1,
						["riftOffsets"] = {
							{
							}, -- [1]
						},
						["currentDungeonIdx"] = 38,
						["teeming"] = false,
						["selection"] = {
							13, -- [1]
						},
						["pulls"] = {
							{
								{
									21, -- [1]
									22, -- [2]
									23, -- [3]
									24, -- [4]
								}, -- [1]
								{
									2, -- [1]
								}, -- [2]
								{
								}, -- [3]
								{
									1, -- [1]
								}, -- [4]
								{
									2, -- [1]
									3, -- [2]
								}, -- [5]
								["color"] = "ff3eff",
							}, -- [1]
							{
								{
									18, -- [1]
									19, -- [2]
									20, -- [3]
									17, -- [4]
								}, -- [1]
								{
								}, -- [2]
								{
									[5] = 6,
									[7] = 5,
								}, -- [3]
								[6] = {
									[6] = 1,
								},
								[18] = {
								},
								["color"] = "3eff9e",
								[19] = {
								},
								[20] = {
								},
							}, -- [2]
							{
								{
									27, -- [1]
									28, -- [2]
									32, -- [3]
									30, -- [4]
									31, -- [5]
									25, -- [6]
									26, -- [7]
									29, -- [8]
								}, -- [1]
								["color"] = "ff3e3e",
							}, -- [3]
							{
								{
									64, -- [1]
									62, -- [2]
									63, -- [3]
									61, -- [4]
								}, -- [1]
								{
									[8] = 10,
								}, -- [2]
								[5] = {
									[5] = 10,
									[7] = 9,
								},
								[6] = {
									[6] = 7,
								},
								["color"] = "3e9eff",
							}, -- [4]
							{
								{
									[3] = 67,
									[2] = 66,
									[7] = 65,
									[6] = 68,
								}, -- [1]
								{
									[8] = 11,
								}, -- [2]
								{
									[5] = 14,
								}, -- [3]
								[5] = {
									11, -- [1]
								},
								[6] = {
									[4] = 8,
								},
								["color"] = "fffb3e",
							}, -- [5]
							{
								{
									[5] = 71,
									[6] = 72,
									[7] = 69,
									[8] = 70,
								}, -- [1]
								{
									12, -- [1]
								}, -- [2]
								{
									[4] = 15,
								}, -- [3]
								[5] = {
									[2] = 12,
								},
								[6] = {
									[3] = 9,
									[9] = 10,
								},
								["color"] = "3eff3e",
							}, -- [6]
							{
								{
									96, -- [1]
									93, -- [2]
									94, -- [3]
									95, -- [4]
								}, -- [1]
								{
									[6] = 16,
								}, -- [2]
								{
									[5] = 22,
								}, -- [3]
								[5] = {
									[8] = 16,
									[9] = 17,
								},
								[6] = {
									[7] = 14,
								},
								["color"] = "ff3e9e",
							}, -- [7]
							{
								[7] = {
									2, -- [1]
									1, -- [2]
									3, -- [3]
									4, -- [4]
								},
								["color"] = "3effff",
							}, -- [8]
							{
								[8] = {
									1, -- [1]
								},
								["color"] = "ff9b3e",
							}, -- [9]
							{
								["color"] = "3e3eff",
								[10] = {
									[4] = 1,
									[5] = 2,
								},
								[9] = {
									1, -- [1]
									2, -- [2]
									3, -- [3]
									[6] = 4,
								},
							}, -- [10]
							{
								["color"] = "a1ff3e",
								[10] = {
									[2] = 4,
									[4] = 3,
								},
								[12] = {
									1, -- [1]
								},
								[9] = {
									[3] = 5,
								},
							}, -- [11]
							{
								["color"] = "ff3eff",
								[10] = {
									5, -- [1]
								},
								[12] = {
									[2] = 2,
								},
								[9] = {
									[3] = 7,
									[4] = 6,
								},
							}, -- [12]
							{
								["color"] = "3eff9e",
								[10] = {
									[3] = 7,
									[4] = 6,
								},
								[12] = {
									4, -- [1]
									3, -- [2]
								},
							}, -- [13]
							{
								[13] = {
									1, -- [1]
								},
								["color"] = "ff3e3e",
							}, -- [14]
							{
								[14] = {
									1, -- [1]
								},
								["color"] = "3e9eff",
							}, -- [15]
							{
								{
								}, -- [1]
								{
								}, -- [2]
								{
								}, -- [3]
								[20] = {
								},
								[15] = {
									[2] = 1,
									[3] = 2,
								},
								[18] = {
								},
								["color"] = "fffb3e",
								[19] = {
								},
								[16] = {
									1, -- [1]
								},
							}, -- [16]
							{
								["color"] = "3eff3e",
								[16] = {
									[2] = 4,
									[3] = 3,
								},
								[15] = {
									7, -- [1]
									[4] = 5,
									[5] = 6,
								},
							}, -- [17]
							{
								["color"] = "ff3e9e",
								[16] = {
									[2] = 2,
								},
								[15] = {
									3, -- [1]
									[3] = 4,
								},
							}, -- [18]
							{
								[14] = {
									3, -- [1]
									2, -- [2]
								},
								["color"] = "3effff",
							}, -- [19]
							{
								[17] = {
									1, -- [1]
								},
								["color"] = "ff9b3e",
							}, -- [20]
						},
					},
					["text"] = "S3 SG by Dratnos",
					["mdi"] = {
						["freeholdJoined"] = false,
						["beguiling"] = 1,
						["freehold"] = 1,
					},
					["difficulty"] = 35,
				}, -- [2]
				{
					["value"] = 0,
					["text"] = "<New Preset>",
				}, -- [3]
			}, -- [38]
			{
				{
					["value"] = {
					},
					["text"] = "Default",
					["colorPaletteInfo"] = {
						["autoColoring"] = true,
						["colorPaletteIdx"] = 4,
					},
				}, -- [1]
				{
					["value"] = 0,
					["text"] = "<New Preset>",
				}, -- [2]
			}, -- [39]
		},
		["newDataCollectionActive"] = false,
		["toolbar"] = {
			["color"] = {
				["a"] = 1,
				["b"] = 1,
				["g"] = 1,
				["r"] = 1,
			},
			["brushSize"] = 3,
		},
		["toolbarExpanded"] = false,
		["enemyStyle"] = 1,
		["currentExpansion"] = 3,
		["dungeonImport"] = {
		},
		["language"] = 1,
		["version"] = 3922,
		["enemyForcesFormat"] = 2,
		["currentSeason"] = 7,
		["defaultColor"] = "228b22",
		["nonFullscreenScale"] = 0.7500001649598818,
		["yoffset"] = -126.5041961669922,
	},
}
