
SimulationCraftDB = {
	["profileKeys"] = {
		["Mildew - Hyjal"] = "Mildew - Hyjal",
		["Noxiishields - Area 52"] = "Noxiishields - Area 52",
		["Welorialin - Area 52"] = "Welorialin - Area 52",
		["Noxiipally - Area 52"] = "Noxiipally - Area 52",
		["Noxheals - Area 52"] = "Noxheals - Area 52",
		["Noxxii - Suramar"] = "Noxxii - Suramar",
		["Koragoraia - Illidan"] = "Koragoraia - Illidan",
		["Noxii - Hyjal"] = "Noxii - Hyjal",
	},
	["profiles"] = {
		["Mildew - Hyjal"] = {
			["minimap"] = {
				["hide"] = true,
			},
			["frame"] = {
				["ofsx"] = 0,
				["point"] = "CENTER",
				["relativePoint"] = "CENTER",
				["height"] = 400,
				["ofsy"] = 0,
				["width"] = 750,
			},
		},
		["Noxiishields - Area 52"] = {
			["minimap"] = {
				["hide"] = true,
			},
		},
		["Welorialin - Area 52"] = {
		},
		["Noxiipally - Area 52"] = {
			["minimap"] = {
				["hide"] = true,
			},
		},
		["Noxheals - Area 52"] = {
			["minimap"] = {
				["hide"] = true,
			},
			["frame"] = {
				["ofsx"] = 0,
				["point"] = "CENTER",
				["relativePoint"] = "CENTER",
				["height"] = 400,
				["ofsy"] = 0,
				["width"] = 750,
			},
		},
		["Noxxii - Suramar"] = {
			["minimap"] = {
				["hide"] = false,
			},
			["frame"] = {
				["ofsx"] = 0,
				["point"] = "CENTER",
				["relativePoint"] = "CENTER",
				["height"] = 400,
				["ofsy"] = 0,
				["width"] = 750,
			},
		},
		["Koragoraia - Illidan"] = {
		},
		["Noxii - Hyjal"] = {
			["frame"] = {
				["ofsx"] = 0,
				["point"] = "CENTER",
				["relativePoint"] = "CENTER",
				["height"] = 400,
				["ofsy"] = 0,
				["width"] = 750,
			},
			["minimap"] = {
				["hide"] = true,
			},
		},
	},
}
