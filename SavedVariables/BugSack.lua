
BugSackDB = {
	["auto"] = false,
	["fontSize"] = "GameFontHighlight",
	["useMaster"] = false,
	["altwipe"] = true,
	["mute"] = true,
	["soundMedia"] = "BugSack: Fatality",
	["chatframe"] = false,
}
BugSackLDBIconDB = {
	["minimapPos"] = 159.4121695373627,
}
