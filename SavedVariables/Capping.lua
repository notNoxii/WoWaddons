
CappingSettings = {
	["profileKeys"] = {
		["Noxiipally - Area 52"] = "Default",
		["Noxiishields - Area 52"] = "Default",
	},
	["profiles"] = {
		["Default"] = {
			["barOnAlt"] = "NONE",
			["fontSize"] = 17,
			["alignTime"] = "RIGHT",
			["colorBarBackground"] = {
				0, -- [1]
				0, -- [2]
				0, -- [3]
				0.75, -- [4]
			},
			["outline"] = "NONE",
			["colorAlliance"] = {
				0, -- [1]
				0, -- [2]
				1, -- [3]
				1, -- [4]
			},
			["colorOther"] = {
				1, -- [1]
				1, -- [2]
				0, -- [3]
				1, -- [4]
			},
			["spacing"] = 0,
			["barOnShift"] = "SAY",
			["useMasterForQueue"] = true,
			["icon"] = true,
			["fill"] = false,
			["alignIcon"] = "LEFT",
			["height"] = 28,
			["barOnControl"] = "INSTANCE_CHAT",
			["colorText"] = {
				1, -- [1]
				1, -- [2]
				1, -- [3]
				1, -- [4]
			},
			["growUp"] = false,
			["monochrome"] = false,
			["alignText"] = "LEFT",
			["colorQueue"] = {
				0.6, -- [1]
				0.6, -- [2]
				0.6, -- [3]
				1, -- [4]
			},
			["timeText"] = true,
			["font"] = "Ubuntu Medium",
			["width"] = 292,
			["position"] = {
				"TOPLEFT", -- [1]
				"TOPLEFT", -- [2]
				440.0709838867188, -- [3]
				-92.81120300292969, -- [4]
			},
			["queueBars"] = true,
			["barTexture"] = "Blizzard Raid Bar",
			["lock"] = true,
			["colorHorde"] = {
				1, -- [1]
				0, -- [2]
				0, -- [3]
				1, -- [4]
			},
		},
	},
}
