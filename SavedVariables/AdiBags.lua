
AdiBagsDB = {
	["char"] = {
		["Mildew - Hyjal"] = {
			["collapsedSections"] = {
			},
		},
		["Noxiishields - Area 52"] = {
			["collapsedSections"] = {
			},
		},
		["Noxheals - Area 52"] = {
			["collapsedSections"] = {
			},
		},
		["Noxiipally - Area 52"] = {
			["collapsedSections"] = {
			},
		},
		["Noxii - Hyjal"] = {
			["collapsedSections"] = {
			},
		},
	},
	["namespaces"] = {
		["ItemLevel"] = {
		},
		["FilterOverride"] = {
			["profiles"] = {
				["Default"] = {
					["version"] = 3,
				},
			},
		},
		["ItemCategory"] = {
			["profiles"] = {
				["Default"] = {
					["splitBySubclass"] = {
						false, -- [1]
					},
					["mergeGems"] = true,
					["mergeGlyphs"] = true,
				},
			},
		},
		["NewItem"] = {
		},
		["AdiBags_TooltipInfo"] = {
		},
		["MoneyFrame"] = {
			["profiles"] = {
				["Default"] = {
					["small"] = false,
				},
			},
		},
		["CurrencyFrame"] = {
			["profiles"] = {
				["Default"] = {
					["shown"] = {
						["Stygia"] = false,
						["Cyphers of the First Ones"] = false,
						["Honor"] = false,
						["Tower Knowledge"] = false,
						["Wakening Essence"] = false,
						["Infused Ruby"] = false,
						["Artifact Fragment"] = false,
						["Cataloged Research"] = false,
						["Reservoir Anima"] = false,
						["Timewarped Badge"] = false,
						["Soul Ash"] = false,
						["Stygian Ember"] = false,
					},
					["text"] = {
						["name"] = "Ubuntu",
						["r"] = 0.9999977946281433,
						["b"] = 0.9999977946281433,
						["g"] = 0.9999977946281433,
						["size"] = 15,
					},
					["hideZeroes"] = true,
				},
			},
		},
		["ItemSets"] = {
			["char"] = {
				["Noxiipally - Area 52"] = {
					["mergedSets"] = {
					},
				},
				["Noxiishields - Area 52"] = {
					["mergedSets"] = {
					},
				},
				["Noxii - Hyjal"] = {
					["mergedSets"] = {
					},
				},
			},
			["profiles"] = {
				["Default"] = {
					["oneSectionPerSet"] = true,
				},
			},
		},
		["Shadowlands"] = {
			["profiles"] = {
				["Default"] = {
					["moveLegendaryPowers"] = false,
					["moveProtoformSynthesis"] = true,
					["moveConduits"] = true,
					["moveTrinkets"] = false,
					["moveVenari"] = true,
					["moveSpecialCypherEquipment"] = true,
					["moveCompanionEXP"] = true,
					["moveAnima"] = true,
					["moveRohSuir"] = true,
					["moveEmberCourt"] = true,
					["moveFood"] = false,
					["moveRuneVessel"] = true,
					["moveAscendedCrafting"] = true,
					["moveLegendaryItems"] = true,
					["moveUpgradeableItems"] = false,
					["moveQueensConservatory"] = true,
					["moveShardsofDomination"] = true,
					["moveZerethMortis"] = true,
					["moveSinstones"] = true,
					["moveAbominableStitching"] = true,
					["moveRings"] = false,
					["moveOutdoorItems"] = true,
					["moveRareProtoMaterial"] = true,
					["moveClassSetTokens"] = true,
					["moveCatalogedResearch"] = true,
					["showcoloredCategories"] = true,
				},
			},
		},
		["DataSource"] = {
			["profiles"] = {
				["Default"] = {
					["mergeBags"] = false,
					["showIcons"] = true,
					["format"] = "free/total",
					["showBank"] = true,
					["showTags"] = true,
				},
			},
		},
		["Junk"] = {
			["profiles"] = {
				["Default"] = {
					["include"] = {
					},
					["sources"] = {
					},
					["exclude"] = {
						[110560] = true,
						[6948] = true,
						[140192] = true,
					},
				},
			},
		},
		["Equipment"] = {
			["profiles"] = {
				["Default"] = {
					["dispatchRule"] = "category",
					["armorTypes"] = false,
				},
			},
		},
	},
	["profileKeys"] = {
		["Mildew - Hyjal"] = "Default",
		["Noxiishields - Area 52"] = "Default",
		["Welorialin - Area 52"] = "Default",
		["Noxheals - Area 52"] = "Default",
		["Noxiipally - Area 52"] = "Default",
		["Noxxii - Suramar"] = "Default",
		["Koragoraia - Illidan"] = "Default",
		["Noxii - Hyjal"] = "Default",
	},
	["profiles"] = {
		["Default"] = {
			["showBagType"] = true,
			["questIndicator"] = true,
			["virtualStacks"] = {
				["freeSpace"] = true,
				["others"] = true,
				["stackable"] = true,
				["notWhenTrading"] = 1,
				["incomplete"] = true,
			},
			["columnWidth"] = {
				["Backpack"] = 14,
				["Bank"] = 6,
			},
			["bags"] = {
			},
			["autoDeposit"] = false,
			["modules"] = {
			},
			["scale"] = 0.951,
			["skin"] = {
				["ReagentBankColor"] = {
					0, -- [1]
					0.5, -- [2]
					0, -- [3]
					1, -- [4]
				},
				["BackpackColor"] = {
					0, -- [1]
					0, -- [2]
					0, -- [3]
					1, -- [4]
				},
				["BankColor"] = {
					0, -- [1]
					0, -- [2]
					0.5, -- [3]
					1, -- [4]
				},
				["border"] = "Square Full White",
				["background"] = "Blizzard Collections Background",
				["borderWidth"] = 16,
				["insets"] = 3,
			},
			["bagFont"] = {
				["name"] = "Friz Quadrata TT",
				["r"] = 1,
				["b"] = 1,
				["g"] = 1,
				["size"] = 15,
			},
			["dimJunk"] = true,
			["positions"] = {
				["anchor"] = {
					["xOffset"] = -3.886474609375,
					["point"] = "BOTTOMRIGHT",
					["yOffset"] = 197.0292205810547,
				},
				["Bank"] = {
					["xOffset"] = 32,
					["point"] = "TOPLEFT",
					["yOffset"] = -104,
				},
				["Backpack"] = {
					["xOffset"] = -32,
					["point"] = "BOTTOMRIGHT",
					["yOffset"] = 200,
				},
			},
			["filterPriorities"] = {
			},
			["enabled"] = true,
			["qualityOpacity"] = 1,
			["qualityHighlight"] = true,
			["rightClickConfig"] = true,
			["compactLayout"] = true,
			["hideAnchor"] = false,
			["autoOpen"] = true,
			["sortingOrder"] = "default",
			["scrapIndicator"] = true,
			["sectionFont"] = {
				["name"] = "Friz Quadrata TT",
				["r"] = 0.9999977946281433,
				["b"] = 0,
				["g"] = 0.8196060657501221,
				["size"] = 11,
			},
			["allHighlight"] = false,
			["filters"] = {
			},
			["positionMode"] = "anchored",
			["maxHeight"] = 0.65,
		},
	},
}
