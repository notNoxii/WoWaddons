
MADB = {
	["noMMMW"] = false,
	["characters"] = {
	},
	["alwaysShowNudger"] = false,
	["tooltips"] = true,
	["profiles"] = {
		["default"] = {
			["name"] = "default",
			["frames"] = {
				["CompactRaidFrameManagerToggleButton"] = {
					["name"] = "CompactRaidFrameManagerToggleButton",
					["orgPos"] = {
						"RIGHT", -- [1]
						"CompactRaidFrameManager", -- [2]
						"RIGHT", -- [3]
						-9.000000953674316, -- [4]
						0, -- [5]
					},
					["disableLayerOverlay"] = true,
					["pos"] = {
						"RIGHT", -- [1]
						"CompactRaidFrameManager", -- [2]
						"RIGHT", -- [3]
						-9, -- [4]
						-0.00018310546875, -- [5]
					},
				},
				["CompactRaidFrameManager"] = {
					["orgPos"] = {
						"TOPLEFT", -- [1]
						"UIParent", -- [2]
						"TOPLEFT", -- [3]
						-182, -- [4]
						-140, -- [5]
					},
					["disableLayerArtwork"] = true,
					["name"] = "CompactRaidFrameManager",
					["disableLayerOverlay"] = true,
					["pos"] = {
						"TOPLEFT", -- [1]
						"UIParent", -- [2]
						"TOPLEFT", -- [3]
						196.1673431396484, -- [4]
						-214.0009155273438, -- [5]
					},
				},
				["QuestFrame"] = {
					["UIPanelWindows"] = {
						["pushable"] = 0,
						["area"] = "left",
					},
					["orgPos"] = {
						"TOPLEFT", -- [1]
						"UIParent", -- [2]
						"TOPLEFT", -- [3]
						16, -- [4]
						-116.0000076293945, -- [5]
					},
					["name"] = "QuestFrame",
					["pos"] = {
						"BOTTOMLEFT", -- [1]
						"UIParent", -- [2]
						"BOTTOMLEFT", -- [3]
						15.99998950958252, -- [4]
						583.9999389648438, -- [5]
					},
				},
				["PlayerTalentFrame"] = {
					["name"] = "PlayerTalentFrame",
					["pos"] = {
						"BOTTOMLEFT", -- [1]
						"UIParent", -- [2]
						"BOTTOMLEFT", -- [3]
						382.8119812011719, -- [4]
						577.9264526367188, -- [5]
					},
				},
				["MiniMapWorldMapButton"] = {
					["orgPos"] = {
						"TOPRIGHT", -- [1]
						"MinimapBackdrop", -- [2]
						"TOPRIGHT", -- [3]
						-2, -- [4]
						23, -- [5]
					},
					["name"] = "MiniMapWorldMapButton",
					["pos"] = {
						"CENTER", -- [1]
						"Minimap", -- [2]
						"CENTER", -- [3]
						69.7645711152436, -- [4]
						77.65700471242599, -- [5]
					},
				},
				["ObjectiveTrackerFrameMover"] = {
					["orgPos"] = {
						"TOPRIGHT", -- [1]
						"MinimapCluster", -- [2]
						"BOTTOMRIGHT", -- [3]
						-10, -- [4]
						0, -- [5]
					},
					["name"] = "ObjectiveTrackerFrameMover",
					["pos"] = {
						"TOPRIGHT", -- [1]
						"MinimapCluster", -- [2]
						"BOTTOMRIGHT", -- [3]
						-89.7734375, -- [4]
						-17.82171630859375, -- [5]
					},
				},
				["MiniMapTracking"] = {
					["orgPos"] = {
						"BOTTOMLEFT", -- [1]
						"Minimap", -- [2]
						"BOTTOMLEFT", -- [3]
						2.327743053436279, -- [4]
						2.327743053436279, -- [5]
					},
					["name"] = "MiniMapTracking",
					["hidden"] = true,
					["pos"] = {
						"CENTER", -- [1]
						"Minimap", -- [2]
						"CENTER", -- [3]
						-67.7014933378814, -- [4]
						22.65407502492599, -- [5]
					},
				},
				["ChatEditBoxesMover"] = {
					["orgPos"] = {
						"TOPLEFT", -- [1]
						"ChatFrame1", -- [2]
						"BOTTOMLEFT", -- [3]
						-5, -- [4]
						-2, -- [5]
					},
					["name"] = "ChatEditBoxesMover",
					["pos"] = {
						"TOPLEFT", -- [1]
						"ChatFrame1", -- [2]
						"BOTTOMLEFT", -- [3]
						-4.999981164932251, -- [4]
						-2, -- [5]
					},
				},
				["ObjectiveTrackerBonusBannerFrame"] = {
					["orgPos"] = {
						"TOP", -- [1]
						"UIParent", -- [2]
						"TOP", -- [3]
						0, -- [4]
						-170.0000152587891, -- [5]
					},
					["name"] = "ObjectiveTrackerBonusBannerFrame",
					["pos"] = {
						"TOP", -- [1]
						"UIParent", -- [2]
						"TOP", -- [3]
						33.09747314453125, -- [4]
						-192.0653686523438, -- [5]
					},
				},
				["ChatConfigFrame"] = {
					["UIPanelWindows"] = {
						["whileDead"] = 1,
						["xoffset"] = -16,
						["yoffset"] = 12,
						["pushable"] = 0,
						["area"] = "center",
					},
					["orgPos"] = {
						"CENTER", -- [1]
						"UIParent", -- [2]
						"CENTER", -- [3]
						0, -- [4]
						0, -- [5]
					},
					["name"] = "ChatConfigFrame",
					["pos"] = {
						"BOTTOMLEFT", -- [1]
						"UIParent", -- [2]
						"BOTTOMLEFT", -- [3]
						713.780029296875, -- [4]
						297.5, -- [5]
					},
				},
				["FriendsFrame"] = {
					["UIPanelWindows"] = {
						["whileDead"] = 1,
						["pushable"] = 0,
						["area"] = "left",
					},
					["orgPos"] = {
						"TOP", -- [1]
						"UIParent", -- [2]
						"TOP", -- [3]
						-381.0000305175781, -- [4]
						-158, -- [5]
					},
					["name"] = "FriendsFrame",
					["pos"] = {
						"BOTTOMLEFT", -- [1]
						"UIParent", -- [2]
						"BOTTOMLEFT", -- [3]
						15.99998950958252, -- [4]
						659.9999389648438, -- [5]
					},
				},
				["ObjectiveTrackerFrameScaleMover"] = {
					["orgPos"] = {
						"TOPRIGHT", -- [1]
						"MinimapCluster", -- [2]
						"BOTTOMRIGHT", -- [3]
						-10, -- [4]
						0, -- [5]
					},
					["name"] = "ObjectiveTrackerFrameScaleMover",
					["pos"] = {
						"TOPRIGHT", -- [1]
						"MinimapCluster", -- [2]
						"BOTTOMRIGHT", -- [3]
						-102.50244140625, -- [4]
						-19.51898193359375, -- [5]
					},
				},
				["TalkingHeadFrame"] = {
					["orgPos"] = {
						"BOTTOM", -- [1]
						"UIParent", -- [2]
						"BOTTOM", -- [3]
						0, -- [4]
						110, -- [5]
					},
					["name"] = "TalkingHeadFrame",
					["hidden"] = true,
					["pos"] = {
						"BOTTOM", -- [1]
						"UIParent", -- [2]
						"BOTTOM", -- [3]
						-0.848663330078125, -- [4]
						1013.995666503906, -- [5]
					},
				},
			},
		},
	},
	["closeGUIOnEscape"] = false,
	["playSound"] = false,
	["noBags"] = false,
	["frameListRows"] = 18,
}
