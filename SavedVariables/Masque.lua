
MasqueDB = {
	["namespaces"] = {
		["LibDualSpec-1.0"] = {
			["char"] = {
				["Noxiipally - Area 52"] = {
				},
			},
		},
	},
	["profileKeys"] = {
		["Noxiipally - Area 52"] = "Default",
		["Noxiishields - Area 52"] = "Default",
	},
	["profiles"] = {
		["Default"] = {
			["Groups"] = {
				["WeakAuras_NZnRTrJXeN3"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_uz9x4FyzRJT"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_br4m(f)H8Hp"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_t4QDk2AdbVh"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_LK9FJ2zEyiG"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_a(1mNiY4f3Y"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_t3TTmuIFTjh"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_(65ezzq9s3I"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_gasI6YR9BI6"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_BoC382PxnED"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_6CooJaZWQyP"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_ZnQjS2HM8U1"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_H94YxBKme21"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_M4gkiBmm4vm"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_AHM)l)Dr53D"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_rTXlvc1pl7c"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_UyVEaOJ0InK"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_UIE3RIovkbi"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_EqoZ5v9Q7eC"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_2AtFoIn5C5w"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_70se9qpMM8i"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_2VIbY4iAQaw"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_)lDcquXHC)u"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_fqgmtWA2KmD"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_lapRpm676GT"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_LEOAyj(CxAI"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Gxwd4O9yeuB"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_DM(Nci78HHa"] = {
					["Upgraded"] = true,
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_X1hmULs1U9R"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_VqbwpsYzv(u"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_(Mg8hqvKgXx"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_ogkicfrTpri"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Tnx2KB5JomZ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_kJjPNQmy6Kb"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_h546XEQPxG5"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_sUoieXCoAbn"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_1cX5J)KTdfi"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_6OsospGVrcw"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_qlxcyfxRcMc"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_pdaSHLseXFt"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_XlHnIbyVvXx"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_7m9Tp4(CYWF"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_DQigjOYORyx"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_9TubxrSu2v0"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_rjtCTqW62GP"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_hKg1FywfFyL"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_ZCg6M)XBqMP"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_6CnI6ZVkFlc"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_jyqByaPXW8a"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_HB0H0CcQrmS"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_T65iXM619wt"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_U)wRB1BOEFw"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_(5NzTUGQz)v"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_u4OLZrsRD8Q"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_SQnTyLUcykS"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_9Q8t1Cih1go"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_zOasaDx)ABU"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_cayupHuM(hC"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_mIKwDb7ifkC"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_PS9pBLYwg9t"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_DmIBuI0xucL"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_)oD(CUCAPbo"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_QM6U7uFTgOZ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_OLIHHC3s7Wh"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_XskxBaRR8Lk"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_tv6FcmJqE)5"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_x52ezzdqYxL"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_R0afOo5i3Dt"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_LcZcKMMRl5c"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_jZG71JaR4QC"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_5OyO42Iweyl"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_tP5N8kczsrS"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_K7pZkym7Lnb"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["ElvUI_Stance Bar"] = {
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_u4WcavuXXKh"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_lcG4YenI19X"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_PYjGGrgxLgb"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_wAn4qZ)MU8Z"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_OBs(7Q7578K"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_SnJPrpIFLiT"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_urbvfpoZRTc"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_N(aMkmVOSqs"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_2aihXFbfgJc"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_a8VMUT6bSXS"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_OLNBtKJ)hx4"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_XC0aF6AlHZj"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["OmniBar_Avenging Wrath"] = {
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_Op41Ayu3mto"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_T64BcGxeXVQ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_rTSAHZzXauP"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_)OqN2VFyM7z"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_oZxY6IQD94V"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_FsUl7b3IYHh"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_HVO0WX1sdir"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_01Ln(EtJaVa"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_hELd5y7oDmK"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_)UN0AdLL3Od"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_XPBsAIUVkwq"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_7a(20CpaR7C"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["OmniBar_Counterspell"] = {
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_SZP7KjXhNbi"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_zO2yjZs8KsM"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Rt0o7J(wgm6"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_)IAy)soIlh7"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Zo86BtpPV5F"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_mGe0SbcehbN"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_V(S7OtMC5bU"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_bd1GVFmFYFR"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_OsmRlzaaDBF"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_W7N6a8NInq)"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_zqw7E4Ij0cn"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_zGMTgYueJ)c"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_88AkerFgMn8"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["ElvUI"] = {
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_wDZn(MdjiMY"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_RXd)Xjp39Cv"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_9VCHPr0oOhZ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["OmniBar_Arcane Power"] = {
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_hjNIwJ6KZaG"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_Bf0FAua8brO"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_vS1jUXwUfXE"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_yjALdqByu2x"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_YE3Heez(ran"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_1bxo9JRJZXb"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_cG1aKn1j2o4"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_D1SRBf8nq9A"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_nEfWNiSMnyn"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_rtNQWq0o9LQ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_WfBJdfQJpuv"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Mnd4(ESCGc8"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_PtDhLIiAKEo"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_Lv1pf89IfCd"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_WZ7(yfBDRmt"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_oxSC7hHO)Ua"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_MLZVxK4eT6W"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_LauuuZgj2Ty"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_pNaLVuqVvD2"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_zT4a2vdiHyM"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_OdxG)zRJ(sN"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_PpyPhEYBpSQ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_kVYLirPJel)"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_4ptii2zGQCK"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_pFqqOS3GnFO"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Oo4bHEW9Jdk"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_oxmChWqViPM"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_QuvD74FoyGd"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_cOtXY5YKHog"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_PVv4eCioiUn"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_8bQepNVn5RT"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_aR9tWxpL)zn"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_VvC8SJEuxAe"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_TDFN9qZMSIq"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_046f8(d5gae"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_69sfdwIEryu"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_NF1(zq8fByX"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_fxAPA7QVL4C"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_6MW1EVYh2sX"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_7rW4BioXFAq"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_zi4DAzH2qMC"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Qhge2ZvgMp7"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_d4wF(gHy)2P"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_jiOkeI1Ufjn"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_JaalfvDr8yl"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_uwollVZoC1t"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_MYxZKce0HiG"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_s8bW02L3naT"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_9MLd2ciNxqu"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_3wElbhWXaZJ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_XmJzadDqy5v"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["OmniBar_Invoke Xuen, the White Tiger"] = {
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_vJboha(vSvH"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_vPa7ElgQaEK"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_yWbkg2k(mBe"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_0wvK4Th0Heo"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_5Z47RaRJKUh"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_V45ogntc0(G"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_8RpPS)Q2uCy"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_3)L(aBsodwt"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_2EXgH3ivpTT"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_1oOE4z0al0("] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_XbT3Tj7DnPk"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_u469jL1KIXu"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_3wtWZbRHlPo"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_pb8P1dOh2hB"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_14ZcYgwiQnS"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_ri27W2MG3TH"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Tr4KDEDe1nW"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_PhkDlg3fyKM"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_SqNlaqBTvEB"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_5H0yYJwJ7xI"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Rmi4OWiwawL"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_vkTW5J7v3xf"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_vOsT0LugAN0"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_21)FrBGRWQu"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_bXRYjMJkPpQ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_anSvAuZoV4j"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_5JwMqUSxunK"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_njZa3wBHLVQ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_U4IjxGwR8Z4"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_IMEmuuQJSt5"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_QfMzmlWXLBB"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Lwffo1Lwa(J"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_dOS0TXa(LYw"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_UX892WS5qH5"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_POe9pjvL8Ks"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_UIobo7yOXtA"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_j6QSRqD1nic"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_K)YTlQ1At9x"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_HIwQxXVtCSh"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_O6yaAYXODqF"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_gnBquECfacf"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_52KnoqlnbHi"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_09ZEU)2MWdI"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_)YW4qt1c)U6"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_WRoP3qZmuPh"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_4X8pj3rVJ6J"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_brf0kFJySxN"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_iFVPzKlWEV1"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_0SKavECSUgV"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_v1TrYYM5IH)"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_LOQy0EqJZ6H"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_0cZE1lvysOL"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_PDXigA4ehLm"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_DCFY8cY7SC)"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_3(dKG(IqcTr"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_MYLJIoMoq9t"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_lTVX4yju4KJ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["MRT_Raid cooldowns Col 2"] = {
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_)bdRtZSDSpf"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_UC(VzxhfbGb"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_kHTNiuo44Wz"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_kePrXRP1Z9U"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_ZXUdVsqDiWJ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_(PUF1emplYZ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_fairFrJmEmJ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Q)v7ms46mRF"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_29So9CIwHTv"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_7DGks(x5oB7"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_GBemWACGgHG"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_VxmW4DSJmvX"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_ETexcnHibwO"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["OmniBar_Storm, Earth, and Fire"] = {
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_5RGPUN9wHtu"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_CMBbvQPr9TI"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_8PlLnKbZhoO"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_cn4y91sde3U"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Zk58FKqvwHY"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_gzJIT2nmKCH"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_T3uwe4ydqlt"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_CzBS5qoS32T"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_fUa9d9e4IKj"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_e6)Lq2vkPq5"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_F45N6inI0RE"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_LM)Uk4emiSk"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_6drIvfsDTUc"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_tFxC(bfM0EW"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_AkxUqMLtCAx"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_sQirZQ4gjV6"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_xoRMlSG1jeD"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_Up1U7F8Jxfi"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["Bartender4_7"] = {
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_MY8kd38h0Eu"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_Dj74k4fCeGX"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_V3a0YWVZo1t"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_DyvZZj2Jvo2"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_IWXBTm7jgZI"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_xLtfam3xzgs"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_eZoTpZ0ZWbb"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_BRzxcz5Mqo8"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_0iTb38d)pj5"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_6yO6IfbBA0D"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_TiMUZc3RbcR"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_SQUUbyVP1YP"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["Blizzard Buffs_TempEnchant"] = {
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_DLQUdbkCkPT"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_LLOWk9bxPz8"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["MRT_Raid cooldowns Col 9"] = {
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_RdjTUhfy57C"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_KM0Zwf8nIiy"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_7(2DPb8LWTN"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_)yLQBseTQbE"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_l(JWPod4Awp"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_k0Eu)xa8ocM"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_)IQZY)R(CaU"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_(lhLYmK2oeq"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_81IK(1KlsY4"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_YOes1OIuks("] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["OmniBar_Frostwyrm's Fury"] = {
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_johEjT)8drE"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_0HNKU4u9AEI"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_5Qx0(WaTXmq"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_jQCGyoKcPjC"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_FqZM4xkb)jY"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_m9eoL6YVRTP"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_PhrT7XsrFNW"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_i9yq32Z3iYW"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_XX9i)aUnHx1"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_vfI33raHBc6"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["OmniBar"] = {
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_U4sa3HC5O4("] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_pxIU(nyVCel"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_)K5CC1MFUIW"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_QxAvv0W4XKr"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_o1OgEV2Zi5t"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_CwOWRP03Lb8"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["MRT_Raid cooldowns Col 10"] = {
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_zWgSyu8ZhvT"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_w62P2(K3Mio"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_m68dOp5ijC2"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_Qpk5U(A5WCq"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_HdVSCIVkKNw"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_CEgxkkViiTR"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_VTauRyTe)n5"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Yu8dmZ1UkID"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_(q1)7TwzVF)"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_2vH1xf1yMXO"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["Masque"] = {
					["SkinID"] = "Zoomed",
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_BlP4uDbg(Yx"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_GtCBnIJpQLZ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_elTCof61F8I"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Z5fzD5CJ1U4"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_M2um4Rerr02"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_qMsQ2EZKiTc"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_KVwvbBtdnu0"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_3tYny3WFmwY"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_gbuGefKwVBd"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_qzVGQtmKQwu"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_WIeTIKOuNzP"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_7PefAoBVM9I"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_dfLbNqE(Vbi"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_3H9fXAAoT4m"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Rfq(e70L)uE"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_Bd6sD8gTy1p"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_P(ZDulJaR1g"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_daN9sifBdYU"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_JC1)8hT138l"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_TgYiwCemup0"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_SyRfhcwFSlE"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_o81JgmFtSyn"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_r1stwginuXH"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_M9tKaeyRkDy"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_cQ5DYva10Lx"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_CcRdokchIWM"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["OmniBar_Disrupt"] = {
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_TGF7fowwdBt"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_2Zpd3r9HRBI"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_IBfxCUqhxxv"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_OQQlTz1Kg5d"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_zePJbBBhb6d"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_b3al)KV2tOo"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_IRd8gudvOLf"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_LPnYHt5YjAh"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_WRablbboNE1"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_BYWlkNrvn6("] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_jKSS8r0ozGQ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_YZ05I7RO(Hv"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_hvafKJsB)oN"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_oArM8MiOqyK"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_3m)r5rAsKmY"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_4p7ALvLuAl9"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Poxh8ptKZTm"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_17SsHGX3huE"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_RRb4yDeWv3o"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_(8ge3bTLFef"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_fuc2xBX6)ll"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_TvFo0BzbWa5"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_cu8ValqZp6x"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_7eyZYtj9h7q"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_nsLp1fAq6DT"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_gBcu8z9yRbx"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_IKJyNP6O1ZV"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_3VDXi4Sh)LC"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_r3OwaRe)OcP"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_AKlj7TQpZq6"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_teDDbPjHV2t"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_ReDCPIwAWev"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_zIf3O1rjxg9"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_w5GEwMS89rg"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_yuGo0z)hHCg"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_T9m8UmsL0iz"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_wHqhz9V9itZ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_0bkKYHy4)NV"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_NZ4x(V03gsm"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_7CxKjl3MXCq"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_jMoG1OIZ)Ym"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_DAluosrcmO7"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_6)vRmlkR5cO"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_llnCDc)WdEs"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_E6gGWKtvzp3"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_wwxaWZlcJX0"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_YvEAcJcsm1y"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_t0(oNZowTH8"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["OmniBar_Adrenaline Rush"] = {
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_EyXPO7NKK0T"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_9XVwXfUbql9"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_W4YuK69CfxU"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_I8PdstZx9rV"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_1J6prrmunAO"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_NehVLyTwh7W"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Dij)5ebK9ZG"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_JvZ0v1aBd(t"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["OmniBar_Summon Darkglare"] = {
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_AmOy8OwF5FT"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_2TyQRIbhgus"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_yEVMH9p5HbV"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_sVCYfmWqdYm"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_li4Kglu0w5E"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_0R54eoi8C4Y"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_1XO502tHjKQ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_8oylbRSRuNR"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_rdh2xsrwcmV"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_ieZiW3l4IhI"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_8nELOteOwty"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_BW7Apzt(LNT"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_6)nOpbDAOTy"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_d6xqSWTV1ad"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_CFszk4DbXb8"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_lMXvo)(nOfB"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_BthnvvT24kv"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_YsSvgCA1uqq"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_khQLRVm))Wi"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_VOOXUEiveoS"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_hTg5xJ)pEfg"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_SDGt8eNZrRI"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_7cLoewVwoA8"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["OmniBar_The Hunt"] = {
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_awglLF0AkcA"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_LoVW6Kt5)H)"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_(mE0wleEHPK"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_yj7Cu(EECsz"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_SFrK8dtIXT3"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_u0psn4(fYAu"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_yEDTgWGojcc"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_9wVoTcFr8xT"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_zQx78kRccr0"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Bk5uQecFr6R"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_4UZxUlf4O)G"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_DCzLugMSfY)"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_IigR9X2lcuN"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_EKAjJ7tRg(H"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_zsuTgPKV40D"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_qgsSZlGNaEg"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_xMz4pSMyLDM"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_LBkyE8WH0Om"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_R1cLvT8pPef"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_LxB19yWA7E4"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_xpkim8Ds5LV"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_vP9bYXcN2(9"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_Ed2yNWZKn3d"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_oDpnnQDFHGI"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["OmniBar_Shadowy Duel"] = {
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_tcq1T0Jo40a"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_WTEY374mwCV"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_58ZLsxMV7rS"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_ThtHzPIB1(J"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_TWdLHJBey2v"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_5egAAcBFQft"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_WcFfOKs76aq"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_i)sHGOcwg24"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_SslxHgVPb9q"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_t5FCcQ2dOTw"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["MRT_Raid cooldowns Col 7"] = {
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_qN(LwoBYy5H"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_X18UJiTUZLQ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_S7tdFAWSe0b"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_QrAlV6qx(D)"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Jwt6avsUtz0"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_BcgNq3wvcsS"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_I(0UfgCwcIJ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_e10og7Nksgs"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_LrVC8jDhI23"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_yWHf9DedjYA"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_hzZmAAimHbr"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_(vNs2J9nzEa"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_6p70kWAWwBm"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_KZvX1TpMF8t"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_kQyh)n7Xbfo"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_BjSwka9mcSj"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_8SiZ(FfhQYu"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_mZdXyMVmRmC"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_jXtML3XDg5("] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_bfbweIG8wOr"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_I3nMaH1J92S"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_iQcbxc8Jy6j"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["OmniBar_Incarnation: Guardian of Ursoc"] = {
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_rgLsticXHgE"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_DSPAlHWLxwA"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_oQysEvDf)kL"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_SXLJGcO)0PJ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_cLMpgAS)Y5l"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_sIuljY5TWt0"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Y8K1eOueeXU"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_V4xSJIHhw5p"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_dcTtt81p)Ah"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_Ma6X2yq3nWV"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_kOh8cDFa(bG"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["MRT_Raid cooldowns Col 8"] = {
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_HRWgCYtMLzU"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_qoSalOFOIpp"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_JegjrWcXR5S"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["OmniBar_Dark Soul: Instability"] = {
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_xjbmEreBqHJ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_X7NWQUN6ynp"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["OmniBar_Blind"] = {
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_X98boaIdWqc"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_K64UgFPfLN2"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_jrSMrVHr7uX"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_U(IBC4HivfM"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_vTyLVVKuyob"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_ykgewN0cwoR"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_SwA(OmJnKQn"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_9psENlDXltN"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_)nrMd9DTwbX"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_T4zaZobuwMG"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_GbzRiexw12T"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_6HEXBphoyuu"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_8waPsvvjjFl"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_OxGwERGA7cB"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_Lv)zozEtBEU"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_zmKd3CP8gYj"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_iAbtUCDY0cG"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Wi8PLD3og2H"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_N8EP2UE)h0Y"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_tYJhcPjNRpB"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_z(o(DOLxzOD"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_UOb1tzGkTaj"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_8FyAtamvQQh"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_JlQiieRX8DU"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["GladiusEx_Debuffs"] = {
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_53rXhXaP(LF"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_)oxfmPkrJIq"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_aC3eUBXgKoc"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_kZFE33Z(F6f"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_7fKUcOBAF2a"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_KlRmSu(Kmqe"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_J5VN34)OZ6M"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_H8rjbZ499iW"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_oIVx5UEAZTu"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_WYQ14wKbUjf"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_6rHmSc8VPij"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_DogBEYD(nVV"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_X8RZiHblKEZ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_1F01mQu7d6p"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_BMzwkQY5wtf"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_wsdXywKbr5g"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_ktynmtAwRMF"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_sBcAls1l65c"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_ncnLVCzffTP"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Z2nP97osAPX"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_Ia4jI1w97Xz"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_GqkEkuxM6nw"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_f4U(mQNNTxp"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_gyothkZCnNY"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_7z9UrKzfIrr"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_pf6p3nPN2r5"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_jgL7XD3ae1D"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_)nFw6964R)t"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_qRh25Ajz1BY"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["OmniBar_Invoke Niuzao, the Black Ox"] = {
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_ZcJX)YFZ3Rq"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_6IFSNnG9WdX"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Ak(V1je2MIM"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_dHqluMHQaUW"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_1yl1OOZXPkg"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_6aL74G2PS99"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_bPxVWbBPMdz"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_3rqy6kJYefY"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_8r4RVKyv4B4"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Mxpx6FWqGKj"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_MoeVaNA5gS4"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_n2nDOA8k4eK"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["OmniBar_Skull Bash"] = {
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_ytzl10uHkNh"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_fh80c92Lr1b"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_zFRTAlY75s9"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_8pFuOIMTIDP"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_r3XodxtmE0a"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_SgTfuevd1vv"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_HkUDdEKq1xf"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_v0NJ97qj4if"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_RS)bLb98JGX"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_rY85hH11bOW"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_w)GFxG72KdN"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_5KAsu998h41"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_)6hLeKgwpWt"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_xO(gE5mg1tG"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_fGLvrvI1yGI"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_EBJ6(CYSTaY"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_OIOVYBz()dH"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_KcoqQkSrLEG"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_js8yFANTEIh"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_G5z4ZFhxVmK"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_JdJrRRYrn2("] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_2soDm44)z0P"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_8etigNCYPEE"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_j)X2vjNVmFK"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_N(3k)dRfdaX"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_uYBOU1aE9BB"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_ARdSGzcDiqT"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_WIGhvxD31er"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_JHx89HcZRJ4"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_4ZCMUft519D"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["OmniBar_Spell Lock"] = {
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_MHWENKpS0rO"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_ZqA2WC4ChgH"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_jAVXpY6ildk"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_EhciFZW1vK8"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_lFEzbFtJ3vO"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_7FYqB1o560g"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_X0yy5(4McvN"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_xyLdYanWrp5"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_qoRTV98q5cw"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_nnFBU7NS229"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_2BiUMr4YKQi"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_yotGA1TSDXR"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_H(z0kyD7i5Y"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_PXSsqSbNdmA"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["Bartender4_2"] = {
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_7(ZpJ7Vsl39"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_QgSVKXQ4y7F"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_LfPi9a934P1"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_YhCZqadsmA2"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_HfTxXrQ6xW3"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_4fgYWwaWP(0"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_WTugsdc49eC"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_ic3ZJnhlhe0"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_wigAb84zohW"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_lOj16ZyyNxJ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_7lOAUdgC7wX"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_JtQXjJXNLLN"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_S7U5u(iTtnq"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_bV0HOq4hdVu"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_E6thF6nrbVe"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_emXL0BNKf(W"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Q)mvs0WREB6"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_1Hd(IdUAouV"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_YTXJVwO40uz"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_Ou)gn8E6zK8"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_CvFX8KPkMen"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_9xLDGKRuUTO"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_T7PWCpMXlYE"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_bA3aJfV2rZt"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_9i7jsCVNslt"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_BXRyUz((FZB"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_o1aQ(dw2xlj"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_tC7JxiIv3kK"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_dzDVDC)6bGy"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_nxgBZouMkmc"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_bJn99WcT7II"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Z)PQbIeuXtI"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_b3fHP9W8hb2"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_hiGRNS1QQ5N"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_UHdZXJMZ00T"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_TQb07IL7)2A"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_0s4n1G1Jd(("] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_wDoYd41ODdo"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_bdanHsXO7dP"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_l6FSfZOJNdJ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_JCGoV8etzes"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_NLrvteM7ktb"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_hqFT9hqcyof"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_LlXrXIIo67i"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_uyULrPO6(7Y"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_WtNWEUBK7cF"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_EYK6M3TRv7z"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_yIlf4zguf18"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_G36yuyzzhdv"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_rbBAyE697Ds"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_o0FcfRWZ0KK"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_(9b)BJTVigJ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_OdkP)MrK0rL"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_YJ6RauuDAar"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_9ySL5XCxceb"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_NknlSDkHrB7"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_D571oUR0iaP"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_Q5VY7Xzf90U"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_V6TRNP1(0lO"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_zY3pFIqFud0"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_aNFdG)Nvuqs"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_2wg0a4f(KDi"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["OmniBar_Innervate"] = {
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_B5dvy358O8H"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_X2zQ34OhbzD"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_GIEY2)hdONG"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_WzDpNcatSHe"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_CWnAV1(zjcC"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_rXaFEAW0qwl"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_6yvqba3TAs("] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_KBgb9B0N8Mx"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_GTvV94wkzbW"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_KH7fU(1ThmM"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_MBqW1rB7Jcs"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["OmniBar_Avenger's Shield"] = {
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_NE)c)uEA9rm"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_wCOPjK7h9Ip"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_pJkCyqfrPAT"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_fHIKOFXXI)4"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_IqsCW2mG)eJ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_tIAvniMkh2f"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_V5dEErBgsSs"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_VjkpUF7vhSH"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_UEgNhYblyCd"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_XJmRfMoXxFP"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_e51G0zX2ti)"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_k7xu4mlvW99"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_WZYzUwVdTqE"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_YIfJIrjoJRF"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_0Ymv9nRnTap"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_RTM0QiuKvaL"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_pPtnT(XrrMz"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_ysuJxwk4t1v"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["Blizzard Buffs_Debuffs"] = {
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_nNMouVZRMwW"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_vUGChf8GZxf"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_WUVaKgzQyGw"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_VGcNd7)TeXW"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_6v1O2dhh(6W"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_9IR2s5j61qr"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_kIC)eDwC8Fy"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_Lk4HtAZWZRw"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_8gXeOvE7dvi"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_2Ufi1fls0s7"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_HUC3Goq2tDl"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_lYF8dFWDDg2"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_vfVdZEJr4Pa"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_P(dOiSEXW9X"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Bl4VBAWYSkW"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_nRsKz)xbCcA"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_lAtBea3314Q"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_8HvEhPSRVkh"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_PitH7G)HbKL"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_lWHfN1GXkUk"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_o2n2ukDalhX"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_urJoSNtCFC("] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_bvmXtrgpuvB"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["OmniBar_Spear of Bastion"] = {
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_S)neyK46cLO"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_5OCrmjtI9aj"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_TB0clplXW46"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_MOtG7fWhOT2"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_H7Nmm7aj3Fd"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_fjvxczvRc4f"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Sfm9EsWH9R1"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_d2ngqR1ma6("] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_3kycPNyiNLf"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_rPCUjTbsdVO"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_YrLZ3a(tp(Y"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Z7XzWnee57q"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_rUc2xQR4)MO"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_HKsgkB9ng64"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_ZND6Mhb)dFT"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_0zzbEe7ndDI"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_fNpBah6e3pH"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_)mGsu0XoYcr"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_LrYwRVsOhKU"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_u7I3EkHLcBe"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_fKAc2Qe62Jj"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_JFnve9KEA3T"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_vG6YnBUsjdY"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_FtYfMCxdCYq"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_)kTyhgSxQZt"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_dnLchSQjq6G"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_cd05tMLprS5"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_LN3WuxlWXCo"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_o7)3F6FCDZP"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_cQwm(7IsA)J"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_7zhEzP(JUHL"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_sQySrNch84e"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_XhnLW)0o7vX"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_peNMwCCZwO5"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_arzizB0fJe7"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_x0xQlCcBaRq"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_EQUKCoSto8J"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_lAhWzy8RdNL"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_5F44AFDt8T7"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_qC)2poPSCkg"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_bv9aXox1ahk"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_A768KTjvBJr"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_aPfD9KZqkYX"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_xfdzF9gSntD"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_F0F)yHtkI2F"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_CS5zOcIIyPn"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_BvO9)tnQgZo"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_NIfrTjEZvO6"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_9MuwkaRlKZg"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_U9QS5vpPqd7"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_(WsOqzyLfah"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["OmniBar_Mind Freeze"] = {
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_WyBXKY8Pj3y"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_ipqS3cMHglP"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_a)MAGgK(Xzb"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_EvMdagptrog"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_(rrwJch9R2L"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_(KgO4dvZGRg"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_CItutJ9itBl"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_gwJTrHH4kOB"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_LvCBeZHBv4g"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_LhScTYhykUx"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_)n9YBkm4VLf"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_2AjLnqrr8gu"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_uoYV8(iqf(r"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_gUomWFeQMIk"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_)tUk0yrlz0E"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_DfLZxUVoFEs"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_BpLq(P45rIu"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_DI7AOEJAY1J"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_MKO3Jnrags("] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_3Ufc2YoI6CF"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_9ruegyNoIOx"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_XeWo9Yztf7u"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_q0WaKGPON)4"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_(9EKEo)Jx4u"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Wmmuo0(8bTS"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_fM3dedGhhGI"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_cJUStSSuPyb"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_bsRXJctwWdX"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_qiPKC0BlQcn"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Yxlv9a8CcpE"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["OmniBar_Pummel"] = {
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["ElvUI_Debuffs"] = {
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_YrtPwTk6YgW"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_T4TKPgj)Ozv"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_oWVNlKVsUBR"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_3yJD8y3PUnj"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_OB25zKVg)eL"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_hRubJqm(bfa"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_Hixmt8qmXrN"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_adxLuTgqU0u"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_(PUlvbW6C(f"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_LCxUehjofZ1"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Djw1LMUMVX2"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_e2(Es3axFz2"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_k19ZDzKxk27"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_0))4JRgqW9C"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_HomZ13sXuir"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_CnLqxPylBpy"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_tiKlJN51MWR"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_1XTxKN4Tip1"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_gUGuAYIeWPw"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_soY6y)HszN6"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_XgXvi)hUKGr"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_2r52yFe)J1z"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_uWn6HB5eQPD"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_5FQPq4Sdak5"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_7iTC9KIPVPn"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_tNbs75qmr)E"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_(R(QeBPRsek"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_7Fu4hwKXxsy"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_wRO7pM0hZ8b"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_MLoKy72Pivj"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_UHDXcUpLcZS"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_ZDNeK2tVmeZ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_SS37))QLZAt"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_(f7X6OP40PX"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_aewzKtDnLow"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_WGs(uz4ALVi"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_LsW33MBrIDx"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_eILPm1VKbyB"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Z(ATggUQ8UW"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_xrLGehT0Ygj"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_iDp2u537jFu"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_xddV)PTMzbq"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_9nCKW6115se"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_gsWWEX(Cp9i"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_ue5St4yy(3r"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_w5BGNfC3)DT"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["OmniBar_Solar Beam"] = {
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_gEhJLXJbq3G"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["Blizzard Buffs_Buffs"] = {
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_QggGIN4uvsw"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Xa34d8gc4uW"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_(50RS2)YB0J"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_H0bYxCgJq2F"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_pmOgOe6jGnt"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Ca4EWGmD5y9"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["OmniBar_Wind Shear"] = {
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_cVcCXOmbEWr"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_YaNY4KIRWKM"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_E92TRKT(hn0"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_uo56geE1rre"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_jrT)mC4vwMz"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_OUH92OP0AL7"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_NYVy)mGUAB0"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_ivGbp5eukcU"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_L0QiYFj1Tc("] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_1hfQd(NJrLw"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_WGnQKp4kLMn"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_EMK1)BHCZvj"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_EymQINMF1Rj"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_KQTRfg9rhS3"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_)Uj(j1gMzas"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_JPI6rJN1iHz"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_StxeW0tUh7c"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["OmniBar_Power Infusion"] = {
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_lQicTuiAYZn"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_LcU892GBbxz"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_YMyW0zCpwXy"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_RO1E)3S)0M2"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_NZk98Qd35Vg"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_2PTFjjex6p9"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["Bartender4_6"] = {
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_rj0uaUoD3ku"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_na68qEUfiko"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_)nV60ubJh9X"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_7yx3uIQIDNI"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_)EAIPRU1FTM"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Qx4w7Eah(Xt"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_)Krd7i5Ogwm"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_x96LtANJknL"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Ynb317BEnbh"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_jYCThFcem2("] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_LZuBoBxhPSR"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_7uWiaBhHnL3"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_OifMMUEedkj"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_xucLV(KjxnS"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_5hxGBeUBUZ2"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_KHLH8Zht7Er"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_p4w4FtF)5cY"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_HQGpfXiQKcR"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_gf13IwpG7T9"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_PzDQ1(TSFX3"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_2g15V)nBwiN"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_0cSLbFD4P(z"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_QzZNVM2Bp9X"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_mRlwgBIShWZ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_GafiL6YcGO5"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_mhEPqv)9Bul"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_aVtUXJ8ytfR"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_rza)oGUNlnN"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_dwgtPD8Cxdn"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_yEb2e1ZB6R0"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_y)QduTciOve"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_4t4Qwclk5sb"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_0MwRXRruMdY"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_hPRAaQcL3jn"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_NhRqUTPhtF0"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_WD3lVHjvKWu"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_5vT6jSYlfBA"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_YgsHLroLxNX"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_qHr3(fbXGDF"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_oEOx1xiz8eH"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_nxGPdT3uo)W"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_Gpf3hIilSPJ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_pB)gAzs1)KF"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_6Sgu5Rts)Nu"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_1jLmKYmyZ8i"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_OIMGNqsRNKW"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_h8KY7tpoGQu"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_)v1xL5gfduf"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_QEKa4KGao1w"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_6wSgrb7Ug6f"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_iUojC0PrUS0"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_P9Xl7aYh)Vb"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_vkxAX1uj8OJ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_2Jisd7JTwZl"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_G1X9)RIXtRW"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_kkHZXtqVMpL"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_wUHIpvrOs)H"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_M(uDUrdLt(t"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Cu2lchlvlCc"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_)6o2zuZrOZI"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_OKuaIYs0Nmd"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_tqZ7usTGNb7"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_A6mUi6GPv(5"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_VcZlIJdBTks"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_ojz(ZbktXSQ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_ETRx(4dAx9A"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_VUBiC1XBtVj"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_ufzlaxhii9i"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_HE7Zj0gTRPD"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_kMwKiFxzuVY"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras"] = {
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_CuyKRxwRBC7"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_W6kbBh10gGt"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_6Q)wzTaQlti"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_pZA5hIHNsq0"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_3KR0ettjC5N"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_RyaLiMUsOEk"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_OSMzVi7HSJk"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_kpo1020XDLE"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_lgvsEovHywF"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_aRfkP03(6on"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_oBiRhNPJHHF"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_MyOL1ZmwYuz"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_78CI7pyAYjk"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_4ltDQgzM)5D"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_4ny6WBK3dr7"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_6EbIVWxLh7d"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_u8QtLwAWB4I"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["OmniBar_Muzzle"] = {
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_vbnVUrqV6Ne"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_eDrijbkLIaV"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_qb2J2cahwcm"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_skSv8tablXh"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_x6FyRUMSndX"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_ePK(Y6tGQWJ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_NvgRfCLlbXA"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_OF0U)F3eVTW"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_qQRobkk0F5p"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_oqSvFdJfXYq"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Qmgq8wjD1A2"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_yDBLLn(d6Qo"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_K0znsc8)Kgx"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_m7o5)kv64uG"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_q4ydn7XlVRA"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_IOUm()tuOVd"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_ZCLoBjjlfDy"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_R2Uo1G4o4up"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_GD8brvH39Fc"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_pTx(CVIXp7Q"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_9BIUsL4mjbd"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["Bartender4"] = {
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_RAMO2AolNrR"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_oFLodK3C0uw"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_frMMv6ugylI"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_8C9inkVkZ5V"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_MdCBeTqtvUq"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_kcivhoLlIly"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_(IVZBtCXzfs"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_Osp3eraFM65"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_ycIBRd3JveD"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Zvl81iKnxrr"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_7mNIMxGNoEf"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_dqT3(EUltie"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_ql5Tj0ioFGc"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_vaud250GR5)"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["OmniBar_Fire Elemental"] = {
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_kRwkh5YodnU"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_XkB6fhMYoWh"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_tRAxrRX3YmP"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_lxdTQeIL3nG"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_1esTRGc(VX5"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_SQrS7McX)X8"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_D2MShJMuBIu"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_zbpN9FDz8L6"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_qKzH3nivSXg"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_oIFJKSOWXs5"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_ikiUOUB3j9H"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_VIpWxXNKZrI"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_olzLoTdEvAO"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_SGxd1ifRNVP"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_X2Tf5c)yj9H"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_PC0K2qQUoPY"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_r)3XK(COlTt"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_6VqOFbll4H4"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_CsGwz9e)E1o"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_T)71U(pNYiW"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_qONzHgd2BLv"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_7Pi9HkF0f47"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_oW0AyoWJuPd"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_zZjlxreu6mF"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Yo739eIqbHt"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_A4)CRZVji2("] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_G4Hc(FaSI)W"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_oIbVRB9UAsV"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_HVCZ4yZK4e8"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_2GZzan9hRDS"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_RKNXXX9mNK5"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_GueGhxA6VJv"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_3YhPpn4bN5E"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_9QcWmPMQ3FG"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_6CB(6b5jjnH"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["ElvUI_Buffs"] = {
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_SV9tMKxzOXh"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_CLSj0u)EMfF"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_obvIr1gstS3"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_KRJ7k9n0SRi"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_Sx56hZT00Ix"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["MRT"] = {
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_ZZ1YunCrZDT"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_IjETJNwL6rr"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_fQxDWOmSpAn"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_n4(VJ324SHR"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_joeSCCp4T1h"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_R9WJK(DLcIM"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["Bartender4_8"] = {
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_DskVHqk7JOc"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_WmXw7tDRrsT"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_0RuYZZeESy3"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_DKPhfKKKS5c"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_DX4u8twSw)d"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["OmniBar_Berserk"] = {
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_TvBKBqZSyxq"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_lx92X2E7zkB"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_5sznu1IqbPe"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_arXTPX6C)yT"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_iCDo4Dj87MW"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_DND0WSOQvXQ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_duWzfFO)Gwl"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_zjnuxwc0Nr4"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_JfM6HbbHA)M"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_F8yqf4snELC"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_y6k59yG0Dia"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_cwPCKYseWEL"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_DEfo2)0M7eZ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["Bartender4_3"] = {
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_8tMCCtk0Pyl"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Crl9uaUemHY"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_hvkFRGfyxbe"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_m1DoMHhHck8"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_FKwRo6airkj"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["OmniBar_Unholy Assault"] = {
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_xjuHgcRpxhF"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_ZZTcxf0Xh0R"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_ABlGQG5aDYk"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_LLKDbZ4hdfW"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["Bartender4_5"] = {
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_1RHnRWyJ7lb"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_96jDI3RhVSc"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_CTy65q9)KxW"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_PIyKNbqQbyB"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_eYRu9MdPGHt"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_P0PU(rEWZHT"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_ZSMbSvEz2Ts"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_vGZO82HVeuZ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_OvimJ1bq3wO"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_DNjTwV6BXBH"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_ouY59wfLRfx"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_)8tIj2IruSY"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_CP7g0RXhOwz"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_GJMErSYdPHK"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_aUakg2iupCZ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_br(Qk(01dwQ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_bxOZncs(Yd1"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_dq71YmmwRG)"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_z)XU9kMBuL6"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_P(jAZ2F4OCm"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_kQYS0lUh7YB"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_AQACvZ4ot0Z"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_lLNRm7Nrgqw"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_3mi6RRXF4Fr"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_rN1dE(dVMCR"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_iyHAskq8UXM"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_BwkTp3AXutk"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_DlPSG0VDpXW"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_54kL2hZwhOq"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_93zOLL0aNsg"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_exuNGGJJh61"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_LZVmVNhlRvu"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_F3SzsQDiP26"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_ca4e8YvgC(S"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_FdinYtVfUZR"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_VvCQo907ASm"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_)CHGVpSrQJ0"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_)zDqwXXt7Z9"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_YZOgSPhdhOR"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_RyaTEp5oFab"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_ST1Xgs1AHEE"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_MCEoj)Rl5sG"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_lPSn3hMuLE2"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_UBZrX35(4Pd"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_MUIpnbNx0XG"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_BBHbz0fxZbq"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_qHf4ZgQqx()"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_5(ZkEqjEuxn"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_cdhggxMAe3e"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_nO2j8lpI3ck"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_N(zYt5EEh1P"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_D29ATiCuEiJ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Kl6TcBpztix"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["OmniBar_Strangulate"] = {
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_jSTSJ2E3dKd"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Y5Fnxocy6kp"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_RVKqvB6lcFC"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_xdUdk73NARK"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_5i6XAggWLhl"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_x2pS6ZiBFd1"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_TKmHJD4fl7b"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_W14QN4w9Mpx"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_(jQzOXCTz1("] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_CdPb5KdmWat"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_lXewcFMN9IR"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_6m(9NhvNR3M"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_FGVo5qwWRsM"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_NboVIrNOQuj"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_4iLqUdDjIxL"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_gVtsFnzWbxA"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_PLnqYa8(vmB"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_eM81Yd8)p(G"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_ZxOmaxR((bm"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_25PxF9aTcqd"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_O1)4wOB6nIj"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_iR)TtdBfT9i"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_DEO5jhe)Lp)"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_92dDH1Xxrwf"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_8rfHEuYrruG"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Ob4kP6cE(OQ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_UUaNdQzi7l4"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_4OXeZE1i338"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_vCk1m1aeIr("] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_s)GG1lb5Foq"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_YWciIP2WDdb"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_eqfRRG(J(hs"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_8tGxVRSHQJn"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["OmniBar_War Banner"] = {
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_icDsWnb0ik9"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_79aDPskww68"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_E582I62m39N"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_ccX8LEDvQfe"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_1lE439TXu5K"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_68WnS26kqdc"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_4w4AYBGfQ1)"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_rA0U8ESEN3t"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_xCyfFgaYs7Z"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_So44Rgp7Hc8"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_T22j(KpNJva"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_oin)4QnwESZ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_ePPoBfL4SIg"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_2Uej3eHB1Ci"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_WM)p4WtR6Xw"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_7zCCWWQ4leU"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Er5HjreFftE"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_N5JG08RfVUS"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["OmniBar_Icy Veins"] = {
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_FcET2xuDuY4"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_4auLr6T4osR"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Uz4xqEQwBJ3"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_p85g29As64V"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_A6PGv3NlT39"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_1OGhyQ4ElqM"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["MRT_Raid cooldowns Col 3"] = {
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_9BPMUqdPodb"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_s9HAbe(4Sdx"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_wOQI9JZ0zXx"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["OmniBar_Combustion"] = {
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_MYrmeSYEQV8"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_msrp3GU6XNM"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_Ehl11)RbwpA"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_AEgnAaJOchW"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_PmGulPiB1rj"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_X)ZBMN1yq81"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_oqyZs6OAUQs"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_Cv9p6d08AI2"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_rCTo25V8tr6"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_zeJqxrRqMLM"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_64Tz7Vd16bs"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_7w4dc0DYEOB"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_IAiGkxccr4X"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_iWgAbArBkaQ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_aUpuzrL(nqI"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_tiLppfyLWeM"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_it8EcT0yWPB"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_pFCQKfFi7OZ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_5noy9sWNEzn"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_AEJgaN(E3cE"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_t1lq2y1SFEP"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_UJX(9nBVHcJ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_c4xHVtUrPUL"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_fR(ADi70FCA"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_jYVOTyJUVOD"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_(VyiUylQxr6"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_aQxdQvUTryc"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_PcdugMa8dIZ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["OmniBar_Avatar"] = {
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_T52(z5pitsB"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_pvpNao5UJ7Y"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_LDzMOF9UD0j"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_uDiqYBDu6As"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_eFXVtrIuqdK"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_8u2pWCTk9Ym"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_LEJ8OiNloqC"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_wqGYbvmLXmc"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_UIwK5R5Aik9"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["ElvUI_Pet Bar"] = {
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_T)M8Q)Pacdd"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_5mfhGtXn)2s"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_4TfqIpKxzA1"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_2utgAn69NcN"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_2(sgQ7c8Myy"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_jp6U6BadBcK"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_oD9pCqNoK5U"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_RwVWJPivD4u"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_9KGaSX2f1t7"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_(V2RH6pjXVH"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_d4EconJknRh"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_41ceMIjm(Xh"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["OmniBar_Breath of Sindragosa"] = {
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_ul5A0QaE5Of"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_kPhrUAp7JBE"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_00Dfn13GSRR"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_oG5wJl7Sj8V"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_h)ixzGmm7iO"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_mQTVhSLwtLd"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_jsM)jivFysE"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_ttX)Q6I5kvX"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_3ok0nCJdd7G"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_JVBKIfP2nFF"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_2Jufegi21sS"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_PcA3Dgp36w)"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_ZT22d4CWi3a"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_5FzVGUX9AGo"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_(TSkyfYZnJe"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_0QQabSGr0Ya"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_8LUT3KoiynQ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_Evh1FbGeE4o"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_lXIQfEQkleW"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_dzbrR2hwFk9"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_FKVoua4QRkV"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_MKIUcq09Vxf"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_GG3hESkrCGS"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_i)wE044lktf"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_yz1V8298wRU"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_ooB0SPP268j"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_)yapzlwD3uA"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["Blizzard Buffs"] = {
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_kK39GgDIAPk"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_PjLJbE79c2u"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_MmdDeuc6OLk"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_IsaXNNzaE2y"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_9YRcSJMVCSx"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_x4OuNJiCWLA"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_xwBH70WD6fK"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_9M2fIfb)fk("] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_yJgScPtzfY("] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_iw8EXEVocko"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_kpVIjwvUn0H"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_zqa6yWDBkek"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_iWCd)X9REcN"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_0ClWyNHH4rl"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_1DsTGjjLe(4"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_EjEAIXgpCEg"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_lNDvApZWphC"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_fRZDP2uH)WA"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_8E1adVWOP25"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_01pU8Y)b8MU"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_x8J5NVbKddr"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_x2NrkoJBrzP"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_psSYwqUmQ03"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_sJZ247OkA45"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Tr4jpTWcAUH"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_L3(d5j93Kug"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_xu)PVYA5ox9"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_ZvkIa58Ci94"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_(ZjkqDD6Qkl"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_WzUxFrX1cYX"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_zxScg0nWQjw"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_VXf5Dev42Io"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_4hVeOVeXSUz"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_f2vxDEmniN9"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_qJIhjAXxbum"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_n6Lffhwx7Ke"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_6HZGcp2t3ks"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_yyg9S9o2lKH"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_vK2wid4A8Sp"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_GkB5h4FCUv0"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_EXUYQ2CY0QT"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Bb1EyVYHqW0"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_IGKeKO(Mzf9"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["Bartender4_1"] = {
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_(mgSHNUt3Nt"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_hNkYRo9LesJ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_ljMQHMX5npf"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_EggnxqCSaUg"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_ZVejj9oLgrT"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_7RXNGvI(N26"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_sBT7er79Khc"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_00QmheM0mUN"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["Bartender4_4"] = {
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_x1cBVv3as21"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_3CevZBmkrOm"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["ElvUI_ActionBars"] = {
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_ZFiUSdRHd0V"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_SBquGbP90kw"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_jrX8(EhZ9Kr"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_HMQZH0qBBo("] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_OngHD5hN9SS"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_3tu1rbZ125q"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_5T4ZG2j9cMD"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_Ao(XWLldy8b"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_i42kiJ4QZBd"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Gzo(TkTZjUu"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_6AxnLRmpB8t"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_AmnXzJH2GDo"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_T8sDc9oHUDM"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_64TL9XEKe7K"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_kHBeCm2W3lj"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_KMcF(7MPIKV"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_RLfMgJcYJh0"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_)3vgZvQcUuh"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_5H)EjkqGkMa"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_auohb45BL01"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_6BcehU3)yuS"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_NUi8jTpfhyK"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_1uNfyQHn0Rp"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_LDcNJY5zn9M"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_G2svOj0YsNy"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_zw6EGZlw9gY"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_LYx2a7mJG0G"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_zF5E15bv2SA"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_9OZoCU4Z9W5"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_TtoZBZe(wGN"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_70VkpSBYvk("] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_iFDKda24WqK"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["OmniBar_Celestial Alignment"] = {
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_KOwAtuInR4V"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_mY7FZTS)3gD"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_1u1lpMoJDDQ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_wtPM9Gjertq"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_sMx48cX7N(B"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_6SzN)Abdr0P"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Z8ExztcvE9r"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_4lZ7QjAjkyP"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_lNwkQ12c2q0"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_)nuAXLPJx20"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_BBmFLXDeCmt"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_BG1W(F0XSry"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_HzcftabvSvZ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_Ak0DxwUnd0I"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_wDw5hdRpKMR"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_WZoLtxj1RKk"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_TCrUbgAizL2"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_p0)fizpu9Xo"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["MRT_Raid cooldowns Col 6"] = {
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_WxC7HsKWnPw"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_S8JDLXR7UCN"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_h915th)Onm4"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_lOB3BR)PGoM"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_mDTdEvN1Wcz"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_ENO9s1ZIumU"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_VWqI(lXvmIF"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_eGjb6Hbw9qg"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_J1DNfgB0Kqo"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_Gny78Ocv6dv"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_8zZwpavAKeK"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_XQxIImk)C3q"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_r)3C1XvVfg8"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Nj7adySDyoD"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_gmxXs1zlEhf"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_duvbU6nJkNw"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_lOKEROhqigg"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_oqE4BfkoNcy"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_WGbcfbAI01q"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_GhQc84A2NXQ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_0mUm7jcW9jz"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_jCT2ZLx(Je4"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_0240l74gks7"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Fb(LtzgDU)O"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_aDH5E6AKZeh"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_YF6REZayINT"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["OmniBar_Empower Rune Weapon"] = {
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_1alKkP2WYzv"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_xP066BX2W4L"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_xrgEYjf6XhC"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_lZUQqaLYmEr"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_rYEaIQ1rLb4"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_YNoZe3uZCP7"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["OmniBar_Incarnation: Chosen of Elune"] = {
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_NBxG5wXvf(I"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_rSF56VfWp)X"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_nkzvFXngDqA"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_AzaLMbXTnJB"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_Eea06YExIHo"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_6Z4sZI75Xmd"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_49wN42dxC3v"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_4zaNdwsKY3T"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_YoUjwmzZDeF"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_K81zziOa5KG"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_yJRtkRgABQC"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_EfV2oGJZ1)4"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Rk9u1j7Unh4"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_(hVbyoZzqsb"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_k0SAESm3zrS"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["MRT_Raid cooldowns Col 1"] = {
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_G5AMFiTEwhA"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_pfzOMP75wIy"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_RZ5s7)8lV2B"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_GFHk3YMaECk"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_SilSgSBcsvt"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Wul8bu(08z2"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Dq9l7UDcIoz"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_skNL2wznVib"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Sq(emZThcXx"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_sRY4Fp)D7k)"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_YkWC((iZ1MS"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_iEpU)hJlZed"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_W6ePuipkfXz"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_7q(KU6b(M2T"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_5HwsVg38)PN"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_jH40K096J(x"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_cAG4QuQ98wy"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_ibNNyPuNWoW"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["OmniBar_Vendetta"] = {
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_wiG8ZMps3CH"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_srxQOdQtCjV"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_jZ4oSFJQSYT"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["OmniBar_Convoke the Spirits"] = {
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_TC5a(VHC1P7"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_wsjlD(LzpJT"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_DllaDntZJeq"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_)mkdgpLKmNk"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_PNiBMkrqTLF"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_L58OIIQltCj"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_XojpZ3czawB"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_XcOr(enZuea"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_gfJklhYUVmr"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Rokkl79JUlN"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_BUnrwObpNXH"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_oO2o9dMGRhC"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_pJE4aOVkKog"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_mPulovmxo44"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_FT7OPtxDDgD"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_T2(5sJIjYl9"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_YPiQES2W1Or"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_R0I8ShXRQeW"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_nWMR6kOt0wk"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_QpYAAq3SdAr"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_mSNBYRewjz9"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_0nOy9Ya2C3y"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Swabi8cm8pV"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_fIIZHNDwkcL"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_BmF7mbR(Ur7"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_X5we(j5zlEZ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_vS7X9Ckpdx6"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_42hqv7nknyj"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_VjKRj2mzHqx"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_k6kZqXNoZ4k"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_NJJh(7PjpPi"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_zZbvVXhOm0a"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_98n0GPkUmuc"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_tBCMPULBosW"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_Aaea0ZJ7MCW"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_MuVuL0zE8OR"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_qyiSvcSDrNy"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Yc4imbfWkHY"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_k()EiKRaBgC"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_YJ2)omD288B"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_ajHQgVUV7y1"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_jHLJEzDhuek"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_0PdONlGsTiz"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_nSq9kT6kDlm"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_uXKX30OhqjZ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_7nmzkWAe1jB"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_WM6N4rSJGKt"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["OmniBar_Killing Spree"] = {
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["MRT_Raid cooldowns Col 5"] = {
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_w7VX6KBATFc"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_1Sc)Evr2x9E"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_HQIvcYkyjKX"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_CcruHDseHIX"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_MaDGh(z5p7Q"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_)JCgUIuWPMU"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Ldcteg(79aT"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_U4gj4VU6bw2"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_))ftGZ1u5M6"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_5oy4Q8ulW4M"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_sAvZNZ(QIMZ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_tx4WCWeFy45"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_QrUNlneebIF"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_lxSGAw2O12H"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_9N(gQA8mcF1"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Vgu)LkGEQvA"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_B9RviDWspUf"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_nF31t5iXJdl"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_6HaBxuTz5ux"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_jxrIpIvge)4"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_8GGbzcs1(uz"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_0NjxT6TgB6Y"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_AIE7lmwRYLJ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["OmniBar_Ascendance"] = {
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_jTVXaOr0wUL"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_z3DL6PEOm9l"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_RsB5LvhxD28"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_6UlhJDmSXpG"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_3UqLcrVuBXM"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_XxYPaiEY3GQ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_frS3FEC9mMk"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_pz97LlvwDzh"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_ordtujPtuhv"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_xGZEzEogyZ4"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_bEog4eqd3Of"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_RZq7BReoXIm"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_VXq81Am4cQM"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_XaDDVRpROa5"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_))NsmXKq52d"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_GMqfDaJaIZY"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_pk0nPoenPLU"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_XgIwFNffs3r"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_P4PVlW3vUGA"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_lZhZJuPbge8"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_x)oEzHo0ezm"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_vclXL6RyC0h"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_9WjgBE0g57Q"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_MHwl0jtsNVu"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_c3snONz7fu0"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_pjBNW4i0A5b"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_V)ASnUul2Iw"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_L6CXeBOr)kh"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_TK5H9grbpIq"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_m0wJxFZXBO0"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_3Eo5)9e8yYC"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_8rlacG6HT9j"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_joiBeKJWgeo"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_1jl8xB374gY"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_kx48xWHPGC2"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Hi1kF7RZDzE"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_XMPDrXQ)rEi"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_FufsJGYO2EO"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_PO1WAHOBMcA"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_nmDAaOVcUDS"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_BmMHtCezEAG"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["Bartender4_StanceBar"] = {
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_PLTei7au1dy"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_FIiKLbWcRFp"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_YvnGa8YIskB"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_Q0EDgGBm359"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_dMKO7a1ucZU"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["OmniBar_Spear Hand Strike"] = {
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_1zugJnhtXUQ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_KnaDXUZi1)3"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_fRFhiGZ(KZH"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_DMv9DCZvWa8"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_Fc5M2ycOeTm"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_WF4gxIPYPK3"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_GsULeyufg5z"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_(qLoNOZWxmw"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_(GFw84XD8iQ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_593YGlg9bxD"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_)dBM(hyKcT4"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_k1FN(TJFvTv"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_MKeoTi9ehQE"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_chEBb1FgO7q"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_oBQZj)ZBE1V"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_00KBXhlrqgA"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_yPh6)w7VQjM"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_dvsZd95Cbkc"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_fI84EmskbDG"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_e0s06g6SwU2"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_ozNkSkqutdD"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_ElONArFwUwA"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["Bartender4_PetBar"] = {
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_yJ(1VAAq8YW"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_c0KHneOaF7P"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_oAE3NN82T6h"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_xWj2bLMh4w)"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_2sAx5GnLs4O"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_SxiAychz71j"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_waq7yohngyY"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_oM(oEmHL0cV"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_UVvAVWIPe3H"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_yWGIvgG9uj8"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_uo6nVGUT6lh"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["OmniBar_Ashen Hallow"] = {
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_k99)4NCE84C"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_I2Lkuym7b27"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_s)3SqlaRta9"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_aQ816TdESod"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_mYbQ(qPEkvU"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_I6hnLqJsola"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_3Px0stebtg5"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_hakqVXVtOnm"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_(P)PgLgkkRJ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_1j51PORGl7F"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_9v9RSIZNxml"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_FmU39RYZvF9"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_j3Q8vv1K(sX"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_ADS93uqgpO2"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_aC7T4SsDF0h"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_7f1Z(Xuk7Sb"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_)Y)dhZxcBuf"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_mq5M)u2NFw3"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_abShbkEdp8D"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_VEYZU7OEBj8"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_mAcJZ1Vo3EM"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_SXOJnHuy6sE"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_KmdMb5UGiOL"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_zk84YJX)3Gk"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_1rF97WS1sre"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_1nC1UpJV2B7"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_8)R2jne6qkd"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_lJaoBXsfLGZ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_w9d7Ic04KYV"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_YL6aPtM3DQ)"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_eIpGm71q7N4"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_pSYKv88ltK4"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_nMkgPgXaQux"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_YDGpOApodms"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_D3WOuRxNr4c"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_3icVXvq4EwO"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_ARgKhLGi3FF"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_9hfzXqcT5VI"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_g01snwZV45p"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_kHRaLgh8Htk"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_2WgeK6jWlv6"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_mtysVftCFaw"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_QNwFhTAWoWv"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_L0WPfqczZNN"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Z6mbw8vbyZJ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_nyD(TeHM(i4"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_WhoprsuwU0t"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["OmniBar_Metamorphosis"] = {
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_h7HJeC(y(aK"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_eCWGMes)vge"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_UQrmJqIZ3Ev"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Ud0dJfUWfdz"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_RTpfJnMIQ(q"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_9)OzOT1P8gJ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_AiO0Bt6SEJ)"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Vbo7aVLsyZV"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_dZ0jKuCAyP7"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_meO7VLqdZs)"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_rqZmQsXeuDa"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_J9duxJc8M1a"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_tXuHPI8s74F"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_vSEyoES3tYM"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_RRMFmSljadr"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_PSAd8TxEEhH"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_wmWvAKV6sX7"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_BsHBCTSWq4y"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_p6UEe5o1Aa7"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_MFqqUVEc7st"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_4gPgMBQa2pW"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_THEgaGIdrN)"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_O2LKYs(Z4oU"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_KPwNCzxlJUk"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_dntvCZvn1Zy"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_GF0yIffV2zl"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_es6p6wLraaw"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_sTzSO94Dcs8"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_boXtFLsN5Mj"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_Yc8d)0)zXSm"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_HfhOH8AQ0(q"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_IhFUSJr0Jow"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_MLA2RVNT8HZ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_))6WjJpFKeG"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_1cNB44HI2Zv"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_njJItj8JpDB"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_sdEYvqCpOEe"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["GladiusEx_Buffs"] = {
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_eTD(ca8rEKk"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_ostCkc6o585"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_xXWHDF4msZf"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_60oKWOubeVN"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Vda88AKe5Al"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_Jwr5oczl6Dn"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_IyCaZkBgRFU"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_nibANN05JKN"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_4uv2MIsuJm6"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_3hiHE6vOQIS"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_JbOF)MPGlxs"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_71erYWql(uy"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_ZKTN9X6BANb"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_aC1TEjoj58j"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_8qmlvdLhwKD"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_HPkw1se0kdB"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_6pIdANQDCeJ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_139ENsWVzJj"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_3vrPn1g)hrp"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_mIxioX5Bd8q"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_BgcdaTxiWCF"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Jksmj)ioxW9"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_jVVSw8vMLtF"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_DJQoF8ampL6"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_gg3k65a)Mok"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_fl0FNGsoCYW"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_tPLT06vz8Qm"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_xkDaJBevH7L"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_XUEic(vdwiZ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_IJThzJhLWAC"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_KWmYC)JhUfs"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["OmniBar_Rebuke"] = {
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_2Ouh5Cj(Fd1"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_sRVAoz53tYf"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_gAqBpzyOMzh"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_0IYVd6Q1Znp"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_z0mL4okI(yU"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_qqdDytexhDt"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_2SFehijbq2U"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_y2qju4q(ZuD"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_GOZOnntnyYX"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_nGO3rnS6EHq"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_k82oX0vcYlM"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_66XD(3fCzsP"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_g)E9ND1(X8j"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_V)hAOADoqhX"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Z9Ci39LDvXs"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_e9gp6pHl)c4"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_ZSIYf78Qkni"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_3RBaK50Y9aG"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_UBL9uXbKohN"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_05LohBMc)Wj"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_xALra8TTwM1"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_6sdw(SYphYZ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_EX2RuSavVwS"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_4ggRjEE8pm1"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_04OAgC4Uhpo"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_wPYD4MXDoRs"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_To3U0K0zmgU"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_23URnENl6V)"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_Nk)OBpy0YLh"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_KFzL1VA7)bX"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_IjzgalsFMdY"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["GladiusEx"] = {
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_HS)0NI(U2Bh"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_WebHLowwthy"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["OmniBar_Incarnation: King of the Jungle"] = {
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_Y)FUut9QNpQ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_AWGPuXKWqw9"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_VwI1IUeuG8Z"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_jCnQTSc1kEQ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_LG7P17knnj("] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_5g2kYCW5W8O"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_qT(H)wXaf1j"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_bogMe(R6PBZ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["OmniBar_Dark Soul: Misery"] = {
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_ze(X(7sYsNG"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_TfQZ0F(Ku62"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_nShfosLDV9G"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_SO81jx7o51Z"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_SyB8Mx7Rqal"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_81bmCJdVo7Z"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Q4XjAQOl6Cf"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_I09ysRFdmNj"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_kYpHjecd)C0"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_h0mbuRj5vO8"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_XFz8rBW4)yI"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_8uEr2YI7t(W"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_q8CbkCCxUod"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_OscOyvYyEpN"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_uJfVrtQwtCN"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_iC4K0(dvpW6"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_Wyqrh)kNVtK"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_VxNfHjeefL)"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_fgRM9z3RKBd"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_7nwlQExCOfF"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_ZfbtixkMp4E"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_VZZbpHIOUtJ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_yu9Cc22DS1B"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Auh5sh4XvKA"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_VbJl81ZOiKl"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_0iQ6h1WRRIS"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["Bartender4_BagBar"] = {
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["OmniBar_Counter Shot"] = {
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_VtxJD8)Tepv"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_YCkz1pcJvZ9"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_GYbRbVxjLNK"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_)w3afgKudbn"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_WaAe5EQsOLf"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_MbopyNBnuA2"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_60LU0(d8EMf"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_d(472SIhSjW"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_e8SgppmgemM"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_ZClvZEOq1zg"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_Ru5(Px873ER"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_bHQDE(ikEE9"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_NwuIM)t0gcT"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_7O0T8gTdt7Q"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_hVadtcrJDtA"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_UcKltkuyym0"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_u5MCHmmnb7q"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_J77TJQ7qkWN"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_vwAbgQO6ii6"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_FODjR2il10Q"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_5o32MOSDKsb"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_NLRMx9HpwPH"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_jUNhXa(z9)("] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_SrwyDFkBtde"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_7K3SX)CzKL)"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_GpjZIcBiKsJ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_2Vs5(UtIBNU"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["OmniBar_Kick"] = {
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_6zQCKxK)C4J"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_F0Q9xiX5RgA"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_O9Eaj7Uxn2y"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_dVSn3c)B42Y"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_bHeefCMKH51"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_7igZd1VII8E"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_fzP9RBsoKIP"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_PlSAegxq8(K"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_M5WKdSJIOeS"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Np8ChB42BLG"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_L)43hZLJV4j"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_eGeDkf9fDzh"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_l)yQdn(LO5r"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_7oRkpW5okGV"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_h2)Vs1SE1XD"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_ThDbMgK9YH3"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_THeCq)0FYNV"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_tRllZBGMSvk"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_KkIPmqbCNgS"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_NJxsyLKSIwF"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_milvJyhNNEM"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_eKeqSeui)J5"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_QtU0Yzt48lY"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Li)l5I4mqiX"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_(ipDLuJUx6e"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_bgkf(a5ar5E"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["OmniBar_Mindgames"] = {
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_2wC0LFPPCv)"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_lcjlQEV7IgZ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_1gceQF(Qt8t"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_(1FcYey6U2v"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_rwkaw8Ejsuz"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["MRT_Raid cooldowns Col 4"] = {
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_EYn)zek8OZp"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_G(xAAq5JHK6"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_Zr3n8uTWo)N"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_kikj6RPAAua"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_oM9SmbtPIP)"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_A1ddbqsbp1k"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_e(vD9FOHgUy"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_)SgvRvcMuj0"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_pgOHkb6tVWL"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_AHy8sHIempT"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_AHz3O3GTd3Z"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_OOu))zItPq)"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["Bartender4_9"] = {
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
					["Backdrop"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
				},
				["WeakAuras_VUnOkbr3zaz"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_5kJfsMOBQBZ"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_JtNBTVuVb2u"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_bfNKdd5Nyew"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_01AkLhDCRUb"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_zSP1CnLcdwI"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_vNU9xdok80d"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_(ro(LYgYfpw"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_JNaIUsH)Bye"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["Inherit"] = false,
					["SkinID"] = "Zoomed",
				},
				["WeakAuras_LQxtHthjHAG"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
				["WeakAuras_X5oBAAwRpmT"] = {
					["Backdrop"] = true,
					["Upgraded"] = true,
					["Colors"] = {
						["Backdrop"] = {
							0, -- [1]
							0, -- [2]
							0, -- [3]
							0.7487259209156036, -- [4]
						},
					},
					["SkinID"] = "Zoomed",
					["Inherit"] = false,
				},
			},
		},
	},
}
