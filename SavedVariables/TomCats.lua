
TomCats_Account = {
	["loveisintheair"] = {
	},
	["discoveriesVersion"] = "2.4.6",
	["lastVersionSeen"] = "2.4.6",
	["preferences"] = {
		["MapOptions"] = {
			["iconScale"] = 1,
			["iconAnimationEnabled"] = true,
		},
		["TomCats-LunarFestivalMinimapButton"] = {
			["hidden"] = false,
			["position"] = -2.514,
		},
		["TomCats-LoveIsInTheAirMinimapButton"] = {
			["hidden"] = false,
			["position"] = -3.262,
		},
		["TomCats-MinimapButton"] = {
			["hidden"] = true,
			["position"] = -2.888,
		},
		["betaEnabled"] = false,
		["defaultVignetteIcon"] = "default",
	},
	["lastExpirationWarning"] = 0,
	["discoveriesResetCount"] = 0,
	["discoveries"] = {
		["vignetteAtlases"] = {
		},
		["vignettes"] = {
		},
	},
	["lunarfestival"] = {
	},
}
