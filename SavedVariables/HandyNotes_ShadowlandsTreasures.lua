
HandyNotes_ShadowlandsTreasuresDB = {
	["char"] = {
		["Noxiishields - Area 52"] = {
			["hidden"] = {
				[1533] = {
				},
			},
		},
	},
	["profileKeys"] = {
		["Noxiipally - Area 52"] = "Noxiipally - Area 52",
		["Noxiishields - Area 52"] = "Noxiishields - Area 52",
	},
	["profiles"] = {
		["Noxiipally - Area 52"] = {
			["groupsHiddenByZone"] = {
				[1970] = {
					["Lore Concordances"] = false,
					["puzzlecache"] = false,
					["dailymount"] = false,
				},
			},
			["found"] = true,
		},
		["Noxiishields - Area 52"] = {
			["collectablefound"] = true,
			["show_npcs"] = true,
			["icon_scale"] = 1,
			["worldmapoverlay"] = true,
			["default_icon"] = "VignetteLoot",
			["questfound"] = true,
			["show_treasure"] = true,
			["icon_item"] = false,
			["show_on_world"] = true,
			["groupsHidden"] = {
			},
			["achievementsHidden"] = {
				[15229] = true,
			},
			["tooltip_item"] = true,
			["tooltip_questid"] = false,
			["show_routes"] = true,
			["groupsHiddenByZone"] = {
				[1970] = {
					["puzzlecache"] = true,
					["junk"] = true,
					["coreless"] = true,
				},
				[1533] = {
				},
			},
			["achievedfound"] = true,
			["found"] = false,
			["show_on_minimap"] = false,
			["upcoming"] = true,
			["icon_alpha"] = 1,
			["zonesHidden"] = {
			},
			["tooltip_pointanchor"] = false,
		},
	},
}
