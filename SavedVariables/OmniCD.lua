
OmniCDDB = {
	["profileKeys"] = {
		["Noxiipally - Area 52"] = "Default",
		["Noxiishields - Area 52"] = "Default",
	},
	["cooldowns"] = {
	},
	["namespaces"] = {
		["LibDualSpec-1.0"] = {
			["char"] = {
				["Noxiipally - Area 52"] = {
				},
				["Noxiishields - Area 52"] = {
				},
			},
		},
	},
	["global"] = {
		["disableElvMsg"] = true,
		["oodMsg"] = "|cfff16436 A new update is available. (Major update)",
		["oodVer"] = 2621,
		["oodChk"] = 210902,
	},
	["version"] = 2.51,
	["profiles"] = {
		["Default"] = {
			["Party"] = {
				["party"] = {
					["manualPos"] = {
						["raidCDBar"] = {
							["y"] = 756.4006585052921,
							["x"] = 303.8087960956982,
						},
						["interruptBar"] = {
							["y"] = 213.8037042446867,
							["x"] = 787.2025378635517,
						},
					},
				},
			},
		},
	},
}
