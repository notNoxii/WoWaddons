
AddOnSkinsDB = {
	["profileKeys"] = {
		["Mildew - Hyjal"] = "Default",
		["Noxiishields - Area 52"] = "Default",
		["Welorialin - Area 52"] = "Default",
		["Noxiipally - Area 52"] = "Default",
		["Noxheals - Area 52"] = "Default",
		["Noxxii - Suramar"] = "Default",
		["Koragoraia - Illidan"] = "Default",
		["Noxii - Hyjal"] = "Default",
	},
	["profiles"] = {
		["Default"] = {
			["Parchment"] = true,
			["DBMRadarTrans"] = true,
			["DBMSkinHalf"] = true,
			["EmbedRightChat"] = false,
			["DBMFont"] = "Ubuntu Medium",
			["Blizzard_Alerts"] = false,
			["StatusBarTexture"] = "Minimalist",
			["BackgroundTexture"] = "Minimalist",
			["EmbedBackdrop"] = false,
			["EmbedBackdropTransparent"] = false,
			["Details"] = false,
			["BugSack"] = false,
			["DBM-Core"] = false,
			["EmbedSystemMessage"] = false,
			["Bartender4"] = false,
			["Theme"] = "TwoPixel",
			["RCLootCouncil"] = false,
			["PremadeGroupsFilter"] = false,
		},
	},
}
AddOnSkinsDS = {
	[4.58] = {
	},
}
