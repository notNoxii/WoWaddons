
OneRing_Config = {
	["CharProfiles"] = {
	},
	["_GameVersion"] = "9.2.5",
	["_OPieVersion"] = "Xe 6 (3.106)",
	["ProfileStorage"] = {
		["default"] = {
			["Bindings"] = {
				["SpecMenu"] = "ALT-G",
			},
		},
	},
	["PersistentStorage"] = {
		["RingKeeper"] = {
			["OPieFlagStore"] = {
				["StoreVersion"] = 2,
			},
			["SpecMenu"] = {
				{
					["sliceToken"] = "ABue5od8N5o",
					["id"] = 354467,
				}, -- [1]
				{
					["sliceToken"] = "ABue5p5k=fr",
					["id"] = 354469,
				}, -- [2]
				{
					["id"] = "/cast {{spell:50977}}; {{spell:193753}}; {{spell:126892}}; {{spell:193759}}",
					["sliceToken"] = "OPCTAc",
				}, -- [3]
				{
					["sliceToken"] = "ABue5ldsBku",
					["id"] = 354464,
				}, -- [4]
				{
					["sliceToken"] = "ABue5ldsBky",
					["id"] = 354462,
				}, -- [5]
				{
					["sliceToken"] = "ABuexgz2uM1",
					["id"] = 354466,
				}, -- [6]
				{
					["sliceToken"] = "ABue5ldsBkt",
					["id"] = 354463,
				}, -- [7]
				{
					"toy", -- [1]
					172924, -- [2]
					["sliceToken"] = "ABuexgdUvCe",
				}, -- [8]
				{
					"toy", -- [1]
					40768, -- [2]
					["sliceToken"] = "ABue5od8N5p",
				}, -- [9]
				{
					"toy", -- [1]
					183716, -- [2]
					["sliceToken"] = "ABuexgz2uMr",
				}, -- [10]
				{
					"toy", -- [1]
					184353, -- [2]
					["sliceToken"] = "ABuexEHblvu",
				}, -- [11]
				{
					"toy", -- [1]
					188952, -- [2]
					["sliceToken"] = "ABuexEHblvy",
				}, -- [12]
				["name"] = "Specializations and Travel",
				["save"] = true,
				["hotkey"] = "ALT-H",
			},
		},
	},
	["_GameLocale"] = "enUS",
}
OPie_SavedData = nil
