
Chinchilla2DB = {
	["namespaces"] = {
		["Ping"] = {
			["profiles"] = {
				["Default"] = {
					["enabled"] = true,
					["backgroundTexture"] = "Blizzard Tooltip",
					["MINIMAPPING_FADE_TIMER"] = 0.5,
					["scale"] = 1,
					["MINIMAPPING_TIMER"] = 5,
					["border"] = {
						1, -- [1]
						1, -- [2]
						1, -- [3]
						1, -- [4]
					},
					["chat"] = false,
					["positionY"] = 60,
					["positionX"] = 0,
					["borderTexture"] = "Blizzard Tooltip",
					["background"] = {
						0.09, -- [1]
						0.09, -- [2]
						0.19, -- [3]
						1, -- [4]
					},
					["font"] = "Friz Quadrata TT",
					["textColor"] = {
						0.8, -- [1]
						0.8, -- [2]
						0.6, -- [3]
						1, -- [4]
					},
				},
			},
		},
		["Coordinates"] = {
			["profiles"] = {
				["Default"] = {
					["enabled"] = false,
					["backgroundTexture"] = "Blizzard Tooltip",
					["scale"] = 1,
					["border"] = {
						1, -- [1]
						1, -- [2]
						1, -- [3]
						1, -- [4]
					},
					["positionY"] = -50,
					["positionX"] = -30,
					["borderTexture"] = "Blizzard Tooltip",
					["font"] = "Friz Quadrata TT",
					["precision"] = 1,
					["background"] = {
						0.09, -- [1]
						0.09, -- [2]
						0.19, -- [3]
						1, -- [4]
					},
					["textColor"] = {
						0.8, -- [1]
						0.8, -- [2]
						0.6, -- [3]
						1, -- [4]
					},
				},
			},
		},
		["Appearance"] = {
			["profiles"] = {
				["Default"] = {
					["enabled"] = true,
					["strata"] = "LOW",
					["blipScale"] = 1,
					["scale"] = 1.05,
					["alpha"] = 1,
					["borderStyle"] = "Thin",
					["borderColor"] = {
						1, -- [1]
						1, -- [2]
						1, -- [3]
						0.4800000190734863, -- [4]
					},
					["buttonBorderAlpha"] = 0.7000000000000001,
					["borderRadius"] = 80,
					["shape"] = "CORNER-BOTTOMLEFT",
					["combatAlpha"] = 1,
				},
			},
		},
		["Zoom"] = {
			["profiles"] = {
				["Default"] = {
					["enabled"] = true,
					["autoZoomTime"] = 20,
					["autoZoom"] = true,
					["wheelZoom"] = true,
				},
			},
		},
		["TrackingDots"] = {
			["profiles"] = {
				["Default"] = {
					["blinkRate"] = 0.5,
					["blink"] = false,
					["trackingDotStyle"] = "Blizzard",
					["enabled"] = true,
				},
			},
		},
		["QuestTracker"] = {
			["profiles"] = {
				["Default"] = {
					["enabled"] = true,
					["showTitle"] = true,
					["frameHeight"] = 700,
					["showCollapseButton"] = true,
				},
			},
		},
		["RangeCircle"] = {
			["profiles"] = {
				["Default"] = {
					["enabled"] = false,
					["combatRange"] = 90,
					["style"] = "Solid",
					["combatStyle"] = "Solid",
					["color"] = {
						1, -- [1]
						0.82, -- [2]
						0, -- [3]
						0.5, -- [4]
					},
					["range"] = 90,
					["combatColor"] = {
						1, -- [1]
						0.82, -- [2]
						0, -- [3]
						0.25, -- [4]
					},
				},
			},
		},
		["ShowHide"] = {
			["profiles"] = {
				["Default"] = {
					["track"] = false,
					["zoom"] = false,
					["calendarInviteOnly"] = false,
					["onMouseOver"] = false,
					["map"] = false,
					["garrison"] = true,
					["north"] = false,
					["difficulty"] = false,
					["dayNight"] = false,
					["enabled"] = true,
					["vehicleSeats"] = true,
					["mail"] = true,
					["clock"] = false,
					["boss"] = true,
					["lfg"] = true,
				},
			},
		},
		["Position"] = {
			["profiles"] = {
				["Default"] = {
					["enabled"] = true,
					["vehicleSeats"] = {
						"TOPRIGHT", -- [1]
						-50, -- [2]
						-250, -- [3]
					},
					["ticketStatus"] = {
						"TOPRIGHT", -- [1]
						-180, -- [2]
						0, -- [3]
					},
					["minimap"] = {
						"TOPRIGHT", -- [1]
						1.555689749211803, -- [2]
						1.555807004375161, -- [3]
					},
					["boss"] = {
						"TOPRIGHT", -- [1]
						55, -- [2]
						-236, -- [3]
					},
					["questWatch"] = {
						"TOPRIGHT", -- [1]
						0, -- [2]
						-175, -- [3]
					},
					["clamped"] = true,
					["minimapLock"] = true,
					["durability"] = {
						"TOPRIGHT", -- [1]
						-143, -- [2]
						-221, -- [3]
					},
				},
			},
		},
		["Location"] = {
			["profiles"] = {
				["Default"] = {
					["enabled"] = false,
					["backgroundTexture"] = "Blizzard Tooltip",
					["showClose"] = true,
					["scale"] = 1.2,
					["border"] = {
						1, -- [1]
						1, -- [2]
						1, -- [3]
						1, -- [4]
					},
					["positionY"] = 70,
					["positionX"] = 0,
					["borderTexture"] = "Blizzard Tooltip",
					["background"] = {
						0.09, -- [1]
						0.09, -- [2]
						0.19, -- [3]
						1, -- [4]
					},
					["font"] = "Friz Quadrata TT",
				},
			},
		},
		["Compass"] = {
		},
		["MoveButtons"] = {
			["profiles"] = {
				["Default"] = {
					["dayNight"] = 198,
					["lock"] = true,
					["mail"] = 200,
					["zoomIn"] = {
						"TOPRIGHT", -- [1]
						-1.16377395046411, -- [2]
						-5.548650474225754e-05, -- [3]
					},
					["garrison"] = 300,
				},
			},
		},
		["Expander"] = {
			["profiles"] = {
				["Default"] = {
					["enabled"] = true,
					["strata"] = "LOW",
					["scale"] = 3,
					["alpha"] = 1,
					["y"] = 0,
					["x"] = 0,
					["key"] = false,
					["toggle"] = true,
					["hideCombat"] = false,
					["anchor"] = "CENTER",
				},
			},
		},
	},
	["profileKeys"] = {
		["Noxiipally - Area 52"] = "Default",
		["Noxiishields - Area 52"] = "Default",
	},
	["profiles"] = {
		["Default"] = {
		},
	},
}
